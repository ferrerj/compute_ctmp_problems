
#pragma once

#include <fstream>
#include <unordered_set>
#include <unordered_map>
#include <boost/serialization/serialization.hpp>
#include <boost/serialization/map.hpp>
#include <boost/serialization/set.hpp>
#include <boost/serialization/vector.hpp>
#include <boost/archive/text_iarchive.hpp>
#include <boost/archive/text_oarchive.hpp>
#include <utils/serialize_tuple.hxx>


#include <problem_info.hxx>
#include <utils/external.hxx>
#include <languages/fstrips/builtin.hxx>
#include <boost/container/flat_set.hpp>
#include <boost/functional/hash/extensions.hpp>
#include <utils/tuple_hash.hxx>



namespace fs = fs0::language::fstrips;
using namespace fs0;

using OverlapSet2 = boost::container::flat_set<std::pair<int, int>>;
using OverlapSet3 = boost::container::flat_set<std::tuple<int, int, int>>;

// using OverlapSet2 = std::unordered_set<std::pair<int, int>, boost::hash<std::pair<int, int>>>;
// using OverlapSet3 = std::unordered_set<std::tuple<int, int, int>>;

// using OverlapSet2 = std::set<std::pair<int, int>>;
// using OverlapSet3 = std::set<std::tuple<int, int, int>>;

//! An index with all overlaps, mapping each pair <conf_base, trajectory_arm> to 
//! all the object configurations that overlap with the arm trajectory
using OverlapMap = boost::container::flat_map<std::pair<ObjectIdx, ObjectIdx>, std::vector<ObjectIdx>>;

using row = std::tuple<unsigned, unsigned, unsigned>;
using table = std::vector<row>;

//! A map from pairs of <configuration, edge> to a new configuration
using SerializedTranslationMap = std::map<std::pair<int, int>, int>;
// using OTranslationMap = boost::container::flat_map<std::pair<int, int>, int>;
// using OTranslationMap = SerializedTranslationMap;
using OTranslationMap = std::unordered_map<std::pair<int, int>, int, boost::hash<std::pair<int, int>>>;

using PoseMap = OTranslationMap;



template <typename T>
void deserialize(const std::string& filename, T& data) {
	std::ifstream ifs(filename);
	if(ifs.is_open()){
		boost::archive::text_iarchive iarch(ifs);
		iarch >> data;
	}
	ifs.close();
}



class External : public fs0::ExternalI {
public:
	External(const ProblemInfo& info, const std::string& data_dir) :
		_overlaps_ro(load_overlaps_ro(data_dir + "/overlaps_R_O.boost")),
		_overlaps_oo(load_overlaps_oo(data_dir + "/overlaps_O_O.boost")),
		_overlap_idx(index_overlaps(_overlaps_ro)),
		_sources(info.getNumObjects(), -1),
		_targets(info.getNumObjects(), -1),
		_placing_poses(load_translations(data_dir + "/placing_poses.boost", {"cb", "ca", "co"}))
		
    {
		load_graph(info, data_dir + "/base_graph.boost", {"cb", "e", "cb"});
		load_graph(info, data_dir + "/arm_graph.boost", {"ca", "t", "ca"});
	}

	~External() = default;

    void registerComponents() const;
	
protected:
	//! The table of robot-object overlaps
	const OverlapSet3 _overlaps_ro;

	//! The table of object-object overlaps
	const OverlapSet2 _overlaps_oo;
	
	//! An index with all overlaps, mapping each pair <conf_base, trajectory_arm> to 
	//! all the object configurations that overlap with the arm trajectory
	const OverlapMap _overlap_idx;
	
	std::vector<int> _sources;
	std::vector<int> _targets;
	
	
	const PoseMap _placing_poses;


	OverlapSet3 load_overlaps_ro(const std::string& filename) {
		const ProblemInfo& info = ProblemInfo::getInstance();
		
		table data;
		deserialize<table>(filename, data);

		OverlapSet3 overlaps;

		for (const auto& elem:data) {
 			
 			//std::cout << "cb" + std::to_string(std::get<0>(elem)) << ",   ";
 			//std::cout << "t" + std::to_string(std::get<1>(elem)) << ",   ";
 			//std::cout << "co" + std::to_string(std::get<2>(elem))<< std::endl;
			
			
			ObjectIdx conf_base = info.getObjectId("cb" + std::to_string(std::get<0>(elem)));
			ObjectIdx traj = info.getObjectId("t" + std::to_string(std::get<1>(elem)));
			ObjectIdx conf_obj = info.getObjectId("co" + std::to_string(std::get<2>(elem)));
			overlaps.insert(std::make_tuple(conf_base, traj, conf_obj));
		}
		
		return overlaps;
	}
	
	OverlapSet2 load_overlaps_oo(const std::string& filename) {
		const ProblemInfo& info = ProblemInfo::getInstance();
		
		table data;
		deserialize(filename, data);

		OverlapSet2 overlaps;
		for (const auto& elem:data) {

 			//std::cout << "co" + std::to_string(std::get<0>(elem)) << ",   ";
 			//std::cout << "co" + std::to_string(std::get<2>(elem))<< std::endl;

			ObjectIdx conf_base = info.getObjectId("co" + std::to_string(std::get<0>(elem)));
			// The second configuration is ignored
			ObjectIdx conf_obj = info.getObjectId("co" + std::to_string(std::get<2>(elem)));

			overlaps.insert(std::make_pair(conf_base, conf_obj));
		}

		return overlaps;
	}
	
	OverlapMap index_overlaps(const OverlapSet3& overlaps) {
		OverlapMap index;
		
		for (const std::tuple<int, int,int> overlap:overlaps) {
			auto key = std::make_pair(std::get<0>(overlap), std::get<1>(overlap));
			index[key].push_back(std::get<2>(overlap));
		}
		
		return index;
	}
	
	OTranslationMap load_translations(const std::string& filename, const std::vector<std::string>& object_prefixes) {
		assert(object_prefixes.size() == 3);
		const ProblemInfo& info = ProblemInfo::getInstance();
		OTranslationMap transitions;
		
		SerializedTranslationMap serialized;
		deserialize(filename, serialized);
		
// 		std::cout << "Loading file: " << filename << std::endl;
		
		
		for (const auto& elem:serialized) {
			ObjectIdx conf_origin = info.getObjectId(object_prefixes[0] + std::to_string(elem.first.first));
			ObjectIdx edge = info.getObjectId(object_prefixes[1] + std::to_string(elem.first.second));
			ObjectIdx conf_target = info.getObjectId(object_prefixes[2] + std::to_string(elem.second));
			transitions.insert(std::make_pair(std::make_pair(conf_origin, edge), conf_target));
			
			
 			/*std::cout << "Element: (" << object_prefixes[0] + std::to_string(elem.first.first);
 			std::cout << ", ";
 			std::cout << object_prefixes[1] + std::to_string(elem.first.second);
 			std::cout << ", ";
 			std::cout << object_prefixes[2] + std::to_string(elem.second);
 			std::cout << ")" << std::endl;
			*/
		}

		return transitions;
	}
	
	void load_graph(const ProblemInfo& info, const std::string& filename, const std::vector<std::string>& object_prefixes) {
		assert(object_prefixes.size() == 3);
		
		SerializedTranslationMap serialized;
		deserialize(filename, serialized);
		
		for (const auto& elem:serialized) {
			ObjectIdx source = info.getObjectId(object_prefixes[0] + std::to_string(elem.first.first));
			ObjectIdx edge = info.getObjectId(object_prefixes[1] + std::to_string(elem.first.second));
			ObjectIdx target = info.getObjectId(object_prefixes[2] + std::to_string(elem.second));
			
			assert(0 <= edge && edge < _sources.size() && edge < _targets.size());
			assert(_sources[edge] == -1 && "We shouldn't be setting the same source or target twice");
			assert(_targets[edge] == -1 && "We shouldn't be setting the same source or target twice");
			_sources[edge] = source;
			_targets[edge] = target;
			
 			/*std::cout << "Element: (" << object_prefixes[0] + std::to_string(elem.first.first);
 			std::cout << ", ";
 			std::cout << object_prefixes[1] + std::to_string(elem.first.second);
 			std::cout << ", ";
 			std::cout << object_prefixes[2] + std::to_string(elem.second);
 			std::cout << ")" << std::endl;
			*/
		}
	} 
	
	
	
	
public:

	//! (Need to return a new object, since the return value might be an empty vector)
    std::vector<ObjectIdx> get_offending_configurations(ObjectIdx confb, ObjectIdx arm_traj) const {
		auto key = std::make_pair(confb, arm_traj);
		const auto& it = _overlap_idx.find(key);
		if (it == _overlap_idx.end()) return std::vector<ObjectIdx>();
		else return it->second;
	}
	
    bool nonoverlap_ro(const std::vector<ObjectIdx>& params) const {
// 		const ProblemInfo& info = ProblemInfo::getInstance();
        assert(params.size() == 3);
        std::tuple<unsigned, unsigned, unsigned> key{params[0], params[1], params[2]};
		
// 		std::cout << "nonoverlap_ro(" << info.deduceObjectName(params[0], info.getTypeId("conf_base"));
// 		std::cout << ", ";
// 		std::cout << info.deduceObjectName(params[1], info.getTypeId("conf_arm"));
// 		std::cout << ", ";
// 		std::cout << info.deduceObjectName(params[2], info.getTypeId("conf_obj"));
// 		std::cout << ") = ";
// 		std::cout << (_overlaps_ro.find(key) != _overlaps_ro.end());		
// 		std::cout << std::endl;
		
		
        return _overlaps_ro.find(key) == _overlaps_ro.end();
    }

	bool nonoverlap_oo(const std::vector<ObjectIdx>& params) const {
// 		const ProblemInfo& info = ProblemInfo::getInstance();
        assert(params.size() == 2);
        std::pair<unsigned, unsigned> key{params[0], params[1]};
		
// 		std::cout << "nonoverlap_oo(" << info.deduceObjectName(info.getTypeId("conf_obj"), params[0]);
// 		std::cout << ", ";
// 		std::cout << info.deduceObjectName(info.getTypeId("conf_obj"), params[1]);
// 		std::cout << ") = ";
// 		std::cout << (_overlaps_oo.find(key) != _overlaps_oo.end());
// 		std::cout << std::endl;
		
        return _overlaps_oo.find(key) == _overlaps_oo.end();
    }
    
    static int _retrieve(const std::vector<ObjectIdx>& params, const std::vector<int>& where) {
		assert(params.size() == 1);
		assert(0 <= params[0] && params[0] < where.size());
// 		assert(where[params[0]] != -1);
// 		if (where[params[0]] != -1)
		return where[params[0]];
	}    
    
    ObjectIdx target_b(const std::vector<ObjectIdx>& params) const { return _retrieve(params, _targets); }
    ObjectIdx target_a(const std::vector<ObjectIdx>& params) const { return _retrieve(params, _targets); }
	
    ObjectIdx source_b(const std::vector<ObjectIdx>& params) const { return _retrieve(params, _sources); }
    ObjectIdx source_a(const std::vector<ObjectIdx>& params) const { return _retrieve(params, _sources); }
	
	ObjectIdx placing_pose(const std::vector<ObjectIdx>& params) const {
		return get_translation(params, _placing_poses);
	}
	
	bool graspable(const std::vector<ObjectIdx>& params) const {
		assert(params.size()==3);
		auto it = _placing_poses.find(std::make_pair(params[0], params[1]));
		return it != _placing_poses.end() && it->second == params[2];
	}
	
	bool placeable(const std::vector<ObjectIdx>& params) const {
		assert(params.size()==2);
		return exists_translation(params, _placing_poses);
	}

protected:
	//! helper
	ObjectIdx get_translation(const std::vector<ObjectIdx>& params, const OTranslationMap& translations) const {
		assert(params.size() == 2);
		auto it = translations.find(std::make_pair(params[0], params[1]));
		if (it == translations.end()) throw UndefinedValueAccess();
		return it->second;
	}


	
	//! helper
	bool exists_translation(const std::vector<ObjectIdx>& params, const OTranslationMap& translations) const {
		assert(params.size() == 2);
		auto it = translations.find(std::make_pair(params[0], params[1]));
		return it != translations.end();
	}	
    
};



class NonoverlapROFormula : public fs::ExternallyDefinedFormula {
public:
        NonoverlapROFormula(const std::vector<const fs::Term*>& subterms)
		: ExternallyDefinedFormula(subterms),
		_external(dynamic_cast<const External&>(ProblemInfo::getInstance().get_external()))
		{
			assert(subterms.size() == 3);
		}

        NonoverlapROFormula* clone(const std::vector<const fs::Term*>& subterms) const { return new NonoverlapROFormula(subterms); }

        virtual std::string name() const { return "nonoverlap_ro"; }
        
protected:
	bool _satisfied(const std::vector<ObjectIdx>& values) const  { return _external.nonoverlap_ro(values); }
	const External& _external;
};


class NonoverlapOOFormula : public fs::ExternallyDefinedFormula {
public:
        NonoverlapOOFormula(const std::vector<const fs::Term*>& subterms)
		: ExternallyDefinedFormula(subterms),
		_external(dynamic_cast<const External&>(ProblemInfo::getInstance().get_external()))
		{
				assert(subterms.size() == 2);
		}

        NonoverlapOOFormula* clone(const std::vector<const fs::Term*>& subterms) const { return new NonoverlapOOFormula(subterms); }

        virtual std::string name() const { return "nonoverlap_oo"; }
        
protected:
	bool _satisfied(const std::vector<ObjectIdx>& values) const  { return _external.nonoverlap_oo(values); }
	const External& _external;
};


