(define (problem sample)
	(:domain fn-robot-navigation)
	(:objects 

		o1 o2 o3 o4 o5 o6 o7 o8 o9 o10 o11 o12 o13 o14 o15 o16 o17 o18 o19 o20 o21 o22 o23 o24 o25

 - object_id

	cb16
	cb15
	cb13
	cb14
	cb18
	cb17
	cb19
	cb20
	cb21
	cb22
	cb23
	cb24
	cb28
	cb29
	cb26
	cb27
	cb25
	cb30
	cb31
	cb32
	cb33
	cb0
	cb1
	cb2
	cb3
	cb4
	cb5
	cb6
	cb7
	cb8
	cb12
	cb9
	cb11
	cb10
	 - conf_base
	co6
	co18
	co23
	co60
	co63
	co44
	co54
	co48
	co11
	co10
	co17
	co40
	co7
	co3
	co36
	co12
	co37
	co35
	co20
	co33
	co4
	co13
	co26
	co57
	co19
	co49
	co56
	co2
	co9
	co45
	co5
	co41
	co14
	co31
	co53
	co43
	co52
	co47
	co29
	co61
	co59
	co25
	co42
	co62
	co21
	co38
	co51
	co39
	co16
	co55
	co24
	co8
	co15
	co22
	co46
	co27
	co58
	co65
	co1
	co34
	co28
	co0
	co64
	co32
	co30
	co50
 - conf_obj 
 
	ca3
	ca1
	ca6
	ca4
	ca8
	ca10
	ca9
	ca7
	ca5
	ca2
 - conf_arm 
 
	e42
	e43
	e44
	e1053
	e1056
	e1059
	e1062
	e45
	e46
	e1025
	e1027
	e1029
	e1031
	e1033
	e47
	e48
	e49
	e50
	e51
	e52
	e53
	e54
	e55
	e1042
	e56
	e57
	e58
	e59
	e60
	e61
	e62
	e63
	e64
	e67
	e68
	e71
	e72
	e69
	e70
	e65
	e66
	e4
	e5
	e6
	e7
	e2
	e3
	e0
	e1
	e1009
	e1011
	e1013
	e1015
	e1016
	e1018
	e1020
	e1022
	e1024
	e1026
	e1028
	e1030
	e1032
	e1035
	e1037
	e1039
	e1041
	e1043
	e1045
	e1047
	e1049
	e1051
	e1054
	e1057
	e1060
	e1063
	e1066
	e1068
	e1070
	e1072
	e10
	e11
	e8
	e9
	e1001
	e1003
	e1005
	e1007
	e1017
	e1019
	e1021
	e1023
	e1034
	e1036
	e1038
	e1040
	e1046
	e1048
	e1050
	e1052
	e1065
	e1067
	e1069
	e1071
	e12
	e13
	e14
	e15
	e16
	e17
	e1000
	e1002
	e1004
	e1006
	e1044
	e1055
	e1058
	e1061
	e1064
	e18
	e19
	e20
	e21
	e22
	e23
	e32
	e33
	e28
	e29
	e30
	e31
	e24
	e25
	e1008
	e1010
	e1012
	e1014
	e26
	e27
	e34
	e35
	e36
	e37
	e38
	e39
	e40
	e41
 - edge 
 
	t0
	t1
	t2
	t3
	t4
	t5
	t6
	t7
	t8
	t9
	t10
	t11
	t12
	t13
	t14
	t15
	t16
	t17
	t18
	t19
	t20
	t21
	t22
	t23
	t24
	t1000
	t1001
	t1002
	t1003
	t1004
	t1005
	t1006
	t1007
	t1008
	t1009
	t1010
	t1011
	t1012
	t1013
	t1014
	t1015
	t1016
	t1017
	t1018
	t1019
	t1020
	t1021
	t1022
	t1023
	t1024
 - trajectory 
 )
(:init
(= (confb rob ) cb0 )
(= (confa rob ) ca0 )
(= (traj rob ) t_init )
(= (holding) no_object)
; initial object configurations 
 
(= (confo o1) co46); -x 0.655154 -y 0.110603 -z 0.690007
(= (confo o2) co28); -x 0.682547 -y -0.276758 -z 0.690001
(= (confo o3) co61); -x 1.02147 -y 0.0154385 -z 0.690004
(= (confo o4) co18); -x 0.79664 -y -0.344682 -z 0.690007
(= (confo o5) co13); -x 0.92334 -y 0.338984 -z 0.689995
(= (confo o6) co38); -x 1.07896 -y -0.177105 -z 0.690004
(= (confo o7) co15); -x 0.657198 -y -0.100005 -z 0.689996
(= (confo o8) co30); -x 0.655363 -y 0.348731 -z 0.690012
(= (confo o9) co14); -x 1.09186 -y 0.381063 -z 0.689996
(= (confo o10) co53); -x 1.08537 -y -0.258863 -z 0.690003
(= (confo o11) co4); -x 0.928015 -y -0.307913 -z 0.69001
(= (confo o12) co64); -x 0.678783 -y -0.0157038 -z 0.690003
(= (confo o13) co44); -x 0.806506 -y -0.102476 -z 0.689996
(= (confo o14) co40); -x 0.960123 -y 0.193061 -z 0.690015
(= (confo o15) co1); -x 0.65674 -y -0.40133 -z 0.690006
(= (confo o16) co41); -x 1.09678 -y 0.309317 -z 0.690004
(= (confo o17) co50); -x 0.668769 -y 0.231705 -z 0.690077
(= (confo o18) co6); -x 0.778962 -y 0.314364 -z 0.690004
(= (confo o19) co49); -x 0.880673 -y 0.212924 -z 0.690009
(= (confo o20) co58); -x 0.65071 -y -0.195297 -z 0.690008
(= (confo o21) co63); -x 0.804652 -y 0.199106 -z 0.690008
(= (confo o22) co54); -x 0.805661 -y 0.0522932 -z 0.690006
(= (confo o23) co57); -x 0.865569 -y -0.170799 -z 0.689995
(= (confo o24) co52); -x 1.04448 -y -0.0525423 -z 0.690003
(= (confo o25) co51); -x 1.05809 -y 0.159026 -z 0.690003


 )
(:goal (and


;Place o22 on o3 configuration
;Place o3 on o22 configuration
;Place o23 on o11 configuration
(= (confo o22) co61); -x 1.02147 -y 0.0154385 -z 0.690004); -x 0.682547 -y -0.276758 -z 0.690001
(= (confo o3) co54); -x 0.805661 -y 0.0522932 -z 0.690006
(= (confo o23) co4); -x 0.928015 -y -0.307913 -z 0.69001 

))
(:constraints

(@nonoverlap_ro (confb rob) (traj rob) (confo o1))
(@nonoverlap_ro (confb rob) (traj rob) (confo o2))
(@nonoverlap_ro (confb rob) (traj rob) (confo o3))
(@nonoverlap_ro (confb rob) (traj rob) (confo o4))
(@nonoverlap_ro (confb rob) (traj rob) (confo o5))
(@nonoverlap_ro (confb rob) (traj rob) (confo o6))
(@nonoverlap_ro (confb rob) (traj rob) (confo o7))
(@nonoverlap_ro (confb rob) (traj rob) (confo o8))
(@nonoverlap_ro (confb rob) (traj rob) (confo o9))
(@nonoverlap_ro (confb rob) (traj rob) (confo o10))
(@nonoverlap_ro (confb rob) (traj rob) (confo o11))
(@nonoverlap_ro (confb rob) (traj rob) (confo o12))
(@nonoverlap_ro (confb rob) (traj rob) (confo o13))
(@nonoverlap_ro (confb rob) (traj rob) (confo o14))
(@nonoverlap_ro (confb rob) (traj rob) (confo o15))
(@nonoverlap_ro (confb rob) (traj rob) (confo o16))
(@nonoverlap_ro (confb rob) (traj rob) (confo o17))
(@nonoverlap_ro (confb rob) (traj rob) (confo o18))
(@nonoverlap_ro (confb rob) (traj rob) (confo o19))
(@nonoverlap_ro (confb rob) (traj rob) (confo o20))
(@nonoverlap_ro (confb rob) (traj rob) (confo o21))
(@nonoverlap_ro (confb rob) (traj rob) (confo o22))
(@nonoverlap_ro (confb rob) (traj rob) (confo o23))
(@nonoverlap_ro (confb rob) (traj rob) (confo o24))
(@nonoverlap_ro (confb rob) (traj rob) (confo o25))
(@nonoverlap_oo (confo o1) (confo o2))
(@nonoverlap_oo (confo o1) (confo o3))
(@nonoverlap_oo (confo o1) (confo o4))
(@nonoverlap_oo (confo o1) (confo o5))
(@nonoverlap_oo (confo o1) (confo o6))
(@nonoverlap_oo (confo o1) (confo o7))
(@nonoverlap_oo (confo o1) (confo o8))
(@nonoverlap_oo (confo o1) (confo o9))
(@nonoverlap_oo (confo o1) (confo o10))
(@nonoverlap_oo (confo o1) (confo o11))
(@nonoverlap_oo (confo o1) (confo o12))
(@nonoverlap_oo (confo o1) (confo o13))
(@nonoverlap_oo (confo o1) (confo o14))
(@nonoverlap_oo (confo o1) (confo o15))
(@nonoverlap_oo (confo o1) (confo o16))
(@nonoverlap_oo (confo o1) (confo o17))
(@nonoverlap_oo (confo o1) (confo o18))
(@nonoverlap_oo (confo o1) (confo o19))
(@nonoverlap_oo (confo o1) (confo o20))
(@nonoverlap_oo (confo o1) (confo o21))
(@nonoverlap_oo (confo o1) (confo o22))
(@nonoverlap_oo (confo o1) (confo o23))
(@nonoverlap_oo (confo o1) (confo o24))
(@nonoverlap_oo (confo o1) (confo o25))
(@nonoverlap_oo (confo o2) (confo o3))
(@nonoverlap_oo (confo o2) (confo o4))
(@nonoverlap_oo (confo o2) (confo o5))
(@nonoverlap_oo (confo o2) (confo o6))
(@nonoverlap_oo (confo o2) (confo o7))
(@nonoverlap_oo (confo o2) (confo o8))
(@nonoverlap_oo (confo o2) (confo o9))
(@nonoverlap_oo (confo o2) (confo o10))
(@nonoverlap_oo (confo o2) (confo o11))
(@nonoverlap_oo (confo o2) (confo o12))
(@nonoverlap_oo (confo o2) (confo o13))
(@nonoverlap_oo (confo o2) (confo o14))
(@nonoverlap_oo (confo o2) (confo o15))
(@nonoverlap_oo (confo o2) (confo o16))
(@nonoverlap_oo (confo o2) (confo o17))
(@nonoverlap_oo (confo o2) (confo o18))
(@nonoverlap_oo (confo o2) (confo o19))
(@nonoverlap_oo (confo o2) (confo o20))
(@nonoverlap_oo (confo o2) (confo o21))
(@nonoverlap_oo (confo o2) (confo o22))
(@nonoverlap_oo (confo o2) (confo o23))
(@nonoverlap_oo (confo o2) (confo o24))
(@nonoverlap_oo (confo o2) (confo o25))
(@nonoverlap_oo (confo o3) (confo o4))
(@nonoverlap_oo (confo o3) (confo o5))
(@nonoverlap_oo (confo o3) (confo o6))
(@nonoverlap_oo (confo o3) (confo o7))
(@nonoverlap_oo (confo o3) (confo o8))
(@nonoverlap_oo (confo o3) (confo o9))
(@nonoverlap_oo (confo o3) (confo o10))
(@nonoverlap_oo (confo o3) (confo o11))
(@nonoverlap_oo (confo o3) (confo o12))
(@nonoverlap_oo (confo o3) (confo o13))
(@nonoverlap_oo (confo o3) (confo o14))
(@nonoverlap_oo (confo o3) (confo o15))
(@nonoverlap_oo (confo o3) (confo o16))
(@nonoverlap_oo (confo o3) (confo o17))
(@nonoverlap_oo (confo o3) (confo o18))
(@nonoverlap_oo (confo o3) (confo o19))
(@nonoverlap_oo (confo o3) (confo o20))
(@nonoverlap_oo (confo o3) (confo o21))
(@nonoverlap_oo (confo o3) (confo o22))
(@nonoverlap_oo (confo o3) (confo o23))
(@nonoverlap_oo (confo o3) (confo o24))
(@nonoverlap_oo (confo o3) (confo o25))
(@nonoverlap_oo (confo o4) (confo o5))
(@nonoverlap_oo (confo o4) (confo o6))
(@nonoverlap_oo (confo o4) (confo o7))
(@nonoverlap_oo (confo o4) (confo o8))
(@nonoverlap_oo (confo o4) (confo o9))
(@nonoverlap_oo (confo o4) (confo o10))
(@nonoverlap_oo (confo o4) (confo o11))
(@nonoverlap_oo (confo o4) (confo o12))
(@nonoverlap_oo (confo o4) (confo o13))
(@nonoverlap_oo (confo o4) (confo o14))
(@nonoverlap_oo (confo o4) (confo o15))
(@nonoverlap_oo (confo o4) (confo o16))
(@nonoverlap_oo (confo o4) (confo o17))
(@nonoverlap_oo (confo o4) (confo o18))
(@nonoverlap_oo (confo o4) (confo o19))
(@nonoverlap_oo (confo o4) (confo o20))
(@nonoverlap_oo (confo o4) (confo o21))
(@nonoverlap_oo (confo o4) (confo o22))
(@nonoverlap_oo (confo o4) (confo o23))
(@nonoverlap_oo (confo o4) (confo o24))
(@nonoverlap_oo (confo o4) (confo o25))
(@nonoverlap_oo (confo o5) (confo o6))
(@nonoverlap_oo (confo o5) (confo o7))
(@nonoverlap_oo (confo o5) (confo o8))
(@nonoverlap_oo (confo o5) (confo o9))
(@nonoverlap_oo (confo o5) (confo o10))
(@nonoverlap_oo (confo o5) (confo o11))
(@nonoverlap_oo (confo o5) (confo o12))
(@nonoverlap_oo (confo o5) (confo o13))
(@nonoverlap_oo (confo o5) (confo o14))
(@nonoverlap_oo (confo o5) (confo o15))
(@nonoverlap_oo (confo o5) (confo o16))
(@nonoverlap_oo (confo o5) (confo o17))
(@nonoverlap_oo (confo o5) (confo o18))
(@nonoverlap_oo (confo o5) (confo o19))
(@nonoverlap_oo (confo o5) (confo o20))
(@nonoverlap_oo (confo o5) (confo o21))
(@nonoverlap_oo (confo o5) (confo o22))
(@nonoverlap_oo (confo o5) (confo o23))
(@nonoverlap_oo (confo o5) (confo o24))
(@nonoverlap_oo (confo o5) (confo o25))
(@nonoverlap_oo (confo o6) (confo o7))
(@nonoverlap_oo (confo o6) (confo o8))
(@nonoverlap_oo (confo o6) (confo o9))
(@nonoverlap_oo (confo o6) (confo o10))
(@nonoverlap_oo (confo o6) (confo o11))
(@nonoverlap_oo (confo o6) (confo o12))
(@nonoverlap_oo (confo o6) (confo o13))
(@nonoverlap_oo (confo o6) (confo o14))
(@nonoverlap_oo (confo o6) (confo o15))
(@nonoverlap_oo (confo o6) (confo o16))
(@nonoverlap_oo (confo o6) (confo o17))
(@nonoverlap_oo (confo o6) (confo o18))
(@nonoverlap_oo (confo o6) (confo o19))
(@nonoverlap_oo (confo o6) (confo o20))
(@nonoverlap_oo (confo o6) (confo o21))
(@nonoverlap_oo (confo o6) (confo o22))
(@nonoverlap_oo (confo o6) (confo o23))
(@nonoverlap_oo (confo o6) (confo o24))
(@nonoverlap_oo (confo o6) (confo o25))
(@nonoverlap_oo (confo o7) (confo o8))
(@nonoverlap_oo (confo o7) (confo o9))
(@nonoverlap_oo (confo o7) (confo o10))
(@nonoverlap_oo (confo o7) (confo o11))
(@nonoverlap_oo (confo o7) (confo o12))
(@nonoverlap_oo (confo o7) (confo o13))
(@nonoverlap_oo (confo o7) (confo o14))
(@nonoverlap_oo (confo o7) (confo o15))
(@nonoverlap_oo (confo o7) (confo o16))
(@nonoverlap_oo (confo o7) (confo o17))
(@nonoverlap_oo (confo o7) (confo o18))
(@nonoverlap_oo (confo o7) (confo o19))
(@nonoverlap_oo (confo o7) (confo o20))
(@nonoverlap_oo (confo o7) (confo o21))
(@nonoverlap_oo (confo o7) (confo o22))
(@nonoverlap_oo (confo o7) (confo o23))
(@nonoverlap_oo (confo o7) (confo o24))
(@nonoverlap_oo (confo o7) (confo o25))
(@nonoverlap_oo (confo o8) (confo o9))
(@nonoverlap_oo (confo o8) (confo o10))
(@nonoverlap_oo (confo o8) (confo o11))
(@nonoverlap_oo (confo o8) (confo o12))
(@nonoverlap_oo (confo o8) (confo o13))
(@nonoverlap_oo (confo o8) (confo o14))
(@nonoverlap_oo (confo o8) (confo o15))
(@nonoverlap_oo (confo o8) (confo o16))
(@nonoverlap_oo (confo o8) (confo o17))
(@nonoverlap_oo (confo o8) (confo o18))
(@nonoverlap_oo (confo o8) (confo o19))
(@nonoverlap_oo (confo o8) (confo o20))
(@nonoverlap_oo (confo o8) (confo o21))
(@nonoverlap_oo (confo o8) (confo o22))
(@nonoverlap_oo (confo o8) (confo o23))
(@nonoverlap_oo (confo o8) (confo o24))
(@nonoverlap_oo (confo o8) (confo o25))
(@nonoverlap_oo (confo o9) (confo o10))
(@nonoverlap_oo (confo o9) (confo o11))
(@nonoverlap_oo (confo o9) (confo o12))
(@nonoverlap_oo (confo o9) (confo o13))
(@nonoverlap_oo (confo o9) (confo o14))
(@nonoverlap_oo (confo o9) (confo o15))
(@nonoverlap_oo (confo o9) (confo o16))
(@nonoverlap_oo (confo o9) (confo o17))
(@nonoverlap_oo (confo o9) (confo o18))
(@nonoverlap_oo (confo o9) (confo o19))
(@nonoverlap_oo (confo o9) (confo o20))
(@nonoverlap_oo (confo o9) (confo o21))
(@nonoverlap_oo (confo o9) (confo o22))
(@nonoverlap_oo (confo o9) (confo o23))
(@nonoverlap_oo (confo o9) (confo o24))
(@nonoverlap_oo (confo o9) (confo o25))
(@nonoverlap_oo (confo o10) (confo o11))
(@nonoverlap_oo (confo o10) (confo o12))
(@nonoverlap_oo (confo o10) (confo o13))
(@nonoverlap_oo (confo o10) (confo o14))
(@nonoverlap_oo (confo o10) (confo o15))
(@nonoverlap_oo (confo o10) (confo o16))
(@nonoverlap_oo (confo o10) (confo o17))
(@nonoverlap_oo (confo o10) (confo o18))
(@nonoverlap_oo (confo o10) (confo o19))
(@nonoverlap_oo (confo o10) (confo o20))
(@nonoverlap_oo (confo o10) (confo o21))
(@nonoverlap_oo (confo o10) (confo o22))
(@nonoverlap_oo (confo o10) (confo o23))
(@nonoverlap_oo (confo o10) (confo o24))
(@nonoverlap_oo (confo o10) (confo o25))
(@nonoverlap_oo (confo o11) (confo o12))
(@nonoverlap_oo (confo o11) (confo o13))
(@nonoverlap_oo (confo o11) (confo o14))
(@nonoverlap_oo (confo o11) (confo o15))
(@nonoverlap_oo (confo o11) (confo o16))
(@nonoverlap_oo (confo o11) (confo o17))
(@nonoverlap_oo (confo o11) (confo o18))
(@nonoverlap_oo (confo o11) (confo o19))
(@nonoverlap_oo (confo o11) (confo o20))
(@nonoverlap_oo (confo o11) (confo o21))
(@nonoverlap_oo (confo o11) (confo o22))
(@nonoverlap_oo (confo o11) (confo o23))
(@nonoverlap_oo (confo o11) (confo o24))
(@nonoverlap_oo (confo o11) (confo o25))
(@nonoverlap_oo (confo o12) (confo o13))
(@nonoverlap_oo (confo o12) (confo o14))
(@nonoverlap_oo (confo o12) (confo o15))
(@nonoverlap_oo (confo o12) (confo o16))
(@nonoverlap_oo (confo o12) (confo o17))
(@nonoverlap_oo (confo o12) (confo o18))
(@nonoverlap_oo (confo o12) (confo o19))
(@nonoverlap_oo (confo o12) (confo o20))
(@nonoverlap_oo (confo o12) (confo o21))
(@nonoverlap_oo (confo o12) (confo o22))
(@nonoverlap_oo (confo o12) (confo o23))
(@nonoverlap_oo (confo o12) (confo o24))
(@nonoverlap_oo (confo o12) (confo o25))
(@nonoverlap_oo (confo o13) (confo o14))
(@nonoverlap_oo (confo o13) (confo o15))
(@nonoverlap_oo (confo o13) (confo o16))
(@nonoverlap_oo (confo o13) (confo o17))
(@nonoverlap_oo (confo o13) (confo o18))
(@nonoverlap_oo (confo o13) (confo o19))
(@nonoverlap_oo (confo o13) (confo o20))
(@nonoverlap_oo (confo o13) (confo o21))
(@nonoverlap_oo (confo o13) (confo o22))
(@nonoverlap_oo (confo o13) (confo o23))
(@nonoverlap_oo (confo o13) (confo o24))
(@nonoverlap_oo (confo o13) (confo o25))
(@nonoverlap_oo (confo o14) (confo o15))
(@nonoverlap_oo (confo o14) (confo o16))
(@nonoverlap_oo (confo o14) (confo o17))
(@nonoverlap_oo (confo o14) (confo o18))
(@nonoverlap_oo (confo o14) (confo o19))
(@nonoverlap_oo (confo o14) (confo o20))
(@nonoverlap_oo (confo o14) (confo o21))
(@nonoverlap_oo (confo o14) (confo o22))
(@nonoverlap_oo (confo o14) (confo o23))
(@nonoverlap_oo (confo o14) (confo o24))
(@nonoverlap_oo (confo o14) (confo o25))
(@nonoverlap_oo (confo o15) (confo o16))
(@nonoverlap_oo (confo o15) (confo o17))
(@nonoverlap_oo (confo o15) (confo o18))
(@nonoverlap_oo (confo o15) (confo o19))
(@nonoverlap_oo (confo o15) (confo o20))
(@nonoverlap_oo (confo o15) (confo o21))
(@nonoverlap_oo (confo o15) (confo o22))
(@nonoverlap_oo (confo o15) (confo o23))
(@nonoverlap_oo (confo o15) (confo o24))
(@nonoverlap_oo (confo o15) (confo o25))
(@nonoverlap_oo (confo o16) (confo o17))
(@nonoverlap_oo (confo o16) (confo o18))
(@nonoverlap_oo (confo o16) (confo o19))
(@nonoverlap_oo (confo o16) (confo o20))
(@nonoverlap_oo (confo o16) (confo o21))
(@nonoverlap_oo (confo o16) (confo o22))
(@nonoverlap_oo (confo o16) (confo o23))
(@nonoverlap_oo (confo o16) (confo o24))
(@nonoverlap_oo (confo o16) (confo o25))
(@nonoverlap_oo (confo o17) (confo o18))
(@nonoverlap_oo (confo o17) (confo o19))
(@nonoverlap_oo (confo o17) (confo o20))
(@nonoverlap_oo (confo o17) (confo o21))
(@nonoverlap_oo (confo o17) (confo o22))
(@nonoverlap_oo (confo o17) (confo o23))
(@nonoverlap_oo (confo o17) (confo o24))
(@nonoverlap_oo (confo o17) (confo o25))
(@nonoverlap_oo (confo o18) (confo o19))
(@nonoverlap_oo (confo o18) (confo o20))
(@nonoverlap_oo (confo o18) (confo o21))
(@nonoverlap_oo (confo o18) (confo o22))
(@nonoverlap_oo (confo o18) (confo o23))
(@nonoverlap_oo (confo o18) (confo o24))
(@nonoverlap_oo (confo o18) (confo o25))
(@nonoverlap_oo (confo o19) (confo o20))
(@nonoverlap_oo (confo o19) (confo o21))
(@nonoverlap_oo (confo o19) (confo o22))
(@nonoverlap_oo (confo o19) (confo o23))
(@nonoverlap_oo (confo o19) (confo o24))
(@nonoverlap_oo (confo o19) (confo o25))
(@nonoverlap_oo (confo o20) (confo o21))
(@nonoverlap_oo (confo o20) (confo o22))
(@nonoverlap_oo (confo o20) (confo o23))
(@nonoverlap_oo (confo o20) (confo o24))
(@nonoverlap_oo (confo o20) (confo o25))
(@nonoverlap_oo (confo o21) (confo o22))
(@nonoverlap_oo (confo o21) (confo o23))
(@nonoverlap_oo (confo o21) (confo o24))
(@nonoverlap_oo (confo o21) (confo o25))
(@nonoverlap_oo (confo o22) (confo o23))
(@nonoverlap_oo (confo o22) (confo o24))
(@nonoverlap_oo (confo o22) (confo o25))
(@nonoverlap_oo (confo o23) (confo o24))
(@nonoverlap_oo (confo o23) (confo o25))
(@nonoverlap_oo (confo o24) (confo o25))




)
)
