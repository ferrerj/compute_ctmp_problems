
#pragma once

#include <fs_types.hxx>
#include <lib/rapidjson/document.h>
#include "external.hxx"

#include <utils/component_factory.hxx>

namespace fs0 { class Problem; }

using namespace fs0;



class ComponentFactory : public BaseComponentFactory {
    std::map<std::string, Function> instantiateFunctions(const ProblemInfo& info) const override {
		return {
			{"@graspable", [&info](const ObjectIdxVector& parameters){ const External& external = dynamic_cast<const External&>(info.get_external());  return external.graspable(parameters); }},
			{"@placeable", [&info](const ObjectIdxVector& parameters){ const External& external = dynamic_cast<const External&>(info.get_external());  return external.placeable(parameters); }},
			{"@target_arm", [&info](const ObjectIdxVector& parameters){ const External& external = dynamic_cast<const External&>(info.get_external());  return external.target_arm(parameters); }},
			{"@target_base", [&info](const ObjectIdxVector& parameters){ const External& external = dynamic_cast<const External&>(info.get_external());  return external.target_base(parameters); }},
			{"@source_base", [&info](const ObjectIdxVector& parameters){ const External& external = dynamic_cast<const External&>(info.get_external());  return external.source_base(parameters); }},
			{"@nonoverlap_ro", [&info](const ObjectIdxVector& parameters){ const External& external = dynamic_cast<const External&>(info.get_external());  return external.nonoverlap_ro(parameters); }},
			{"@nonoverlap_oo", [&info](const ObjectIdxVector& parameters){ const External& external = dynamic_cast<const External&>(info.get_external());  return external.nonoverlap_oo(parameters); }},
			{"@source_arm", [&info](const ObjectIdxVector& parameters){ const External& external = dynamic_cast<const External&>(info.get_external());  return external.source_arm(parameters); }},
			{"@placing_pose", [&info](const ObjectIdxVector& parameters){ const External& external = dynamic_cast<const External&>(info.get_external());  return external.placing_pose(parameters); }}
		};
	}
};

/* Generate the whole planning problem */
Problem* generate(const rapidjson::Document& data, const std::string& data_dir);
