(define (problem sample)
	(:domain fn-robot-navigation)
	(:objects 

		o1 o2 o3 o4 o5 o6 o7 o8 o9 o10 o11 o12 o13 o14 o15

 - object_id

	cb16
	cb15
	cb13
	cb14
	cb18
	cb17
	cb19
	cb20
	cb21
	cb22
	cb23
	cb24
	cb28
	cb29
	cb26
	cb27
	cb25
	cb30
	cb31
	cb32
	cb33
	cb0
	cb1
	cb2
	cb3
	cb4
	cb5
	cb6
	cb7
	cb8
	cb12
	cb9
	cb11
	cb10
	 - conf_base
	co34
	co35
	co20
	co43
	co31
	co10
	co56
	co0
	co25
	co33
	co54
	co41
	co27
	co58
	co15
	co50
	co16
	co49
	co46
	co29
	co7
	co55
	co11
	co23
	co40
	co14
	co42
	co28
	co26
	co30
	co38
	co2
	co47
	co5
	co44
	co3
	co39
	co36
	co18
	co53
	co22
	co8
	co52
	co48
	co4
	co21
	co51
	co1
	co45
	co17
	co32
	co9
	co12
	co19
	co37
	co24
	co57
	co59
	co6
	co13
 - conf_obj 
 
	ca1
	ca3
	ca7
	ca6
	ca5
	ca4
	ca2
 - conf_arm 
 
	e42
	e43
	e44
	e1053
	e1056
	e1059
	e1062
	e45
	e46
	e1025
	e1027
	e1029
	e1031
	e1033
	e47
	e48
	e49
	e50
	e51
	e52
	e53
	e54
	e55
	e1042
	e56
	e57
	e58
	e59
	e60
	e61
	e62
	e63
	e64
	e67
	e68
	e71
	e72
	e69
	e70
	e65
	e66
	e4
	e5
	e6
	e7
	e2
	e3
	e0
	e1
	e1009
	e1011
	e1013
	e1015
	e1016
	e1018
	e1020
	e1022
	e1024
	e1026
	e1028
	e1030
	e1032
	e1035
	e1037
	e1039
	e1041
	e1043
	e1045
	e1047
	e1049
	e1051
	e1054
	e1057
	e1060
	e1063
	e1066
	e1068
	e1070
	e1072
	e10
	e11
	e8
	e9
	e1001
	e1003
	e1005
	e1007
	e1017
	e1019
	e1021
	e1023
	e1034
	e1036
	e1038
	e1040
	e1046
	e1048
	e1050
	e1052
	e1065
	e1067
	e1069
	e1071
	e12
	e13
	e14
	e15
	e16
	e17
	e1000
	e1002
	e1004
	e1006
	e1044
	e1055
	e1058
	e1061
	e1064
	e18
	e19
	e20
	e21
	e22
	e23
	e32
	e33
	e28
	e29
	e30
	e31
	e24
	e25
	e1008
	e1010
	e1012
	e1014
	e26
	e27
	e34
	e35
	e36
	e37
	e38
	e39
	e40
	e41
 - edge 
 
	t0
	t1
	t2
	t3
	t4
	t5
	t6
	t7
	t8
	t9
	t10
	t11
	t12
	t13
	t14
	t15
	t16
	t17
	t18
	t19
	t20
	t1000
	t1001
	t1002
	t1003
	t1004
	t1005
	t1006
	t1007
	t1008
	t1009
	t1010
	t1011
	t1012
	t1013
	t1014
	t1015
	t1016
	t1017
	t1018
	t1019
	t1020
 - trajectory 
 )
(:init
(= (confb rob ) cb0 )
(= (confa rob ) ca0 )
(= (traj rob) t_init)
(= (holding) no_object)

(= (confo o1) co51); -x 0.628937 -y 0.168876 -z 0.690023
(= (confo o2) co18); -x 1.01996 -y 0.36635 -z 0.689993
(= (confo o3) co54); -x 0.994261 -y 0.00179611 -z 0.689991
(= (confo o4) co10); -x 0.826327 -y 0.314913 -z 0.690011
(= (confo o5) co48); -x 1.06793 -y -0.168512 -z 0.690014
(= (confo o6) co49); -x 0.855883 -y -0.144356 -z 0.690018
(= (confo o7) co35); -x 0.801202 -y -0.294381 -z 0.690026
(= (confo o8) co1); -x 0.633961 -y 0.312366 -z 0.690014
(= (confo o9) co42); -x 1.00859 -y -0.251969 -z 0.69
(= (confo o10) co56); -x 0.841058 -y -0.0142179 -z 0.690039
(= (confo o11) co0); -x 0.962533 -y -0.356423 -z 0.690271
(= (confo o12) co19); -x 0.656019 -y 0.000226605 -z 0.690004
(= (confo o13) co47); -x 0.617712 -y -0.177443 -z 0.690005
(= (confo o14) co38); -x 0.907516 -y 0.327392 -z 0.689992
(= (confo o15) co44); -x 0.623538 -y -0.415246 -z 0.690004



)
(:goal (and

(= (confo o12 ) c_held )



))
(:constraints


(@nonoverlap_ro (confb rob) (traj rob) (confo o1))
(@nonoverlap_ro (confb rob) (traj rob) (confo o2))
(@nonoverlap_ro (confb rob) (traj rob) (confo o3))
(@nonoverlap_ro (confb rob) (traj rob) (confo o4))
(@nonoverlap_ro (confb rob) (traj rob) (confo o5))
(@nonoverlap_ro (confb rob) (traj rob) (confo o6))
(@nonoverlap_ro (confb rob) (traj rob) (confo o7))
(@nonoverlap_ro (confb rob) (traj rob) (confo o8))
(@nonoverlap_ro (confb rob) (traj rob) (confo o9))
(@nonoverlap_ro (confb rob) (traj rob) (confo o10))
(@nonoverlap_ro (confb rob)  (traj rob) (confo o11))
(@nonoverlap_ro (confb rob)  (traj rob) (confo o12))
(@nonoverlap_ro (confb rob)  (traj rob) (confo o13))
(@nonoverlap_ro (confb rob)  (traj rob) (confo o14))
(@nonoverlap_ro (confb rob) (traj rob) (confo o15))


	(@nonoverlap_oo (confo o1) (confo o2))
	(@nonoverlap_oo (confo o1) (confo o3))
	(@nonoverlap_oo (confo o1) (confo o4))
	(@nonoverlap_oo (confo o1) (confo o5))
	(@nonoverlap_oo (confo o1) (confo o6))
	(@nonoverlap_oo (confo o1) (confo o7))
	(@nonoverlap_oo (confo o1) (confo o8))
	(@nonoverlap_oo (confo o1) (confo o9))
	(@nonoverlap_oo (confo o1) (confo o10))
	(@nonoverlap_oo (confo o1) (confo o11))
	(@nonoverlap_oo (confo o1) (confo o12))
	(@nonoverlap_oo (confo o1) (confo o13))
	(@nonoverlap_oo (confo o1) (confo o14))
	(@nonoverlap_oo (confo o1) (confo o15))


	(@nonoverlap_oo (confo o2) (confo o3))
	(@nonoverlap_oo (confo o2) (confo o4))
	(@nonoverlap_oo (confo o2) (confo o5))
	(@nonoverlap_oo (confo o2) (confo o6))
	(@nonoverlap_oo (confo o2) (confo o7))
	(@nonoverlap_oo (confo o2) (confo o8))	
	(@nonoverlap_oo (confo o2) (confo o9))
	(@nonoverlap_oo (confo o2) (confo o10))
	(@nonoverlap_oo (confo o2) (confo o11))
	(@nonoverlap_oo (confo o2) (confo o12))
	(@nonoverlap_oo (confo o2) (confo o13))
	(@nonoverlap_oo (confo o2) (confo o14))
	(@nonoverlap_oo (confo o2) (confo o15))

	(@nonoverlap_oo (confo o3) (confo o4))
	(@nonoverlap_oo (confo o3) (confo o5))
	(@nonoverlap_oo (confo o3) (confo o6))
	(@nonoverlap_oo (confo o3) (confo o7))	
	(@nonoverlap_oo (confo o3) (confo o8))
	(@nonoverlap_oo (confo o3) (confo o9))
	(@nonoverlap_oo (confo o3) (confo o10))
	(@nonoverlap_oo (confo o3) (confo o11))
	(@nonoverlap_oo (confo o3) (confo o12))
	(@nonoverlap_oo (confo o3) (confo o13))
	(@nonoverlap_oo (confo o3) (confo o14))
	(@nonoverlap_oo (confo o3) (confo o15))




	(@nonoverlap_oo (confo o4) (confo o5))
	(@nonoverlap_oo (confo o4) (confo o6))
	(@nonoverlap_oo (confo o4) (confo o7))
	(@nonoverlap_oo (confo o4) (confo o8))
	(@nonoverlap_oo (confo o4) (confo o9))
	(@nonoverlap_oo (confo o4) (confo o10))
	(@nonoverlap_oo (confo o4) (confo o11))
	(@nonoverlap_oo (confo o4) (confo o12))
	(@nonoverlap_oo (confo o4) (confo o13))
	(@nonoverlap_oo (confo o4) (confo o14))
	(@nonoverlap_oo (confo o4) (confo o15))


	(@nonoverlap_oo (confo o5) (confo o6))
	(@nonoverlap_oo (confo o5) (confo o7))
	(@nonoverlap_oo (confo o5) (confo o8))
	(@nonoverlap_oo (confo o5) (confo o9))
	(@nonoverlap_oo (confo o5) (confo o10))
	(@nonoverlap_oo (confo o5) (confo o11))
	(@nonoverlap_oo (confo o5) (confo o12))
	(@nonoverlap_oo (confo o5) (confo o13))
	(@nonoverlap_oo (confo o5) (confo o14))
	(@nonoverlap_oo (confo o5) (confo o15))



	(@nonoverlap_oo (confo o6) (confo o7))
	(@nonoverlap_oo (confo o6) (confo o8))
	(@nonoverlap_oo (confo o6) (confo o9))
	(@nonoverlap_oo (confo o6) (confo o10))
	(@nonoverlap_oo (confo o6) (confo o11))
	(@nonoverlap_oo (confo o6) (confo o12))
	(@nonoverlap_oo (confo o6) (confo o13))
	(@nonoverlap_oo (confo o6) (confo o14))
	(@nonoverlap_oo (confo o6) (confo o15))


	(@nonoverlap_oo (confo o7) (confo o8))
	(@nonoverlap_oo (confo o7) (confo o9))
	(@nonoverlap_oo (confo o7) (confo o10))
	(@nonoverlap_oo (confo o7) (confo o11))
	(@nonoverlap_oo (confo o7) (confo o12))
	(@nonoverlap_oo (confo o7) (confo o13))
	(@nonoverlap_oo (confo o7) (confo o14))
	(@nonoverlap_oo (confo o7) (confo o15))

	(@nonoverlap_oo (confo o8) (confo o9))
	(@nonoverlap_oo (confo o8) (confo o10))
	(@nonoverlap_oo (confo o8) (confo o11))
	(@nonoverlap_oo (confo o8) (confo o12))
	(@nonoverlap_oo (confo o8) (confo o13))
	(@nonoverlap_oo (confo o8) (confo o14))
	(@nonoverlap_oo (confo o8) (confo o15))


	(@nonoverlap_oo (confo o9) (confo o10))
	(@nonoverlap_oo (confo o9) (confo o11))
	(@nonoverlap_oo (confo o9) (confo o12))
	(@nonoverlap_oo (confo o9) (confo o13))
	(@nonoverlap_oo (confo o9) (confo o14))
	(@nonoverlap_oo (confo o9) (confo o15))

	(@nonoverlap_oo (confo o10) (confo o11))
	(@nonoverlap_oo (confo o10) (confo o12))
	(@nonoverlap_oo (confo o10) (confo o13))
	(@nonoverlap_oo (confo o10) (confo o14))
	(@nonoverlap_oo (confo o10) (confo o15))


	(@nonoverlap_oo (confo o11) (confo o12))
	(@nonoverlap_oo (confo o11) (confo o13))
	(@nonoverlap_oo (confo o11) (confo o14))
	(@nonoverlap_oo (confo o11) (confo o15))

	(@nonoverlap_oo (confo o12) (confo o13))
	(@nonoverlap_oo (confo o12) (confo o14))
	(@nonoverlap_oo (confo o12) (confo o15))

	(@nonoverlap_oo (confo o13) (confo o14))
	(@nonoverlap_oo (confo o13) (confo o15))

	(@nonoverlap_oo (confo o14) (confo o15))


)
)
