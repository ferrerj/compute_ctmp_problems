(define (problem sample)
	(:domain fn-robot-navigation)
	(:objects 

		o1 o2 o3 o4 o5 o6 o7 o8 o9 o10 o11 o12 o13 o14 o15 o16 o17 o18 o19 o20 o21 o22 o23 o24 o25

 - object_id

	cb37
	cb36
	cb35
	cb34
	cb38
	cb40
	cb39
	cb41
	cb43
	cb42
	cb45
	cb44
	cb50
	cb53
	cb52
	cb51
	cb47
	cb46
	cb49
	cb48
	cb16
	cb15
	cb13
	cb14
	cb18
	cb17
	cb19
	cb20
	cb21
	cb22
	cb23
	cb24
	cb28
	cb29
	cb26
	cb27
	cb25
	cb30
	cb31
	cb32
	cb33
	cb0
	cb64
	cb63
	cb66
	cb65
	cb59
	cb60
	cb58
	cb61
	cb62
	cb1
	cb2
	cb3
	cb4
	cb54
	cb55
	cb56
	cb57
	cb5
	cb6
	cb7
	cb8
	cb12
	cb9
	cb11
	cb10
	 - conf_base
	co100
	co83
	co101
	co142
	co121
	co85
	co122
	co12
	co145
	co47
	co102
	co107
	co141
	co10
	co65
	co97
	co54
	co62
	co125
	co86
	co104
	co82
	co126
	co66
	co61
	co63
	co123
	co1
	co105
	co146
	co2
	co143
	co103
	co76
	co56
	co36
	co21
	co48
	co128
	co80
	co64
	co34
	co0
	co13
	co22
	co23
	co35
	co79
	co88
	co16
	co39
	co75
	co130
	co49
	co150
	co93
	co115
	co153
	co136
	co26
	co73
	co25
	co38
	co111
	co17
	co109
	co72
	co68
	co6
	co110
	co78
	co27
	co77
	co14
	co108
	co41
	co28
	co52
	co147
	co87
	co129
	co91
	co15
	co40
	co131
	co70
	co89
	co3
	co11
	co24
	co119
	co112
	co29
	co7
	co152
	co134
	co58
	co151
	co92
	co69
	co149
	co74
	co51
	co113
	co90
	co71
	co114
	co4
	co42
	co135
	co133
	co154
	co138
	co117
	co144
	co137
	co127
	co81
	co45
	co44
	co33
	co31
	co20
	co32
	co9
	co106
	co55
	co19
	co118
	co96
	co84
	co116
	co46
	co94
	co67
	co155
	co60
	co95
	co124
	co5
	co37
	co139
	co8
	co50
	co140
	co98
	co132
	co18
	co30
	co43
	co53
	co148
	co120
	co57
	co156
	co59
	co99
 - conf_obj 
 
	ca3
	ca1
	ca4
	ca8
	ca9
	ca11
	ca10
	ca7
	ca6
	ca5
	ca2
 - conf_arm 
 
	e90
	e91
	e92
	e1009
	e1011
	e1013
	e1015
	e1129
	e1132
	e1135
	e1138
	e1139
	e1142
	e1145
	e1148
	e115
	e116
	e117
	e1024
	e1027
	e1030
	e1033
	e118
	e119
	e120
	e121
	e122
	e123
	e124
	e125
	e126
	e139
	e140
	e141
	e1090
	e142
	e143
	e144
	e145
	e146
	e147
	e148
	e149
	e150
	e153
	e154
	e157
	e158
	e155
	e156
	e151
	e152
	e50
	e51
	e52
	e53
	e54
	e55
	e47
	e48
	e49
	e44
	e45
	e46
	e1064
	e1066
	e1068
	e1070
	e1072
	e1074
	e1076
	e1078
	e1080
	e1083
	e1085
	e1087
	e1089
	e1093
	e1096
	e1099
	e1102
	e58
	e59
	e56
	e57
	e1082
	e1084
	e1086
	e1088
	e1151
	e1153
	e1155
	e1157
	e60
	e61
	e62
	e63
	e64
	e65
	e66
	e67
	e68
	e69
	e70
	e71
	e80
	e81
	e76
	e77
	e78
	e79
	e72
	e73
	e1056
	e1058
	e1060
	e1062
	e74
	e75
	e82
	e83
	e84
	e85
	e86
	e87
	e88
	e89
	e6
	e7
	e4
	e5
	e2
	e3
	e0
	e1
	e1008
	e1010
	e1012
	e1014
	e1017
	e1019
	e1021
	e1023
	e1026
	e1029
	e1032
	e1035
	e1036
	e1038
	e1040
	e1042
	e1095
	e1098
	e1101
	e1104
	e1106
	e1108
	e1110
	e1112
	e1114
	e1117
	e1120
	e1123
	e1126
	e1128
	e1131
	e1134
	e1137
	e8
	e9
	e1000
	e1002
	e1004
	e1006
	e1016
	e1018
	e1020
	e1022
	e1025
	e1028
	e1031
	e1034
	e1092
	e1141
	e1144
	e1147
	e1150
	e12
	e13
	e10
	e11
	e14
	e15
	e18
	e19
	e16
	e17
	e1001
	e1003
	e1005
	e1007
	e1037
	e1039
	e1041
	e1043
	e1046
	e1049
	e1052
	e1055
	e1116
	e1119
	e1122
	e1125
	e1152
	e1154
	e1156
	e1158
	e22
	e23
	e20
	e21
	e38
	e39
	e36
	e37
	e1127
	e1130
	e1133
	e1136
	e42
	e43
	e40
	e41
	e24
	e25
	e26
	e1045
	e1048
	e1051
	e1054
	e1057
	e1059
	e1061
	e1063
	e1065
	e1067
	e1069
	e1071
	e1091
	e1094
	e1097
	e1100
	e1103
	e1105
	e1107
	e1109
	e1111
	e1113
	e1115
	e1118
	e1121
	e1124
	e1140
	e1143
	e1146
	e1149
	e33
	e34
	e35
	e30
	e31
	e32
	e27
	e28
	e29
	e127
	e128
	e129
	e130
	e131
	e132
	e133
	e134
	e135
	e136
	e137
	e138
	e109
	e110
	e105
	e106
	e107
	e108
	e111
	e112
	e113
	e114
	e96
	e97
	e98
	e93
	e94
	e95
	e1044
	e1047
	e1050
	e1053
	e1073
	e1075
	e1077
	e1079
	e1081
	e102
	e103
	e104
	e99
	e100
	e101
 - edge 
 
	t0
	t1
	t2
	t3
	t4
	t5
	t6
	t7
	t8
	t9
	t10
	t11
	t12
	t13
	t14
	t15
	t16
	t17
	t18
	t19
	t20
	t21
	t22
	t23
	t24
	t25
	t26
	t27
	t28
	t29
	t30
	t1000
	t1001
	t1002
	t1003
	t1004
	t1005
	t1006
	t1007
	t1008
	t1009
	t1010
	t1011
	t1012
	t1013
	t1014
	t1015
	t1016
	t1017
	t1018
	t1019
	t1020
	t1021
	t1022
	t1023
	t1024
	t1025
	t1026
	t1027
	t1028
	t1029
	t1030
 - trajectory 
 )
(:init
(= (confb rob ) cb0 )
(= (confa rob ) ca0 )
(= (traj rob ) t_init )
(= (holding) no_object)
; initial object configurations 
;Only one table has objects

(= (confo o1) co99); -x 0.672384 -y 0.186419 -z 0.690017
(= (confo o2) co153); -x 0.805143 -y 0.199383 -z 0.69002
(= (confo o3) co147); -x 0.865962 -y -0.170247 -z 0.689998
(= (confo o4) co140); -x 0.615173 -y 0.256612 -z 0.690019
(= (confo o5) co27); -x 0.749706 -y 0.344138 -z 0.690001
(= (confo o6) co68); -x 0.83577 -y -0.0876583 -z 0.690017
(= (confo o7) co89); -x 0.874016 -y 0.370299 -z 0.689992
(= (confo o8) co58); -x 1.01593 -y 0.275224 -z 0.690007
(= (confo o9) co3); -x 0.899649 -y -0.439098 -z 0.690024
(= (confo o10) co156); -x 0.679241 -y -0.0157785 -z 0.690003
(= (confo o11) co75); -x 0.804539 -y -0.402963 -z 0.690023
(= (confo o12) co5); -x 0.617793 -y 0.355057 -z 0.689999
(= (confo o13) co139); -x 0.641108 -y -0.160385 -z 0.690003
(= (confo o14) co120); -x 0.654017 -y 0.111514 -z 0.690002
(= (confo o15) co8); -x 0.654678 -y -0.402679 -z 0.690022
(= (confo o16) co92); -x 1.02768 -y -0.186386 -z 0.690011
(= (confo o17) co73); -x 0.951782 -y -0.053241 -z 0.690011
(= (confo o18) co38); -x 0.971534 -y -0.353545 -z 0.689996
(= (confo o19) co7); -x 1.08108 -y 0.0217289 -z 0.69001
(= (confo o20) co119); -x 0.602886 -y -0.304903 -z 0.690023
(= (confo o21) co135); -x 1.08484 -y -0.256563 -z 0.689991
(= (confo o22) co70); -x 0.902552 -y 0.0953657 -z 0.69001
(= (confo o23) co87); -x 0.883546 -y -0.282583 -z 0.689997
(= (confo o24) co4); -x 1.07859 -y -0.360332 -z 0.690012
(= (confo o25) co133); -x 1.05903 -y 0.160456 -z 0.690011





)
(:goal (and
;Move 10 obstructed objects to another table

(= (confo o6) co34); -x -1.35331 -y -2.12162 -z 0.690021
(= (confo o2) co21); -x -1.34059 -y -1.9272 -z 0.690022
(= (confo o29) co65); -x -0.596955 -y -2.19538 -z 0.689984
(= (confo o26) co76); -x -0.887677 -y -2.02374 -z 0.690024
(= (confo o17) co9); -x -0.638936 -y -1.78834 -z 0.689999
(= (confo o11) co61); -x -1.08656 -y -1.98302 -z 0.690004
(= (confo o28) co138); -x -0.79066 -y -1.83955 -z 0.690022
(= (confo o20) co127); -x -0.839671 -y -2.20894 -z 0.690023
(= (confo o4) co94); -x -0.737356 -y -1.97022 -z 0.69002
(= (confo o9) co45); -x -0.655792 -y -2.04991 -z 0.690003


))
(:constraints

	(@nonoverlap_ro (confb rob) (traj rob) (confo o1))
(@nonoverlap_ro (confb rob) (traj rob) (confo o2))
(@nonoverlap_ro (confb rob) (traj rob) (confo o3))
(@nonoverlap_ro (confb rob) (traj rob) (confo o4))
(@nonoverlap_ro (confb rob) (traj rob) (confo o5))
(@nonoverlap_ro (confb rob) (traj rob) (confo o6))
(@nonoverlap_ro (confb rob) (traj rob) (confo o7))
(@nonoverlap_ro (confb rob) (traj rob) (confo o8))
(@nonoverlap_ro (confb rob) (traj rob) (confo o9))
(@nonoverlap_ro (confb rob) (traj rob) (confo o10))
(@nonoverlap_ro (confb rob) (traj rob) (confo o11))
(@nonoverlap_ro (confb rob) (traj rob) (confo o12))
(@nonoverlap_ro (confb rob) (traj rob) (confo o13))
(@nonoverlap_ro (confb rob) (traj rob) (confo o14))
(@nonoverlap_ro (confb rob) (traj rob) (confo o15))
(@nonoverlap_ro (confb rob) (traj rob) (confo o16))
(@nonoverlap_ro (confb rob) (traj rob) (confo o17))
(@nonoverlap_ro (confb rob) (traj rob) (confo o18))
(@nonoverlap_ro (confb rob) (traj rob) (confo o19))
(@nonoverlap_ro (confb rob) (traj rob) (confo o20))
(@nonoverlap_ro (confb rob) (traj rob) (confo o21))
(@nonoverlap_ro (confb rob) (traj rob) (confo o22))
(@nonoverlap_ro (confb rob) (traj rob) (confo o23))
(@nonoverlap_ro (confb rob) (traj rob) (confo o24))
(@nonoverlap_ro (confb rob) (traj rob) (confo o25))
(@nonoverlap_oo (confo o1) (confo o2))
(@nonoverlap_oo (confo o1) (confo o3))
(@nonoverlap_oo (confo o1) (confo o4))
(@nonoverlap_oo (confo o1) (confo o5))
(@nonoverlap_oo (confo o1) (confo o6))
(@nonoverlap_oo (confo o1) (confo o7))
(@nonoverlap_oo (confo o1) (confo o8))
(@nonoverlap_oo (confo o1) (confo o9))
(@nonoverlap_oo (confo o1) (confo o10))
(@nonoverlap_oo (confo o1) (confo o11))
(@nonoverlap_oo (confo o1) (confo o12))
(@nonoverlap_oo (confo o1) (confo o13))
(@nonoverlap_oo (confo o1) (confo o14))
(@nonoverlap_oo (confo o1) (confo o15))
(@nonoverlap_oo (confo o1) (confo o16))
(@nonoverlap_oo (confo o1) (confo o17))
(@nonoverlap_oo (confo o1) (confo o18))
(@nonoverlap_oo (confo o1) (confo o19))
(@nonoverlap_oo (confo o1) (confo o20))
(@nonoverlap_oo (confo o1) (confo o21))
(@nonoverlap_oo (confo o1) (confo o22))
(@nonoverlap_oo (confo o1) (confo o23))
(@nonoverlap_oo (confo o1) (confo o24))
(@nonoverlap_oo (confo o1) (confo o25))
(@nonoverlap_oo (confo o2) (confo o3))
(@nonoverlap_oo (confo o2) (confo o4))
(@nonoverlap_oo (confo o2) (confo o5))
(@nonoverlap_oo (confo o2) (confo o6))
(@nonoverlap_oo (confo o2) (confo o7))
(@nonoverlap_oo (confo o2) (confo o8))
(@nonoverlap_oo (confo o2) (confo o9))
(@nonoverlap_oo (confo o2) (confo o10))
(@nonoverlap_oo (confo o2) (confo o11))
(@nonoverlap_oo (confo o2) (confo o12))
(@nonoverlap_oo (confo o2) (confo o13))
(@nonoverlap_oo (confo o2) (confo o14))
(@nonoverlap_oo (confo o2) (confo o15))
(@nonoverlap_oo (confo o2) (confo o16))
(@nonoverlap_oo (confo o2) (confo o17))
(@nonoverlap_oo (confo o2) (confo o18))
(@nonoverlap_oo (confo o2) (confo o19))
(@nonoverlap_oo (confo o2) (confo o20))
(@nonoverlap_oo (confo o2) (confo o21))
(@nonoverlap_oo (confo o2) (confo o22))
(@nonoverlap_oo (confo o2) (confo o23))
(@nonoverlap_oo (confo o2) (confo o24))
(@nonoverlap_oo (confo o2) (confo o25))
(@nonoverlap_oo (confo o3) (confo o4))
(@nonoverlap_oo (confo o3) (confo o5))
(@nonoverlap_oo (confo o3) (confo o6))
(@nonoverlap_oo (confo o3) (confo o7))
(@nonoverlap_oo (confo o3) (confo o8))
(@nonoverlap_oo (confo o3) (confo o9))
(@nonoverlap_oo (confo o3) (confo o10))
(@nonoverlap_oo (confo o3) (confo o11))
(@nonoverlap_oo (confo o3) (confo o12))
(@nonoverlap_oo (confo o3) (confo o13))
(@nonoverlap_oo (confo o3) (confo o14))
(@nonoverlap_oo (confo o3) (confo o15))
(@nonoverlap_oo (confo o3) (confo o16))
(@nonoverlap_oo (confo o3) (confo o17))
(@nonoverlap_oo (confo o3) (confo o18))
(@nonoverlap_oo (confo o3) (confo o19))
(@nonoverlap_oo (confo o3) (confo o20))
(@nonoverlap_oo (confo o3) (confo o21))
(@nonoverlap_oo (confo o3) (confo o22))
(@nonoverlap_oo (confo o3) (confo o23))
(@nonoverlap_oo (confo o3) (confo o24))
(@nonoverlap_oo (confo o3) (confo o25))
(@nonoverlap_oo (confo o4) (confo o5))
(@nonoverlap_oo (confo o4) (confo o6))
(@nonoverlap_oo (confo o4) (confo o7))
(@nonoverlap_oo (confo o4) (confo o8))
(@nonoverlap_oo (confo o4) (confo o9))
(@nonoverlap_oo (confo o4) (confo o10))
(@nonoverlap_oo (confo o4) (confo o11))
(@nonoverlap_oo (confo o4) (confo o12))
(@nonoverlap_oo (confo o4) (confo o13))
(@nonoverlap_oo (confo o4) (confo o14))
(@nonoverlap_oo (confo o4) (confo o15))
(@nonoverlap_oo (confo o4) (confo o16))
(@nonoverlap_oo (confo o4) (confo o17))
(@nonoverlap_oo (confo o4) (confo o18))
(@nonoverlap_oo (confo o4) (confo o19))
(@nonoverlap_oo (confo o4) (confo o20))
(@nonoverlap_oo (confo o4) (confo o21))
(@nonoverlap_oo (confo o4) (confo o22))
(@nonoverlap_oo (confo o4) (confo o23))
(@nonoverlap_oo (confo o4) (confo o24))
(@nonoverlap_oo (confo o4) (confo o25))
(@nonoverlap_oo (confo o5) (confo o6))
(@nonoverlap_oo (confo o5) (confo o7))
(@nonoverlap_oo (confo o5) (confo o8))
(@nonoverlap_oo (confo o5) (confo o9))
(@nonoverlap_oo (confo o5) (confo o10))
(@nonoverlap_oo (confo o5) (confo o11))
(@nonoverlap_oo (confo o5) (confo o12))
(@nonoverlap_oo (confo o5) (confo o13))
(@nonoverlap_oo (confo o5) (confo o14))
(@nonoverlap_oo (confo o5) (confo o15))
(@nonoverlap_oo (confo o5) (confo o16))
(@nonoverlap_oo (confo o5) (confo o17))
(@nonoverlap_oo (confo o5) (confo o18))
(@nonoverlap_oo (confo o5) (confo o19))
(@nonoverlap_oo (confo o5) (confo o20))
(@nonoverlap_oo (confo o5) (confo o21))
(@nonoverlap_oo (confo o5) (confo o22))
(@nonoverlap_oo (confo o5) (confo o23))
(@nonoverlap_oo (confo o5) (confo o24))
(@nonoverlap_oo (confo o5) (confo o25))
(@nonoverlap_oo (confo o6) (confo o7))
(@nonoverlap_oo (confo o6) (confo o8))
(@nonoverlap_oo (confo o6) (confo o9))
(@nonoverlap_oo (confo o6) (confo o10))
(@nonoverlap_oo (confo o6) (confo o11))
(@nonoverlap_oo (confo o6) (confo o12))
(@nonoverlap_oo (confo o6) (confo o13))
(@nonoverlap_oo (confo o6) (confo o14))
(@nonoverlap_oo (confo o6) (confo o15))
(@nonoverlap_oo (confo o6) (confo o16))
(@nonoverlap_oo (confo o6) (confo o17))
(@nonoverlap_oo (confo o6) (confo o18))
(@nonoverlap_oo (confo o6) (confo o19))
(@nonoverlap_oo (confo o6) (confo o20))
(@nonoverlap_oo (confo o6) (confo o21))
(@nonoverlap_oo (confo o6) (confo o22))
(@nonoverlap_oo (confo o6) (confo o23))
(@nonoverlap_oo (confo o6) (confo o24))
(@nonoverlap_oo (confo o6) (confo o25))
(@nonoverlap_oo (confo o7) (confo o8))
(@nonoverlap_oo (confo o7) (confo o9))
(@nonoverlap_oo (confo o7) (confo o10))
(@nonoverlap_oo (confo o7) (confo o11))
(@nonoverlap_oo (confo o7) (confo o12))
(@nonoverlap_oo (confo o7) (confo o13))
(@nonoverlap_oo (confo o7) (confo o14))
(@nonoverlap_oo (confo o7) (confo o15))
(@nonoverlap_oo (confo o7) (confo o16))
(@nonoverlap_oo (confo o7) (confo o17))
(@nonoverlap_oo (confo o7) (confo o18))
(@nonoverlap_oo (confo o7) (confo o19))
(@nonoverlap_oo (confo o7) (confo o20))
(@nonoverlap_oo (confo o7) (confo o21))
(@nonoverlap_oo (confo o7) (confo o22))
(@nonoverlap_oo (confo o7) (confo o23))
(@nonoverlap_oo (confo o7) (confo o24))
(@nonoverlap_oo (confo o7) (confo o25))
(@nonoverlap_oo (confo o8) (confo o9))
(@nonoverlap_oo (confo o8) (confo o10))
(@nonoverlap_oo (confo o8) (confo o11))
(@nonoverlap_oo (confo o8) (confo o12))
(@nonoverlap_oo (confo o8) (confo o13))
(@nonoverlap_oo (confo o8) (confo o14))
(@nonoverlap_oo (confo o8) (confo o15))
(@nonoverlap_oo (confo o8) (confo o16))
(@nonoverlap_oo (confo o8) (confo o17))
(@nonoverlap_oo (confo o8) (confo o18))
(@nonoverlap_oo (confo o8) (confo o19))
(@nonoverlap_oo (confo o8) (confo o20))
(@nonoverlap_oo (confo o8) (confo o21))
(@nonoverlap_oo (confo o8) (confo o22))
(@nonoverlap_oo (confo o8) (confo o23))
(@nonoverlap_oo (confo o8) (confo o24))
(@nonoverlap_oo (confo o8) (confo o25))
(@nonoverlap_oo (confo o9) (confo o10))
(@nonoverlap_oo (confo o9) (confo o11))
(@nonoverlap_oo (confo o9) (confo o12))
(@nonoverlap_oo (confo o9) (confo o13))
(@nonoverlap_oo (confo o9) (confo o14))
(@nonoverlap_oo (confo o9) (confo o15))
(@nonoverlap_oo (confo o9) (confo o16))
(@nonoverlap_oo (confo o9) (confo o17))
(@nonoverlap_oo (confo o9) (confo o18))
(@nonoverlap_oo (confo o9) (confo o19))
(@nonoverlap_oo (confo o9) (confo o20))
(@nonoverlap_oo (confo o9) (confo o21))
(@nonoverlap_oo (confo o9) (confo o22))
(@nonoverlap_oo (confo o9) (confo o23))
(@nonoverlap_oo (confo o9) (confo o24))
(@nonoverlap_oo (confo o9) (confo o25))
(@nonoverlap_oo (confo o10) (confo o11))
(@nonoverlap_oo (confo o10) (confo o12))
(@nonoverlap_oo (confo o10) (confo o13))
(@nonoverlap_oo (confo o10) (confo o14))
(@nonoverlap_oo (confo o10) (confo o15))
(@nonoverlap_oo (confo o10) (confo o16))
(@nonoverlap_oo (confo o10) (confo o17))
(@nonoverlap_oo (confo o10) (confo o18))
(@nonoverlap_oo (confo o10) (confo o19))
(@nonoverlap_oo (confo o10) (confo o20))
(@nonoverlap_oo (confo o10) (confo o21))
(@nonoverlap_oo (confo o10) (confo o22))
(@nonoverlap_oo (confo o10) (confo o23))
(@nonoverlap_oo (confo o10) (confo o24))
(@nonoverlap_oo (confo o10) (confo o25))
(@nonoverlap_oo (confo o11) (confo o12))
(@nonoverlap_oo (confo o11) (confo o13))
(@nonoverlap_oo (confo o11) (confo o14))
(@nonoverlap_oo (confo o11) (confo o15))
(@nonoverlap_oo (confo o11) (confo o16))
(@nonoverlap_oo (confo o11) (confo o17))
(@nonoverlap_oo (confo o11) (confo o18))
(@nonoverlap_oo (confo o11) (confo o19))
(@nonoverlap_oo (confo o11) (confo o20))
(@nonoverlap_oo (confo o11) (confo o21))
(@nonoverlap_oo (confo o11) (confo o22))
(@nonoverlap_oo (confo o11) (confo o23))
(@nonoverlap_oo (confo o11) (confo o24))
(@nonoverlap_oo (confo o11) (confo o25))
(@nonoverlap_oo (confo o12) (confo o13))
(@nonoverlap_oo (confo o12) (confo o14))
(@nonoverlap_oo (confo o12) (confo o15))
(@nonoverlap_oo (confo o12) (confo o16))
(@nonoverlap_oo (confo o12) (confo o17))
(@nonoverlap_oo (confo o12) (confo o18))
(@nonoverlap_oo (confo o12) (confo o19))
(@nonoverlap_oo (confo o12) (confo o20))
(@nonoverlap_oo (confo o12) (confo o21))
(@nonoverlap_oo (confo o12) (confo o22))
(@nonoverlap_oo (confo o12) (confo o23))
(@nonoverlap_oo (confo o12) (confo o24))
(@nonoverlap_oo (confo o12) (confo o25))
(@nonoverlap_oo (confo o13) (confo o14))
(@nonoverlap_oo (confo o13) (confo o15))
(@nonoverlap_oo (confo o13) (confo o16))
(@nonoverlap_oo (confo o13) (confo o17))
(@nonoverlap_oo (confo o13) (confo o18))
(@nonoverlap_oo (confo o13) (confo o19))
(@nonoverlap_oo (confo o13) (confo o20))
(@nonoverlap_oo (confo o13) (confo o21))
(@nonoverlap_oo (confo o13) (confo o22))
(@nonoverlap_oo (confo o13) (confo o23))
(@nonoverlap_oo (confo o13) (confo o24))
(@nonoverlap_oo (confo o13) (confo o25))
(@nonoverlap_oo (confo o14) (confo o15))
(@nonoverlap_oo (confo o14) (confo o16))
(@nonoverlap_oo (confo o14) (confo o17))
(@nonoverlap_oo (confo o14) (confo o18))
(@nonoverlap_oo (confo o14) (confo o19))
(@nonoverlap_oo (confo o14) (confo o20))
(@nonoverlap_oo (confo o14) (confo o21))
(@nonoverlap_oo (confo o14) (confo o22))
(@nonoverlap_oo (confo o14) (confo o23))
(@nonoverlap_oo (confo o14) (confo o24))
(@nonoverlap_oo (confo o14) (confo o25))
(@nonoverlap_oo (confo o15) (confo o16))
(@nonoverlap_oo (confo o15) (confo o17))
(@nonoverlap_oo (confo o15) (confo o18))
(@nonoverlap_oo (confo o15) (confo o19))
(@nonoverlap_oo (confo o15) (confo o20))
(@nonoverlap_oo (confo o15) (confo o21))
(@nonoverlap_oo (confo o15) (confo o22))
(@nonoverlap_oo (confo o15) (confo o23))
(@nonoverlap_oo (confo o15) (confo o24))
(@nonoverlap_oo (confo o15) (confo o25))
(@nonoverlap_oo (confo o16) (confo o17))
(@nonoverlap_oo (confo o16) (confo o18))
(@nonoverlap_oo (confo o16) (confo o19))
(@nonoverlap_oo (confo o16) (confo o20))
(@nonoverlap_oo (confo o16) (confo o21))
(@nonoverlap_oo (confo o16) (confo o22))
(@nonoverlap_oo (confo o16) (confo o23))
(@nonoverlap_oo (confo o16) (confo o24))
(@nonoverlap_oo (confo o16) (confo o25))
(@nonoverlap_oo (confo o17) (confo o18))
(@nonoverlap_oo (confo o17) (confo o19))
(@nonoverlap_oo (confo o17) (confo o20))
(@nonoverlap_oo (confo o17) (confo o21))
(@nonoverlap_oo (confo o17) (confo o22))
(@nonoverlap_oo (confo o17) (confo o23))
(@nonoverlap_oo (confo o17) (confo o24))
(@nonoverlap_oo (confo o17) (confo o25))
(@nonoverlap_oo (confo o18) (confo o19))
(@nonoverlap_oo (confo o18) (confo o20))
(@nonoverlap_oo (confo o18) (confo o21))
(@nonoverlap_oo (confo o18) (confo o22))
(@nonoverlap_oo (confo o18) (confo o23))
(@nonoverlap_oo (confo o18) (confo o24))
(@nonoverlap_oo (confo o18) (confo o25))
(@nonoverlap_oo (confo o19) (confo o20))
(@nonoverlap_oo (confo o19) (confo o21))
(@nonoverlap_oo (confo o19) (confo o22))
(@nonoverlap_oo (confo o19) (confo o23))
(@nonoverlap_oo (confo o19) (confo o24))
(@nonoverlap_oo (confo o19) (confo o25))
(@nonoverlap_oo (confo o20) (confo o21))
(@nonoverlap_oo (confo o20) (confo o22))
(@nonoverlap_oo (confo o20) (confo o23))
(@nonoverlap_oo (confo o20) (confo o24))
(@nonoverlap_oo (confo o20) (confo o25))
(@nonoverlap_oo (confo o21) (confo o22))
(@nonoverlap_oo (confo o21) (confo o23))
(@nonoverlap_oo (confo o21) (confo o24))
(@nonoverlap_oo (confo o21) (confo o25))
(@nonoverlap_oo (confo o22) (confo o23))
(@nonoverlap_oo (confo o22) (confo o24))
(@nonoverlap_oo (confo o22) (confo o25))
(@nonoverlap_oo (confo o23) (confo o24))
(@nonoverlap_oo (confo o23) (confo o25))
(@nonoverlap_oo (confo o24) (confo o25))

)
)
