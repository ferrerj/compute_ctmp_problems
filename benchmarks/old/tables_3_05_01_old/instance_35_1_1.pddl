(define (problem sample)
	(:domain fn-robot-navigation)
	(:objects 

		o1 o2 o3 o4 o5 o6 o7 o8 o9 o10 o11 o12 o13 o14 o15 o16 o17 o18 o19 o20 o21 o22 o23 o24 o25 o26 o27 o28 o29 o30 o31 o32 o33 o34 o35

 - object_id

	cb120
	cb119
	cb118
	cb122
	cb121
	cb94
	cb91
	cb93
	cb92
	cb99
	cb98
	cb97
	cb96
	cb95
	cb103
	cb104
	cb105
	cb100
	cb101
	cb102
	cb108
	cb109
	cb106
	cb107
	cb116
	cb115
	cb114
	cb113
	cb112
	cb110
	cb111
	cb117
	cb90
	cb89
	cb88
	cb87
	cb86
	cb66
	cb68
	cb67
	cb70
	cb69
	cb65
	cb63
	cb64
	cb61
	cb62
	cb81
	cb82
	cb83
	cb84
	cb85
	cb78
	cb79
	cb76
	cb77
	cb75
	cb74
	cb73
	cb80
	cb72
	cb71
	cb157
	cb158
	cb159
	cb155
	cb156
	cb150
	cb151
	cb152
	cb154
	cb153
	cb131
	cb132
	cb128
	cb129
	cb130
	cb123
	cb124
	cb125
	cb126
	cb127
	cb133
	cb134
	cb135
	cb136
	cb273
	cb274
	cb275
	cb271
	cb272
	cb161
	cb160
	cb162
	cb163
	cb284
	cb281
	cb282
	cb283
	cb278
	cb277
	cb276
	cb280
	cb279
	cb269
	cb270
	cb268
	cb267
	cb250
	cb252
	cb251
	cb249
	cb253
	cb248
	cb246
	cb247
	cb245
	cb244
	cb264
	cb263
	cb266
	cb265
	cb259
	cb260
	cb258
	cb261
	cb262
	cb255
	cb254
	cb256
	cb257
	cb141
	cb140
	cb139
	cb138
	cb137
	cb148
	cb149
	cb144
	cb143
	cb145
	cb142
	cb146
	cb147
	cb164
	cb165
	cb166
	cb167
	cb179
	cb181
	cb180
	cb177
	cb178
	cb183
	cb184
	cb185
	cb182
	cb189
	cb187
	cb188
	cb186
	cb190
	cb302
	cb299
	cb300
	cb301
	cb298
	cb176
	cb174
	cb175
	cb172
	cb173
	cb289
	cb292
	cb290
	cb291
	cb287
	cb286
	cb285
	cb288
	cb168
	cb170
	cb169
	cb171
	cb294
	cb295
	cb293
	cb296
	cb297
	cb50
	cb48
	cb49
	cb52
	cb51
	cb55
	cb53
	cb54
	cb57
	cb56
	cb59
	cb60
	cb58
	cb47
	cb46
	cb45
	cb44
	cb43
	cb42
	cb362
	cb363
	cb365
	cb364
	cb366
	cb359
	cb358
	cb357
	cb356
	cb360
	cb339
	cb340
	cb341
	cb343
	cb342
	cb338
	cb361
	cb350
	cb351
	cb352
	cb353
	cb354
	cb355
	cb348
	cb349
	cb347
	cb345
	cb346
	cb344
	cb242
	cb241
	cb240
	cb239
	cb238
	cb243
	cb0
	cb305
	cb306
	cb303
	cb304
	cb318
	cb317
	cb319
	cb320
	cb321
	cb316
	cb312
	cb313
	cb314
	cb315
	cb327
	cb326
	cb325
	cb324
	cb310
	cb311
	cb308
	cb307
	cb309
	cb5
	cb3
	cb4
	cb2
	cb1
	cb15
	cb16
	cb17
	cb19
	cb18
	cb22
	cb21
	cb23
	cb20
	cb14
	cb12
	cb13
	cb11
	cb8
	cb7
	cb6
	cb10
	cb9
	cb38
	cb41
	cb40
	cb39
	cb34
	cb37
	cb36
	cb35
	cb29
	cb30
	cb31
	cb32
	cb33
	cb24
	cb26
	cb25
	cb28
	cb27
	cb237
	cb236
	cb234
	cb233
	cb232
	cb235
	cb218
	cb217
	cb219
	cb220
	cb230
	cb229
	cb228
	cb227
	cb226
	cb225
	cb222
	cb223
	cb224
	cb231
	cb221
	cb323
	cb322
	cb330
	cb328
	cb329
	cb332
	cb331
	cb333
	cb334
	cb335
	cb336
	cb337
	cb212
	cb213
	cb214
	cb215
	cb216
	cb195
	cb198
	cb199
	cb196
	cb197
	cb200
	cb191
	cb192
	cb193
	cb194
	cb205
	cb204
	cb203
	cb206
	cb202
	cb210
	cb209
	cb211
	cb207
	cb208
	cb201
	 - conf_base
	co201
	co305
	co53
	co302
	co147
	co384
	co164
	co3
	co181
	co313
	co23
	co352
	co2
	co145
	co355
	co154
	co329
	co38
	co310
	co14
	co295
	co24
	co330
	co326
	co102
	co180
	co149
	co6
	co103
	co146
	co25
	co300
	co331
	co110
	co125
	co278
	co263
	co140
	co386
	co55
	co380
	co351
	co200
	co250
	co86
	co289
	co117
	co276
	co126
	co138
	co285
	co106
	co107
	co97
	co290
	co353
	co91
	co92
	co228
	co80
	co241
	co123
	co45
	co64
	co70
	co357
	co95
	co390
	co101
	co131
	co127
	co57
	co274
	co312
	co183
	co198
	co134
	co163
	co277
	co398
	co166
	co391
	co153
	co10
	co35
	co363
	co71
	co119
	co72
	co349
	co188
	co54
	co22
	co40
	co176
	co255
	co296
	co252
	co303
	co203
	co16
	co158
	co37
	co11
	co319
	co381
	co337
	co156
	co297
	co206
	co320
	co378
	co342
	co29
	co170
	co229
	co371
	co370
	co273
	co133
	co214
	co178
	co328
	co365
	co327
	co397
	co177
	co34
	co333
	co369
	co341
	co261
	co392
	co340
	co356
	co251
	co162
	co315
	co118
	co395
	co227
	co52
	co339
	co311
	co21
	co165
	co314
	co4
	co179
	co36
	co332
	co182
	co39
	co13
	co205
	co298
	co56
	co304
	co155
	co299
	co12
	co73
	co233
	co325
	co167
	co144
	co208
	co150
	co388
	co26
	co307
	co321
	co269
	co9
	co224
	co194
	co223
	co160
	co173
	co18
	co267
	co248
	co246
	co343
	co324
	co172
	co234
	co211
	co259
	co306
	co358
	co309
	co226
	co58
	co129
	co30
	co33
	co20
	co151
	co104
	co193
	co195
	co359
	co197
	co347
	co130
	co210
	co334
	co186
	co291
	co152
	co60
	co336
	co318
	co374
	co375
	co360
	co345
	co389
	co308
	co323
	co59
	co15
	co293
	co0
	co292
	co42
	co294
	co316
	co187
	co143
	co169
	co157
	co17
	co184
	co74
	co348
	co387
	co48
	co87
	co322
	co344
	co161
	co135
	co257
	co225
	co50
	co372
	co222
	co209
	co114
	co171
	co41
	co232
	co258
	co373
	co1
	co376
	co247
	co7
	co175
	co159
	co346
	co377
	co270
	co271
	co75
	co185
	co148
	co5
	co317
	co115
	co100
	co128
	co47
	co88
	co113
	co84
	co69
	co245
	co31
	co192
	co68
	co19
	co8
	co174
	co49
	co120
	co32
	co83
	co67
	co288
	co268
	co99
	co85
	co196
	co382
	co90
	co213
	co260
	co362
	co124
	co93
	co283
	co122
	co350
	co282
	co190
	co51
	co137
	co396
	co279
	co199
	co242
	co132
	co367
	co240
	co287
	co204
	co191
	co366
	co249
	co231
	co354
	co219
	co78
	co281
	co77
	co139
	co237
	co235
	co215
	co79
	co284
	co98
	co109
	co216
	co364
	co217
	co239
	co230
	co262
	co385
	co266
	co142
	co361
	co286
	co82
	co218
	co66
	co254
	co112
	co116
	co46
	co202
	co141
	co265
	co108
	co62
	co238
	co63
	co256
	co111
	co264
	co94
	co65
	co207
	co383
	co272
	co81
	co280
	co236
	co243
	co244
	co338
	co44
	co189
	co253
	co394
	co220
	co221
	co368
	co393
	co275
	co212
	co96
	co379
	co28
	co168
	co301
	co335
	co43
	co76
	co61
	co121
	co27
	co89
	co105
	co136
 - conf_obj 
 
	ca13
	ca6
	ca18
	ca19
	ca3
	ca2
	ca10
	ca8
	ca7
	ca12
	ca14
	ca11
	ca15
	ca16
	ca17
	ca25
	ca23
	ca24
	ca20
	ca21
	ca22
	ca1
	ca9
	ca5
	ca4
 - conf_arm 
 
	e833
	e834
	e835
	e931
	e932
	e933
	e928
	e929
	e930
	e922
	e923
	e924
	e925
	e926
	e927
	e919
	e920
	e921
	e979
	e980
	e981
	e976
	e977
	e978
	e973
	e974
	e975
	e985
	e986
	e987
	e982
	e983
	e984
	e970
	e971
	e972
	e964
	e965
	e966
	e967
	e968
	e969
	e961
	e962
	e963
	e934
	e935
	e936
	e937
	e938
	e939
	e940
	e941
	e942
	e946
	e947
	e948
	e943
	e944
	e945
	e958
	e959
	e960
	e952
	e953
	e954
	e949
	e950
	e951
	e955
	e956
	e957
	e1027
	e1028
	e1029
	e1033
	e1034
	e1035
	e1030
	e1031
	e1032
	e1039
	e1040
	e1041
	e1036
	e1037
	e1038
	e1012
	e1013
	e1014
	e1015
	e1016
	e1017
	e1018
	e1019
	e1020
	e1021
	e1022
	e1023
	e1024
	e1025
	e1026
	e1000
	e1001
	e1002
	e1009
	e1010
	e1011
	e1006
	e1007
	e1008
	e1003
	e1004
	e1005
	e988
	e989
	e990
	e997
	e998
	e999
	e994
	e995
	e996
	e991
	e992
	e993
	e695
	e696
	e697
	e692
	e693
	e694
	e689
	e690
	e691
	e686
	e687
	e688
	e683
	e684
	e685
	e680
	e681
	e682
	e2122
	e2126
	e644
	e645
	e646
	e647
	e648
	e649
	e641
	e642
	e643
	e1809
	e1813
	e1817
	e1821
	e1825
	e1829
	e1988
	e1991
	e1994
	e1997
	e2042
	e2046
	e2050
	e2054
	e2058
	e2062
	e653
	e654
	e655
	e650
	e651
	e652
	e659
	e660
	e661
	e662
	e663
	e664
	e656
	e657
	e658
	e1680
	e1683
	e1686
	e1689
	e1692
	e1695
	e1733
	e1737
	e1741
	e1745
	e1749
	e1753
	e668
	e669
	e670
	e665
	e666
	e667
	e1785
	e1789
	e1793
	e1797
	e1801
	e1805
	e677
	e678
	e679
	e671
	e672
	e673
	e674
	e675
	e676
	e164
	e165
	e166
	e167
	e168
	e169
	e158
	e159
	e160
	e2099
	e2103
	e2107
	e2111
	e2119
	e2206
	e2209
	e2212
	e2215
	e2217
	e2221
	e2225
	e2229
	e161
	e162
	e163
	e154
	e155
	e156
	e157
	e1137
	e1141
	e1145
	e1149
	e1153
	e134
	e135
	e136
	e137
	e1157
	e1577
	e1580
	e1583
	e1586
	e1589
	e1616
	e1619
	e1622
	e1625
	e1641
	e1644
	e1647
	e1650
	e1653
	e1834
	e2028
	e2031
	e2034
	e2037
	e2040
	e2079
	e2083
	e2087
	e2091
	e2095
	e2115
	e2161
	e2165
	e2169
	e2173
	e2177
	e2237
	e2241
	e2245
	e2249
	e2253
	e142
	e143
	e144
	e145
	e138
	e139
	e140
	e141
	e150
	e151
	e152
	e153
	e146
	e147
	e148
	e149
	e220
	e221
	e222
	e223
	e216
	e217
	e218
	e219
	e1203
	e1207
	e1211
	e208
	e209
	e210
	e211
	e1223
	e204
	e205
	e206
	e207
	e1219
	e200
	e201
	e202
	e203
	e192
	e193
	e194
	e195
	e196
	e197
	e198
	e199
	e185
	e186
	e187
	e1215
	e188
	e189
	e190
	e191
	e212
	e213
	e214
	e215
	e1187
	e1191
	e1195
	e1199
	e170
	e171
	e172
	e173
	e174
	e175
	e176
	e177
	e178
	e179
	e180
	e181
	e182
	e183
	e184
	e131
	e132
	e133
	e128
	e129
	e130
	e125
	e126
	e127
	e122
	e123
	e124
	e119
	e120
	e121
	e1604
	e1607
	e1610
	e1613
	e1656
	e1659
	e1662
	e1665
	e1668
	e1671
	e1674
	e1677
	e1714
	e1718
	e1722
	e1726
	e1730
	e1758
	e1762
	e1766
	e1770
	e1774
	e1778
	e1782
	e1838
	e1841
	e1844
	e1847
	e1849
	e1853
	e1857
	e1861
	e1865
	e1869
	e1873
	e1877
	e1881
	e1885
	e2001
	e2004
	e2007
	e2010
	e2013
	e2016
	e2019
	e2022
	e2025
	e2123
	e2127
	e2147
	e2150
	e2153
	e2156
	e2159
	e18
	e19
	e20
	e24
	e25
	e26
	e21
	e22
	e23
	e15
	e16
	e17
	e1002
	e1005
	e1008
	e1011
	e1014
	e1015
	e1018
	e1021
	e1024
	e1027
	e1031
	e1035
	e1039
	e1043
	e1089
	e1093
	e1097
	e1101
	e1105
	e1117
	e1121
	e1124
	e1127
	e1130
	e1133
	e1134
	e1138
	e1142
	e1146
	e1150
	e1154
	e1158
	e1161
	e1164
	e1167
	e1172
	e1175
	e1178
	e1181
	e1184
	e1535
	e1538
	e1541
	e1544
	e1547
	e1550
	e1553
	e1556
	e1559
	e1642
	e1645
	e1648
	e1651
	e1654
	e1658
	e1661
	e1664
	e1667
	e1670
	e1673
	e1676
	e1679
	e1682
	e1685
	e1688
	e1691
	e1694
	e1697
	e1755
	e1810
	e1814
	e1818
	e1822
	e1826
	e1830
	e1921
	e1924
	e1927
	e1930
	e1933
	e1975
	e1978
	e1981
	e1984
	e1987
	e1989
	e1992
	e1995
	e1998
	e2029
	e2032
	e2035
	e2038
	e2041
	e2043
	e2047
	e2051
	e2055
	e2059
	e2063
	e2068
	e2071
	e2074
	e2077
	e2080
	e2084
	e2088
	e2092
	e2096
	e2100
	e2104
	e2108
	e2112
	e2116
	e2120
	e2162
	e2166
	e2170
	e2174
	e2178
	e2182
	e2186
	e2190
	e2194
	e2198
	e2202
	e2218
	e2222
	e2226
	e2230
	e2234
	e2238
	e2242
	e2246
	e2250
	e2254
	e2258
	e43
	e44
	e45
	e46
	e1030
	e1034
	e1038
	e1042
	e39
	e40
	e41
	e42
	e35
	e36
	e37
	e38
	e1046
	e31
	e32
	e33
	e34
	e27
	e28
	e29
	e30
	e1047
	e1051
	e1055
	e1059
	e1063
	e1067
	e1088
	e1092
	e1096
	e1100
	e1104
	e1116
	e1171
	e1174
	e1177
	e1180
	e1183
	e1200
	e1204
	e1208
	e1216
	e1220
	e1735
	e1739
	e1743
	e1747
	e1751
	e2044
	e2048
	e2052
	e2056
	e2060
	e2064
	e59
	e60
	e61
	e62
	e1050
	e63
	e64
	e65
	e66
	e1054
	e67
	e68
	e69
	e70
	e1058
	e47
	e48
	e49
	e50
	e1001
	e1004
	e1013
	e1016
	e1019
	e1022
	e1025
	e1062
	e1066
	e1070
	e1071
	e1075
	e1079
	e1083
	e1107
	e1111
	e1120
	e1123
	e1126
	e1129
	e1132
	e1159
	e1162
	e1165
	e1168
	e1185
	e1188
	e1192
	e1196
	e1212
	e1787
	e1791
	e1795
	e1799
	e1803
	e1807
	e1811
	e1815
	e1819
	e1823
	e1827
	e1831
	e51
	e52
	e53
	e54
	e55
	e56
	e57
	e58
	e79
	e80
	e81
	e82
	e83
	e84
	e85
	e86
	e71
	e72
	e73
	e74
	e1028
	e1032
	e1036
	e1040
	e1044
	e1110
	e1114
	e1135
	e1139
	e1143
	e1147
	e1151
	e1155
	e75
	e76
	e77
	e78
	e107
	e108
	e109
	e110
	e1074
	e1078
	e1082
	e1086
	e111
	e112
	e113
	e114
	e103
	e104
	e105
	e106
	e99
	e100
	e101
	e102
	e95
	e96
	e97
	e98
	e91
	e92
	e93
	e94
	e87
	e88
	e89
	e90
	e1118
	e115
	e116
	e117
	e118
	e1090
	e1094
	e1098
	e1102
	e1106
	e6
	e7
	e8
	e1010
	e3
	e4
	e5
	e0
	e1
	e2
	e1000
	e1003
	e1006
	e1009
	e1012
	e1017
	e1020
	e1023
	e1026
	e1029
	e1033
	e1037
	e1041
	e1045
	e1048
	e1052
	e1056
	e1060
	e1064
	e1068
	e1072
	e1076
	e1080
	e1084
	e1087
	e1091
	e1095
	e1099
	e1103
	e1108
	e1112
	e1115
	e1119
	e1122
	e1125
	e1128
	e1131
	e1136
	e1140
	e1144
	e1148
	e1152
	e1156
	e1160
	e1163
	e1166
	e1169
	e1170
	e1173
	e1176
	e1179
	e1182
	e1186
	e1189
	e1193
	e1197
	e1201
	e1205
	e1209
	e1213
	e1217
	e1221
	e1562
	e1565
	e1568
	e1571
	e1574
	e1643
	e1646
	e1649
	e1652
	e1655
	e1657
	e1660
	e1663
	e1666
	e1669
	e1672
	e1675
	e1678
	e1681
	e1684
	e1687
	e1690
	e1693
	e1696
	e1700
	e1703
	e1706
	e1709
	e1712
	e1715
	e1719
	e1723
	e1727
	e1731
	e1734
	e1738
	e1742
	e1746
	e1750
	e1754
	e1759
	e1763
	e1767
	e1771
	e1775
	e1779
	e1783
	e1786
	e1790
	e1794
	e1798
	e1802
	e1806
	e1835
	e1850
	e1854
	e1858
	e1862
	e1866
	e1870
	e1874
	e1878
	e1882
	e1886
	e1890
	e1894
	e1898
	e1902
	e1936
	e1939
	e1942
	e1945
	e1948
	e1951
	e1954
	e1957
	e1960
	e1963
	e1966
	e1969
	e1972
	e1990
	e1993
	e1996
	e1999
	e2002
	e2005
	e2008
	e2011
	e2014
	e2017
	e2020
	e2023
	e2026
	e2124
	e2128
	e2132
	e2135
	e2138
	e2141
	e2144
	e12
	e13
	e14
	e9
	e10
	e11
	e1007
	e269
	e270
	e271
	e1282
	e272
	e273
	e274
	e275
	e276
	e277
	e278
	e279
	e280
	e281
	e282
	e283
	e1270
	e260
	e261
	e262
	e263
	e264
	e265
	e266
	e267
	e268
	e254
	e255
	e256
	e1225
	e1228
	e1237
	e1284
	e1288
	e1292
	e1296
	e1483
	e1487
	e1491
	e1495
	e1507
	e1510
	e1513
	e1516
	e1615
	e1618
	e1621
	e1624
	e257
	e258
	e259
	e284
	e285
	e286
	e287
	e1243
	e1246
	e1252
	e1273
	e1276
	e1279
	e1316
	e1319
	e1322
	e1325
	e1389
	e1393
	e1397
	e1401
	e1405
	e1475
	e1479
	e1499
	e1503
	e1576
	e1579
	e1582
	e1585
	e1588
	e288
	e289
	e290
	e291
	e292
	e293
	e294
	e295
	e296
	e297
	e298
	e299
	e470
	e471
	e472
	e473
	e466
	e467
	e468
	e469
	e462
	e463
	e464
	e465
	e458
	e459
	e460
	e461
	e454
	e455
	e456
	e457
	e1369
	e1373
	e1377
	e1381
	e1385
	e494
	e495
	e496
	e497
	e486
	e487
	e488
	e489
	e482
	e483
	e484
	e485
	e1441
	e1445
	e1449
	e1453
	e490
	e491
	e492
	e493
	e498
	e499
	e500
	e501
	e502
	e503
	e504
	e505
	e474
	e475
	e476
	e477
	e1421
	e1425
	e1429
	e1433
	e1437
	e478
	e479
	e480
	e481
	e239
	e240
	e241
	e1226
	e1229
	e1232
	e1235
	e1238
	e1239
	e1242
	e1245
	e1249
	e1251
	e1254
	e1257
	e1260
	e1263
	e1266
	e1269
	e1272
	e1275
	e1278
	e1281
	e1317
	e1320
	e1323
	e1326
	e1455
	e1459
	e1463
	e1467
	e1471
	e1476
	e1480
	e1484
	e1488
	e1492
	e1496
	e1500
	e1504
	e1506
	e1509
	e1512
	e1515
	e1519
	e1522
	e1525
	e1528
	e1531
	e2181
	e2185
	e2189
	e2193
	e2197
	e2201
	e2205
	e2208
	e2211
	e2214
	e242
	e243
	e244
	e1248
	e245
	e246
	e247
	e251
	e252
	e253
	e248
	e249
	e250
	e1240
	e233
	e234
	e235
	e1231
	e236
	e237
	e238
	e224
	e225
	e226
	e1224
	e1227
	e1230
	e1233
	e1236
	e1241
	e1244
	e1247
	e1250
	e1253
	e1255
	e1258
	e1261
	e1264
	e1267
	e1271
	e1274
	e1277
	e1280
	e1283
	e1285
	e1289
	e1293
	e1297
	e1302
	e1305
	e1308
	e1311
	e1314
	e1315
	e1318
	e1321
	e1324
	e1368
	e1372
	e1376
	e1380
	e1384
	e1388
	e1392
	e1396
	e1400
	e1404
	e1454
	e1458
	e1462
	e1466
	e1470
	e1474
	e1478
	e1482
	e1486
	e1490
	e1494
	e1498
	e1502
	e1508
	e1511
	e1514
	e1517
	e1520
	e1523
	e1526
	e1529
	e1532
	e1534
	e1537
	e1540
	e1543
	e1546
	e1549
	e1552
	e1555
	e1558
	e1575
	e1578
	e1581
	e1584
	e1587
	e1614
	e1617
	e1620
	e1623
	e1920
	e1923
	e1926
	e1929
	e1932
	e1935
	e1938
	e1941
	e1944
	e1947
	e1962
	e1965
	e1968
	e1971
	e1974
	e1977
	e1980
	e1983
	e1986
	e2233
	e2257
	e227
	e228
	e229
	e230
	e231
	e232
	e1234
	e318
	e319
	e320
	e315
	e316
	e317
	e1256
	e1259
	e1262
	e1265
	e1268
	e1301
	e1304
	e1313
	e1387
	e1391
	e1395
	e1399
	e1403
	e1440
	e1444
	e1448
	e1452
	e1603
	e1606
	e1609
	e1612
	e321
	e322
	e323
	e324
	e325
	e326
	e506
	e507
	e508
	e1518
	e1521
	e1524
	e1527
	e1530
	e2067
	e2070
	e2073
	e2076
	e509
	e510
	e511
	e512
	e513
	e514
	e515
	e516
	e517
	e614
	e615
	e616
	e620
	e621
	e622
	e617
	e618
	e619
	e623
	e624
	e625
	e584
	e585
	e586
	e587
	e588
	e589
	e578
	e579
	e580
	e581
	e582
	e583
	e575
	e576
	e577
	e527
	e528
	e529
	e530
	e531
	e532
	e518
	e519
	e520
	e2180
	e2184
	e2188
	e2192
	e2196
	e2200
	e2204
	e2207
	e2210
	e2213
	e524
	e525
	e526
	e521
	e522
	e523
	e542
	e543
	e544
	e533
	e534
	e535
	e1049
	e1053
	e1057
	e1061
	e1065
	e1069
	e1190
	e1194
	e1198
	e1202
	e1206
	e1210
	e1214
	e1218
	e1222
	e1533
	e1536
	e1539
	e1542
	e1545
	e1548
	e1551
	e1554
	e1557
	e1919
	e1922
	e1925
	e1928
	e1931
	e1973
	e1976
	e1979
	e1982
	e1985
	e2027
	e2030
	e2033
	e2036
	e2039
	e2160
	e2164
	e2168
	e2172
	e2176
	e2216
	e2220
	e2224
	e2228
	e2236
	e2240
	e2244
	e2248
	e2252
	e536
	e537
	e538
	e539
	e540
	e541
	e554
	e555
	e556
	e548
	e549
	e550
	e551
	e552
	e553
	e545
	e546
	e547
	e1073
	e1077
	e1081
	e1085
	e1109
	e1113
	e1286
	e1290
	e1294
	e1298
	e1833
	e1961
	e1964
	e1967
	e1970
	e557
	e558
	e559
	e1204
	e1205
	e1206
	e1207
	e1208
	e1209
	e1210
	e1211
	e1212
	e1213
	e1214
	e1215
	e1180
	e1181
	e1182
	e1183
	e2187
	e2191
	e2195
	e2199
	e2203
	e1192
	e1193
	e1194
	e1195
	e1196
	e1197
	e1198
	e1199
	e1184
	e1185
	e1186
	e1187
	e2183
	e1188
	e1189
	e1190
	e1191
	e1200
	e1201
	e1202
	e1203
	e1256
	e1257
	e1258
	e1259
	e1232
	e1233
	e1234
	e1235
	e1875
	e1879
	e1883
	e1887
	e1224
	e1225
	e1226
	e1227
	e1220
	e1221
	e1222
	e1223
	e1216
	e1217
	e1218
	e1219
	e1851
	e1855
	e1859
	e1863
	e1867
	e1871
	e1228
	e1229
	e1230
	e1231
	e1248
	e1249
	e1250
	e1251
	e1252
	e1253
	e1254
	e1255
	e1240
	e1241
	e1242
	e1243
	e1236
	e1237
	e1238
	e1239
	e2125
	e2129
	e1244
	e1245
	e1246
	e1247
	e1160
	e1161
	e1162
	e1163
	e1891
	e1895
	e1899
	e1903
	e1164
	e1165
	e1166
	e1167
	e1168
	e1169
	e1170
	e1171
	e1172
	e1173
	e1174
	e1175
	e1176
	e1177
	e1178
	e1179
	e1069
	e1070
	e1071
	e1066
	e1067
	e1068
	e2066
	e2069
	e2072
	e2075
	e2098
	e2102
	e2106
	e2110
	e2118
	e1072
	e1073
	e1074
	e1075
	e1076
	e1077
	e1118
	e1119
	e1120
	e1121
	e2101
	e2105
	e2109
	e2113
	e1102
	e1103
	e1104
	e1105
	e2121
	e1106
	e1107
	e1108
	e1109
	e1110
	e1111
	e1112
	e1113
	e1098
	e1099
	e1100
	e1101
	e1094
	e1095
	e1096
	e1097
	e1090
	e1091
	e1092
	e1093
	e1086
	e1087
	e1088
	e1089
	e1082
	e1083
	e1084
	e1085
	e1078
	e1079
	e1080
	e1081
	e1756
	e1114
	e1115
	e1116
	e1117
	e1058
	e1059
	e1060
	e1061
	e1054
	e1055
	e1056
	e1057
	e1050
	e1051
	e1052
	e1053
	e1062
	e1063
	e1064
	e1065
	e1046
	e1047
	e1048
	e1049
	e1042
	e1043
	e1044
	e1045
	e1736
	e1740
	e1744
	e1748
	e1752
	e2078
	e2082
	e2086
	e2090
	e2094
	e2114
	e2232
	e2256
	e825
	e826
	e827
	e828
	e821
	e822
	e823
	e824
	e817
	e818
	e819
	e820
	e813
	e814
	e815
	e816
	e809
	e810
	e811
	e812
	e1788
	e1792
	e1796
	e1800
	e1804
	e1808
	e829
	e830
	e831
	e832
	e402
	e403
	e404
	e405
	e398
	e399
	e400
	e401
	e390
	e391
	e392
	e393
	e394
	e395
	e396
	e397
	e386
	e387
	e388
	e389
	e1287
	e1291
	e1295
	e1299
	e378
	e379
	e380
	e381
	e366
	e367
	e368
	e369
	e1340
	e1343
	e1352
	e1407
	e1410
	e1413
	e1416
	e1438
	e1442
	e1446
	e1450
	e1457
	e1461
	e1465
	e1469
	e1473
	e374
	e375
	e376
	e377
	e370
	e371
	e372
	e373
	e382
	e383
	e384
	e385
	e442
	e443
	e444
	e445
	e438
	e439
	e440
	e441
	e1485
	e1489
	e1493
	e1497
	e446
	e447
	e448
	e449
	e450
	e451
	e452
	e453
	e426
	e427
	e428
	e429
	e418
	e419
	e420
	e421
	e1328
	e1331
	e1334
	e1337
	e1355
	e1358
	e1361
	e1364
	e1477
	e1481
	e1501
	e1505
	e422
	e423
	e424
	e425
	e430
	e431
	e432
	e433
	e434
	e435
	e436
	e437
	e409
	e410
	e411
	e406
	e407
	e408
	e415
	e416
	e417
	e412
	e413
	e414
	e363
	e364
	e365
	e360
	e361
	e362
	e354
	e355
	e356
	e1591
	e1594
	e1597
	e1600
	e1627
	e1630
	e1633
	e1636
	e1639
	e357
	e358
	e359
	e309
	e310
	e311
	e1307
	e312
	e313
	e314
	e300
	e301
	e302
	e1300
	e1303
	e1306
	e1309
	e1312
	e1329
	e1332
	e1335
	e1338
	e1341
	e1344
	e1347
	e1350
	e1353
	e1356
	e1359
	e1362
	e1365
	e1366
	e1370
	e1374
	e1378
	e1382
	e1386
	e1390
	e1394
	e1398
	e1402
	e1408
	e1411
	e1414
	e1417
	e1419
	e1423
	e1427
	e1431
	e1435
	e1439
	e1443
	e1447
	e1451
	e1456
	e1460
	e1464
	e1468
	e1472
	e1561
	e1564
	e1567
	e1570
	e1573
	e1592
	e1595
	e1598
	e1601
	e1602
	e1605
	e1608
	e1611
	e1628
	e1631
	e1634
	e1637
	e1640
	e1906
	e1909
	e1912
	e1915
	e1918
	e1950
	e1953
	e1956
	e1959
	e303
	e304
	e305
	e306
	e307
	e308
	e1310
	e345
	e346
	e347
	e1349
	e342
	e343
	e344
	e339
	e340
	e341
	e1367
	e1371
	e1375
	e1379
	e1383
	e1418
	e1422
	e1426
	e1430
	e1434
	e351
	e352
	e353
	e348
	e349
	e350
	e1346
	e330
	e331
	e332
	e333
	e334
	e335
	e336
	e337
	e338
	e327
	e328
	e329
	e1327
	e1330
	e1333
	e1336
	e1339
	e1342
	e1345
	e1348
	e1351
	e1354
	e1357
	e1360
	e1363
	e1406
	e1409
	e1412
	e1415
	e1590
	e1593
	e1596
	e1599
	e1626
	e1629
	e1632
	e1635
	e1638
	e1837
	e1840
	e1843
	e1846
	e1889
	e1893
	e1897
	e1901
	e1905
	e1908
	e1911
	e1914
	e1917
	e2131
	e2134
	e2137
	e2140
	e2143
	e2146
	e2149
	e2152
	e2155
	e2158
	e608
	e609
	e610
	e605
	e606
	e607
	e602
	e603
	e604
	e611
	e612
	e613
	e590
	e591
	e592
	e1699
	e1702
	e1705
	e1708
	e1711
	e1836
	e1839
	e1842
	e1845
	e1904
	e1907
	e1910
	e1913
	e1916
	e596
	e597
	e598
	e599
	e600
	e601
	e593
	e594
	e595
	e632
	e633
	e634
	e626
	e627
	e628
	e629
	e630
	e631
	e635
	e636
	e637
	e638
	e639
	e640
	e572
	e573
	e574
	e563
	e564
	e565
	e1572
	e566
	e567
	e568
	e569
	e570
	e571
	e560
	e561
	e562
	e1420
	e1424
	e1428
	e1432
	e1436
	e1560
	e1563
	e1566
	e1569
	e1848
	e1852
	e1856
	e1860
	e1864
	e1868
	e1872
	e1876
	e1880
	e1884
	e1934
	e1937
	e1940
	e1943
	e1946
	e1949
	e1952
	e1955
	e1958
	e2000
	e2003
	e2006
	e2009
	e2012
	e2015
	e2018
	e2021
	e2024
	e842
	e843
	e844
	e845
	e846
	e847
	e836
	e837
	e838
	e839
	e840
	e841
	e913
	e914
	e915
	e910
	e911
	e912
	e916
	e917
	e918
	e904
	e905
	e906
	e907
	e908
	e909
	e872
	e873
	e874
	e875
	e2235
	e2259
	e876
	e877
	e878
	e879
	e880
	e881
	e882
	e883
	e884
	e885
	e886
	e887
	e868
	e869
	e870
	e871
	e852
	e853
	e854
	e855
	e848
	e849
	e850
	e851
	e2219
	e2223
	e2227
	e2231
	e856
	e857
	e858
	e859
	e860
	e861
	e862
	e863
	e864
	e865
	e866
	e867
	e1126
	e1127
	e1128
	e1129
	e1122
	e1123
	e1124
	e1125
	e2239
	e2243
	e2247
	e2251
	e2255
	e900
	e901
	e902
	e903
	e896
	e897
	e898
	e899
	e892
	e893
	e894
	e895
	e888
	e889
	e890
	e891
	e2163
	e2167
	e2171
	e2175
	e2179
	e1133
	e1134
	e1135
	e1136
	e1137
	e1138
	e1130
	e1131
	e1132
	e1142
	e1143
	e1144
	e1139
	e1140
	e1141
	e1145
	e1146
	e1147
	e1148
	e1149
	e1150
	e1151
	e1152
	e1153
	e1154
	e1155
	e1156
	e1157
	e1158
	e1159
	e753
	e754
	e755
	e756
	e2081
	e2085
	e2089
	e2093
	e2097
	e2117
	e733
	e734
	e735
	e736
	e2045
	e2049
	e2053
	e2057
	e2061
	e2065
	e737
	e738
	e739
	e740
	e741
	e742
	e743
	e744
	e749
	e750
	e751
	e752
	e745
	e746
	e747
	e748
	e805
	e806
	e807
	e808
	e797
	e798
	e799
	e800
	e801
	e802
	e803
	e804
	e793
	e794
	e795
	e796
	e785
	e786
	e787
	e788
	e1812
	e1816
	e1820
	e1824
	e1828
	e1832
	e789
	e790
	e791
	e792
	e761
	e762
	e763
	e764
	e765
	e766
	e767
	e768
	e1780
	e769
	e770
	e771
	e772
	e1784
	e773
	e774
	e775
	e776
	e777
	e778
	e779
	e780
	e1764
	e1768
	e1772
	e1776
	e781
	e782
	e783
	e784
	e725
	e726
	e727
	e728
	e721
	e722
	e723
	e724
	e717
	e718
	e719
	e720
	e713
	e714
	e715
	e716
	e1760
	e1761
	e1765
	e1769
	e1773
	e1777
	e1781
	e1888
	e1892
	e1896
	e1900
	e729
	e730
	e731
	e732
	e757
	e758
	e759
	e760
	e1716
	e1720
	e1724
	e1728
	e1732
	e698
	e699
	e700
	e1698
	e1701
	e1704
	e1707
	e1713
	e1717
	e1721
	e1725
	e1729
	e1757
	e2130
	e2133
	e2136
	e2139
	e2142
	e2145
	e2148
	e2151
	e2154
	e2157
	e701
	e702
	e703
	e1710
	e707
	e708
	e709
	e704
	e705
	e706
	e710
	e711
	e712
 - edge 
 
	t0
	t1
	t2
	t3
	t4
	t5
	t6
	t7
	t8
	t9
	t10
	t11
	t12
	t13
	t14
	t15
	t16
	t17
	t18
	t19
	t20
	t21
	t22
	t23
	t24
	t25
	t26
	t27
	t28
	t29
	t30
	t31
	t32
	t33
	t34
	t35
	t36
	t37
	t38
	t39
	t40
	t41
	t42
	t43
	t44
	t45
	t46
	t47
	t48
	t49
	t50
	t51
	t52
	t53
	t54
	t55
	t56
	t57
	t58
	t59
	t60
	t61
	t62
	t63
	t64
	t65
	t66
	t67
	t68
	t69
	t70
	t71
	t72
	t1000
	t1001
	t1002
	t1003
	t1004
	t1005
	t1006
	t1007
	t1008
	t1009
	t1010
	t1011
	t1012
	t1013
	t1014
	t1015
	t1016
	t1017
	t1018
	t1019
	t1020
	t1021
	t1022
	t1023
	t1024
	t1025
	t1026
	t1027
	t1028
	t1029
	t1030
	t1031
	t1032
	t1033
	t1034
	t1035
	t1036
	t1037
	t1038
	t1039
	t1040
	t1041
	t1042
	t1043
	t1044
	t1045
	t1046
	t1047
	t1048
	t1049
	t1050
	t1051
	t1052
	t1053
	t1054
	t1055
	t1056
	t1057
	t1058
	t1059
	t1061
	t1060
	t1062
	t1063
	t1064
	t1065
	t1066
	t1067
	t1068
	t1069
	t1070
	t1071
	t1072
 - trajectory 
 )
(:init
(= (confb rob ) cb0 )
(= (confa rob ) ca0 )
(= (traj rob ) t_init )
(= (holding) no_object)
; initial object configurations 
 

(= (confo o1) co335); -x 0.690508 -y 0.152749 -z 0.680001
(= (confo o2) co375); -x 0.848836 -y 0.0257876 -z 0.68
(= (confo o3) co89); -x 0.6549 -y -0.00151133 -z 0.680001
(= (confo o4) co20); -x 0.964181 -y 0.117066 -z 0.68
(= (confo o5) co330); -x -1.16831 -y 1.99798 -z 0.67999
(= (confo o6) co129); -x 0.988861 -y -0.218551 -z 0.679992
(= (confo o7) co348); -x 0.873884 -y -0.319182 -z 0.68
(= (confo o8) co162); -x -1.00027 -y -1.8543 -z 0.679991
(= (confo o9) co226); -x 0.996729 -y -0.391233 -z 0.680001
(= (confo o10) co308); -x 0.846577 -y 0.299488 -z 0.680137
(= (confo o11) co61); -x 0.655889 -y -0.201673 -z 0.679991
(= (confo o12) co363); -x -1.02897 -y 2.16076 -z 0.679991
(= (confo o13) co345); -x 0.845534 -y -0.0972838 -z 0.680001
(= (confo o14) co43); -x 0.655863 -y -0.300112 -z 0.679991
(= (confo o15) co15); -x 0.699841 -y 0.245581 -z 0.679991
(= (confo o16) co270); -x 0.893471 -y -0.193245 -z 0.679991
(= (confo o17) co75); -x 0.596292 -y 0.284276 -z 0.680019
(= (confo o18) co314); -x -1.26508 -y -2.02776 -z 0.680131
(= (confo o19) co211); -x 0.755166 -y -0.000293322 -z 0.680038
(= (confo o20) co318); -x 0.85338 -y -0.399843 -z 0.680123
(= (confo o21) co151); -x 0.945 -y 0.401404 -z 0.68
(= (confo o22) co76); -x 0.656055 -y -0.101623 -z 0.679991
(= (confo o23) co357); -x -0.67436 -y -1.84122 -z 0.68
(= (confo o24) co50); -x 1.01422 -y -0.0749103 -z 0.68
(= (confo o25) co383); -x -0.783353 -y 2.09996 -z 0.68
(= (confo o26) co344); -x 0.873958 -y 0.181914 -z 0.679993
(= (confo o27) co28); -x 0.654493 -y -0.400032 -z 0.680001
(= (confo o28) co291); -x 0.709815 -y 0.407356 -z 0.679991
(= (confo o29) co293); -x 0.846468 -y 0.401457 -z 0.680097
(= (confo o30) co169); -x 0.755074 -y -0.401755 -z 0.680001
(= (confo o31) co74); -x 0.932172 -y 0.292614 -z 0.679991
(= (confo o32) co259); -x 0.755057 -y 0.199 -z 0.680001
(= (confo o33) co187); -x 0.755398 -y -0.301978 -z 0.679991
(= (confo o34) co224); -x 0.945343 -y -0.000589446 -z 0.680003
(= (confo o35) co217); -x -0.84047 -y 1.90728 -z 0.68


 )
(:goal (and

(= (holding) o2)


))
(:constraints


(@nonoverlap_ro (confb rob) (traj rob) (confo o1))
(@nonoverlap_ro (confb rob) (traj rob) (confo o2))
(@nonoverlap_ro (confb rob) (traj rob) (confo o3))
(@nonoverlap_ro (confb rob) (traj rob) (confo o4))
(@nonoverlap_ro (confb rob) (traj rob) (confo o5))
(@nonoverlap_ro (confb rob) (traj rob) (confo o6))
(@nonoverlap_ro (confb rob) (traj rob) (confo o7))
(@nonoverlap_ro (confb rob) (traj rob) (confo o8))
(@nonoverlap_ro (confb rob) (traj rob) (confo o9))
(@nonoverlap_ro (confb rob) (traj rob) (confo o10))
(@nonoverlap_ro (confb rob) (traj rob) (confo o11))
(@nonoverlap_ro (confb rob) (traj rob) (confo o12))
(@nonoverlap_ro (confb rob) (traj rob) (confo o13))
(@nonoverlap_ro (confb rob) (traj rob) (confo o14))
(@nonoverlap_ro (confb rob) (traj rob) (confo o15))
(@nonoverlap_ro (confb rob) (traj rob) (confo o16))
(@nonoverlap_ro (confb rob) (traj rob) (confo o17))
(@nonoverlap_ro (confb rob) (traj rob) (confo o18))
(@nonoverlap_ro (confb rob) (traj rob) (confo o19))
(@nonoverlap_ro (confb rob) (traj rob) (confo o20))
(@nonoverlap_ro (confb rob) (traj rob) (confo o21))
(@nonoverlap_ro (confb rob) (traj rob) (confo o22))
(@nonoverlap_ro (confb rob) (traj rob) (confo o23))
(@nonoverlap_ro (confb rob) (traj rob) (confo o24))
(@nonoverlap_ro (confb rob) (traj rob) (confo o25))
(@nonoverlap_ro (confb rob) (traj rob) (confo o26))
(@nonoverlap_ro (confb rob) (traj rob) (confo o27))
(@nonoverlap_ro (confb rob) (traj rob) (confo o28))
(@nonoverlap_ro (confb rob) (traj rob) (confo o29))
(@nonoverlap_ro (confb rob) (traj rob) (confo o30))
(@nonoverlap_ro (confb rob) (traj rob) (confo o31))
(@nonoverlap_ro (confb rob) (traj rob) (confo o32))
(@nonoverlap_ro (confb rob) (traj rob) (confo o33))
(@nonoverlap_ro (confb rob) (traj rob) (confo o34))
(@nonoverlap_ro (confb rob) (traj rob) (confo o35))
(@nonoverlap_oo (confo o1) (confo o2))
(@nonoverlap_oo (confo o1) (confo o3))
(@nonoverlap_oo (confo o1) (confo o4))
(@nonoverlap_oo (confo o1) (confo o5))
(@nonoverlap_oo (confo o1) (confo o6))
(@nonoverlap_oo (confo o1) (confo o7))
(@nonoverlap_oo (confo o1) (confo o8))
(@nonoverlap_oo (confo o1) (confo o9))
(@nonoverlap_oo (confo o1) (confo o10))
(@nonoverlap_oo (confo o1) (confo o11))
(@nonoverlap_oo (confo o1) (confo o12))
(@nonoverlap_oo (confo o1) (confo o13))
(@nonoverlap_oo (confo o1) (confo o14))
(@nonoverlap_oo (confo o1) (confo o15))
(@nonoverlap_oo (confo o1) (confo o16))
(@nonoverlap_oo (confo o1) (confo o17))
(@nonoverlap_oo (confo o1) (confo o18))
(@nonoverlap_oo (confo o1) (confo o19))
(@nonoverlap_oo (confo o1) (confo o20))
(@nonoverlap_oo (confo o1) (confo o21))
(@nonoverlap_oo (confo o1) (confo o22))
(@nonoverlap_oo (confo o1) (confo o23))
(@nonoverlap_oo (confo o1) (confo o24))
(@nonoverlap_oo (confo o1) (confo o25))
(@nonoverlap_oo (confo o1) (confo o26))
(@nonoverlap_oo (confo o1) (confo o27))
(@nonoverlap_oo (confo o1) (confo o28))
(@nonoverlap_oo (confo o1) (confo o29))
(@nonoverlap_oo (confo o1) (confo o30))
(@nonoverlap_oo (confo o1) (confo o31))
(@nonoverlap_oo (confo o1) (confo o32))
(@nonoverlap_oo (confo o1) (confo o33))
(@nonoverlap_oo (confo o1) (confo o34))
(@nonoverlap_oo (confo o1) (confo o35))
(@nonoverlap_oo (confo o2) (confo o3))
(@nonoverlap_oo (confo o2) (confo o4))
(@nonoverlap_oo (confo o2) (confo o5))
(@nonoverlap_oo (confo o2) (confo o6))
(@nonoverlap_oo (confo o2) (confo o7))
(@nonoverlap_oo (confo o2) (confo o8))
(@nonoverlap_oo (confo o2) (confo o9))
(@nonoverlap_oo (confo o2) (confo o10))
(@nonoverlap_oo (confo o2) (confo o11))
(@nonoverlap_oo (confo o2) (confo o12))
(@nonoverlap_oo (confo o2) (confo o13))
(@nonoverlap_oo (confo o2) (confo o14))
(@nonoverlap_oo (confo o2) (confo o15))
(@nonoverlap_oo (confo o2) (confo o16))
(@nonoverlap_oo (confo o2) (confo o17))
(@nonoverlap_oo (confo o2) (confo o18))
(@nonoverlap_oo (confo o2) (confo o19))
(@nonoverlap_oo (confo o2) (confo o20))
(@nonoverlap_oo (confo o2) (confo o21))
(@nonoverlap_oo (confo o2) (confo o22))
(@nonoverlap_oo (confo o2) (confo o23))
(@nonoverlap_oo (confo o2) (confo o24))
(@nonoverlap_oo (confo o2) (confo o25))
(@nonoverlap_oo (confo o2) (confo o26))
(@nonoverlap_oo (confo o2) (confo o27))
(@nonoverlap_oo (confo o2) (confo o28))
(@nonoverlap_oo (confo o2) (confo o29))
(@nonoverlap_oo (confo o2) (confo o30))
(@nonoverlap_oo (confo o2) (confo o31))
(@nonoverlap_oo (confo o2) (confo o32))
(@nonoverlap_oo (confo o2) (confo o33))
(@nonoverlap_oo (confo o2) (confo o34))
(@nonoverlap_oo (confo o2) (confo o35))
(@nonoverlap_oo (confo o3) (confo o4))
(@nonoverlap_oo (confo o3) (confo o5))
(@nonoverlap_oo (confo o3) (confo o6))
(@nonoverlap_oo (confo o3) (confo o7))
(@nonoverlap_oo (confo o3) (confo o8))
(@nonoverlap_oo (confo o3) (confo o9))
(@nonoverlap_oo (confo o3) (confo o10))
(@nonoverlap_oo (confo o3) (confo o11))
(@nonoverlap_oo (confo o3) (confo o12))
(@nonoverlap_oo (confo o3) (confo o13))
(@nonoverlap_oo (confo o3) (confo o14))
(@nonoverlap_oo (confo o3) (confo o15))
(@nonoverlap_oo (confo o3) (confo o16))
(@nonoverlap_oo (confo o3) (confo o17))
(@nonoverlap_oo (confo o3) (confo o18))
(@nonoverlap_oo (confo o3) (confo o19))
(@nonoverlap_oo (confo o3) (confo o20))
(@nonoverlap_oo (confo o3) (confo o21))
(@nonoverlap_oo (confo o3) (confo o22))
(@nonoverlap_oo (confo o3) (confo o23))
(@nonoverlap_oo (confo o3) (confo o24))
(@nonoverlap_oo (confo o3) (confo o25))
(@nonoverlap_oo (confo o3) (confo o26))
(@nonoverlap_oo (confo o3) (confo o27))
(@nonoverlap_oo (confo o3) (confo o28))
(@nonoverlap_oo (confo o3) (confo o29))
(@nonoverlap_oo (confo o3) (confo o30))
(@nonoverlap_oo (confo o3) (confo o31))
(@nonoverlap_oo (confo o3) (confo o32))
(@nonoverlap_oo (confo o3) (confo o33))
(@nonoverlap_oo (confo o3) (confo o34))
(@nonoverlap_oo (confo o3) (confo o35))
(@nonoverlap_oo (confo o4) (confo o5))
(@nonoverlap_oo (confo o4) (confo o6))
(@nonoverlap_oo (confo o4) (confo o7))
(@nonoverlap_oo (confo o4) (confo o8))
(@nonoverlap_oo (confo o4) (confo o9))
(@nonoverlap_oo (confo o4) (confo o10))
(@nonoverlap_oo (confo o4) (confo o11))
(@nonoverlap_oo (confo o4) (confo o12))
(@nonoverlap_oo (confo o4) (confo o13))
(@nonoverlap_oo (confo o4) (confo o14))
(@nonoverlap_oo (confo o4) (confo o15))
(@nonoverlap_oo (confo o4) (confo o16))
(@nonoverlap_oo (confo o4) (confo o17))
(@nonoverlap_oo (confo o4) (confo o18))
(@nonoverlap_oo (confo o4) (confo o19))
(@nonoverlap_oo (confo o4) (confo o20))
(@nonoverlap_oo (confo o4) (confo o21))
(@nonoverlap_oo (confo o4) (confo o22))
(@nonoverlap_oo (confo o4) (confo o23))
(@nonoverlap_oo (confo o4) (confo o24))
(@nonoverlap_oo (confo o4) (confo o25))
(@nonoverlap_oo (confo o4) (confo o26))
(@nonoverlap_oo (confo o4) (confo o27))
(@nonoverlap_oo (confo o4) (confo o28))
(@nonoverlap_oo (confo o4) (confo o29))
(@nonoverlap_oo (confo o4) (confo o30))
(@nonoverlap_oo (confo o4) (confo o31))
(@nonoverlap_oo (confo o4) (confo o32))
(@nonoverlap_oo (confo o4) (confo o33))
(@nonoverlap_oo (confo o4) (confo o34))
(@nonoverlap_oo (confo o4) (confo o35))
(@nonoverlap_oo (confo o5) (confo o6))
(@nonoverlap_oo (confo o5) (confo o7))
(@nonoverlap_oo (confo o5) (confo o8))
(@nonoverlap_oo (confo o5) (confo o9))
(@nonoverlap_oo (confo o5) (confo o10))
(@nonoverlap_oo (confo o5) (confo o11))
(@nonoverlap_oo (confo o5) (confo o12))
(@nonoverlap_oo (confo o5) (confo o13))
(@nonoverlap_oo (confo o5) (confo o14))
(@nonoverlap_oo (confo o5) (confo o15))
(@nonoverlap_oo (confo o5) (confo o16))
(@nonoverlap_oo (confo o5) (confo o17))
(@nonoverlap_oo (confo o5) (confo o18))
(@nonoverlap_oo (confo o5) (confo o19))
(@nonoverlap_oo (confo o5) (confo o20))
(@nonoverlap_oo (confo o5) (confo o21))
(@nonoverlap_oo (confo o5) (confo o22))
(@nonoverlap_oo (confo o5) (confo o23))
(@nonoverlap_oo (confo o5) (confo o24))
(@nonoverlap_oo (confo o5) (confo o25))
(@nonoverlap_oo (confo o5) (confo o26))
(@nonoverlap_oo (confo o5) (confo o27))
(@nonoverlap_oo (confo o5) (confo o28))
(@nonoverlap_oo (confo o5) (confo o29))
(@nonoverlap_oo (confo o5) (confo o30))
(@nonoverlap_oo (confo o5) (confo o31))
(@nonoverlap_oo (confo o5) (confo o32))
(@nonoverlap_oo (confo o5) (confo o33))
(@nonoverlap_oo (confo o5) (confo o34))
(@nonoverlap_oo (confo o5) (confo o35))
(@nonoverlap_oo (confo o6) (confo o7))
(@nonoverlap_oo (confo o6) (confo o8))
(@nonoverlap_oo (confo o6) (confo o9))
(@nonoverlap_oo (confo o6) (confo o10))
(@nonoverlap_oo (confo o6) (confo o11))
(@nonoverlap_oo (confo o6) (confo o12))
(@nonoverlap_oo (confo o6) (confo o13))
(@nonoverlap_oo (confo o6) (confo o14))
(@nonoverlap_oo (confo o6) (confo o15))
(@nonoverlap_oo (confo o6) (confo o16))
(@nonoverlap_oo (confo o6) (confo o17))
(@nonoverlap_oo (confo o6) (confo o18))
(@nonoverlap_oo (confo o6) (confo o19))
(@nonoverlap_oo (confo o6) (confo o20))
(@nonoverlap_oo (confo o6) (confo o21))
(@nonoverlap_oo (confo o6) (confo o22))
(@nonoverlap_oo (confo o6) (confo o23))
(@nonoverlap_oo (confo o6) (confo o24))
(@nonoverlap_oo (confo o6) (confo o25))
(@nonoverlap_oo (confo o6) (confo o26))
(@nonoverlap_oo (confo o6) (confo o27))
(@nonoverlap_oo (confo o6) (confo o28))
(@nonoverlap_oo (confo o6) (confo o29))
(@nonoverlap_oo (confo o6) (confo o30))
(@nonoverlap_oo (confo o6) (confo o31))
(@nonoverlap_oo (confo o6) (confo o32))
(@nonoverlap_oo (confo o6) (confo o33))
(@nonoverlap_oo (confo o6) (confo o34))
(@nonoverlap_oo (confo o6) (confo o35))
(@nonoverlap_oo (confo o7) (confo o8))
(@nonoverlap_oo (confo o7) (confo o9))
(@nonoverlap_oo (confo o7) (confo o10))
(@nonoverlap_oo (confo o7) (confo o11))
(@nonoverlap_oo (confo o7) (confo o12))
(@nonoverlap_oo (confo o7) (confo o13))
(@nonoverlap_oo (confo o7) (confo o14))
(@nonoverlap_oo (confo o7) (confo o15))
(@nonoverlap_oo (confo o7) (confo o16))
(@nonoverlap_oo (confo o7) (confo o17))
(@nonoverlap_oo (confo o7) (confo o18))
(@nonoverlap_oo (confo o7) (confo o19))
(@nonoverlap_oo (confo o7) (confo o20))
(@nonoverlap_oo (confo o7) (confo o21))
(@nonoverlap_oo (confo o7) (confo o22))
(@nonoverlap_oo (confo o7) (confo o23))
(@nonoverlap_oo (confo o7) (confo o24))
(@nonoverlap_oo (confo o7) (confo o25))
(@nonoverlap_oo (confo o7) (confo o26))
(@nonoverlap_oo (confo o7) (confo o27))
(@nonoverlap_oo (confo o7) (confo o28))
(@nonoverlap_oo (confo o7) (confo o29))
(@nonoverlap_oo (confo o7) (confo o30))
(@nonoverlap_oo (confo o7) (confo o31))
(@nonoverlap_oo (confo o7) (confo o32))
(@nonoverlap_oo (confo o7) (confo o33))
(@nonoverlap_oo (confo o7) (confo o34))
(@nonoverlap_oo (confo o7) (confo o35))
(@nonoverlap_oo (confo o8) (confo o9))
(@nonoverlap_oo (confo o8) (confo o10))
(@nonoverlap_oo (confo o8) (confo o11))
(@nonoverlap_oo (confo o8) (confo o12))
(@nonoverlap_oo (confo o8) (confo o13))
(@nonoverlap_oo (confo o8) (confo o14))
(@nonoverlap_oo (confo o8) (confo o15))
(@nonoverlap_oo (confo o8) (confo o16))
(@nonoverlap_oo (confo o8) (confo o17))
(@nonoverlap_oo (confo o8) (confo o18))
(@nonoverlap_oo (confo o8) (confo o19))
(@nonoverlap_oo (confo o8) (confo o20))
(@nonoverlap_oo (confo o8) (confo o21))
(@nonoverlap_oo (confo o8) (confo o22))
(@nonoverlap_oo (confo o8) (confo o23))
(@nonoverlap_oo (confo o8) (confo o24))
(@nonoverlap_oo (confo o8) (confo o25))
(@nonoverlap_oo (confo o8) (confo o26))
(@nonoverlap_oo (confo o8) (confo o27))
(@nonoverlap_oo (confo o8) (confo o28))
(@nonoverlap_oo (confo o8) (confo o29))
(@nonoverlap_oo (confo o8) (confo o30))
(@nonoverlap_oo (confo o8) (confo o31))
(@nonoverlap_oo (confo o8) (confo o32))
(@nonoverlap_oo (confo o8) (confo o33))
(@nonoverlap_oo (confo o8) (confo o34))
(@nonoverlap_oo (confo o8) (confo o35))
(@nonoverlap_oo (confo o9) (confo o10))
(@nonoverlap_oo (confo o9) (confo o11))
(@nonoverlap_oo (confo o9) (confo o12))
(@nonoverlap_oo (confo o9) (confo o13))
(@nonoverlap_oo (confo o9) (confo o14))
(@nonoverlap_oo (confo o9) (confo o15))
(@nonoverlap_oo (confo o9) (confo o16))
(@nonoverlap_oo (confo o9) (confo o17))
(@nonoverlap_oo (confo o9) (confo o18))
(@nonoverlap_oo (confo o9) (confo o19))
(@nonoverlap_oo (confo o9) (confo o20))
(@nonoverlap_oo (confo o9) (confo o21))
(@nonoverlap_oo (confo o9) (confo o22))
(@nonoverlap_oo (confo o9) (confo o23))
(@nonoverlap_oo (confo o9) (confo o24))
(@nonoverlap_oo (confo o9) (confo o25))
(@nonoverlap_oo (confo o9) (confo o26))
(@nonoverlap_oo (confo o9) (confo o27))
(@nonoverlap_oo (confo o9) (confo o28))
(@nonoverlap_oo (confo o9) (confo o29))
(@nonoverlap_oo (confo o9) (confo o30))
(@nonoverlap_oo (confo o9) (confo o31))
(@nonoverlap_oo (confo o9) (confo o32))
(@nonoverlap_oo (confo o9) (confo o33))
(@nonoverlap_oo (confo o9) (confo o34))
(@nonoverlap_oo (confo o9) (confo o35))
(@nonoverlap_oo (confo o10) (confo o11))
(@nonoverlap_oo (confo o10) (confo o12))
(@nonoverlap_oo (confo o10) (confo o13))
(@nonoverlap_oo (confo o10) (confo o14))
(@nonoverlap_oo (confo o10) (confo o15))
(@nonoverlap_oo (confo o10) (confo o16))
(@nonoverlap_oo (confo o10) (confo o17))
(@nonoverlap_oo (confo o10) (confo o18))
(@nonoverlap_oo (confo o10) (confo o19))
(@nonoverlap_oo (confo o10) (confo o20))
(@nonoverlap_oo (confo o10) (confo o21))
(@nonoverlap_oo (confo o10) (confo o22))
(@nonoverlap_oo (confo o10) (confo o23))
(@nonoverlap_oo (confo o10) (confo o24))
(@nonoverlap_oo (confo o10) (confo o25))
(@nonoverlap_oo (confo o10) (confo o26))
(@nonoverlap_oo (confo o10) (confo o27))
(@nonoverlap_oo (confo o10) (confo o28))
(@nonoverlap_oo (confo o10) (confo o29))
(@nonoverlap_oo (confo o10) (confo o30))
(@nonoverlap_oo (confo o10) (confo o31))
(@nonoverlap_oo (confo o10) (confo o32))
(@nonoverlap_oo (confo o10) (confo o33))
(@nonoverlap_oo (confo o10) (confo o34))
(@nonoverlap_oo (confo o10) (confo o35))
(@nonoverlap_oo (confo o11) (confo o12))
(@nonoverlap_oo (confo o11) (confo o13))
(@nonoverlap_oo (confo o11) (confo o14))
(@nonoverlap_oo (confo o11) (confo o15))
(@nonoverlap_oo (confo o11) (confo o16))
(@nonoverlap_oo (confo o11) (confo o17))
(@nonoverlap_oo (confo o11) (confo o18))
(@nonoverlap_oo (confo o11) (confo o19))
(@nonoverlap_oo (confo o11) (confo o20))
(@nonoverlap_oo (confo o11) (confo o21))
(@nonoverlap_oo (confo o11) (confo o22))
(@nonoverlap_oo (confo o11) (confo o23))
(@nonoverlap_oo (confo o11) (confo o24))
(@nonoverlap_oo (confo o11) (confo o25))
(@nonoverlap_oo (confo o11) (confo o26))
(@nonoverlap_oo (confo o11) (confo o27))
(@nonoverlap_oo (confo o11) (confo o28))
(@nonoverlap_oo (confo o11) (confo o29))
(@nonoverlap_oo (confo o11) (confo o30))
(@nonoverlap_oo (confo o11) (confo o31))
(@nonoverlap_oo (confo o11) (confo o32))
(@nonoverlap_oo (confo o11) (confo o33))
(@nonoverlap_oo (confo o11) (confo o34))
(@nonoverlap_oo (confo o11) (confo o35))
(@nonoverlap_oo (confo o12) (confo o13))
(@nonoverlap_oo (confo o12) (confo o14))
(@nonoverlap_oo (confo o12) (confo o15))
(@nonoverlap_oo (confo o12) (confo o16))
(@nonoverlap_oo (confo o12) (confo o17))
(@nonoverlap_oo (confo o12) (confo o18))
(@nonoverlap_oo (confo o12) (confo o19))
(@nonoverlap_oo (confo o12) (confo o20))
(@nonoverlap_oo (confo o12) (confo o21))
(@nonoverlap_oo (confo o12) (confo o22))
(@nonoverlap_oo (confo o12) (confo o23))
(@nonoverlap_oo (confo o12) (confo o24))
(@nonoverlap_oo (confo o12) (confo o25))
(@nonoverlap_oo (confo o12) (confo o26))
(@nonoverlap_oo (confo o12) (confo o27))
(@nonoverlap_oo (confo o12) (confo o28))
(@nonoverlap_oo (confo o12) (confo o29))
(@nonoverlap_oo (confo o12) (confo o30))
(@nonoverlap_oo (confo o12) (confo o31))
(@nonoverlap_oo (confo o12) (confo o32))
(@nonoverlap_oo (confo o12) (confo o33))
(@nonoverlap_oo (confo o12) (confo o34))
(@nonoverlap_oo (confo o12) (confo o35))
(@nonoverlap_oo (confo o13) (confo o14))
(@nonoverlap_oo (confo o13) (confo o15))
(@nonoverlap_oo (confo o13) (confo o16))
(@nonoverlap_oo (confo o13) (confo o17))
(@nonoverlap_oo (confo o13) (confo o18))
(@nonoverlap_oo (confo o13) (confo o19))
(@nonoverlap_oo (confo o13) (confo o20))
(@nonoverlap_oo (confo o13) (confo o21))
(@nonoverlap_oo (confo o13) (confo o22))
(@nonoverlap_oo (confo o13) (confo o23))
(@nonoverlap_oo (confo o13) (confo o24))
(@nonoverlap_oo (confo o13) (confo o25))
(@nonoverlap_oo (confo o13) (confo o26))
(@nonoverlap_oo (confo o13) (confo o27))
(@nonoverlap_oo (confo o13) (confo o28))
(@nonoverlap_oo (confo o13) (confo o29))
(@nonoverlap_oo (confo o13) (confo o30))
(@nonoverlap_oo (confo o13) (confo o31))
(@nonoverlap_oo (confo o13) (confo o32))
(@nonoverlap_oo (confo o13) (confo o33))
(@nonoverlap_oo (confo o13) (confo o34))
(@nonoverlap_oo (confo o13) (confo o35))
(@nonoverlap_oo (confo o14) (confo o15))
(@nonoverlap_oo (confo o14) (confo o16))
(@nonoverlap_oo (confo o14) (confo o17))
(@nonoverlap_oo (confo o14) (confo o18))
(@nonoverlap_oo (confo o14) (confo o19))
(@nonoverlap_oo (confo o14) (confo o20))
(@nonoverlap_oo (confo o14) (confo o21))
(@nonoverlap_oo (confo o14) (confo o22))
(@nonoverlap_oo (confo o14) (confo o23))
(@nonoverlap_oo (confo o14) (confo o24))
(@nonoverlap_oo (confo o14) (confo o25))
(@nonoverlap_oo (confo o14) (confo o26))
(@nonoverlap_oo (confo o14) (confo o27))
(@nonoverlap_oo (confo o14) (confo o28))
(@nonoverlap_oo (confo o14) (confo o29))
(@nonoverlap_oo (confo o14) (confo o30))
(@nonoverlap_oo (confo o14) (confo o31))
(@nonoverlap_oo (confo o14) (confo o32))
(@nonoverlap_oo (confo o14) (confo o33))
(@nonoverlap_oo (confo o14) (confo o34))
(@nonoverlap_oo (confo o14) (confo o35))
(@nonoverlap_oo (confo o15) (confo o16))
(@nonoverlap_oo (confo o15) (confo o17))
(@nonoverlap_oo (confo o15) (confo o18))
(@nonoverlap_oo (confo o15) (confo o19))
(@nonoverlap_oo (confo o15) (confo o20))
(@nonoverlap_oo (confo o15) (confo o21))
(@nonoverlap_oo (confo o15) (confo o22))
(@nonoverlap_oo (confo o15) (confo o23))
(@nonoverlap_oo (confo o15) (confo o24))
(@nonoverlap_oo (confo o15) (confo o25))
(@nonoverlap_oo (confo o15) (confo o26))
(@nonoverlap_oo (confo o15) (confo o27))
(@nonoverlap_oo (confo o15) (confo o28))
(@nonoverlap_oo (confo o15) (confo o29))
(@nonoverlap_oo (confo o15) (confo o30))
(@nonoverlap_oo (confo o15) (confo o31))
(@nonoverlap_oo (confo o15) (confo o32))
(@nonoverlap_oo (confo o15) (confo o33))
(@nonoverlap_oo (confo o15) (confo o34))
(@nonoverlap_oo (confo o15) (confo o35))
(@nonoverlap_oo (confo o16) (confo o17))
(@nonoverlap_oo (confo o16) (confo o18))
(@nonoverlap_oo (confo o16) (confo o19))
(@nonoverlap_oo (confo o16) (confo o20))
(@nonoverlap_oo (confo o16) (confo o21))
(@nonoverlap_oo (confo o16) (confo o22))
(@nonoverlap_oo (confo o16) (confo o23))
(@nonoverlap_oo (confo o16) (confo o24))
(@nonoverlap_oo (confo o16) (confo o25))
(@nonoverlap_oo (confo o16) (confo o26))
(@nonoverlap_oo (confo o16) (confo o27))
(@nonoverlap_oo (confo o16) (confo o28))
(@nonoverlap_oo (confo o16) (confo o29))
(@nonoverlap_oo (confo o16) (confo o30))
(@nonoverlap_oo (confo o16) (confo o31))
(@nonoverlap_oo (confo o16) (confo o32))
(@nonoverlap_oo (confo o16) (confo o33))
(@nonoverlap_oo (confo o16) (confo o34))
(@nonoverlap_oo (confo o16) (confo o35))
(@nonoverlap_oo (confo o17) (confo o18))
(@nonoverlap_oo (confo o17) (confo o19))
(@nonoverlap_oo (confo o17) (confo o20))
(@nonoverlap_oo (confo o17) (confo o21))
(@nonoverlap_oo (confo o17) (confo o22))
(@nonoverlap_oo (confo o17) (confo o23))
(@nonoverlap_oo (confo o17) (confo o24))
(@nonoverlap_oo (confo o17) (confo o25))
(@nonoverlap_oo (confo o17) (confo o26))
(@nonoverlap_oo (confo o17) (confo o27))
(@nonoverlap_oo (confo o17) (confo o28))
(@nonoverlap_oo (confo o17) (confo o29))
(@nonoverlap_oo (confo o17) (confo o30))
(@nonoverlap_oo (confo o17) (confo o31))
(@nonoverlap_oo (confo o17) (confo o32))
(@nonoverlap_oo (confo o17) (confo o33))
(@nonoverlap_oo (confo o17) (confo o34))
(@nonoverlap_oo (confo o17) (confo o35))
(@nonoverlap_oo (confo o18) (confo o19))
(@nonoverlap_oo (confo o18) (confo o20))
(@nonoverlap_oo (confo o18) (confo o21))
(@nonoverlap_oo (confo o18) (confo o22))
(@nonoverlap_oo (confo o18) (confo o23))
(@nonoverlap_oo (confo o18) (confo o24))
(@nonoverlap_oo (confo o18) (confo o25))
(@nonoverlap_oo (confo o18) (confo o26))
(@nonoverlap_oo (confo o18) (confo o27))
(@nonoverlap_oo (confo o18) (confo o28))
(@nonoverlap_oo (confo o18) (confo o29))
(@nonoverlap_oo (confo o18) (confo o30))
(@nonoverlap_oo (confo o18) (confo o31))
(@nonoverlap_oo (confo o18) (confo o32))
(@nonoverlap_oo (confo o18) (confo o33))
(@nonoverlap_oo (confo o18) (confo o34))
(@nonoverlap_oo (confo o18) (confo o35))
(@nonoverlap_oo (confo o19) (confo o20))
(@nonoverlap_oo (confo o19) (confo o21))
(@nonoverlap_oo (confo o19) (confo o22))
(@nonoverlap_oo (confo o19) (confo o23))
(@nonoverlap_oo (confo o19) (confo o24))
(@nonoverlap_oo (confo o19) (confo o25))
(@nonoverlap_oo (confo o19) (confo o26))
(@nonoverlap_oo (confo o19) (confo o27))
(@nonoverlap_oo (confo o19) (confo o28))
(@nonoverlap_oo (confo o19) (confo o29))
(@nonoverlap_oo (confo o19) (confo o30))
(@nonoverlap_oo (confo o19) (confo o31))
(@nonoverlap_oo (confo o19) (confo o32))
(@nonoverlap_oo (confo o19) (confo o33))
(@nonoverlap_oo (confo o19) (confo o34))
(@nonoverlap_oo (confo o19) (confo o35))
(@nonoverlap_oo (confo o20) (confo o21))
(@nonoverlap_oo (confo o20) (confo o22))
(@nonoverlap_oo (confo o20) (confo o23))
(@nonoverlap_oo (confo o20) (confo o24))
(@nonoverlap_oo (confo o20) (confo o25))
(@nonoverlap_oo (confo o20) (confo o26))
(@nonoverlap_oo (confo o20) (confo o27))
(@nonoverlap_oo (confo o20) (confo o28))
(@nonoverlap_oo (confo o20) (confo o29))
(@nonoverlap_oo (confo o20) (confo o30))
(@nonoverlap_oo (confo o20) (confo o31))
(@nonoverlap_oo (confo o20) (confo o32))
(@nonoverlap_oo (confo o20) (confo o33))
(@nonoverlap_oo (confo o20) (confo o34))
(@nonoverlap_oo (confo o20) (confo o35))
(@nonoverlap_oo (confo o21) (confo o22))
(@nonoverlap_oo (confo o21) (confo o23))
(@nonoverlap_oo (confo o21) (confo o24))
(@nonoverlap_oo (confo o21) (confo o25))
(@nonoverlap_oo (confo o21) (confo o26))
(@nonoverlap_oo (confo o21) (confo o27))
(@nonoverlap_oo (confo o21) (confo o28))
(@nonoverlap_oo (confo o21) (confo o29))
(@nonoverlap_oo (confo o21) (confo o30))
(@nonoverlap_oo (confo o21) (confo o31))
(@nonoverlap_oo (confo o21) (confo o32))
(@nonoverlap_oo (confo o21) (confo o33))
(@nonoverlap_oo (confo o21) (confo o34))
(@nonoverlap_oo (confo o21) (confo o35))
(@nonoverlap_oo (confo o22) (confo o23))
(@nonoverlap_oo (confo o22) (confo o24))
(@nonoverlap_oo (confo o22) (confo o25))
(@nonoverlap_oo (confo o22) (confo o26))
(@nonoverlap_oo (confo o22) (confo o27))
(@nonoverlap_oo (confo o22) (confo o28))
(@nonoverlap_oo (confo o22) (confo o29))
(@nonoverlap_oo (confo o22) (confo o30))
(@nonoverlap_oo (confo o22) (confo o31))
(@nonoverlap_oo (confo o22) (confo o32))
(@nonoverlap_oo (confo o22) (confo o33))
(@nonoverlap_oo (confo o22) (confo o34))
(@nonoverlap_oo (confo o22) (confo o35))
(@nonoverlap_oo (confo o23) (confo o24))
(@nonoverlap_oo (confo o23) (confo o25))
(@nonoverlap_oo (confo o23) (confo o26))
(@nonoverlap_oo (confo o23) (confo o27))
(@nonoverlap_oo (confo o23) (confo o28))
(@nonoverlap_oo (confo o23) (confo o29))
(@nonoverlap_oo (confo o23) (confo o30))
(@nonoverlap_oo (confo o23) (confo o31))
(@nonoverlap_oo (confo o23) (confo o32))
(@nonoverlap_oo (confo o23) (confo o33))
(@nonoverlap_oo (confo o23) (confo o34))
(@nonoverlap_oo (confo o23) (confo o35))
(@nonoverlap_oo (confo o24) (confo o25))
(@nonoverlap_oo (confo o24) (confo o26))
(@nonoverlap_oo (confo o24) (confo o27))
(@nonoverlap_oo (confo o24) (confo o28))
(@nonoverlap_oo (confo o24) (confo o29))
(@nonoverlap_oo (confo o24) (confo o30))
(@nonoverlap_oo (confo o24) (confo o31))
(@nonoverlap_oo (confo o24) (confo o32))
(@nonoverlap_oo (confo o24) (confo o33))
(@nonoverlap_oo (confo o24) (confo o34))
(@nonoverlap_oo (confo o24) (confo o35))
(@nonoverlap_oo (confo o25) (confo o26))
(@nonoverlap_oo (confo o25) (confo o27))
(@nonoverlap_oo (confo o25) (confo o28))
(@nonoverlap_oo (confo o25) (confo o29))
(@nonoverlap_oo (confo o25) (confo o30))
(@nonoverlap_oo (confo o25) (confo o31))
(@nonoverlap_oo (confo o25) (confo o32))
(@nonoverlap_oo (confo o25) (confo o33))
(@nonoverlap_oo (confo o25) (confo o34))
(@nonoverlap_oo (confo o25) (confo o35))
(@nonoverlap_oo (confo o26) (confo o27))
(@nonoverlap_oo (confo o26) (confo o28))
(@nonoverlap_oo (confo o26) (confo o29))
(@nonoverlap_oo (confo o26) (confo o30))
(@nonoverlap_oo (confo o26) (confo o31))
(@nonoverlap_oo (confo o26) (confo o32))
(@nonoverlap_oo (confo o26) (confo o33))
(@nonoverlap_oo (confo o26) (confo o34))
(@nonoverlap_oo (confo o26) (confo o35))
(@nonoverlap_oo (confo o27) (confo o28))
(@nonoverlap_oo (confo o27) (confo o29))
(@nonoverlap_oo (confo o27) (confo o30))
(@nonoverlap_oo (confo o27) (confo o31))
(@nonoverlap_oo (confo o27) (confo o32))
(@nonoverlap_oo (confo o27) (confo o33))
(@nonoverlap_oo (confo o27) (confo o34))
(@nonoverlap_oo (confo o27) (confo o35))
(@nonoverlap_oo (confo o28) (confo o29))
(@nonoverlap_oo (confo o28) (confo o30))
(@nonoverlap_oo (confo o28) (confo o31))
(@nonoverlap_oo (confo o28) (confo o32))
(@nonoverlap_oo (confo o28) (confo o33))
(@nonoverlap_oo (confo o28) (confo o34))
(@nonoverlap_oo (confo o28) (confo o35))
(@nonoverlap_oo (confo o29) (confo o30))
(@nonoverlap_oo (confo o29) (confo o31))
(@nonoverlap_oo (confo o29) (confo o32))
(@nonoverlap_oo (confo o29) (confo o33))
(@nonoverlap_oo (confo o29) (confo o34))
(@nonoverlap_oo (confo o29) (confo o35))
(@nonoverlap_oo (confo o30) (confo o31))
(@nonoverlap_oo (confo o30) (confo o32))
(@nonoverlap_oo (confo o30) (confo o33))
(@nonoverlap_oo (confo o30) (confo o34))
(@nonoverlap_oo (confo o30) (confo o35))
(@nonoverlap_oo (confo o31) (confo o32))
(@nonoverlap_oo (confo o31) (confo o33))
(@nonoverlap_oo (confo o31) (confo o34))
(@nonoverlap_oo (confo o31) (confo o35))
(@nonoverlap_oo (confo o32) (confo o33))
(@nonoverlap_oo (confo o32) (confo o34))
(@nonoverlap_oo (confo o32) (confo o35))
(@nonoverlap_oo (confo o33) (confo o34))
(@nonoverlap_oo (confo o33) (confo o35))
(@nonoverlap_oo (confo o34) (confo o35))


)
)
