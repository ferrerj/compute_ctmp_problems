(define (problem sample)
	(:domain fn-robot-navigation)
	(:objects 

		o1 o2 o3 o4 o5 o6 o7 o8 o9 o10 o11 o12 o13 o14 o15 o16 o17 o18 o19 o20 o21 o22 o23 o24 o25 o26 o27 o28 o29 o30

 - object_id

	cb37
	cb36
	cb35
	cb34
	cb38
	cb40
	cb39
	cb41
	cb43
	cb42
	cb45
	cb44
	cb50
	cb53
	cb52
	cb51
	cb47
	cb46
	cb49
	cb48
	cb16
	cb15
	cb13
	cb14
	cb18
	cb17
	cb19
	cb20
	cb21
	cb22
	cb23
	cb24
	cb28
	cb29
	cb26
	cb27
	cb25
	cb30
	cb31
	cb32
	cb33
	cb0
	cb64
	cb63
	cb66
	cb65
	cb59
	cb60
	cb58
	cb61
	cb62
	cb1
	cb2
	cb3
	cb4
	cb54
	cb55
	cb56
	cb57
	cb5
	cb6
	cb7
	cb8
	cb12
	cb9
	cb11
	cb10
	 - conf_base
	co183
	co192
	co182
	co317
	co84
	co257
	co200
	co191
	co264
	co167
	co127
	co261
	co124
	co285
	co122
	co20
	co341
	co22
	co11
	co312
	co129
	co68
	co203
	co255
	co152
	co339
	co216
	co283
	co310
	co235
	co263
	co218
	co19
	co336
	co108
	co292
	co311
	co99
	co86
	co31
	co202
	co18
	co118
	co75
	co337
	co9
	co230
	co169
	co130
	co315
	co214
	co150
	co105
	co288
	co237
	co204
	co260
	co83
	co219
	co234
	co213
	co87
	co265
	co335
	co256
	co309
	co240
	co334
	co284
	co81
	co0
	co1
	co201
	co151
	co109
	co289
	co241
	co215
	co251
	co229
	co104
	co338
	co149
	co340
	co287
	co2
	co314
	co252
	co126
	co168
	co165
	co77
	co123
	co33
	co60
	co69
	co166
	co148
	co147
	co217
	co85
	co12
	co238
	co106
	co82
	co47
	co103
	co258
	co34
	co59
	co291
	co21
	co48
	co35
	co46
	co64
	co233
	co70
	co13
	co267
	co27
	co38
	co206
	co186
	co184
	co112
	co344
	co247
	co188
	co177
	co159
	co197
	co209
	co6
	co37
	co89
	co232
	co294
	co95
	co272
	co246
	co299
	co347
	co298
	co325
	co248
	co63
	co154
	co273
	co14
	co342
	co301
	co307
	co281
	co306
	co353
	co332
	co102
	co244
	co132
	co51
	co250
	co173
	co228
	co320
	co172
	co193
	co321
	co349
	co52
	co196
	co275
	co327
	co254
	co295
	co62
	co97
	co155
	co221
	co185
	co343
	co253
	co205
	co140
	co117
	co187
	co39
	co345
	co78
	co293
	co282
	co153
	co15
	co92
	co296
	co226
	co156
	co3
	co90
	co40
	co224
	co24
	co326
	co135
	co73
	co249
	co346
	co50
	co274
	co225
	co28
	co222
	co227
	co61
	co243
	co270
	co110
	co16
	co212
	co10
	co266
	co36
	co144
	co163
	co146
	co181
	co164
	co280
	co120
	co100
	co116
	co96
	co157
	co176
	co137
	co113
	co41
	co93
	co158
	co139
	co208
	co136
	co207
	co115
	co54
	co324
	co94
	co300
	co195
	co133
	co72
	co297
	co114
	co25
	co245
	co171
	co348
	co268
	co138
	co323
	co4
	co271
	co175
	co319
	co88
	co125
	co316
	co351
	co350
	co262
	co128
	co277
	co210
	co45
	co43
	co66
	co57
	co67
	co32
	co286
	co56
	co44
	co58
	co142
	co119
	co8
	co98
	co76
	co141
	co30
	co290
	co313
	co199
	co161
	co259
	co190
	co189
	co107
	co239
	co179
	co80
	co160
	co143
	co198
	co330
	co279
	co276
	co329
	co236
	co220
	co302
	co303
	co278
	co304
	co328
	co178
	co180
	co162
	co131
	co5
	co305
	co71
	co318
	co145
	co322
	co111
	co23
	co242
	co352
	co53
	co231
	co174
	co91
	co17
	co26
	co42
	co29
	co55
	co211
	co65
	co121
	co74
	co79
	co170
	co194
	co308
	co269
	co7
	co354
	co134
	co331
	co101
	co333
	co223
	co49
 - conf_obj 
 
	ca9
	ca4
	ca16
	ca1
	ca8
	ca6
	ca5
	ca10
	ca13
	ca14
	ca15
	ca12
	ca11
	ca22
	ca19
	ca18
	ca20
	ca21
	ca17
	ca7
	ca3
	ca2
 - conf_arm 
 
	e90
	e91
	e92
	e1009
	e1011
	e1013
	e1015
	e1129
	e1132
	e1135
	e1138
	e1139
	e1142
	e1145
	e1148
	e115
	e116
	e117
	e1024
	e1027
	e1030
	e1033
	e118
	e119
	e120
	e121
	e122
	e123
	e124
	e125
	e126
	e139
	e140
	e141
	e1090
	e142
	e143
	e144
	e145
	e146
	e147
	e148
	e149
	e150
	e153
	e154
	e157
	e158
	e155
	e156
	e151
	e152
	e50
	e51
	e52
	e53
	e54
	e55
	e47
	e48
	e49
	e44
	e45
	e46
	e1064
	e1066
	e1068
	e1070
	e1072
	e1074
	e1076
	e1078
	e1080
	e1083
	e1085
	e1087
	e1089
	e1093
	e1096
	e1099
	e1102
	e58
	e59
	e56
	e57
	e1082
	e1084
	e1086
	e1088
	e1151
	e1153
	e1155
	e1157
	e60
	e61
	e62
	e63
	e64
	e65
	e66
	e67
	e68
	e69
	e70
	e71
	e80
	e81
	e76
	e77
	e78
	e79
	e72
	e73
	e1056
	e1058
	e1060
	e1062
	e74
	e75
	e82
	e83
	e84
	e85
	e86
	e87
	e88
	e89
	e6
	e7
	e4
	e5
	e2
	e3
	e0
	e1
	e1008
	e1010
	e1012
	e1014
	e1017
	e1019
	e1021
	e1023
	e1026
	e1029
	e1032
	e1035
	e1036
	e1038
	e1040
	e1042
	e1095
	e1098
	e1101
	e1104
	e1106
	e1108
	e1110
	e1112
	e1114
	e1117
	e1120
	e1123
	e1126
	e1128
	e1131
	e1134
	e1137
	e8
	e9
	e1000
	e1002
	e1004
	e1006
	e1016
	e1018
	e1020
	e1022
	e1025
	e1028
	e1031
	e1034
	e1092
	e1141
	e1144
	e1147
	e1150
	e12
	e13
	e10
	e11
	e14
	e15
	e18
	e19
	e16
	e17
	e1001
	e1003
	e1005
	e1007
	e1037
	e1039
	e1041
	e1043
	e1046
	e1049
	e1052
	e1055
	e1116
	e1119
	e1122
	e1125
	e1152
	e1154
	e1156
	e1158
	e22
	e23
	e20
	e21
	e38
	e39
	e36
	e37
	e1127
	e1130
	e1133
	e1136
	e42
	e43
	e40
	e41
	e24
	e25
	e26
	e1045
	e1048
	e1051
	e1054
	e1057
	e1059
	e1061
	e1063
	e1065
	e1067
	e1069
	e1071
	e1091
	e1094
	e1097
	e1100
	e1103
	e1105
	e1107
	e1109
	e1111
	e1113
	e1115
	e1118
	e1121
	e1124
	e1140
	e1143
	e1146
	e1149
	e33
	e34
	e35
	e30
	e31
	e32
	e27
	e28
	e29
	e127
	e128
	e129
	e130
	e131
	e132
	e133
	e134
	e135
	e136
	e137
	e138
	e109
	e110
	e105
	e106
	e107
	e108
	e111
	e112
	e113
	e114
	e96
	e97
	e98
	e93
	e94
	e95
	e1044
	e1047
	e1050
	e1053
	e1073
	e1075
	e1077
	e1079
	e1081
	e102
	e103
	e104
	e99
	e100
	e101
 - edge 
 
	t0
	t1
	t2
	t3
	t4
	t5
	t6
	t7
	t8
	t9
	t10
	t11
	t12
	t13
	t14
	t15
	t16
	t17
	t18
	t19
	t20
	t21
	t22
	t23
	t24
	t25
	t26
	t27
	t28
	t29
	t30
	t31
	t32
	t33
	t34
	t35
	t36
	t37
	t38
	t39
	t40
	t41
	t42
	t43
	t44
	t45
	t46
	t47
	t48
	t49
	t50
	t51
	t52
	t53
	t54
	t55
	t56
	t57
	t58
	t59
	t60
	t1000
	t1001
	t1002
	t1003
	t1004
	t1005
	t1006
	t1007
	t1008
	t1009
	t1010
	t1011
	t1012
	t1013
	t1014
	t1015
	t1016
	t1017
	t1018
	t1019
	t1020
	t1021
	t1022
	t1023
	t1024
	t1025
	t1026
	t1027
	t1028
	t1029
	t1030
	t1031
	t1032
	t1033
	t1034
	t1035
	t1036
	t1037
	t1038
	t1039
	t1040
	t1041
	t1042
	t1043
	t1044
	t1045
	t1046
	t1047
	t1048
	t1049
	t1050
	t1051
	t1052
	t1053
	t1054
	t1055
	t1056
	t1057
	t1058
	t1059
	t1060
 - trajectory 
 )
(:init
(= (confb rob ) cb0 )
(= (confa rob ) ca0 )
(= (traj rob ) t_init )
(= (holding) no_object)
; initial object configurations 
 
(= (confo o1) co276); -x -0.861083 -y -2.01689 -z 0.690023
(= (confo o2) co196); -x 0.847397 -y 0.218476 -z 0.690009
(= (confo o3) co314); -x -1.00093 -y -1.85518 -z 0.690002
(= (confo o4) co26); -x 0.649318 -y 0.344392 -z 0.690019
(= (confo o5) co18); -x -0.674703 -y -1.88277 -z 0.689999
(= (confo o6) co301); -x 0.85532 -y -0.10081 -z 0.689992
(= (confo o7) co255); -x -1.11231 -y -1.98895 -z 0.690004
(= (confo o8) co313); -x -0.779537 -y -1.82627 -z 0.690022
(= (confo o9) co148); -x -1.25934 -y -2.10646 -z 0.690003
(= (confo o10) co55); -x 0.65589 -y -5.22191e-05 -z 0.69002
(= (confo o11) co37); -x 0.777217 -y -0.340626 -z 0.690003
(= (confo o12) co262); -x -0.798484 -y -2.14482 -z 0.689983
(= (confo o13) co318); -x 0.629526 -y -0.173643 -z 0.690022
(= (confo o14) co147); -x -1.2778 -y -1.91712 -z 0.690022
(= (confo o15) co185); -x 0.75035 -y -0.244235 -z 0.690023
(= (confo o16) co120); -x 0.60487 -y -0.303572 -z 0.690004
(= (confo o17) co43); -x -0.65955 -y -2.07277 -z 0.689996
(= (confo o18) co179); -x -0.743321 -y -1.99616 -z 0.690002
(= (confo o19) co209); -x 0.756574 -y 0.297056 -z 0.690001
(= (confo o20) co257); -x -1.18537 -y -1.88 -z 0.690023
(= (confo o21) co265); -x -1.02677 -y -2.12184 -z 0.690004
(= (confo o22) co224); -x 0.925907 -y 0.0629021 -z 0.689991
(= (confo o23) co61); -x 0.923189 -y -0.340501 -z 0.689998
(= (confo o24) co223); -x 0.672235 -y 0.0831916 -z 0.690023
(= (confo o25) co146); -x 0.605417 -y 0.202282 -z 0.690022
(= (confo o26) co215); -x -0.920862 -y -1.941 -z 0.690018
(= (confo o27) co7); -x 0.655067 -y -0.403121 -z 0.689996
(= (confo o28) co177); -x 0.75577 -y -0.000607285 -z 0.690017
(= (confo o29) co345); -x 0.724525 -y 0.148532 -z 0.690016
(= (confo o30) co20); -x -1.3148 -y -2.02409 -z 0.690022


 )
(:goal (and


;Place o28 at o1 configuration
;Place o1 at o28 configuration
;Place o15 at o7 configuration
; Place o12 at o4 configuration
; Place o11 at o3 configuration


(= (confo o28) co276); -x -0.861083 -y -2.01689 -z 0.690023
(= (confo o1)  co177); -x 0.75577 -y -0.000607285 -z 0.690017
(= (confo o15) co255); -x -1.11231 -y -1.98895 -z 0.690004
(= (confo o12) co26); -x 0.649318 -y 0.344392 -z 0.690019
(= (confo o11)  co314); -x -1.00093 -y -1.85518 -z 0.690002

))
(:constraints

(@nonoverlap_ro (confb rob) (traj rob) (confo o1))
(@nonoverlap_ro (confb rob) (traj rob) (confo o2))
(@nonoverlap_ro (confb rob) (traj rob) (confo o3))
(@nonoverlap_ro (confb rob) (traj rob) (confo o4))
(@nonoverlap_ro (confb rob) (traj rob) (confo o5))
(@nonoverlap_ro (confb rob) (traj rob) (confo o6))
(@nonoverlap_ro (confb rob) (traj rob) (confo o7)) 
(@nonoverlap_ro (confb rob) (traj rob) (confo o8))
(@nonoverlap_ro (confb rob) (traj rob) (confo o9))
(@nonoverlap_ro (confb rob) (traj rob) (confo o10))
(@nonoverlap_ro (confb rob) (traj rob) (confo o11))
(@nonoverlap_ro (confb rob) (traj rob) (confo o12))
(@nonoverlap_ro (confb rob) (traj rob) (confo o13))
(@nonoverlap_ro (confb rob) (traj rob) (confo o14))
(@nonoverlap_ro (confb rob) (traj rob) (confo o15))
(@nonoverlap_ro (confb rob) (traj rob) (confo o16))
(@nonoverlap_ro (confb rob) (traj rob) (confo o17))
(@nonoverlap_ro (confb rob) (traj rob) (confo o18))
(@nonoverlap_ro (confb rob) (traj rob) (confo o19))
(@nonoverlap_ro (confb rob) (traj rob) (confo o20))
(@nonoverlap_ro (confb rob) (traj rob) (confo o21))
(@nonoverlap_ro (confb rob) (traj rob) (confo o22))
(@nonoverlap_ro (confb rob) (traj rob) (confo o23))
(@nonoverlap_ro (confb rob) (traj rob) (confo o24))
(@nonoverlap_ro (confb rob) (traj rob) (confo o25))
(@nonoverlap_ro (confb rob) (traj rob) (confo o26))
(@nonoverlap_ro (confb rob) (traj rob) (confo o27))
(@nonoverlap_ro (confb rob) (traj rob) (confo o28))
(@nonoverlap_ro (confb rob) (traj rob) (confo o29))
(@nonoverlap_ro (confb rob) (traj rob) (confo o30))
(@nonoverlap_oo (confo o1) (confo o2))
(@nonoverlap_oo (confo o1) (confo o3))
(@nonoverlap_oo (confo o1) (confo o4))
(@nonoverlap_oo (confo o1) (confo o5))
(@nonoverlap_oo (confo o1) (confo o6))
(@nonoverlap_oo (confo o1) (confo o7))
(@nonoverlap_oo (confo o1) (confo o8))
(@nonoverlap_oo (confo o1) (confo o9))
(@nonoverlap_oo (confo o1) (confo o10))
(@nonoverlap_oo (confo o1) (confo o11))
(@nonoverlap_oo (confo o1) (confo o12))
(@nonoverlap_oo (confo o1) (confo o13))
(@nonoverlap_oo (confo o1) (confo o14))
(@nonoverlap_oo (confo o1) (confo o15))
(@nonoverlap_oo (confo o1) (confo o16))
(@nonoverlap_oo (confo o1) (confo o17))
(@nonoverlap_oo (confo o1) (confo o18))
(@nonoverlap_oo (confo o1) (confo o19))
(@nonoverlap_oo (confo o1) (confo o20))
(@nonoverlap_oo (confo o1) (confo o21))
(@nonoverlap_oo (confo o1) (confo o22))
(@nonoverlap_oo (confo o1) (confo o23))
(@nonoverlap_oo (confo o1) (confo o24))
(@nonoverlap_oo (confo o1) (confo o25))
(@nonoverlap_oo (confo o1) (confo o26))
(@nonoverlap_oo (confo o1) (confo o27))
(@nonoverlap_oo (confo o1) (confo o28))
(@nonoverlap_oo (confo o1) (confo o29))
(@nonoverlap_oo (confo o1) (confo o30))
(@nonoverlap_oo (confo o2) (confo o3))
(@nonoverlap_oo (confo o2) (confo o4))
(@nonoverlap_oo (confo o2) (confo o5))
(@nonoverlap_oo (confo o2) (confo o6))
(@nonoverlap_oo (confo o2) (confo o7))
(@nonoverlap_oo (confo o2) (confo o8))
(@nonoverlap_oo (confo o2) (confo o9))
(@nonoverlap_oo (confo o2) (confo o10))
(@nonoverlap_oo (confo o2) (confo o11))
(@nonoverlap_oo (confo o2) (confo o12))
(@nonoverlap_oo (confo o2) (confo o13))
(@nonoverlap_oo (confo o2) (confo o14))
(@nonoverlap_oo (confo o2) (confo o15))
(@nonoverlap_oo (confo o2) (confo o16))
(@nonoverlap_oo (confo o2) (confo o17))
(@nonoverlap_oo (confo o2) (confo o18))
(@nonoverlap_oo (confo o2) (confo o19))
(@nonoverlap_oo (confo o2) (confo o20))
(@nonoverlap_oo (confo o2) (confo o21))
(@nonoverlap_oo (confo o2) (confo o22))
(@nonoverlap_oo (confo o2) (confo o23))
(@nonoverlap_oo (confo o2) (confo o24))
(@nonoverlap_oo (confo o2) (confo o25))
(@nonoverlap_oo (confo o2) (confo o26))
(@nonoverlap_oo (confo o2) (confo o27))
(@nonoverlap_oo (confo o2) (confo o28))
(@nonoverlap_oo (confo o2) (confo o29))
(@nonoverlap_oo (confo o2) (confo o30))
(@nonoverlap_oo (confo o3) (confo o4))
(@nonoverlap_oo (confo o3) (confo o5))
(@nonoverlap_oo (confo o3) (confo o6))
(@nonoverlap_oo (confo o3) (confo o7))
(@nonoverlap_oo (confo o3) (confo o8))
(@nonoverlap_oo (confo o3) (confo o9))
(@nonoverlap_oo (confo o3) (confo o10))
(@nonoverlap_oo (confo o3) (confo o11))
(@nonoverlap_oo (confo o3) (confo o12))
(@nonoverlap_oo (confo o3) (confo o13))
(@nonoverlap_oo (confo o3) (confo o14))
(@nonoverlap_oo (confo o3) (confo o15))
(@nonoverlap_oo (confo o3) (confo o16))
(@nonoverlap_oo (confo o3) (confo o17))
(@nonoverlap_oo (confo o3) (confo o18))
(@nonoverlap_oo (confo o3) (confo o19))
(@nonoverlap_oo (confo o3) (confo o20))
(@nonoverlap_oo (confo o3) (confo o21))
(@nonoverlap_oo (confo o3) (confo o22))
(@nonoverlap_oo (confo o3) (confo o23))
(@nonoverlap_oo (confo o3) (confo o24))
(@nonoverlap_oo (confo o3) (confo o25))
(@nonoverlap_oo (confo o3) (confo o26))
(@nonoverlap_oo (confo o3) (confo o27))
(@nonoverlap_oo (confo o3) (confo o28))
(@nonoverlap_oo (confo o3) (confo o29))
(@nonoverlap_oo (confo o3) (confo o30))
(@nonoverlap_oo (confo o4) (confo o5))
(@nonoverlap_oo (confo o4) (confo o6))
(@nonoverlap_oo (confo o4) (confo o7))
(@nonoverlap_oo (confo o4) (confo o8))
(@nonoverlap_oo (confo o4) (confo o9))
(@nonoverlap_oo (confo o4) (confo o10))
(@nonoverlap_oo (confo o4) (confo o11))
(@nonoverlap_oo (confo o4) (confo o12))
(@nonoverlap_oo (confo o4) (confo o13))
(@nonoverlap_oo (confo o4) (confo o14))
(@nonoverlap_oo (confo o4) (confo o15))
(@nonoverlap_oo (confo o4) (confo o16))
(@nonoverlap_oo (confo o4) (confo o17))
(@nonoverlap_oo (confo o4) (confo o18))
(@nonoverlap_oo (confo o4) (confo o19))
(@nonoverlap_oo (confo o4) (confo o20))
(@nonoverlap_oo (confo o4) (confo o21))
(@nonoverlap_oo (confo o4) (confo o22))
(@nonoverlap_oo (confo o4) (confo o23))
(@nonoverlap_oo (confo o4) (confo o24))
(@nonoverlap_oo (confo o4) (confo o25))
(@nonoverlap_oo (confo o4) (confo o26))
(@nonoverlap_oo (confo o4) (confo o27))
(@nonoverlap_oo (confo o4) (confo o28))
(@nonoverlap_oo (confo o4) (confo o29))
(@nonoverlap_oo (confo o4) (confo o30))
(@nonoverlap_oo (confo o5) (confo o6))
(@nonoverlap_oo (confo o5) (confo o7))
(@nonoverlap_oo (confo o5) (confo o8))
(@nonoverlap_oo (confo o5) (confo o9))
(@nonoverlap_oo (confo o5) (confo o10))
(@nonoverlap_oo (confo o5) (confo o11))
(@nonoverlap_oo (confo o5) (confo o12))
(@nonoverlap_oo (confo o5) (confo o13))
(@nonoverlap_oo (confo o5) (confo o14))
(@nonoverlap_oo (confo o5) (confo o15))
(@nonoverlap_oo (confo o5) (confo o16))
(@nonoverlap_oo (confo o5) (confo o17))
(@nonoverlap_oo (confo o5) (confo o18))
(@nonoverlap_oo (confo o5) (confo o19))
(@nonoverlap_oo (confo o5) (confo o20))
(@nonoverlap_oo (confo o5) (confo o21))
(@nonoverlap_oo (confo o5) (confo o22))
(@nonoverlap_oo (confo o5) (confo o23))
(@nonoverlap_oo (confo o5) (confo o24))
(@nonoverlap_oo (confo o5) (confo o25))
(@nonoverlap_oo (confo o5) (confo o26))
(@nonoverlap_oo (confo o5) (confo o27))
(@nonoverlap_oo (confo o5) (confo o28))
(@nonoverlap_oo (confo o5) (confo o29))
(@nonoverlap_oo (confo o5) (confo o30))
(@nonoverlap_oo (confo o6) (confo o7))
(@nonoverlap_oo (confo o6) (confo o8))
(@nonoverlap_oo (confo o6) (confo o9))
(@nonoverlap_oo (confo o6) (confo o10))
(@nonoverlap_oo (confo o6) (confo o11))
(@nonoverlap_oo (confo o6) (confo o12))
(@nonoverlap_oo (confo o6) (confo o13))
(@nonoverlap_oo (confo o6) (confo o14))
(@nonoverlap_oo (confo o6) (confo o15))
(@nonoverlap_oo (confo o6) (confo o16))
(@nonoverlap_oo (confo o6) (confo o17))
(@nonoverlap_oo (confo o6) (confo o18))
(@nonoverlap_oo (confo o6) (confo o19))
(@nonoverlap_oo (confo o6) (confo o20))
(@nonoverlap_oo (confo o6) (confo o21))
(@nonoverlap_oo (confo o6) (confo o22))
(@nonoverlap_oo (confo o6) (confo o23))
(@nonoverlap_oo (confo o6) (confo o24))
(@nonoverlap_oo (confo o6) (confo o25))
(@nonoverlap_oo (confo o6) (confo o26))
(@nonoverlap_oo (confo o6) (confo o27))
(@nonoverlap_oo (confo o6) (confo o28))
(@nonoverlap_oo (confo o6) (confo o29))
(@nonoverlap_oo (confo o6) (confo o30))
(@nonoverlap_oo (confo o7) (confo o8))
(@nonoverlap_oo (confo o7) (confo o9))
(@nonoverlap_oo (confo o7) (confo o10))
(@nonoverlap_oo (confo o7) (confo o11))
(@nonoverlap_oo (confo o7) (confo o12))
(@nonoverlap_oo (confo o7) (confo o13))
(@nonoverlap_oo (confo o7) (confo o14))
(@nonoverlap_oo (confo o7) (confo o15))
(@nonoverlap_oo (confo o7) (confo o16))
(@nonoverlap_oo (confo o7) (confo o17))
(@nonoverlap_oo (confo o7) (confo o18))
(@nonoverlap_oo (confo o7) (confo o19))
(@nonoverlap_oo (confo o7) (confo o20))
(@nonoverlap_oo (confo o7) (confo o21))
(@nonoverlap_oo (confo o7) (confo o22))
(@nonoverlap_oo (confo o7) (confo o23))
(@nonoverlap_oo (confo o7) (confo o24))
(@nonoverlap_oo (confo o7) (confo o25))
(@nonoverlap_oo (confo o7) (confo o26))
(@nonoverlap_oo (confo o7) (confo o27))
(@nonoverlap_oo (confo o7) (confo o28))
(@nonoverlap_oo (confo o7) (confo o29))
(@nonoverlap_oo (confo o7) (confo o30))
(@nonoverlap_oo (confo o8) (confo o9))
(@nonoverlap_oo (confo o8) (confo o10))
(@nonoverlap_oo (confo o8) (confo o11))
(@nonoverlap_oo (confo o8) (confo o12))
(@nonoverlap_oo (confo o8) (confo o13))
(@nonoverlap_oo (confo o8) (confo o14))
(@nonoverlap_oo (confo o8) (confo o15))
(@nonoverlap_oo (confo o8) (confo o16))
(@nonoverlap_oo (confo o8) (confo o17))
(@nonoverlap_oo (confo o8) (confo o18))
(@nonoverlap_oo (confo o8) (confo o19))
(@nonoverlap_oo (confo o8) (confo o20))
(@nonoverlap_oo (confo o8) (confo o21))
(@nonoverlap_oo (confo o8) (confo o22))
(@nonoverlap_oo (confo o8) (confo o23))
(@nonoverlap_oo (confo o8) (confo o24))
(@nonoverlap_oo (confo o8) (confo o25))
(@nonoverlap_oo (confo o8) (confo o26))
(@nonoverlap_oo (confo o8) (confo o27))
(@nonoverlap_oo (confo o8) (confo o28))
(@nonoverlap_oo (confo o8) (confo o29))
(@nonoverlap_oo (confo o8) (confo o30))
(@nonoverlap_oo (confo o9) (confo o10))
(@nonoverlap_oo (confo o9) (confo o11))
(@nonoverlap_oo (confo o9) (confo o12))
(@nonoverlap_oo (confo o9) (confo o13))
(@nonoverlap_oo (confo o9) (confo o14))
(@nonoverlap_oo (confo o9) (confo o15))
(@nonoverlap_oo (confo o9) (confo o16))
(@nonoverlap_oo (confo o9) (confo o17))
(@nonoverlap_oo (confo o9) (confo o18))
(@nonoverlap_oo (confo o9) (confo o19))
(@nonoverlap_oo (confo o9) (confo o20))
(@nonoverlap_oo (confo o9) (confo o21))
(@nonoverlap_oo (confo o9) (confo o22))
(@nonoverlap_oo (confo o9) (confo o23))
(@nonoverlap_oo (confo o9) (confo o24))
(@nonoverlap_oo (confo o9) (confo o25))
(@nonoverlap_oo (confo o9) (confo o26))
(@nonoverlap_oo (confo o9) (confo o27))
(@nonoverlap_oo (confo o9) (confo o28))
(@nonoverlap_oo (confo o9) (confo o29))
(@nonoverlap_oo (confo o9) (confo o30))
(@nonoverlap_oo (confo o10) (confo o11))
(@nonoverlap_oo (confo o10) (confo o12))
(@nonoverlap_oo (confo o10) (confo o13))
(@nonoverlap_oo (confo o10) (confo o14))
(@nonoverlap_oo (confo o10) (confo o15))
(@nonoverlap_oo (confo o10) (confo o16))
(@nonoverlap_oo (confo o10) (confo o17))
(@nonoverlap_oo (confo o10) (confo o18))
(@nonoverlap_oo (confo o10) (confo o19))
(@nonoverlap_oo (confo o10) (confo o20))
(@nonoverlap_oo (confo o10) (confo o21))
(@nonoverlap_oo (confo o10) (confo o22))
(@nonoverlap_oo (confo o10) (confo o23))
(@nonoverlap_oo (confo o10) (confo o24))
(@nonoverlap_oo (confo o10) (confo o25))
(@nonoverlap_oo (confo o10) (confo o26))
(@nonoverlap_oo (confo o10) (confo o27))
(@nonoverlap_oo (confo o10) (confo o28))
(@nonoverlap_oo (confo o10) (confo o29))
(@nonoverlap_oo (confo o10) (confo o30))
(@nonoverlap_oo (confo o11) (confo o12))
(@nonoverlap_oo (confo o11) (confo o13))
(@nonoverlap_oo (confo o11) (confo o14))
(@nonoverlap_oo (confo o11) (confo o15))
(@nonoverlap_oo (confo o11) (confo o16))
(@nonoverlap_oo (confo o11) (confo o17))
(@nonoverlap_oo (confo o11) (confo o18))
(@nonoverlap_oo (confo o11) (confo o19))
(@nonoverlap_oo (confo o11) (confo o20))
(@nonoverlap_oo (confo o11) (confo o21))
(@nonoverlap_oo (confo o11) (confo o22))
(@nonoverlap_oo (confo o11) (confo o23))
(@nonoverlap_oo (confo o11) (confo o24))
(@nonoverlap_oo (confo o11) (confo o25))
(@nonoverlap_oo (confo o11) (confo o26))
(@nonoverlap_oo (confo o11) (confo o27))
(@nonoverlap_oo (confo o11) (confo o28))
(@nonoverlap_oo (confo o11) (confo o29))
(@nonoverlap_oo (confo o11) (confo o30))
(@nonoverlap_oo (confo o12) (confo o13))
(@nonoverlap_oo (confo o12) (confo o14))
(@nonoverlap_oo (confo o12) (confo o15))
(@nonoverlap_oo (confo o12) (confo o16))
(@nonoverlap_oo (confo o12) (confo o17))
(@nonoverlap_oo (confo o12) (confo o18))
(@nonoverlap_oo (confo o12) (confo o19))
(@nonoverlap_oo (confo o12) (confo o20))
(@nonoverlap_oo (confo o12) (confo o21))
(@nonoverlap_oo (confo o12) (confo o22))
(@nonoverlap_oo (confo o12) (confo o23))
(@nonoverlap_oo (confo o12) (confo o24))
(@nonoverlap_oo (confo o12) (confo o25))
(@nonoverlap_oo (confo o12) (confo o26))
(@nonoverlap_oo (confo o12) (confo o27))
(@nonoverlap_oo (confo o12) (confo o28))
(@nonoverlap_oo (confo o12) (confo o29))
(@nonoverlap_oo (confo o12) (confo o30))
(@nonoverlap_oo (confo o13) (confo o14))
(@nonoverlap_oo (confo o13) (confo o15))
(@nonoverlap_oo (confo o13) (confo o16))
(@nonoverlap_oo (confo o13) (confo o17))
(@nonoverlap_oo (confo o13) (confo o18))
(@nonoverlap_oo (confo o13) (confo o19))
(@nonoverlap_oo (confo o13) (confo o20))
(@nonoverlap_oo (confo o13) (confo o21))
(@nonoverlap_oo (confo o13) (confo o22))
(@nonoverlap_oo (confo o13) (confo o23))
(@nonoverlap_oo (confo o13) (confo o24))
(@nonoverlap_oo (confo o13) (confo o25))
(@nonoverlap_oo (confo o13) (confo o26))
(@nonoverlap_oo (confo o13) (confo o27))
(@nonoverlap_oo (confo o13) (confo o28))
(@nonoverlap_oo (confo o13) (confo o29))
(@nonoverlap_oo (confo o13) (confo o30))
(@nonoverlap_oo (confo o14) (confo o15))
(@nonoverlap_oo (confo o14) (confo o16))
(@nonoverlap_oo (confo o14) (confo o17))
(@nonoverlap_oo (confo o14) (confo o18))
(@nonoverlap_oo (confo o14) (confo o19))
(@nonoverlap_oo (confo o14) (confo o20))
(@nonoverlap_oo (confo o14) (confo o21))
(@nonoverlap_oo (confo o14) (confo o22))
(@nonoverlap_oo (confo o14) (confo o23))
(@nonoverlap_oo (confo o14) (confo o24))
(@nonoverlap_oo (confo o14) (confo o25))
(@nonoverlap_oo (confo o14) (confo o26))
(@nonoverlap_oo (confo o14) (confo o27))
(@nonoverlap_oo (confo o14) (confo o28))
(@nonoverlap_oo (confo o14) (confo o29))
(@nonoverlap_oo (confo o14) (confo o30))
(@nonoverlap_oo (confo o15) (confo o16))
(@nonoverlap_oo (confo o15) (confo o17))
(@nonoverlap_oo (confo o15) (confo o18))
(@nonoverlap_oo (confo o15) (confo o19))
(@nonoverlap_oo (confo o15) (confo o20))
(@nonoverlap_oo (confo o15) (confo o21))
(@nonoverlap_oo (confo o15) (confo o22))
(@nonoverlap_oo (confo o15) (confo o23))
(@nonoverlap_oo (confo o15) (confo o24))
(@nonoverlap_oo (confo o15) (confo o25))
(@nonoverlap_oo (confo o15) (confo o26))
(@nonoverlap_oo (confo o15) (confo o27))
(@nonoverlap_oo (confo o15) (confo o28))
(@nonoverlap_oo (confo o15) (confo o29))
(@nonoverlap_oo (confo o15) (confo o30))
(@nonoverlap_oo (confo o16) (confo o17))
(@nonoverlap_oo (confo o16) (confo o18))
(@nonoverlap_oo (confo o16) (confo o19))
(@nonoverlap_oo (confo o16) (confo o20))
(@nonoverlap_oo (confo o16) (confo o21))
(@nonoverlap_oo (confo o16) (confo o22))
(@nonoverlap_oo (confo o16) (confo o23))
(@nonoverlap_oo (confo o16) (confo o24))
(@nonoverlap_oo (confo o16) (confo o25))
(@nonoverlap_oo (confo o16) (confo o26))
(@nonoverlap_oo (confo o16) (confo o27))
(@nonoverlap_oo (confo o16) (confo o28))
(@nonoverlap_oo (confo o16) (confo o29))
(@nonoverlap_oo (confo o16) (confo o30))
(@nonoverlap_oo (confo o17) (confo o18))
(@nonoverlap_oo (confo o17) (confo o19))
(@nonoverlap_oo (confo o17) (confo o20))
(@nonoverlap_oo (confo o17) (confo o21))
(@nonoverlap_oo (confo o17) (confo o22))
(@nonoverlap_oo (confo o17) (confo o23))
(@nonoverlap_oo (confo o17) (confo o24))
(@nonoverlap_oo (confo o17) (confo o25))
(@nonoverlap_oo (confo o17) (confo o26))
(@nonoverlap_oo (confo o17) (confo o27))
(@nonoverlap_oo (confo o17) (confo o28))
(@nonoverlap_oo (confo o17) (confo o29))
(@nonoverlap_oo (confo o17) (confo o30))
(@nonoverlap_oo (confo o18) (confo o19))
(@nonoverlap_oo (confo o18) (confo o20))
(@nonoverlap_oo (confo o18) (confo o21))
(@nonoverlap_oo (confo o18) (confo o22))
(@nonoverlap_oo (confo o18) (confo o23))
(@nonoverlap_oo (confo o18) (confo o24))
(@nonoverlap_oo (confo o18) (confo o25))
(@nonoverlap_oo (confo o18) (confo o26))
(@nonoverlap_oo (confo o18) (confo o27))
(@nonoverlap_oo (confo o18) (confo o28))
(@nonoverlap_oo (confo o18) (confo o29))
(@nonoverlap_oo (confo o18) (confo o30))
(@nonoverlap_oo (confo o19) (confo o20))
(@nonoverlap_oo (confo o19) (confo o21))
(@nonoverlap_oo (confo o19) (confo o22))
(@nonoverlap_oo (confo o19) (confo o23))
(@nonoverlap_oo (confo o19) (confo o24))
(@nonoverlap_oo (confo o19) (confo o25))
(@nonoverlap_oo (confo o19) (confo o26))
(@nonoverlap_oo (confo o19) (confo o27))
(@nonoverlap_oo (confo o19) (confo o28))
(@nonoverlap_oo (confo o19) (confo o29))
(@nonoverlap_oo (confo o19) (confo o30))
(@nonoverlap_oo (confo o20) (confo o21))
(@nonoverlap_oo (confo o20) (confo o22))
(@nonoverlap_oo (confo o20) (confo o23))
(@nonoverlap_oo (confo o20) (confo o24))
(@nonoverlap_oo (confo o20) (confo o25))
(@nonoverlap_oo (confo o20) (confo o26))
(@nonoverlap_oo (confo o20) (confo o27))
(@nonoverlap_oo (confo o20) (confo o28))
(@nonoverlap_oo (confo o20) (confo o29))
(@nonoverlap_oo (confo o20) (confo o30))
(@nonoverlap_oo (confo o21) (confo o22))
(@nonoverlap_oo (confo o21) (confo o23))
(@nonoverlap_oo (confo o21) (confo o24))
(@nonoverlap_oo (confo o21) (confo o25))
(@nonoverlap_oo (confo o21) (confo o26))
(@nonoverlap_oo (confo o21) (confo o27))
(@nonoverlap_oo (confo o21) (confo o28))
(@nonoverlap_oo (confo o21) (confo o29))
(@nonoverlap_oo (confo o21) (confo o30))
(@nonoverlap_oo (confo o22) (confo o23))
(@nonoverlap_oo (confo o22) (confo o24))
(@nonoverlap_oo (confo o22) (confo o25))
(@nonoverlap_oo (confo o22) (confo o26))
(@nonoverlap_oo (confo o22) (confo o27))
(@nonoverlap_oo (confo o22) (confo o28))
(@nonoverlap_oo (confo o22) (confo o29))
(@nonoverlap_oo (confo o22) (confo o30))
(@nonoverlap_oo (confo o23) (confo o24))
(@nonoverlap_oo (confo o23) (confo o25))
(@nonoverlap_oo (confo o23) (confo o26))
(@nonoverlap_oo (confo o23) (confo o27))
(@nonoverlap_oo (confo o23) (confo o28))
(@nonoverlap_oo (confo o23) (confo o29))
(@nonoverlap_oo (confo o23) (confo o30))
(@nonoverlap_oo (confo o24) (confo o25))
(@nonoverlap_oo (confo o24) (confo o26))
(@nonoverlap_oo (confo o24) (confo o27))
(@nonoverlap_oo (confo o24) (confo o28))
(@nonoverlap_oo (confo o24) (confo o29))
(@nonoverlap_oo (confo o24) (confo o30))
(@nonoverlap_oo (confo o25) (confo o26))
(@nonoverlap_oo (confo o25) (confo o27))
(@nonoverlap_oo (confo o25) (confo o28))
(@nonoverlap_oo (confo o25) (confo o29))
(@nonoverlap_oo (confo o25) (confo o30))
(@nonoverlap_oo (confo o26) (confo o27))
(@nonoverlap_oo (confo o26) (confo o28))
(@nonoverlap_oo (confo o26) (confo o29))
(@nonoverlap_oo (confo o26) (confo o30))
(@nonoverlap_oo (confo o27) (confo o28))
(@nonoverlap_oo (confo o27) (confo o29))
(@nonoverlap_oo (confo o27) (confo o30))
(@nonoverlap_oo (confo o28) (confo o29))
(@nonoverlap_oo (confo o28) (confo o30))
(@nonoverlap_oo (confo o29) (confo o30))




)
)
