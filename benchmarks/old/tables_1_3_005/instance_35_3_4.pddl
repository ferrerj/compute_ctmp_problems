(define (problem sample)
	(:domain fn-robot-navigation)
	(:objects 

		o1 o2 o3 o4 o5 o6 o7 o8 o9 o10 o11 o12 o13 o14 o15 o16 o17 o18 o19 o20 o21 o22 o23 o24 o25 o26 o27 o28 o29 o30 o31 o32 o33 o34 o35

 - object_id

	cb98
	cb97
	cb96
	cb95
	cb111
	cb110
	cb112
	cb113
	cb121
	cb120
	cb119
	cb117
	cb118
	cb116
	cb114
	cb115
	cb122
	cb126
	cb125
	cb124
	cb123
	cb99
	cb108
	cb109
	cb107
	cb104
	cb105
	cb106
	cb101
	cb100
	cb103
	cb102
	cb90
	cb91
	cb92
	cb93
	cb94
	cb87
	cb86
	cb85
	cb89
	cb88
	cb68
	cb65
	cb67
	cb66
	cb52
	cb56
	cb55
	cb54
	cb53
	cb69
	cb73
	cb74
	cb71
	cb72
	cb70
	cb83
	cb81
	cb82
	cb80
	cb78
	cb77
	cb79
	cb76
	cb75
	cb84
	cb64
	cb63
	cb62
	cb61
	cb60
	cb58
	cb57
	cb59
	cb0
	cb20
	cb22
	cb23
	cb21
	cb3
	cb2
	cb5
	cb4
	cb1
	cb6
	cb10
	cb9
	cb8
	cb7
	cb11
	cb12
	cb13
	cb14
	cb15
	cb18
	cb19
	cb16
	cb17
	cb25
	cb24
	cb27
	cb26
	cb45
	cb44
	cb43
	cb42
	cb30
	cb29
	cb28
	cb32
	cb31
	cb46
	cb48
	cb47
	cb51
	cb50
	cb49
	cb41
	cb40
	cb39
	cb38
	cb34
	cb35
	cb36
	cb33
	cb37
	 - conf_base
	co114
	co39
	co170
	co212
	co118
	co198
	co192
	co204
	co187
	co221
	co210
	co202
	co179
	co129
	co43
	co135
	co169
	co126
	co181
	co70
	co140
	co123
	co96
	co120
	co162
	co176
	co177
	co28
	co168
	co127
	co183
	co214
	co67
	co23
	co86
	co173
	co94
	co73
	co33
	co104
	co84
	co109
	co78
	co53
	co92
	co209
	co144
	co158
	co216
	co111
	co155
	co196
	co77
	co143
	co69
	co116
	co165
	co0
	co121
	co63
	co42
	co37
	co41
	co48
	co45
	co47
	co56
	co32
	co25
	co58
	co18
	co52
	co57
	co138
	co5
	co60
	co99
	co55
	co171
	co30
	co51
	co161
	co125
	co89
	co97
	co103
	co193
	co108
	co197
	co159
	co3
	co74
	co213
	co72
	co164
	co174
	co83
	co141
	co79
	co102
	co195
	co184
	co208
	co180
	co207
	co157
	co152
	co172
	co35
	co7
	co81
	co15
	co218
	co153
	co167
	co201
	co137
	co88
	co131
	co21
	co163
	co132
	co199
	co146
	co205
	co113
	co151
	co190
	co149
	co156
	co66
	co203
	co24
	co182
	co38
	co31
	co107
	co61
	co98
	co26
	co91
	co85
	co215
	co211
	co76
	co1
	co4
	co11
	co82
	co133
	co65
	co8
	co12
	co222
	co16
	co122
	co117
	co62
	co13
	co90
	co49
	co178
	co19
	co142
	co106
	co93
	co134
	co128
	co105
	co9
	co17
	co139
	co136
	co130
	co220
	co71
	co175
	co20
	co115
	co150
	co6
	co87
	co206
	co80
	co160
	co124
	co95
	co34
	co194
	co191
	co188
	co185
	co110
	co217
	co148
	co166
	co200
	co101
	co147
	co2
	co14
	co68
	co112
	co36
	co40
	co44
	co46
	co54
	co50
	co145
	co59
	co27
	co189
	co119
	co154
	co10
	co22
	co29
	co75
	co219
	co100
	co186
	co64
 - conf_obj 
 
	ca16
	ca27
	ca8
	ca29
	ca5
	ca3
	ca10
	ca11
	ca9
	ca25
	ca26
	ca23
	ca15
	ca28
	ca24
	ca41
	ca42
	ca18
	ca19
	ca36
	ca30
	ca33
	ca35
	ca32
	ca34
	ca31
	ca20
	ca21
	ca22
	ca37
	ca40
	ca17
	ca38
	ca39
	ca2
	ca1
	ca14
	ca13
	ca6
	ca4
	ca7
	ca12
 - conf_arm 
 
	e225
	e226
	e227
	e1014
	e1017
	e1020
	e1023
	e1044
	e1047
	e1068
	e1071
	e1074
	e1077
	e1080
	e1083
	e1225
	e1228
	e1231
	e1234
	e1237
	e1255
	e1258
	e1261
	e1264
	e1267
	e1270
	e1273
	e1276
	e1279
	e1282
	e1285
	e1288
	e1291
	e1294
	e1366
	e1369
	e1372
	e1375
	e1378
	e252
	e253
	e254
	e243
	e244
	e245
	e240
	e241
	e242
	e1240
	e1243
	e1246
	e1249
	e1252
	e1321
	e1324
	e1327
	e1330
	e1333
	e249
	e250
	e251
	e246
	e247
	e248
	e255
	e256
	e257
	e267
	e268
	e269
	e264
	e265
	e266
	e261
	e262
	e263
	e258
	e259
	e260
	e270
	e271
	e272
	e273
	e274
	e275
	e276
	e277
	e278
	e279
	e280
	e281
	e282
	e283
	e284
	e291
	e292
	e293
	e294
	e295
	e296
	e285
	e286
	e287
	e288
	e289
	e290
	e228
	e229
	e230
	e1173
	e1176
	e1179
	e1182
	e1185
	e1188
	e1191
	e1194
	e1197
	e1200
	e1297
	e1300
	e1303
	e1306
	e1354
	e1357
	e1360
	e1363
	e237
	e238
	e239
	e231
	e232
	e233
	e234
	e235
	e236
	e300
	e301
	e302
	e297
	e298
	e299
	e306
	e307
	e308
	e303
	e304
	e305
	e327
	e328
	e329
	e324
	e325
	e326
	e321
	e322
	e323
	e333
	e334
	e335
	e330
	e331
	e332
	e375
	e376
	e377
	e366
	e367
	e368
	e369
	e370
	e371
	e372
	e373
	e374
	e378
	e379
	e380
	e363
	e364
	e365
	e360
	e361
	e362
	e357
	e358
	e359
	e354
	e355
	e356
	e318
	e319
	e320
	e315
	e316
	e317
	e312
	e313
	e314
	e309
	e310
	e311
	e336
	e337
	e338
	e342
	e343
	e344
	e339
	e340
	e341
	e351
	e352
	e353
	e348
	e349
	e350
	e345
	e346
	e347
	e138
	e139
	e140
	e1241
	e1244
	e1247
	e1250
	e1253
	e1256
	e1259
	e1262
	e1265
	e1268
	e1271
	e1274
	e1277
	e1280
	e1309
	e1312
	e1315
	e1318
	e150
	e151
	e152
	e147
	e148
	e149
	e144
	e145
	e146
	e141
	e142
	e143
	e219
	e220
	e221
	e216
	e217
	e218
	e222
	e223
	e224
	e213
	e214
	e215
	e210
	e211
	e212
	e207
	e208
	e209
	e204
	e205
	e206
	e201
	e202
	e203
	e1229
	e1232
	e1235
	e1238
	e1283
	e1286
	e1289
	e1292
	e1295
	e1298
	e1301
	e1304
	e1307
	e1336
	e1339
	e1342
	e1345
	e1348
	e1351
	e129
	e130
	e131
	e135
	e136
	e137
	e132
	e133
	e134
	e126
	e127
	e128
	e1138
	e1141
	e1144
	e1147
	e1150
	e1322
	e1325
	e1328
	e1331
	e1334
	e153
	e154
	e155
	e168
	e169
	e170
	e162
	e163
	e164
	e165
	e166
	e167
	e156
	e157
	e158
	e159
	e160
	e161
	e195
	e196
	e197
	e192
	e193
	e194
	e186
	e187
	e188
	e183
	e184
	e185
	e189
	e190
	e191
	e180
	e181
	e182
	e174
	e175
	e176
	e177
	e178
	e179
	e171
	e172
	e173
	e198
	e199
	e200
	e117
	e118
	e119
	e114
	e115
	e116
	e111
	e112
	e113
	e1226
	e123
	e124
	e125
	e120
	e121
	e122
	e96
	e97
	e98
	e1201
	e1204
	e1207
	e1210
	e1213
	e1216
	e1219
	e1222
	e1355
	e1358
	e1361
	e1364
	e1367
	e1370
	e1373
	e1376
	e1379
	e99
	e100
	e101
	e102
	e103
	e104
	e105
	e106
	e107
	e108
	e109
	e110
	e9
	e10
	e11
	e6
	e7
	e8
	e3
	e4
	e5
	e0
	e1
	e2
	e1000
	e1003
	e1006
	e1009
	e1013
	e1016
	e1019
	e1022
	e1026
	e1029
	e1032
	e1035
	e1038
	e1041
	e1043
	e1046
	e1050
	e1053
	e1056
	e1059
	e1062
	e1063
	e1067
	e1070
	e1073
	e1076
	e1079
	e1082
	e1084
	e1087
	e1090
	e1093
	e1098
	e1101
	e1104
	e1107
	e1110
	e1113
	e1116
	e1119
	e1122
	e1125
	e1126
	e1129
	e1132
	e1135
	e1139
	e1142
	e1145
	e1148
	e1151
	e1153
	e1156
	e1159
	e1162
	e1165
	e1168
	e1172
	e1175
	e1178
	e1181
	e1184
	e1187
	e1190
	e1193
	e1196
	e1199
	e1203
	e1206
	e1209
	e1212
	e1215
	e1218
	e1221
	e1224
	e1227
	e1242
	e1245
	e1248
	e1251
	e1254
	e1257
	e1260
	e1263
	e1266
	e1269
	e1272
	e1275
	e1278
	e1281
	e1284
	e1287
	e1290
	e1293
	e1296
	e1310
	e1313
	e1316
	e1319
	e1323
	e1326
	e1329
	e1332
	e1335
	e1338
	e1341
	e1344
	e1347
	e1350
	e1353
	e1368
	e1371
	e1374
	e1377
	e1380
	e63
	e64
	e65
	e1025
	e1031
	e1034
	e1037
	e1112
	e1115
	e1118
	e1121
	e1124
	e87
	e88
	e89
	e84
	e85
	e86
	e93
	e94
	e95
	e90
	e91
	e92
	e75
	e76
	e77
	e78
	e79
	e80
	e81
	e82
	e83
	e72
	e73
	e74
	e66
	e67
	e68
	e69
	e70
	e71
	e15
	e16
	e17
	e12
	e13
	e14
	e1001
	e1004
	e1007
	e1010
	e1012
	e1015
	e1018
	e1021
	e1024
	e1027
	e1030
	e1033
	e1036
	e1039
	e1049
	e1052
	e1055
	e1058
	e1061
	e1064
	e1066
	e1069
	e1072
	e1075
	e1078
	e1081
	e1085
	e1088
	e1091
	e1094
	e1097
	e1100
	e1103
	e1106
	e1109
	e1111
	e1114
	e1117
	e1120
	e1123
	e1127
	e1130
	e1133
	e1136
	e1154
	e1157
	e1160
	e1163
	e1166
	e1169
	e1171
	e1174
	e1177
	e1180
	e1183
	e1186
	e1189
	e1192
	e1195
	e1198
	e1202
	e1205
	e1208
	e1211
	e1214
	e1217
	e1220
	e1223
	e1230
	e1233
	e1236
	e1239
	e1299
	e1302
	e1305
	e1308
	e1311
	e1314
	e1317
	e1320
	e1337
	e1340
	e1343
	e1346
	e1349
	e1352
	e1356
	e1359
	e1362
	e1365
	e18
	e19
	e20
	e21
	e22
	e23
	e42
	e43
	e44
	e45
	e46
	e47
	e39
	e40
	e41
	e1028
	e33
	e34
	e35
	e36
	e37
	e38
	e30
	e31
	e32
	e27
	e28
	e29
	e1040
	e24
	e25
	e26
	e1002
	e1005
	e1008
	e1011
	e1042
	e1045
	e1048
	e1051
	e1054
	e1057
	e1060
	e1096
	e1099
	e1102
	e1105
	e1108
	e1128
	e1131
	e1134
	e1137
	e1140
	e1143
	e1146
	e1149
	e1152
	e48
	e49
	e50
	e1065
	e1086
	e1089
	e1092
	e1095
	e1155
	e1158
	e1161
	e1164
	e1167
	e1170
	e60
	e61
	e62
	e57
	e58
	e59
	e54
	e55
	e56
	e51
	e52
	e53
 - edge 
 
	t0
	t1
	t2
	t3
	t4
	t5
	t6
	t7
	t8
	t9
	t10
	t11
	t12
	t13
	t14
	t15
	t16
	t17
	t18
	t19
	t20
	t21
	t22
	t23
	t24
	t25
	t26
	t27
	t28
	t29
	t30
	t31
	t32
	t33
	t34
	t35
	t36
	t37
	t38
	t39
	t40
	t41
	t42
	t43
	t44
	t45
	t46
	t47
	t48
	t49
	t50
	t51
	t52
	t53
	t54
	t55
	t56
	t57
	t58
	t59
	t60
	t61
	t62
	t63
	t64
	t65
	t66
	t67
	t68
	t69
	t70
	t71
	t72
	t73
	t74
	t75
	t76
	t77
	t78
	t79
	t80
	t1000
	t1001
	t1002
	t1003
	t1004
	t1005
	t1006
	t1007
	t1008
	t1009
	t1010
	t1011
	t1012
	t1013
	t1014
	t1015
	t1016
	t1017
	t1018
	t1019
	t1020
	t1021
	t1022
	t1023
	t1024
	t1025
	t1026
	t1027
	t1028
	t1029
	t1030
	t1031
	t1032
	t1033
	t1034
	t1035
	t1036
	t1037
	t1038
	t1039
	t1040
	t1041
	t1042
	t1043
	t1044
	t1045
	t1046
	t1047
	t1048
	t1049
	t1050
	t1051
	t1052
	t1053
	t1054
	t1055
	t1056
	t1057
	t1058
	t1059
	t1060
	t1061
	t1062
	t1063
	t1064
	t1065
	t1066
	t1067
	t1068
	t1069
	t1070
	t1071
	t1072
	t1073
	t1074
	t1075
	t1076
	t1077
	t1078
	t1079
	t1080
 - trajectory 
 )
(:init
(= (confb rob ) cb0 )
(= (confa rob ) ca0 )
(= (traj rob ) t_init )
(= (holding) no_object)
; initial object configurations 
 

(= (confo o1) co50); -x 0.656453 -y 0.0978102 -z 0.689997
(= (confo o2) co141); -x 0.847023 -y 0.0195624 -z 0.690006
(= (confo o3) co40); -x 0.655424 -y -0.0512188 -z 0.690003
(= (confo o4) co144); -x 0.948111 -y 0.144673 -z 0.690009
(= (confo o5) co82); -x 0.868063 -y 0.186782 -z 0.690006
(= (confo o6) co52); -x 0.993291 -y -0.247967 -z 0.690002
(= (confo o7) co222); -x 0.90746 -y -0.348652 -z 0.690008
(= (confo o8) co191); -x 0.684197 -y 0.36144 -z 0.689994
(= (confo o9) co63); -x 0.994533 -y -0.449093 -z 0.690004
(= (confo o10) co1); -x 0.860265 -y 0.311038 -z 0.689994
(= (confo o11) co195); -x 0.85614 -y -0.449808 -z 0.689997
(= (confo o12) co25); -x 0.993502 -y 0.249635 -z 0.690003
(= (confo o13) co179); -x 0.821278 -y -0.082152 -z 0.690006
(= (confo o14) co36); -x 0.656171 -y -0.300737 -z 0.690003
(= (confo o15) co212); -x 0.794307 -y -0.198273 -z 0.690003
(= (confo o16) co105); -x 0.891051 -y -0.205559 -z 0.690006
(= (confo o17) co136); -x 0.614459 -y 0.261324 -z 0.690003
(= (confo o18) co143); -x 0.755238 -y 0.29812 -z 0.69001
(= (confo o19) co216); -x 0.762058 -y 0.00484897 -z 0.690003
(= (confo o20) co168); -x 0.80543 -y -0.399322 -z 0.689995
(= (confo o21) co17); -x 0.882114 -y 0.417691 -z 0.690001
(= (confo o22) co176); -x 0.800138 -y 0.0948207 -z 0.689996
(= (confo o23) co62); -x 0.907883 -y -0.0744512 -z 0.690004
(= (confo o24) co138); -x 0.99759 -y -0.0833308 -z 0.690004
(= (confo o25) co12); -x 0.993558 -y 0.349819 -z 0.689995
(= (confo o26) co60); -x 0.994503 -y 0.0511322 -z 0.689995
(= (confo o27) co22); -x 0.656438 -y -0.399406 -z 0.690003
(= (confo o28) co187); -x 0.793425 -y 0.400128 -z 0.690002
(= (confo o29) co181); -x 0.844954 -y -0.297222 -z 0.690004
(= (confo o30) co88); -x 0.747462 -y 0.195158 -z 0.690004
(= (confo o31) co38); -x 0.931055 -y 0.280175 -z 0.690004
(= (confo o32) co220); -x 0.577549 -y 0.116357 -z 0.690005
(= (confo o33) co137); -x 0.755382 -y -0.351379 -z 0.690004
(= (confo o34) co33); -x 0.938368 -y 0.0206037 -z 0.69
(= (confo o35) co20); -x 0.595292 -y 0.370218 -z 0.689997




)
(:goal (and

;;Place o22 at o10 configuration
;;Place o10 at o22 configuration
;; Place 018, at 029 configuration
(= (confo o22) co1); -x 0.860265 -y 0.311038 -z 0.689994
(= (confo o10) co176); -x 0.800138 -y 0.0948207 -z 0.689996
(= (confo o18) co181); -x 0.844954 -y -0.297222 -z 0.690004

))
(:constraints

(@nonoverlap_ro (confb rob) (traj rob) (confo o1))
(@nonoverlap_ro (confb rob) (traj rob) (confo o2))
(@nonoverlap_ro (confb rob) (traj rob) (confo o3))
(@nonoverlap_ro (confb rob) (traj rob) (confo o4))
(@nonoverlap_ro (confb rob) (traj rob) (confo o5))
(@nonoverlap_ro (confb rob) (traj rob) (confo o6))
(@nonoverlap_ro (confb rob) (traj rob) (confo o7))
(@nonoverlap_ro (confb rob) (traj rob) (confo o8))
(@nonoverlap_ro (confb rob) (traj rob) (confo o9))
(@nonoverlap_ro (confb rob) (traj rob) (confo o10))
(@nonoverlap_ro (confb rob) (traj rob) (confo o11))
(@nonoverlap_ro (confb rob) (traj rob) (confo o12))
(@nonoverlap_ro (confb rob) (traj rob) (confo o13))
(@nonoverlap_ro (confb rob) (traj rob) (confo o14))
(@nonoverlap_ro (confb rob) (traj rob) (confo o15))
(@nonoverlap_ro (confb rob) (traj rob) (confo o16))
(@nonoverlap_ro (confb rob) (traj rob) (confo o17))
(@nonoverlap_ro (confb rob) (traj rob) (confo o18))
(@nonoverlap_ro (confb rob) (traj rob) (confo o19))
(@nonoverlap_ro (confb rob) (traj rob) (confo o20))
(@nonoverlap_ro (confb rob) (traj rob) (confo o21))
(@nonoverlap_ro (confb rob) (traj rob) (confo o22))
(@nonoverlap_ro (confb rob) (traj rob) (confo o23))
(@nonoverlap_ro (confb rob) (traj rob) (confo o24))
(@nonoverlap_ro (confb rob) (traj rob) (confo o25))
(@nonoverlap_ro (confb rob) (traj rob) (confo o26))
(@nonoverlap_ro (confb rob) (traj rob) (confo o27))
(@nonoverlap_ro (confb rob) (traj rob) (confo o28))
(@nonoverlap_ro (confb rob) (traj rob) (confo o29))
(@nonoverlap_ro (confb rob) (traj rob) (confo o30))
(@nonoverlap_ro (confb rob) (traj rob) (confo o31))
(@nonoverlap_ro (confb rob) (traj rob) (confo o32))
(@nonoverlap_ro (confb rob) (traj rob) (confo o33))
(@nonoverlap_ro (confb rob) (traj rob) (confo o34))
(@nonoverlap_ro (confb rob) (traj rob) (confo o35))
(@nonoverlap_oo (confo o1) (confo o2))
(@nonoverlap_oo (confo o1) (confo o3))
(@nonoverlap_oo (confo o1) (confo o4))
(@nonoverlap_oo (confo o1) (confo o5))
(@nonoverlap_oo (confo o1) (confo o6))
(@nonoverlap_oo (confo o1) (confo o7))
(@nonoverlap_oo (confo o1) (confo o8))
(@nonoverlap_oo (confo o1) (confo o9))
(@nonoverlap_oo (confo o1) (confo o10))
(@nonoverlap_oo (confo o1) (confo o11))
(@nonoverlap_oo (confo o1) (confo o12))
(@nonoverlap_oo (confo o1) (confo o13))
(@nonoverlap_oo (confo o1) (confo o14))
(@nonoverlap_oo (confo o1) (confo o15))
(@nonoverlap_oo (confo o1) (confo o16))
(@nonoverlap_oo (confo o1) (confo o17))
(@nonoverlap_oo (confo o1) (confo o18))
(@nonoverlap_oo (confo o1) (confo o19))
(@nonoverlap_oo (confo o1) (confo o20))
(@nonoverlap_oo (confo o1) (confo o21))
(@nonoverlap_oo (confo o1) (confo o22))
(@nonoverlap_oo (confo o1) (confo o23))
(@nonoverlap_oo (confo o1) (confo o24))
(@nonoverlap_oo (confo o1) (confo o25))
(@nonoverlap_oo (confo o1) (confo o26))
(@nonoverlap_oo (confo o1) (confo o27))
(@nonoverlap_oo (confo o1) (confo o28))
(@nonoverlap_oo (confo o1) (confo o29))
(@nonoverlap_oo (confo o1) (confo o30))
(@nonoverlap_oo (confo o1) (confo o31))
(@nonoverlap_oo (confo o1) (confo o32))
(@nonoverlap_oo (confo o1) (confo o33))
(@nonoverlap_oo (confo o1) (confo o34))
(@nonoverlap_oo (confo o1) (confo o35))
(@nonoverlap_oo (confo o2) (confo o3))
(@nonoverlap_oo (confo o2) (confo o4))
(@nonoverlap_oo (confo o2) (confo o5))
(@nonoverlap_oo (confo o2) (confo o6))
(@nonoverlap_oo (confo o2) (confo o7))
(@nonoverlap_oo (confo o2) (confo o8))
(@nonoverlap_oo (confo o2) (confo o9))
(@nonoverlap_oo (confo o2) (confo o10))
(@nonoverlap_oo (confo o2) (confo o11))
(@nonoverlap_oo (confo o2) (confo o12))
(@nonoverlap_oo (confo o2) (confo o13))
(@nonoverlap_oo (confo o2) (confo o14))
(@nonoverlap_oo (confo o2) (confo o15))
(@nonoverlap_oo (confo o2) (confo o16))
(@nonoverlap_oo (confo o2) (confo o17))
(@nonoverlap_oo (confo o2) (confo o18))
(@nonoverlap_oo (confo o2) (confo o19))
(@nonoverlap_oo (confo o2) (confo o20))
(@nonoverlap_oo (confo o2) (confo o21))
(@nonoverlap_oo (confo o2) (confo o22))
(@nonoverlap_oo (confo o2) (confo o23))
(@nonoverlap_oo (confo o2) (confo o24))
(@nonoverlap_oo (confo o2) (confo o25))
(@nonoverlap_oo (confo o2) (confo o26))
(@nonoverlap_oo (confo o2) (confo o27))
(@nonoverlap_oo (confo o2) (confo o28))
(@nonoverlap_oo (confo o2) (confo o29))
(@nonoverlap_oo (confo o2) (confo o30))
(@nonoverlap_oo (confo o2) (confo o31))
(@nonoverlap_oo (confo o2) (confo o32))
(@nonoverlap_oo (confo o2) (confo o33))
(@nonoverlap_oo (confo o2) (confo o34))
(@nonoverlap_oo (confo o2) (confo o35))
(@nonoverlap_oo (confo o3) (confo o4))
(@nonoverlap_oo (confo o3) (confo o5))
(@nonoverlap_oo (confo o3) (confo o6))
(@nonoverlap_oo (confo o3) (confo o7))
(@nonoverlap_oo (confo o3) (confo o8))
(@nonoverlap_oo (confo o3) (confo o9))
(@nonoverlap_oo (confo o3) (confo o10))
(@nonoverlap_oo (confo o3) (confo o11))
(@nonoverlap_oo (confo o3) (confo o12))
(@nonoverlap_oo (confo o3) (confo o13))
(@nonoverlap_oo (confo o3) (confo o14))
(@nonoverlap_oo (confo o3) (confo o15))
(@nonoverlap_oo (confo o3) (confo o16))
(@nonoverlap_oo (confo o3) (confo o17))
(@nonoverlap_oo (confo o3) (confo o18))
(@nonoverlap_oo (confo o3) (confo o19))
(@nonoverlap_oo (confo o3) (confo o20))
(@nonoverlap_oo (confo o3) (confo o21))
(@nonoverlap_oo (confo o3) (confo o22))
(@nonoverlap_oo (confo o3) (confo o23))
(@nonoverlap_oo (confo o3) (confo o24))
(@nonoverlap_oo (confo o3) (confo o25))
(@nonoverlap_oo (confo o3) (confo o26))
(@nonoverlap_oo (confo o3) (confo o27))
(@nonoverlap_oo (confo o3) (confo o28))
(@nonoverlap_oo (confo o3) (confo o29))
(@nonoverlap_oo (confo o3) (confo o30))
(@nonoverlap_oo (confo o3) (confo o31))
(@nonoverlap_oo (confo o3) (confo o32))
(@nonoverlap_oo (confo o3) (confo o33))
(@nonoverlap_oo (confo o3) (confo o34))
(@nonoverlap_oo (confo o3) (confo o35))
(@nonoverlap_oo (confo o4) (confo o5))
(@nonoverlap_oo (confo o4) (confo o6))
(@nonoverlap_oo (confo o4) (confo o7))
(@nonoverlap_oo (confo o4) (confo o8))
(@nonoverlap_oo (confo o4) (confo o9))
(@nonoverlap_oo (confo o4) (confo o10))
(@nonoverlap_oo (confo o4) (confo o11))
(@nonoverlap_oo (confo o4) (confo o12))
(@nonoverlap_oo (confo o4) (confo o13))
(@nonoverlap_oo (confo o4) (confo o14))
(@nonoverlap_oo (confo o4) (confo o15))
(@nonoverlap_oo (confo o4) (confo o16))
(@nonoverlap_oo (confo o4) (confo o17))
(@nonoverlap_oo (confo o4) (confo o18))
(@nonoverlap_oo (confo o4) (confo o19))
(@nonoverlap_oo (confo o4) (confo o20))
(@nonoverlap_oo (confo o4) (confo o21))
(@nonoverlap_oo (confo o4) (confo o22))
(@nonoverlap_oo (confo o4) (confo o23))
(@nonoverlap_oo (confo o4) (confo o24))
(@nonoverlap_oo (confo o4) (confo o25))
(@nonoverlap_oo (confo o4) (confo o26))
(@nonoverlap_oo (confo o4) (confo o27))
(@nonoverlap_oo (confo o4) (confo o28))
(@nonoverlap_oo (confo o4) (confo o29))
(@nonoverlap_oo (confo o4) (confo o30))
(@nonoverlap_oo (confo o4) (confo o31))
(@nonoverlap_oo (confo o4) (confo o32))
(@nonoverlap_oo (confo o4) (confo o33))
(@nonoverlap_oo (confo o4) (confo o34))
(@nonoverlap_oo (confo o4) (confo o35))
(@nonoverlap_oo (confo o5) (confo o6))
(@nonoverlap_oo (confo o5) (confo o7))
(@nonoverlap_oo (confo o5) (confo o8))
(@nonoverlap_oo (confo o5) (confo o9))
(@nonoverlap_oo (confo o5) (confo o10))
(@nonoverlap_oo (confo o5) (confo o11))
(@nonoverlap_oo (confo o5) (confo o12))
(@nonoverlap_oo (confo o5) (confo o13))
(@nonoverlap_oo (confo o5) (confo o14))
(@nonoverlap_oo (confo o5) (confo o15))
(@nonoverlap_oo (confo o5) (confo o16))
(@nonoverlap_oo (confo o5) (confo o17))
(@nonoverlap_oo (confo o5) (confo o18))
(@nonoverlap_oo (confo o5) (confo o19))
(@nonoverlap_oo (confo o5) (confo o20))
(@nonoverlap_oo (confo o5) (confo o21))
(@nonoverlap_oo (confo o5) (confo o22))
(@nonoverlap_oo (confo o5) (confo o23))
(@nonoverlap_oo (confo o5) (confo o24))
(@nonoverlap_oo (confo o5) (confo o25))
(@nonoverlap_oo (confo o5) (confo o26))
(@nonoverlap_oo (confo o5) (confo o27))
(@nonoverlap_oo (confo o5) (confo o28))
(@nonoverlap_oo (confo o5) (confo o29))
(@nonoverlap_oo (confo o5) (confo o30))
(@nonoverlap_oo (confo o5) (confo o31))
(@nonoverlap_oo (confo o5) (confo o32))
(@nonoverlap_oo (confo o5) (confo o33))
(@nonoverlap_oo (confo o5) (confo o34))
(@nonoverlap_oo (confo o5) (confo o35))
(@nonoverlap_oo (confo o6) (confo o7))
(@nonoverlap_oo (confo o6) (confo o8))
(@nonoverlap_oo (confo o6) (confo o9))
(@nonoverlap_oo (confo o6) (confo o10))
(@nonoverlap_oo (confo o6) (confo o11))
(@nonoverlap_oo (confo o6) (confo o12))
(@nonoverlap_oo (confo o6) (confo o13))
(@nonoverlap_oo (confo o6) (confo o14))
(@nonoverlap_oo (confo o6) (confo o15))
(@nonoverlap_oo (confo o6) (confo o16))
(@nonoverlap_oo (confo o6) (confo o17))
(@nonoverlap_oo (confo o6) (confo o18))
(@nonoverlap_oo (confo o6) (confo o19))
(@nonoverlap_oo (confo o6) (confo o20))
(@nonoverlap_oo (confo o6) (confo o21))
(@nonoverlap_oo (confo o6) (confo o22))
(@nonoverlap_oo (confo o6) (confo o23))
(@nonoverlap_oo (confo o6) (confo o24))
(@nonoverlap_oo (confo o6) (confo o25))
(@nonoverlap_oo (confo o6) (confo o26))
(@nonoverlap_oo (confo o6) (confo o27))
(@nonoverlap_oo (confo o6) (confo o28))
(@nonoverlap_oo (confo o6) (confo o29))
(@nonoverlap_oo (confo o6) (confo o30))
(@nonoverlap_oo (confo o6) (confo o31))
(@nonoverlap_oo (confo o6) (confo o32))
(@nonoverlap_oo (confo o6) (confo o33))
(@nonoverlap_oo (confo o6) (confo o34))
(@nonoverlap_oo (confo o6) (confo o35))
(@nonoverlap_oo (confo o7) (confo o8))
(@nonoverlap_oo (confo o7) (confo o9))
(@nonoverlap_oo (confo o7) (confo o10))
(@nonoverlap_oo (confo o7) (confo o11))
(@nonoverlap_oo (confo o7) (confo o12))
(@nonoverlap_oo (confo o7) (confo o13))
(@nonoverlap_oo (confo o7) (confo o14))
(@nonoverlap_oo (confo o7) (confo o15))
(@nonoverlap_oo (confo o7) (confo o16))
(@nonoverlap_oo (confo o7) (confo o17))
(@nonoverlap_oo (confo o7) (confo o18))
(@nonoverlap_oo (confo o7) (confo o19))
(@nonoverlap_oo (confo o7) (confo o20))
(@nonoverlap_oo (confo o7) (confo o21))
(@nonoverlap_oo (confo o7) (confo o22))
(@nonoverlap_oo (confo o7) (confo o23))
(@nonoverlap_oo (confo o7) (confo o24))
(@nonoverlap_oo (confo o7) (confo o25))
(@nonoverlap_oo (confo o7) (confo o26))
(@nonoverlap_oo (confo o7) (confo o27))
(@nonoverlap_oo (confo o7) (confo o28))
(@nonoverlap_oo (confo o7) (confo o29))
(@nonoverlap_oo (confo o7) (confo o30))
(@nonoverlap_oo (confo o7) (confo o31))
(@nonoverlap_oo (confo o7) (confo o32))
(@nonoverlap_oo (confo o7) (confo o33))
(@nonoverlap_oo (confo o7) (confo o34))
(@nonoverlap_oo (confo o7) (confo o35))
(@nonoverlap_oo (confo o8) (confo o9))
(@nonoverlap_oo (confo o8) (confo o10))
(@nonoverlap_oo (confo o8) (confo o11))
(@nonoverlap_oo (confo o8) (confo o12))
(@nonoverlap_oo (confo o8) (confo o13))
(@nonoverlap_oo (confo o8) (confo o14))
(@nonoverlap_oo (confo o8) (confo o15))
(@nonoverlap_oo (confo o8) (confo o16))
(@nonoverlap_oo (confo o8) (confo o17))
(@nonoverlap_oo (confo o8) (confo o18))
(@nonoverlap_oo (confo o8) (confo o19))
(@nonoverlap_oo (confo o8) (confo o20))
(@nonoverlap_oo (confo o8) (confo o21))
(@nonoverlap_oo (confo o8) (confo o22))
(@nonoverlap_oo (confo o8) (confo o23))
(@nonoverlap_oo (confo o8) (confo o24))
(@nonoverlap_oo (confo o8) (confo o25))
(@nonoverlap_oo (confo o8) (confo o26))
(@nonoverlap_oo (confo o8) (confo o27))
(@nonoverlap_oo (confo o8) (confo o28))
(@nonoverlap_oo (confo o8) (confo o29))
(@nonoverlap_oo (confo o8) (confo o30))
(@nonoverlap_oo (confo o8) (confo o31))
(@nonoverlap_oo (confo o8) (confo o32))
(@nonoverlap_oo (confo o8) (confo o33))
(@nonoverlap_oo (confo o8) (confo o34))
(@nonoverlap_oo (confo o8) (confo o35))
(@nonoverlap_oo (confo o9) (confo o10))
(@nonoverlap_oo (confo o9) (confo o11))
(@nonoverlap_oo (confo o9) (confo o12))
(@nonoverlap_oo (confo o9) (confo o13))
(@nonoverlap_oo (confo o9) (confo o14))
(@nonoverlap_oo (confo o9) (confo o15))
(@nonoverlap_oo (confo o9) (confo o16))
(@nonoverlap_oo (confo o9) (confo o17))
(@nonoverlap_oo (confo o9) (confo o18))
(@nonoverlap_oo (confo o9) (confo o19))
(@nonoverlap_oo (confo o9) (confo o20))
(@nonoverlap_oo (confo o9) (confo o21))
(@nonoverlap_oo (confo o9) (confo o22))
(@nonoverlap_oo (confo o9) (confo o23))
(@nonoverlap_oo (confo o9) (confo o24))
(@nonoverlap_oo (confo o9) (confo o25))
(@nonoverlap_oo (confo o9) (confo o26))
(@nonoverlap_oo (confo o9) (confo o27))
(@nonoverlap_oo (confo o9) (confo o28))
(@nonoverlap_oo (confo o9) (confo o29))
(@nonoverlap_oo (confo o9) (confo o30))
(@nonoverlap_oo (confo o9) (confo o31))
(@nonoverlap_oo (confo o9) (confo o32))
(@nonoverlap_oo (confo o9) (confo o33))
(@nonoverlap_oo (confo o9) (confo o34))
(@nonoverlap_oo (confo o9) (confo o35))
(@nonoverlap_oo (confo o10) (confo o11))
(@nonoverlap_oo (confo o10) (confo o12))
(@nonoverlap_oo (confo o10) (confo o13))
(@nonoverlap_oo (confo o10) (confo o14))
(@nonoverlap_oo (confo o10) (confo o15))
(@nonoverlap_oo (confo o10) (confo o16))
(@nonoverlap_oo (confo o10) (confo o17))
(@nonoverlap_oo (confo o10) (confo o18))
(@nonoverlap_oo (confo o10) (confo o19))
(@nonoverlap_oo (confo o10) (confo o20))
(@nonoverlap_oo (confo o10) (confo o21))
(@nonoverlap_oo (confo o10) (confo o22))
(@nonoverlap_oo (confo o10) (confo o23))
(@nonoverlap_oo (confo o10) (confo o24))
(@nonoverlap_oo (confo o10) (confo o25))
(@nonoverlap_oo (confo o10) (confo o26))
(@nonoverlap_oo (confo o10) (confo o27))
(@nonoverlap_oo (confo o10) (confo o28))
(@nonoverlap_oo (confo o10) (confo o29))
(@nonoverlap_oo (confo o10) (confo o30))
(@nonoverlap_oo (confo o10) (confo o31))
(@nonoverlap_oo (confo o10) (confo o32))
(@nonoverlap_oo (confo o10) (confo o33))
(@nonoverlap_oo (confo o10) (confo o34))
(@nonoverlap_oo (confo o10) (confo o35))
(@nonoverlap_oo (confo o11) (confo o12))
(@nonoverlap_oo (confo o11) (confo o13))
(@nonoverlap_oo (confo o11) (confo o14))
(@nonoverlap_oo (confo o11) (confo o15))
(@nonoverlap_oo (confo o11) (confo o16))
(@nonoverlap_oo (confo o11) (confo o17))
(@nonoverlap_oo (confo o11) (confo o18))
(@nonoverlap_oo (confo o11) (confo o19))
(@nonoverlap_oo (confo o11) (confo o20))
(@nonoverlap_oo (confo o11) (confo o21))
(@nonoverlap_oo (confo o11) (confo o22))
(@nonoverlap_oo (confo o11) (confo o23))
(@nonoverlap_oo (confo o11) (confo o24))
(@nonoverlap_oo (confo o11) (confo o25))
(@nonoverlap_oo (confo o11) (confo o26))
(@nonoverlap_oo (confo o11) (confo o27))
(@nonoverlap_oo (confo o11) (confo o28))
(@nonoverlap_oo (confo o11) (confo o29))
(@nonoverlap_oo (confo o11) (confo o30))
(@nonoverlap_oo (confo o11) (confo o31))
(@nonoverlap_oo (confo o11) (confo o32))
(@nonoverlap_oo (confo o11) (confo o33))
(@nonoverlap_oo (confo o11) (confo o34))
(@nonoverlap_oo (confo o11) (confo o35))
(@nonoverlap_oo (confo o12) (confo o13))
(@nonoverlap_oo (confo o12) (confo o14))
(@nonoverlap_oo (confo o12) (confo o15))
(@nonoverlap_oo (confo o12) (confo o16))
(@nonoverlap_oo (confo o12) (confo o17))
(@nonoverlap_oo (confo o12) (confo o18))
(@nonoverlap_oo (confo o12) (confo o19))
(@nonoverlap_oo (confo o12) (confo o20))
(@nonoverlap_oo (confo o12) (confo o21))
(@nonoverlap_oo (confo o12) (confo o22))
(@nonoverlap_oo (confo o12) (confo o23))
(@nonoverlap_oo (confo o12) (confo o24))
(@nonoverlap_oo (confo o12) (confo o25))
(@nonoverlap_oo (confo o12) (confo o26))
(@nonoverlap_oo (confo o12) (confo o27))
(@nonoverlap_oo (confo o12) (confo o28))
(@nonoverlap_oo (confo o12) (confo o29))
(@nonoverlap_oo (confo o12) (confo o30))
(@nonoverlap_oo (confo o12) (confo o31))
(@nonoverlap_oo (confo o12) (confo o32))
(@nonoverlap_oo (confo o12) (confo o33))
(@nonoverlap_oo (confo o12) (confo o34))
(@nonoverlap_oo (confo o12) (confo o35))
(@nonoverlap_oo (confo o13) (confo o14))
(@nonoverlap_oo (confo o13) (confo o15))
(@nonoverlap_oo (confo o13) (confo o16))
(@nonoverlap_oo (confo o13) (confo o17))
(@nonoverlap_oo (confo o13) (confo o18))
(@nonoverlap_oo (confo o13) (confo o19))
(@nonoverlap_oo (confo o13) (confo o20))
(@nonoverlap_oo (confo o13) (confo o21))
(@nonoverlap_oo (confo o13) (confo o22))
(@nonoverlap_oo (confo o13) (confo o23))
(@nonoverlap_oo (confo o13) (confo o24))
(@nonoverlap_oo (confo o13) (confo o25))
(@nonoverlap_oo (confo o13) (confo o26))
(@nonoverlap_oo (confo o13) (confo o27))
(@nonoverlap_oo (confo o13) (confo o28))
(@nonoverlap_oo (confo o13) (confo o29))
(@nonoverlap_oo (confo o13) (confo o30))
(@nonoverlap_oo (confo o13) (confo o31))
(@nonoverlap_oo (confo o13) (confo o32))
(@nonoverlap_oo (confo o13) (confo o33))
(@nonoverlap_oo (confo o13) (confo o34))
(@nonoverlap_oo (confo o13) (confo o35))
(@nonoverlap_oo (confo o14) (confo o15))
(@nonoverlap_oo (confo o14) (confo o16))
(@nonoverlap_oo (confo o14) (confo o17))
(@nonoverlap_oo (confo o14) (confo o18))
(@nonoverlap_oo (confo o14) (confo o19))
(@nonoverlap_oo (confo o14) (confo o20))
(@nonoverlap_oo (confo o14) (confo o21))
(@nonoverlap_oo (confo o14) (confo o22))
(@nonoverlap_oo (confo o14) (confo o23))
(@nonoverlap_oo (confo o14) (confo o24))
(@nonoverlap_oo (confo o14) (confo o25))
(@nonoverlap_oo (confo o14) (confo o26))
(@nonoverlap_oo (confo o14) (confo o27))
(@nonoverlap_oo (confo o14) (confo o28))
(@nonoverlap_oo (confo o14) (confo o29))
(@nonoverlap_oo (confo o14) (confo o30))
(@nonoverlap_oo (confo o14) (confo o31))
(@nonoverlap_oo (confo o14) (confo o32))
(@nonoverlap_oo (confo o14) (confo o33))
(@nonoverlap_oo (confo o14) (confo o34))
(@nonoverlap_oo (confo o14) (confo o35))
(@nonoverlap_oo (confo o15) (confo o16))
(@nonoverlap_oo (confo o15) (confo o17))
(@nonoverlap_oo (confo o15) (confo o18))
(@nonoverlap_oo (confo o15) (confo o19))
(@nonoverlap_oo (confo o15) (confo o20))
(@nonoverlap_oo (confo o15) (confo o21))
(@nonoverlap_oo (confo o15) (confo o22))
(@nonoverlap_oo (confo o15) (confo o23))
(@nonoverlap_oo (confo o15) (confo o24))
(@nonoverlap_oo (confo o15) (confo o25))
(@nonoverlap_oo (confo o15) (confo o26))
(@nonoverlap_oo (confo o15) (confo o27))
(@nonoverlap_oo (confo o15) (confo o28))
(@nonoverlap_oo (confo o15) (confo o29))
(@nonoverlap_oo (confo o15) (confo o30))
(@nonoverlap_oo (confo o15) (confo o31))
(@nonoverlap_oo (confo o15) (confo o32))
(@nonoverlap_oo (confo o15) (confo o33))
(@nonoverlap_oo (confo o15) (confo o34))
(@nonoverlap_oo (confo o15) (confo o35))
(@nonoverlap_oo (confo o16) (confo o17))
(@nonoverlap_oo (confo o16) (confo o18))
(@nonoverlap_oo (confo o16) (confo o19))
(@nonoverlap_oo (confo o16) (confo o20))
(@nonoverlap_oo (confo o16) (confo o21))
(@nonoverlap_oo (confo o16) (confo o22))
(@nonoverlap_oo (confo o16) (confo o23))
(@nonoverlap_oo (confo o16) (confo o24))
(@nonoverlap_oo (confo o16) (confo o25))
(@nonoverlap_oo (confo o16) (confo o26))
(@nonoverlap_oo (confo o16) (confo o27))
(@nonoverlap_oo (confo o16) (confo o28))
(@nonoverlap_oo (confo o16) (confo o29))
(@nonoverlap_oo (confo o16) (confo o30))
(@nonoverlap_oo (confo o16) (confo o31))
(@nonoverlap_oo (confo o16) (confo o32))
(@nonoverlap_oo (confo o16) (confo o33))
(@nonoverlap_oo (confo o16) (confo o34))
(@nonoverlap_oo (confo o16) (confo o35))
(@nonoverlap_oo (confo o17) (confo o18))
(@nonoverlap_oo (confo o17) (confo o19))
(@nonoverlap_oo (confo o17) (confo o20))
(@nonoverlap_oo (confo o17) (confo o21))
(@nonoverlap_oo (confo o17) (confo o22))
(@nonoverlap_oo (confo o17) (confo o23))
(@nonoverlap_oo (confo o17) (confo o24))
(@nonoverlap_oo (confo o17) (confo o25))
(@nonoverlap_oo (confo o17) (confo o26))
(@nonoverlap_oo (confo o17) (confo o27))
(@nonoverlap_oo (confo o17) (confo o28))
(@nonoverlap_oo (confo o17) (confo o29))
(@nonoverlap_oo (confo o17) (confo o30))
(@nonoverlap_oo (confo o17) (confo o31))
(@nonoverlap_oo (confo o17) (confo o32))
(@nonoverlap_oo (confo o17) (confo o33))
(@nonoverlap_oo (confo o17) (confo o34))
(@nonoverlap_oo (confo o17) (confo o35))
(@nonoverlap_oo (confo o18) (confo o19))
(@nonoverlap_oo (confo o18) (confo o20))
(@nonoverlap_oo (confo o18) (confo o21))
(@nonoverlap_oo (confo o18) (confo o22))
(@nonoverlap_oo (confo o18) (confo o23))
(@nonoverlap_oo (confo o18) (confo o24))
(@nonoverlap_oo (confo o18) (confo o25))
(@nonoverlap_oo (confo o18) (confo o26))
(@nonoverlap_oo (confo o18) (confo o27))
(@nonoverlap_oo (confo o18) (confo o28))
(@nonoverlap_oo (confo o18) (confo o29))
(@nonoverlap_oo (confo o18) (confo o30))
(@nonoverlap_oo (confo o18) (confo o31))
(@nonoverlap_oo (confo o18) (confo o32))
(@nonoverlap_oo (confo o18) (confo o33))
(@nonoverlap_oo (confo o18) (confo o34))
(@nonoverlap_oo (confo o18) (confo o35))
(@nonoverlap_oo (confo o19) (confo o20))
(@nonoverlap_oo (confo o19) (confo o21))
(@nonoverlap_oo (confo o19) (confo o22))
(@nonoverlap_oo (confo o19) (confo o23))
(@nonoverlap_oo (confo o19) (confo o24))
(@nonoverlap_oo (confo o19) (confo o25))
(@nonoverlap_oo (confo o19) (confo o26))
(@nonoverlap_oo (confo o19) (confo o27))
(@nonoverlap_oo (confo o19) (confo o28))
(@nonoverlap_oo (confo o19) (confo o29))
(@nonoverlap_oo (confo o19) (confo o30))
(@nonoverlap_oo (confo o19) (confo o31))
(@nonoverlap_oo (confo o19) (confo o32))
(@nonoverlap_oo (confo o19) (confo o33))
(@nonoverlap_oo (confo o19) (confo o34))
(@nonoverlap_oo (confo o19) (confo o35))
(@nonoverlap_oo (confo o20) (confo o21))
(@nonoverlap_oo (confo o20) (confo o22))
(@nonoverlap_oo (confo o20) (confo o23))
(@nonoverlap_oo (confo o20) (confo o24))
(@nonoverlap_oo (confo o20) (confo o25))
(@nonoverlap_oo (confo o20) (confo o26))
(@nonoverlap_oo (confo o20) (confo o27))
(@nonoverlap_oo (confo o20) (confo o28))
(@nonoverlap_oo (confo o20) (confo o29))
(@nonoverlap_oo (confo o20) (confo o30))
(@nonoverlap_oo (confo o20) (confo o31))
(@nonoverlap_oo (confo o20) (confo o32))
(@nonoverlap_oo (confo o20) (confo o33))
(@nonoverlap_oo (confo o20) (confo o34))
(@nonoverlap_oo (confo o20) (confo o35))
(@nonoverlap_oo (confo o21) (confo o22))
(@nonoverlap_oo (confo o21) (confo o23))
(@nonoverlap_oo (confo o21) (confo o24))
(@nonoverlap_oo (confo o21) (confo o25))
(@nonoverlap_oo (confo o21) (confo o26))
(@nonoverlap_oo (confo o21) (confo o27))
(@nonoverlap_oo (confo o21) (confo o28))
(@nonoverlap_oo (confo o21) (confo o29))
(@nonoverlap_oo (confo o21) (confo o30))
(@nonoverlap_oo (confo o21) (confo o31))
(@nonoverlap_oo (confo o21) (confo o32))
(@nonoverlap_oo (confo o21) (confo o33))
(@nonoverlap_oo (confo o21) (confo o34))
(@nonoverlap_oo (confo o21) (confo o35))
(@nonoverlap_oo (confo o22) (confo o23))
(@nonoverlap_oo (confo o22) (confo o24))
(@nonoverlap_oo (confo o22) (confo o25))
(@nonoverlap_oo (confo o22) (confo o26))
(@nonoverlap_oo (confo o22) (confo o27))
(@nonoverlap_oo (confo o22) (confo o28))
(@nonoverlap_oo (confo o22) (confo o29))
(@nonoverlap_oo (confo o22) (confo o30))
(@nonoverlap_oo (confo o22) (confo o31))
(@nonoverlap_oo (confo o22) (confo o32))
(@nonoverlap_oo (confo o22) (confo o33))
(@nonoverlap_oo (confo o22) (confo o34))
(@nonoverlap_oo (confo o22) (confo o35))
(@nonoverlap_oo (confo o23) (confo o24))
(@nonoverlap_oo (confo o23) (confo o25))
(@nonoverlap_oo (confo o23) (confo o26))
(@nonoverlap_oo (confo o23) (confo o27))
(@nonoverlap_oo (confo o23) (confo o28))
(@nonoverlap_oo (confo o23) (confo o29))
(@nonoverlap_oo (confo o23) (confo o30))
(@nonoverlap_oo (confo o23) (confo o31))
(@nonoverlap_oo (confo o23) (confo o32))
(@nonoverlap_oo (confo o23) (confo o33))
(@nonoverlap_oo (confo o23) (confo o34))
(@nonoverlap_oo (confo o23) (confo o35))
(@nonoverlap_oo (confo o24) (confo o25))
(@nonoverlap_oo (confo o24) (confo o26))
(@nonoverlap_oo (confo o24) (confo o27))
(@nonoverlap_oo (confo o24) (confo o28))
(@nonoverlap_oo (confo o24) (confo o29))
(@nonoverlap_oo (confo o24) (confo o30))
(@nonoverlap_oo (confo o24) (confo o31))
(@nonoverlap_oo (confo o24) (confo o32))
(@nonoverlap_oo (confo o24) (confo o33))
(@nonoverlap_oo (confo o24) (confo o34))
(@nonoverlap_oo (confo o24) (confo o35))
(@nonoverlap_oo (confo o25) (confo o26))
(@nonoverlap_oo (confo o25) (confo o27))
(@nonoverlap_oo (confo o25) (confo o28))
(@nonoverlap_oo (confo o25) (confo o29))
(@nonoverlap_oo (confo o25) (confo o30))
(@nonoverlap_oo (confo o25) (confo o31))
(@nonoverlap_oo (confo o25) (confo o32))
(@nonoverlap_oo (confo o25) (confo o33))
(@nonoverlap_oo (confo o25) (confo o34))
(@nonoverlap_oo (confo o25) (confo o35))
(@nonoverlap_oo (confo o26) (confo o27))
(@nonoverlap_oo (confo o26) (confo o28))
(@nonoverlap_oo (confo o26) (confo o29))
(@nonoverlap_oo (confo o26) (confo o30))
(@nonoverlap_oo (confo o26) (confo o31))
(@nonoverlap_oo (confo o26) (confo o32))
(@nonoverlap_oo (confo o26) (confo o33))
(@nonoverlap_oo (confo o26) (confo o34))
(@nonoverlap_oo (confo o26) (confo o35))
(@nonoverlap_oo (confo o27) (confo o28))
(@nonoverlap_oo (confo o27) (confo o29))
(@nonoverlap_oo (confo o27) (confo o30))
(@nonoverlap_oo (confo o27) (confo o31))
(@nonoverlap_oo (confo o27) (confo o32))
(@nonoverlap_oo (confo o27) (confo o33))
(@nonoverlap_oo (confo o27) (confo o34))
(@nonoverlap_oo (confo o27) (confo o35))
(@nonoverlap_oo (confo o28) (confo o29))
(@nonoverlap_oo (confo o28) (confo o30))
(@nonoverlap_oo (confo o28) (confo o31))
(@nonoverlap_oo (confo o28) (confo o32))
(@nonoverlap_oo (confo o28) (confo o33))
(@nonoverlap_oo (confo o28) (confo o34))
(@nonoverlap_oo (confo o28) (confo o35))
(@nonoverlap_oo (confo o29) (confo o30))
(@nonoverlap_oo (confo o29) (confo o31))
(@nonoverlap_oo (confo o29) (confo o32))
(@nonoverlap_oo (confo o29) (confo o33))
(@nonoverlap_oo (confo o29) (confo o34))
(@nonoverlap_oo (confo o29) (confo o35))
(@nonoverlap_oo (confo o30) (confo o31))
(@nonoverlap_oo (confo o30) (confo o32))
(@nonoverlap_oo (confo o30) (confo o33))
(@nonoverlap_oo (confo o30) (confo o34))
(@nonoverlap_oo (confo o30) (confo o35))
(@nonoverlap_oo (confo o31) (confo o32))
(@nonoverlap_oo (confo o31) (confo o33))
(@nonoverlap_oo (confo o31) (confo o34))
(@nonoverlap_oo (confo o31) (confo o35))
(@nonoverlap_oo (confo o32) (confo o33))
(@nonoverlap_oo (confo o32) (confo o34))
(@nonoverlap_oo (confo o32) (confo o35))
(@nonoverlap_oo (confo o33) (confo o34))
(@nonoverlap_oo (confo o33) (confo o35))
(@nonoverlap_oo (confo o34) (confo o35))


)
)
