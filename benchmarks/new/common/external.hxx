
#pragma once

#include <fstream>
#include <unordered_set>
#include <unordered_map>
#include <boost/serialization/serialization.hpp>
#include <boost/serialization/map.hpp>
#include <boost/serialization/set.hpp>
#include <boost/serialization/vector.hpp>
#include <boost/archive/text_iarchive.hpp>
#include <boost/archive/text_oarchive.hpp>

#include <aptk2/tools/logging.hxx>
#include <utils/serialize_tuple.hxx>
#include <problem_info.hxx>
#include <utils/external.hxx>
#include <languages/fstrips/builtin.hxx>
#include <boost/container/flat_set.hpp>
#include <boost/functional/hash/extensions.hpp>
#include <utils/tuple_hash.hxx>



namespace fs = fs0::language::fstrips;
using namespace fs0;

//!  An table of overlaps containing an entry (x,y) if trajectory x overlaps with (virtual) object configuration y
using OverlapTable = boost::container::flat_set<std::pair<int, int>>;
// using ObjectMapping = boost::container::flat_map<std::pair<unsigned, unsigned>, unsigned>;
using ObjectMapping = std::unordered_map<std::pair<unsigned, unsigned>, unsigned, boost::hash<std::pair<int, int>>>;
using MultiObjectMapping = std::unordered_map<std::pair<unsigned, unsigned>, std::set<unsigned>, boost::hash<std::pair<int, int>>>;

// Alternative data structures:
// using OverlapTable = std::set<std::pair<int, int>, boost::hash<std::pair<int, int>>>;
// using OverlapTable = std::unordered_set<std::pair<int, int>, boost::hash<std::pair<int, int>>>;

//! An index with all overlaps, mapping each pair (real_conf_base, trajectory_arm) to 
//! all the (real) object configurations that overlap with the arm trajectory
using OverlapMap = boost::container::flat_map<std::pair<ObjectIdx, ObjectIdx>, std::vector<ObjectIdx>>;

using row = std::tuple<unsigned, unsigned, unsigned>;
using table = std::vector<row>;

//! A map from pairs of (configuration, edge) to a new configuration
using SerializedTranslationMap = std::map<std::pair<int, int>, int>;
// using OTranslationMap = boost::container::flat_map<std::pair<int, int>, int>;
// using OTranslationMap = SerializedTranslationMap;
using OTranslationMap = std::unordered_map<std::pair<int, int>, int, boost::hash<std::pair<int, int>>>;

using PoseMap = OTranslationMap;



template <typename T>
void deserialize(const std::string& filename, T& data) {
	std::ifstream ifs(filename);
	if(ifs.is_open()){
		boost::archive::text_iarchive iarch(ifs);
		iarch >> data;
	}
	ifs.close();
}



class External : public fs0::ExternalI {
public:
	External(const ProblemInfo& info, const std::string& data_dir) :
		_overlaps_when_empty(load_overlaps(info, data_dir + "/overlaps_empty.boost")),
		_overlaps_when_holding(load_overlaps(info, data_dir + "/overlaps_holding.boost")),
		_relative_to_real(),
		_real_to_relative(load_relative_mapping(info, data_dir + "/real_relative.boost")),
		_offending_when_holding(),
		_offending_when_empty(),
		_sources(info.getNumObjects(), -1),
		_targets(info.getNumObjects(), -1),
		_placings(info.getNumObjects(), -1),
		_no_object_id(info.getObjectId("no_object")),
		_c_held_id(info.getObjectId("c_held")),
		_ca0(info.getObjectId("ca0"))
    {
		load_real_to_virtual_maps(info, data_dir + "/real_virtual.boost");
		load_graph(info, data_dir + "/base_graph.boost", {"cb", "e", "cb"});
		load_graph(info, data_dir + "/arm_graph.boost", {"ca", "t", "ca"});
		load_placings(info, data_dir + "/placing_poses.boost");
		_offending_when_holding = index_offending(info, _overlaps_when_holding);
		_offending_when_empty = index_offending(info, _overlaps_when_empty);
	}

	~External() = default;

    void registerComponents() const;
	
protected:

	//! Overlaps between trajectory and virtual object configuration when the arm is empty
	const OverlapTable _overlaps_when_empty; 

	//! Overlaps between trajectory and virtual object configuration when the arm is holding some object
	const OverlapTable _overlaps_when_holding;

	//! A mapping between (base_conf, real_obj_conf) and virtual_obj_conf
	ObjectMapping _real_to_virtual_map;
	
	MultiObjectMapping _relative_to_real;
	ObjectMapping _real_to_relative;
	
	
	//! A mapping between (base_conf, virtual_obj_conf) and real_obj_conf
	ObjectMapping _virtual_to_real_map;
	
	//! An index with all overlaps, mapping each pair (conf_base, trajectory_arm) to 
	//! all the object configurations that overlap with the arm trajectory
	OverlapMap _offending_when_holding;
	OverlapMap _offending_when_empty;
	
	std::vector<int> _sources;
	std::vector<int> _targets;
	
	//! A mapping from arm configurations to the object configuration in which the object can be left / picked in that particular arm config
	std::vector<int> _placings;
	

	const ObjectIdx _no_object_id;
	const ObjectIdx _c_held_id;
	const ObjectIdx _ca0;

	
	static bool check_is_relative_id(int id) { return id >= 10000; }
	static bool check_is_real_id(int id) { return id > 1000 && id < 10000 ; }
	static bool check_is_virtual_id(int id) { return id >= 0 && id < 1000; }


	OverlapTable load_overlaps(const ProblemInfo& info, const std::string& filename) {
		
		LPT_DEBUG("data-overlaps", "Loading overlaps from '" << filename << "'");
		
		using data_t = std::set<std::pair<unsigned, unsigned>>;
		data_t data;
		deserialize(filename, data);

		OverlapTable overlaps;

		for (const auto& elem:data) {
			std::string trajectory = "t" + std::to_string(elem.first);
 			
			ObjectIdx trajectory_id = info.getObjectId(trajectory);
			ObjectIdx relative_id = elem.second; // Not an actual object ID, just an identifier
			assert(check_is_relative_id(relative_id));
			overlaps.insert(std::make_pair(trajectory_id, relative_id));
			
			LPT_DEBUG("data-overlaps", "Trajectory " << trajectory << " overlaps with (relative) object configuration " << relative_id);
		}
		
		return overlaps;
	}
	
	
	
	ObjectMapping load_relative_mapping(const ProblemInfo& info, const std::string& filename) {
		ObjectMapping result;
		LPT_DEBUG("data-real-to-relative", "Loading real-to-relative object mappings from '" << filename << "'");
		
		// <base_conf, real_obj_conf> -> relative
		using data_t = std::map<std::pair<unsigned, unsigned>, unsigned>;
		data_t data;
		deserialize(filename, data);

		ObjectMapping mapping;

		for (const auto& elem:data) {
			std::string base_conf = "cb" + std::to_string(elem.first.first);
			std::string real_object_conf = "co" + std::to_string(elem.first.second);
 			
			ObjectIdx b_id = info.getObjectId(base_conf);
			ObjectIdx ro_id = info.getObjectId(real_object_conf);
			ObjectIdx relative_id = elem.second; // Just an identifier
			
			check_is_relative_id(relative_id);

			auto res = result.insert(std::make_pair(std::make_pair(b_id, ro_id), relative_id));
			if (!res.second) throw std::runtime_error("Duplicate real-to-relative object mapping");
			
			LPT_DEBUG("data-real-to-relative", "Base configuration " << base_conf << " and (real) object configuration " << real_object_conf << " map to (relative) object configuration " << relative_id);
			
// 			auto res2 = _relative_to_real.insert(std::make_pair(std::make_pair(b_id, relative_id), ro_id));
			_relative_to_real[std::make_pair(b_id, relative_id)].insert(ro_id);
// 			if (!res2.second) throw std::runtime_error("Duplicate relative-to-real object mapping");
		}
		
		return result;
	}
	
	
	

	void load_real_to_virtual_maps(const ProblemInfo& info, const std::string& filename) {
		assert(_real_to_virtual_map.empty());
		assert(_virtual_to_real_map.empty());
		
		LPT_DEBUG("data-load-mappings", "Loading object mappings from '" << filename << "'");
		
		// <base_conf, real_obj_conf> -> virtual_obj_conf
		using data_t = std::map<std::pair<unsigned, unsigned>, unsigned>;
		data_t data;
		deserialize(filename, data);

		ObjectMapping mapping;

		for (const auto& elem:data) {
			std::string base_conf = "cb" + std::to_string(elem.first.first);
			std::string real_object_conf = "co" + std::to_string(elem.first.second);
 			
			ObjectIdx b_id = info.getObjectId(base_conf);
			ObjectIdx ro_id = info.getObjectId(real_object_conf);
			ObjectIdx virtual_id = elem.second; // Just an identifier
			assert(check_is_virtual_id(virtual_id));


			const auto elem1 = std::make_pair(std::make_pair(b_id, ro_id), virtual_id);
			auto res1 = _real_to_virtual_map.insert(elem1);
			if (!res1.second) throw std::runtime_error("Duplicate object mapping");
			
			auto elem2 = std::make_pair(std::make_pair(b_id, virtual_id), ro_id);
			auto res2 = _virtual_to_real_map.insert(elem2);
			if (!res2.second) throw std::runtime_error("Duplicate object mapping");
			
			LPT_DEBUG("data-load-mappings", "Base configuration " << base_conf << " and (real) object configuration " << real_object_conf << " map to (virtual) object configuration " << virtual_id);
		}
	}
	
	
	
	OverlapMap index_offending(const ProblemInfo& info, const OverlapTable& overlaps) {
		OverlapMap index;
		
		LPT_DEBUG("data-offending-index", "Indexing Offending Configurations");
		for (ObjectIdx conf_base:info.getTypeObjects("conf_base")) {
			std::string base_config_name = info.deduceObjectName(conf_base, info.getTypeId("conf_base"));
			LPT_DEBUG("data-offending-index", "Let's check all (real) overlaps for (real) base config " << base_config_name);
			for (const std::pair<int, int>& overlap:overlaps) {
				ObjectIdx trajectory = overlap.first;
				ObjectIdx relative_id = overlap.second;
				assert(check_is_relative_id(relative_id));
				
				auto it = _relative_to_real.find(std::make_pair(conf_base, relative_id));
				if (it == _relative_to_real.end()) {
					LPT_DEBUG("data-offending-index", "Relative object ID " << relative_id << " has no corresponding real object for base config " << base_config_name);
				} else {
					auto key = std::make_pair(conf_base, trajectory);
					for (const auto& real_conf_o:it->second) {
						index[key].push_back(real_conf_o);
						LPT_DEBUG("data-offending-index", "Base config " << base_config_name << " with trajectory " << info.deduceObjectName(trajectory, info.getTypeId("trajectory")) << " overlap with real object configuration " <<  info.deduceObjectName(real_conf_o, info.getTypeId("conf_obj")));
					}
				}
				
			}
		}
		
		return index;
	}
	
	
	void load_placings(const ProblemInfo& info, const std::string& filename) {
		// Placings are serialized in a map of (trajectory, virtual obj conf) pairs
		
		LPT_DEBUG("data-load", "Loading problem placings from '" << filename << "'");
		
		std::map<unsigned, unsigned> serialized;
		deserialize(filename, serialized);
		
		for (const auto& elem:serialized) {
			ObjectIdx confa = info.getObjectId("ca" + std::to_string(elem.first));
			ObjectIdx virtual_id = elem.second; // Just an identifier
			assert(check_is_virtual_id(virtual_id));
			
			assert(0 <= confa && confa < _placings.size());
			assert(_placings[confa] == -1 && "We shouldn't be setting the same placing twice");
			_placings[confa] = virtual_id;
			
			
			LPT_DEBUG("data-load", "Arm configuration " << info.deduceObjectName(confa, info.getTypeId("conf_arm")) << " places object in virtual configuration " << virtual_id);
		}
	}

	
	void load_graph(const ProblemInfo& info, const std::string& filename, const std::vector<std::string>& object_prefixes) {
		assert(object_prefixes.size() == 3);
		
		LPT_DEBUG("data-load", "Loading graph '" << filename << "'");
		
		SerializedTranslationMap serialized;
		deserialize(filename, serialized);
		
		for (const auto& elem:serialized) {
			std::string source_name = object_prefixes[0] + std::to_string(elem.first.first);
			std::string edge_name = object_prefixes[1] + std::to_string(elem.first.second);
			std::string target_name = object_prefixes[2] + std::to_string(elem.second);
			ObjectIdx source = info.getObjectId(source_name);
			ObjectIdx edge = info.getObjectId(edge_name);
			ObjectIdx target = info.getObjectId(target_name);
			
			assert(0 <= edge && edge < _sources.size() && edge < _targets.size());
			assert(_sources[edge] == -1 && "We shouldn't be setting the same source or target twice");
			assert(_targets[edge] == -1 && "We shouldn't be setting the same source or target twice");
			_sources[edge] = source;
			_targets[edge] = target;
			
			LPT_DEBUG("data-load", "source(" << edge_name << ") = " << source_name << "; target(" << edge_name << ") = " << target_name);
		}
	} 
	
	
public:

	//! (Need to return a new object, since the return value might be an empty vector)
    std::vector<ObjectIdx> get_offending_configurations(ObjectIdx confb, ObjectIdx arm_traj, ObjectIdx held_o) const override {
		auto key = std::make_pair(confb, arm_traj);
		
		if (held_o != _no_object_id) { // some object is being held
			const auto& it = _offending_when_holding.find(key);
			return (it == _offending_when_holding.end()) ? std::vector<ObjectIdx>() : it->second;
		} else {
			const auto& it = _offending_when_empty.find(key);
			return (it == _offending_when_empty.end()) ? std::vector<ObjectIdx>() : it->second;
		}
	}

	int from_real_to_relative(ObjectIdx conf_b, ObjectIdx conf_o) const {
		auto it = _real_to_relative.find(std::make_pair(conf_b, conf_o));
		if (it == _real_to_relative.end()) return -1;
		return it->second;
	}
	
	//! Returns the real object config that corresponds to a given pair of real base config and virtual object config, if exists, or -1 otherwise
	int from_virtual_to_real(ObjectIdx conf_b, unsigned conf_o) const {
		auto it = _virtual_to_real_map.find(std::make_pair(conf_b, conf_o));
		if (it == _virtual_to_real_map.end()) return -1;
		return it->second;
	}
	
	
    // (@nonoverlap ?cb - conf_base ?t - trajectory ?held - nullable_object_id ?o_conf - conf_obj)
    bool nonoverlap(const std::vector<ObjectIdx>& params) const {
		assert(params.size() == 4);
		return nonoverlap(params[0], params[1], params[2], params[3]);
    }
    
    bool nonoverlap(ObjectIdx confb, ObjectIdx traj, ObjectIdx held, int confo_real) const {
		
// 		const ProblemInfo& info = ProblemInfo::getInstance(); // TODO - REMOVE LOG LINE
// 		LPT_DEBUG("nonoverlap-checks", "Checking @nonoverlap(" << info.deduceObjectName(confb, "conf_base") << ", " << info.deduceObjectName(traj, "trajectory") << ", " << info.deduceObjectName(held, "nullable_object_id") << ", " << info.deduceObjectName(confo_real, "conf_obj") << ")");

		if (confo_real == _c_held_id) { // The gripper cannot overlap with an object being held by itself
			return true;
		}

		unsigned relative_conf_o = from_real_to_relative(confb, confo_real);
		if (relative_conf_o == -1) {
			// The real object is not mapped to any virtual configuration with this base 
			// (e.g. because it's too far from the base), hence there's no possible overlap
			return true;
		}

        std::pair<unsigned, unsigned> key{traj, relative_conf_o};
		bool overlap = false;

		if (held == _no_object_id) { // Gripper is empty
			ObjectIdx confa = _targets[traj];
			if (graspable(confb, confa, confo_real)) {
				overlap = false; // If the object is graspable for that trajectory, then we dictate there can be no overlap
			} else {
				overlap = _overlaps_when_empty.find(key) != _overlaps_when_empty.end();
			}
		} else { // Some object is being held
			overlap = _overlaps_when_holding.find(key) != _overlaps_when_holding.end();
		}

		return !overlap;
    }    

    
    static int _retrieve(const std::vector<ObjectIdx>& params, const std::vector<int>& where) {
		assert(params.size() == 1);
		return _retrieve(params[0], where);
	}

    static int _retrieve(ObjectIdx value, const std::vector<int>& where) {
		assert(0 <= value && value < where.size());
		// assert(where[value] != -1); // TODO REACTIVATE
		return where[value];
	}
    
    ObjectIdx target_b(const std::vector<ObjectIdx>& params) const { return _retrieve(params, _targets); }
    ObjectIdx target_a(const std::vector<ObjectIdx>& params) const { return _retrieve(params, _targets); }

    ObjectIdx source_b(const std::vector<ObjectIdx>& params) const { return _retrieve(params, _sources); }
    ObjectIdx source_a(const std::vector<ObjectIdx>& params) const { return _retrieve(params, _sources); }
    
    ObjectIdx placing(ObjectIdx confa) const { return _retrieve(confa, _placings); }


	// (@placeable (confb rob) (confa rob) )
	// @placeable(confb, confa) iff there is a real configuration that corresponds to the virtual configuration where confa would leave the object
	bool placeable(const std::vector<ObjectIdx>& params) const {
		assert(params.size()==2);
		return placeable(params[0], params[1]);
	}
	
	bool placeable(ObjectIdx confb, ObjectIdx confa) const {
		ObjectIdx virtual_confo = placing(confa);
		auto it = _virtual_to_real_map.find(std::make_pair(confb, virtual_confo));
		return it != _virtual_to_real_map.end();
	}

    // confo(o) := (@placing_pose (confb rob) (confa rob))
    // @placing_pose(confb, confa) is the real configuration that corresponds to the virtual configuration where confa would leave the object
	ObjectIdx placing_pose(const std::vector<ObjectIdx>& params) const {
		assert(params.size()==2);
		return placing_pose(params[0], params[1]);
	}
	
	ObjectIdx placing_pose(ObjectIdx confb, ObjectIdx confa) const {
		ObjectIdx virtual_confo = placing(confa);
		auto it = _virtual_to_real_map.find(std::make_pair(confb, virtual_confo));
		if (it == _virtual_to_real_map.end()) throw UndefinedValueAccess();
		return it->second;
	}
	
	// @graspable (confb rob) (confa rob) (confo ?o)
	// @graspable(confb, confa, confo) iff @placeable(confb, confa) and @placing_pose(confb, confa) = confo
	bool graspable(const std::vector<ObjectIdx>& params) const {
		assert(params.size()==3);
		return graspable(params[0], params[1], params[2]);
	}
	
	bool graspable(ObjectIdx confb, ObjectIdx confa, ObjectIdx confo_real) const {
		ObjectIdx virtual_confo = placing(confa);
		
// 		const ProblemInfo& info = ProblemInfo::getInstance();
// 		// TODO - REMOVE LOG LINE
// 		LPT_DEBUG("graspable-checks", "Checking @graspable for base " << info.deduceObjectName(confb, info.getTypeId("conf_base")) << ", arm: " << info.deduceObjectName(confa, info.getTypeId("conf_arm"))
// 		                  << ", (real) config: " << info.deduceObjectName(confo_real, info.getTypeId("conf_obj")) << ", trajectory maps to virtual conf: " << virtual_confo);
		
		ObjectIdx c = from_virtual_to_real(confb, virtual_confo);
		return c != -1 && c == confo_real;
	}
	
	// @can_transition(confb(rob), t))
	bool can_transition(const std::vector<ObjectIdx>& params) const {
		assert(params.size()==3);
		return can_transition(params[0], params[1], params[2]);
	}
	
	bool can_transition(ObjectIdx confb, ObjectIdx last_traj, ObjectIdx intended_traj) const {
		ObjectIdx target_conf = _retrieve(intended_traj, _targets);
		if (target_conf == _ca0) { // We have a "back to resting pose" arm move
			return true;
		}
		
		return placeable(confb, target_conf);
	}

	/*
protected:
	### OLD CODE, TO BE REMOVED, EVENTUALLY ### 
	//! helper
	ObjectIdx get_translation(const std::vector<ObjectIdx>& params, const OTranslationMap& translations) const {
		assert(params.size() == 2);
		auto it = translations.find(std::make_pair(params[0], params[1]));
		if (it == translations.end()) throw UndefinedValueAccess();
		return it->second;
	}
	
	//! helper
	bool exists_translation(const std::vector<ObjectIdx>& params, const OTranslationMap& translations) const {
		assert(params.size() == 2);
		auto it = translations.find(std::make_pair(params[0], params[1]));
		return it != translations.end();
	}
	*/
};



class NonoverlapFormula : public fs::ExternallyDefinedFormula {
public:
        NonoverlapFormula(const std::vector<const fs::Term*>& subterms)
		: ExternallyDefinedFormula(subterms),
		_external(dynamic_cast<const External&>(ProblemInfo::getInstance().get_external()))
		{
			assert(subterms.size() == 4);
		}

        NonoverlapFormula* clone(const std::vector<const fs::Term*>& subterms) const { return new NonoverlapFormula(subterms); }

        virtual std::string name() const { return "nonoverlap"; }
        
protected:
	bool _satisfied(const std::vector<ObjectIdx>& values) const  { return _external.nonoverlap(values); }
	const External& _external;
};


