;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; Robot domain with mobile objects
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; A robot moves around a 3D room with movable objects on one or more tables
;;; The encoding uses state constraints to handle the non-overlapping requirement
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(define (domain fn-robot-navigation)

    (:types

        ;; Any mobile body
        body_id - object

        ;; A mobile body with autonomous motion
        robot_id - body_id

        ;; A mobile body without autonomous motion - only these can be moved by the robots
        nullable_object_id - body_id
        
        object_id - nullable_object_id

        ;; an identifier of a robot base configuration
        conf_base - object

        ;; an identifier of a robot arm configuration
        conf_arm - object

        ;; an identifier of an object configuration
        conf_obj - object

        ;; a possible direction for a translation
        edge - object

        ;; a possible trajectory for the arm
        trajectory - object

    )

    (:constants
        rob - robot_id
        c_held - conf_obj
        ;; undefined confs
        undef_base - conf_base
        undef_obj - conf_obj
        undef_arm - conf_arm
        t0 - trajectory
        ca0 - conf_arm
        no_object - nullable_object_id
    )

    (:predicates
		(@graspable  ?b_conf - conf_base ?ca - conf_arm  ?o_conf - conf_obj)
        (@placeable  ?b_conf - conf_base ?ca - conf_arm)

		(@nonoverlap ?cb - conf_base ?t - trajectory ?held - nullable_object_id ?o_conf - conf_obj)
		
		(@can_transition ?cb - conf_base ?t - trajectory)
    )

    (:functions
        ;; Returns the current configuration of the given body
        (confb ?b - robot_id) - conf_base

        (confa ?b - robot_id) - conf_arm

        (confo ?b - object_id) - conf_obj

        (traj ?b - robot_id) - trajectory

        (holding) - nullable_object_id

        (@source_b ?e - edge) - conf_base
        (@source_a ?t - trajectory) - conf_arm

        (@target_b ?e - edge) - conf_base
        (@target_a ?t - trajectory) - conf_arm

        (@placing_pose ?b_conf - conf_base ?ca - conf_arm) - conf_obj
    )

    ;; Transition actions for robot base
    (:action transition_base :parameters (?e - edge)
        :precondition (and
                        (= (confa rob) ca0)
                        (not (= (@source_b ?e) -1))
                        (= (confb rob) (@source_b ?e)))
        :effect (and
                  (assign (confb rob) (@target_b ?e)))
    )

    ;; Transition actions for robot arm
    (:action transition_arm :parameters(?t - trajectory)
        :precondition (and
						(@can_transition (confb rob) (traj rob) ?t)
                        (not (= (@source_a ?t) -1))
                        (= (confa rob) (@source_a ?t)))
        :effect (and
                  (assign (confa rob) (@target_a ?t))
                  (assign (traj rob) ?t))
    )

    (:action grasp-object
      :parameters (?o - object_id)
      :precondition (and 
                      (= (holding) no_object)   
                      (@graspable (confb rob) (confa rob) (confo ?o)))
      :effect (and 
                (assign (holding) ?o)
                (assign (confo ?o) c_held))
     )

    (:action place-object
        :parameters (?o - object_id)
        :precondition (and 
                        (= (holding) ?o)
                        (@placeable (confb rob) (confa rob)))
        :effect (and
                  (assign (holding) no_object)
                  (assign (confo ?o) (@placing_pose (confb rob) (confa rob))))
    )
)




