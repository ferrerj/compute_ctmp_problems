(define (problem sample)
	(:domain fn-robot-navigation)
	(:objects 

		o1 o2 o3 o4 o5 o6 o7 o8 o9 o10 o11 o12 o13 o14 o15 o16 o17 o18 o19 o20 o21 o22 o23 o24 o25 o26 o27 o28 o29 o30 o31 o32 o33 o34 o35

 - object_id

	cb23
	cb22
	cb25
	cb26
	cb24
	cb30
	cb31
	cb27
	cb28
	cb29
	cb18
	cb19
	cb20
	cb21
	cb14
	cb15
	cb16
	cb17
	cb0
	cb1
	cb2
	cb3
	cb4
	cb5
	cb13
	cb10
	cb12
	cb11
	cb7
	cb6
	cb9
	cb8
	 - conf_base
	co91
	co87
	co94
	co104
	co118
	co121
	co80
	co99
	co113
	co109
	co117
	co114
	co96
	co101
	co116
	co89
	co106
	co110
	co95
	co86
	co82
	co107
	co93
	co43
	co112
	co105
	co1
	co49
	co73
	co123
	co97
	co0
	co84
	co46
	co77
	co102
	co100
	co78
	co81
	co45
	co51
	co48
	co57
	co54
	co66
	co61
	co79
	co75
	co71
	co127
	co111
	co92
	co41
	co83
	co68
	co72
	co126
	co130
	co124
	co120
	co119
	co122
	co133
	co128
	co3
	co132
	co69
	co52
	co76
	co59
	co63
	co74
	co55
	co115
	co131
	co39
	co125
	co85
	co129
	co38
	co88
	co16
	co24
	co13
	co10
	co7
	co6
	co4
	co2
	co21
	co18
	co29
	co98
	co19
	co23
	co26
	co33
	co65
	co35
	co64
	co34
	co36
	co42
	co40
	co108
	co47
	co44
	co5
	co53
	co50
	co60
	co12
	co103
	co90
	co30
	co9
	co70
	co58
	co31
	co27
	co67
	co15
	co56
	co62
	co8
	co11
	co14
	co17
	co22
	co20
	co25
	co28
	co32
	co37
 - conf_obj 
 
	ca44
	ca45
	ca25
	ca22
	ca23
	ca24
	ca19
	ca21
	ca27
	ca26
	ca4
	ca5
	ca14
	ca8
	ca10
	ca11
	ca28
	ca41
	ca42
	ca43
	ca34
	ca36
	ca35
	ca39
	ca37
	ca38
	ca40
	ca33
	ca30
	ca29
	ca32
	ca31
	ca17
	ca18
	ca20
	ca16
	ca15
	ca1
	ca9
	ca2
	ca3
	ca7
	ca6
	ca12
	ca13
 - conf_arm 
 
	e59
	e60
	e61
	e10059
	e10077
	e10080
	e10083
	e10086
	e10089
	e10091
	e10094
	e10097
	e62
	e63
	e64
	e10002
	e10006
	e10010
	e10014
	e10018
	e10062
	e10065
	e10068
	e10071
	e65
	e66
	e67
	e10074
	e68
	e69
	e70
	e71
	e72
	e73
	e74
	e75
	e76
	e91
	e92
	e93
	e89
	e90
	e10078
	e10081
	e10084
	e10087
	e97
	e98
	e99
	e94
	e95
	e96
	e80
	e81
	e82
	e86
	e87
	e88
	e83
	e84
	e85
	e77
	e78
	e79
	e47
	e48
	e49
	e10047
	e10050
	e10053
	e10056
	e10063
	e10066
	e10069
	e10072
	e10075
	e10090
	e10092
	e10095
	e10098
	e50
	e51
	e52
	e53
	e54
	e55
	e56
	e57
	e58
	e35
	e36
	e37
	e10022
	e10025
	e10028
	e10031
	e10034
	e10035
	e10038
	e10041
	e10044
	e10061
	e38
	e39
	e40
	e41
	e42
	e43
	e44
	e45
	e46
	e4
	e5
	e6
	e7
	e0
	e1
	e2
	e3
	e10000
	e10004
	e10011
	e10015
	e10019
	e10021
	e10024
	e10027
	e10030
	e10033
	e10036
	e10039
	e10042
	e10045
	e10048
	e10051
	e10054
	e10057
	e10060
	e10064
	e10067
	e10070
	e10073
	e10076
	e10079
	e10082
	e10085
	e10088
	e10093
	e10096
	e10099
	e16
	e17
	e18
	e19
	e10007
	e8
	e9
	e10
	e11
	e10003
	e10008
	e10012
	e10016
	e12
	e13
	e14
	e15
	e26
	e27
	e28
	e29
	e30
	e31
	e32
	e33
	e34
	e20
	e21
	e22
	e10001
	e10005
	e10009
	e10013
	e10017
	e10020
	e10023
	e10026
	e10029
	e10032
	e10037
	e10040
	e10043
	e10046
	e10049
	e10052
	e10055
	e10058
	e23
	e24
	e25
 - edge 
 
	t0
	t1
	t2
	t3
	t4
	t5
	t6
	t7
	t8
	t9
	t10
	t11
	t12
	t13
	t14
	t15
	t16
	t17
	t18
	t19
	t20
	t21
	t22
	t23
	t24
	t25
	t26
	t27
	t28
	t29
	t30
	t31
	t32
	t33
	t34
	t35
	t36
	t37
	t38
	t39
	t40
	t41
	t42
	t43
	t44
	t45
	t46
	t47
	t48
	t49
	t50
	t51
	t52
	t53
	t54
	t55
	t56
	t57
	t58
	t59
	t60
	t61
	t62
	t63
	t64
	t65
	t66
	t67
	t68
	t69
	t70
	t71
	t72
	t73
	t74
	t75
	t76
	t77
	t78
	t79
	t80
	t81
	t82
	t83
	t84
	t85
	t86
	t87
	t88
	t89
	t90
	t91
	t92
	t93
	t94
	t95
	t96
	t97
	t98
	t99
	t100
	t101
	t102
	t103
	t104
	t105
	t106
	t107
	t108
	t109
	t110
	t111
	t112
	t113
	t114
	t115
	t116
	t117
	t118
	t119
	t120
	t121
	t122
	t123
	t124
	t125
	t126
	t127
	t128
	t129
	t130
	t131
	t132
	t133
	t134
	t135
	t136
	t137
	t138
	t139
	t140
	t141
	t142
	t143
	t144
	t145
	t146
	t147
	t148
	t149
	t150
	t151
	t152
	t153
	t154
	t10000
	t10001
	t10002
	t10003
	t10004
	t10005
	t10006
	t10007
	t10008
	t10009
	t10010
	t10011
	t10012
	t10013
	t10014
	t10015
	t10016
	t10017
	t10018
	t10019
	t10020
	t10021
	t10022
	t10023
	t10024
	t10025
	t10026
	t10027
	t10028
	t10029
	t10030
	t10031
	t10032
	t10033
	t10034
	t10035
	t10036
	t10037
	t10038
	t10039
	t10040
	t10041
	t10042
	t10043
	t10044
	t10045
	t10046
	t10047
	t10048
	t10049
	t10050
	t10051
	t10052
	t10053
	t10054
	t10055
	t10056
	t10057
	t10058
	t10059
	t10060
	t10061
	t10062
	t10063
	t10064
	t10065
	t10066
	t10067
	t10068
	t10069
	t10070
	t10071
	t10072
	t10073
	t10074
	t10075
	t10076
	t10077
	t10078
	t10079
	t10080
	t10081
	t10082
	t10083
	t10084
	t10085
	t10086
	t10087
	t10088
	t10089
	t10090
	t10091
	t10092
	t10094
	t10093
	t10095
	t10096
	t10097
	t10098
	t10099
	t10100
	t10101
	t10102
	t10103
	t10104
	t10105
	t10106
	t10107
	t10108
	t10109
	t10110
	t10111
	t10112
	t10113
	t10114
	t10115
	t10116
	t10117
	t10118
	t10119
	t10120
	t10121
	t10122
	t10123
	t10124
	t10125
	t10126
	t10127
	t10128
	t10129
	t10130
	t10131
	t10132
	t10133
	t10134
	t10135
	t10136
	t10137
	t10138
	t10139
	t10140
	t10141
	t10142
	t10143
	t10144
	t10145
	t10146
	t10147
	t10148
	t10149
	t10150
	t10151
	t10152
	t10153
	t10154
 - trajectory 
 )
(:init
(= (confb rob ) cb0 )
(= (confa rob ) ca0 )
(= (traj rob ) t_init )
(= (holding) no_object)
; initial object configurations 

(= (confo o1) co25); -x 0.654613 -y 0.075084 -z 0.679981
(= (confo o2) co91); -x 0.804284 -y -0.300402 -z 0.679981
(= (confo o3) co46); -x 0.967903 -y 0.00118329 -z 0.679981
(= (confo o4) co83); -x 0.857421 -y 0.127136 -z 0.679982
(= (confo o5) co79); -x 0.730408 -y 0.29859 -z 0.679982
(= (confo o6) co34); -x 1.03983 -y -0.219893 -z 0.679964
(= (confo o7) co57); -x 0.729445 -y -0.0751143 -z 0.680001
(= (confo o8) co37); -x 0.654881 -y 0.299693 -z 0.679982
(= (confo o9) co76); -x 1.01968 -y -0.398734 -z 0.680001
(= (confo o10) co119); -x 0.871172 -y 0.275856 -z 0.680001
(= (confo o11) co106); -x 0.945331 -y -0.174448 -z 0.680001
(= (confo o12) co22); -x 0.654917 -y -0.00154313 -z 0.679981
(= (confo o13) co118); -x 0.803833 -y 0.0356388 -z 0.680003
(= (confo o14) co116); -x 0.940893 -y 0.108094 -z 0.68
(= (confo o15) co8); -x 0.654238 -y -0.375006 -z 0.680001
(= (confo o16) co20); -x 0.654466 -y -0.0749074 -z 0.679981
(= (confo o17) co32); -x 0.654772 -y 0.224878 -z 0.679981
(= (confo o18) co45); -x 0.729221 -y -0.375181 -z 0.680001
(= (confo o19) co61); -x 0.729522 -y -0.000122777 -z 0.679981
(= (confo o20) co87); -x 0.803315 -y -0.375289 -z 0.680003
(= (confo o21) co75); -x 0.72979 -y 0.224621 -z 0.680002
(= (confo o22) co66); -x 0.729615 -y 0.0748208 -z 0.680001
(= (confo o23) co128); -x 0.879027 -y -0.0762539 -z 0.679982
(= (confo o24) co59); -x 1.02047 -y -0.0998665 -z 0.680001
(= (confo o25) co47); -x 1.02074 -y 0.200147 -z 0.679983
(= (confo o26) co14); -x 0.654209 -y -0.226652 -z 0.679981
(= (confo o27) co48); -x 0.729256 -y -0.300154 -z 0.680001
(= (confo o28) co100); -x 0.972731 -y -0.307805 -z 0.68
(= (confo o29) co124); -x 0.878713 -y -0.301277 -z 0.679981
(= (confo o30) co86); -x 0.946691 -y 0.2753 -z 0.68
(= (confo o31) co81); -x 0.837832 -y 0.199644 -z 0.68
(= (confo o32) co94); -x 0.804393 -y -0.150372 -z 0.680001
(= (confo o33) co5); -x 1.02101 -y 0.0928858 -z 0.680003
(= (confo o34) co11); -x 0.654286 -y -0.299976 -z 0.680001
(= (confo o35) co42); -x 1.02084 -y 0.350247 -z 0.68




 )
(:goal (and

(= (confo o13) co83); -x 0.857421 -y 0.127136 -z 0.679982
(= (confo o4) co118); -x 0.803833 -y 0.0356388 -z 0.680003
(= (confo o27) co75); -x 0.72979 -y 0.224621 -z 0.680002

))
(:constraints

(@nonoverlap_ro (confb rob) (traj rob) (confo o1))
(@nonoverlap_ro (confb rob) (traj rob) (confo o2))
(@nonoverlap_ro (confb rob) (traj rob) (confo o3))
(@nonoverlap_ro (confb rob) (traj rob) (confo o4))
(@nonoverlap_ro (confb rob) (traj rob) (confo o5))
(@nonoverlap_ro (confb rob) (traj rob) (confo o6))
(@nonoverlap_ro (confb rob) (traj rob) (confo o7))
(@nonoverlap_ro (confb rob) (traj rob) (confo o8))
(@nonoverlap_ro (confb rob) (traj rob) (confo o9))
(@nonoverlap_ro (confb rob) (traj rob) (confo o10))
(@nonoverlap_ro (confb rob) (traj rob) (confo o11))
(@nonoverlap_ro (confb rob) (traj rob) (confo o12))
(@nonoverlap_ro (confb rob) (traj rob) (confo o13))
(@nonoverlap_ro (confb rob) (traj rob) (confo o14))
(@nonoverlap_ro (confb rob) (traj rob) (confo o15))
(@nonoverlap_ro (confb rob) (traj rob) (confo o16))
(@nonoverlap_ro (confb rob) (traj rob) (confo o17))
(@nonoverlap_ro (confb rob) (traj rob) (confo o18))
(@nonoverlap_ro (confb rob) (traj rob) (confo o19))
(@nonoverlap_ro (confb rob) (traj rob) (confo o20))
(@nonoverlap_ro (confb rob) (traj rob) (confo o21))
(@nonoverlap_ro (confb rob) (traj rob) (confo o22))
(@nonoverlap_ro (confb rob) (traj rob) (confo o23))
(@nonoverlap_ro (confb rob) (traj rob) (confo o24))
(@nonoverlap_ro (confb rob) (traj rob) (confo o25))
(@nonoverlap_ro (confb rob) (traj rob) (confo o26))
(@nonoverlap_ro (confb rob) (traj rob) (confo o27))
(@nonoverlap_ro (confb rob) (traj rob) (confo o28))
(@nonoverlap_ro (confb rob) (traj rob) (confo o29))
(@nonoverlap_ro (confb rob) (traj rob) (confo o30))
(@nonoverlap_ro (confb rob) (traj rob) (confo o31))
(@nonoverlap_ro (confb rob) (traj rob) (confo o32))
(@nonoverlap_ro (confb rob) (traj rob) (confo o33))
(@nonoverlap_ro (confb rob) (traj rob) (confo o34))
(@nonoverlap_ro (confb rob) (traj rob) (confo o35))
(@nonoverlap_oo (confo o1) (confo o2))
(@nonoverlap_oo (confo o1) (confo o3))
(@nonoverlap_oo (confo o1) (confo o4))
(@nonoverlap_oo (confo o1) (confo o5))
(@nonoverlap_oo (confo o1) (confo o6))
(@nonoverlap_oo (confo o1) (confo o7))
(@nonoverlap_oo (confo o1) (confo o8))
(@nonoverlap_oo (confo o1) (confo o9))
(@nonoverlap_oo (confo o1) (confo o10))
(@nonoverlap_oo (confo o1) (confo o11))
(@nonoverlap_oo (confo o1) (confo o12))
(@nonoverlap_oo (confo o1) (confo o13))
(@nonoverlap_oo (confo o1) (confo o14))
(@nonoverlap_oo (confo o1) (confo o15))
(@nonoverlap_oo (confo o1) (confo o16))
(@nonoverlap_oo (confo o1) (confo o17))
(@nonoverlap_oo (confo o1) (confo o18))
(@nonoverlap_oo (confo o1) (confo o19))
(@nonoverlap_oo (confo o1) (confo o20))
(@nonoverlap_oo (confo o1) (confo o21))
(@nonoverlap_oo (confo o1) (confo o22))
(@nonoverlap_oo (confo o1) (confo o23))
(@nonoverlap_oo (confo o1) (confo o24))
(@nonoverlap_oo (confo o1) (confo o25))
(@nonoverlap_oo (confo o1) (confo o26))
(@nonoverlap_oo (confo o1) (confo o27))
(@nonoverlap_oo (confo o1) (confo o28))
(@nonoverlap_oo (confo o1) (confo o29))
(@nonoverlap_oo (confo o1) (confo o30))
(@nonoverlap_oo (confo o1) (confo o31))
(@nonoverlap_oo (confo o1) (confo o32))
(@nonoverlap_oo (confo o1) (confo o33))
(@nonoverlap_oo (confo o1) (confo o34))
(@nonoverlap_oo (confo o1) (confo o35))
(@nonoverlap_oo (confo o2) (confo o3))
(@nonoverlap_oo (confo o2) (confo o4))
(@nonoverlap_oo (confo o2) (confo o5))
(@nonoverlap_oo (confo o2) (confo o6))
(@nonoverlap_oo (confo o2) (confo o7))
(@nonoverlap_oo (confo o2) (confo o8))
(@nonoverlap_oo (confo o2) (confo o9))
(@nonoverlap_oo (confo o2) (confo o10))
(@nonoverlap_oo (confo o2) (confo o11))
(@nonoverlap_oo (confo o2) (confo o12))
(@nonoverlap_oo (confo o2) (confo o13))
(@nonoverlap_oo (confo o2) (confo o14))
(@nonoverlap_oo (confo o2) (confo o15))
(@nonoverlap_oo (confo o2) (confo o16))
(@nonoverlap_oo (confo o2) (confo o17))
(@nonoverlap_oo (confo o2) (confo o18))
(@nonoverlap_oo (confo o2) (confo o19))
(@nonoverlap_oo (confo o2) (confo o20))
(@nonoverlap_oo (confo o2) (confo o21))
(@nonoverlap_oo (confo o2) (confo o22))
(@nonoverlap_oo (confo o2) (confo o23))
(@nonoverlap_oo (confo o2) (confo o24))
(@nonoverlap_oo (confo o2) (confo o25))
(@nonoverlap_oo (confo o2) (confo o26))
(@nonoverlap_oo (confo o2) (confo o27))
(@nonoverlap_oo (confo o2) (confo o28))
(@nonoverlap_oo (confo o2) (confo o29))
(@nonoverlap_oo (confo o2) (confo o30))
(@nonoverlap_oo (confo o2) (confo o31))
(@nonoverlap_oo (confo o2) (confo o32))
(@nonoverlap_oo (confo o2) (confo o33))
(@nonoverlap_oo (confo o2) (confo o34))
(@nonoverlap_oo (confo o2) (confo o35))
(@nonoverlap_oo (confo o3) (confo o4))
(@nonoverlap_oo (confo o3) (confo o5))
(@nonoverlap_oo (confo o3) (confo o6))
(@nonoverlap_oo (confo o3) (confo o7))
(@nonoverlap_oo (confo o3) (confo o8))
(@nonoverlap_oo (confo o3) (confo o9))
(@nonoverlap_oo (confo o3) (confo o10))
(@nonoverlap_oo (confo o3) (confo o11))
(@nonoverlap_oo (confo o3) (confo o12))
(@nonoverlap_oo (confo o3) (confo o13))
(@nonoverlap_oo (confo o3) (confo o14))
(@nonoverlap_oo (confo o3) (confo o15))
(@nonoverlap_oo (confo o3) (confo o16))
(@nonoverlap_oo (confo o3) (confo o17))
(@nonoverlap_oo (confo o3) (confo o18))
(@nonoverlap_oo (confo o3) (confo o19))
(@nonoverlap_oo (confo o3) (confo o20))
(@nonoverlap_oo (confo o3) (confo o21))
(@nonoverlap_oo (confo o3) (confo o22))
(@nonoverlap_oo (confo o3) (confo o23))
(@nonoverlap_oo (confo o3) (confo o24))
(@nonoverlap_oo (confo o3) (confo o25))
(@nonoverlap_oo (confo o3) (confo o26))
(@nonoverlap_oo (confo o3) (confo o27))
(@nonoverlap_oo (confo o3) (confo o28))
(@nonoverlap_oo (confo o3) (confo o29))
(@nonoverlap_oo (confo o3) (confo o30))
(@nonoverlap_oo (confo o3) (confo o31))
(@nonoverlap_oo (confo o3) (confo o32))
(@nonoverlap_oo (confo o3) (confo o33))
(@nonoverlap_oo (confo o3) (confo o34))
(@nonoverlap_oo (confo o3) (confo o35))
(@nonoverlap_oo (confo o4) (confo o5))
(@nonoverlap_oo (confo o4) (confo o6))
(@nonoverlap_oo (confo o4) (confo o7))
(@nonoverlap_oo (confo o4) (confo o8))
(@nonoverlap_oo (confo o4) (confo o9))
(@nonoverlap_oo (confo o4) (confo o10))
(@nonoverlap_oo (confo o4) (confo o11))
(@nonoverlap_oo (confo o4) (confo o12))
(@nonoverlap_oo (confo o4) (confo o13))
(@nonoverlap_oo (confo o4) (confo o14))
(@nonoverlap_oo (confo o4) (confo o15))
(@nonoverlap_oo (confo o4) (confo o16))
(@nonoverlap_oo (confo o4) (confo o17))
(@nonoverlap_oo (confo o4) (confo o18))
(@nonoverlap_oo (confo o4) (confo o19))
(@nonoverlap_oo (confo o4) (confo o20))
(@nonoverlap_oo (confo o4) (confo o21))
(@nonoverlap_oo (confo o4) (confo o22))
(@nonoverlap_oo (confo o4) (confo o23))
(@nonoverlap_oo (confo o4) (confo o24))
(@nonoverlap_oo (confo o4) (confo o25))
(@nonoverlap_oo (confo o4) (confo o26))
(@nonoverlap_oo (confo o4) (confo o27))
(@nonoverlap_oo (confo o4) (confo o28))
(@nonoverlap_oo (confo o4) (confo o29))
(@nonoverlap_oo (confo o4) (confo o30))
(@nonoverlap_oo (confo o4) (confo o31))
(@nonoverlap_oo (confo o4) (confo o32))
(@nonoverlap_oo (confo o4) (confo o33))
(@nonoverlap_oo (confo o4) (confo o34))
(@nonoverlap_oo (confo o4) (confo o35))
(@nonoverlap_oo (confo o5) (confo o6))
(@nonoverlap_oo (confo o5) (confo o7))
(@nonoverlap_oo (confo o5) (confo o8))
(@nonoverlap_oo (confo o5) (confo o9))
(@nonoverlap_oo (confo o5) (confo o10))
(@nonoverlap_oo (confo o5) (confo o11))
(@nonoverlap_oo (confo o5) (confo o12))
(@nonoverlap_oo (confo o5) (confo o13))
(@nonoverlap_oo (confo o5) (confo o14))
(@nonoverlap_oo (confo o5) (confo o15))
(@nonoverlap_oo (confo o5) (confo o16))
(@nonoverlap_oo (confo o5) (confo o17))
(@nonoverlap_oo (confo o5) (confo o18))
(@nonoverlap_oo (confo o5) (confo o19))
(@nonoverlap_oo (confo o5) (confo o20))
(@nonoverlap_oo (confo o5) (confo o21))
(@nonoverlap_oo (confo o5) (confo o22))
(@nonoverlap_oo (confo o5) (confo o23))
(@nonoverlap_oo (confo o5) (confo o24))
(@nonoverlap_oo (confo o5) (confo o25))
(@nonoverlap_oo (confo o5) (confo o26))
(@nonoverlap_oo (confo o5) (confo o27))
(@nonoverlap_oo (confo o5) (confo o28))
(@nonoverlap_oo (confo o5) (confo o29))
(@nonoverlap_oo (confo o5) (confo o30))
(@nonoverlap_oo (confo o5) (confo o31))
(@nonoverlap_oo (confo o5) (confo o32))
(@nonoverlap_oo (confo o5) (confo o33))
(@nonoverlap_oo (confo o5) (confo o34))
(@nonoverlap_oo (confo o5) (confo o35))
(@nonoverlap_oo (confo o6) (confo o7))
(@nonoverlap_oo (confo o6) (confo o8))
(@nonoverlap_oo (confo o6) (confo o9))
(@nonoverlap_oo (confo o6) (confo o10))
(@nonoverlap_oo (confo o6) (confo o11))
(@nonoverlap_oo (confo o6) (confo o12))
(@nonoverlap_oo (confo o6) (confo o13))
(@nonoverlap_oo (confo o6) (confo o14))
(@nonoverlap_oo (confo o6) (confo o15))
(@nonoverlap_oo (confo o6) (confo o16))
(@nonoverlap_oo (confo o6) (confo o17))
(@nonoverlap_oo (confo o6) (confo o18))
(@nonoverlap_oo (confo o6) (confo o19))
(@nonoverlap_oo (confo o6) (confo o20))
(@nonoverlap_oo (confo o6) (confo o21))
(@nonoverlap_oo (confo o6) (confo o22))
(@nonoverlap_oo (confo o6) (confo o23))
(@nonoverlap_oo (confo o6) (confo o24))
(@nonoverlap_oo (confo o6) (confo o25))
(@nonoverlap_oo (confo o6) (confo o26))
(@nonoverlap_oo (confo o6) (confo o27))
(@nonoverlap_oo (confo o6) (confo o28))
(@nonoverlap_oo (confo o6) (confo o29))
(@nonoverlap_oo (confo o6) (confo o30))
(@nonoverlap_oo (confo o6) (confo o31))
(@nonoverlap_oo (confo o6) (confo o32))
(@nonoverlap_oo (confo o6) (confo o33))
(@nonoverlap_oo (confo o6) (confo o34))
(@nonoverlap_oo (confo o6) (confo o35))
(@nonoverlap_oo (confo o7) (confo o8))
(@nonoverlap_oo (confo o7) (confo o9))
(@nonoverlap_oo (confo o7) (confo o10))
(@nonoverlap_oo (confo o7) (confo o11))
(@nonoverlap_oo (confo o7) (confo o12))
(@nonoverlap_oo (confo o7) (confo o13))
(@nonoverlap_oo (confo o7) (confo o14))
(@nonoverlap_oo (confo o7) (confo o15))
(@nonoverlap_oo (confo o7) (confo o16))
(@nonoverlap_oo (confo o7) (confo o17))
(@nonoverlap_oo (confo o7) (confo o18))
(@nonoverlap_oo (confo o7) (confo o19))
(@nonoverlap_oo (confo o7) (confo o20))
(@nonoverlap_oo (confo o7) (confo o21))
(@nonoverlap_oo (confo o7) (confo o22))
(@nonoverlap_oo (confo o7) (confo o23))
(@nonoverlap_oo (confo o7) (confo o24))
(@nonoverlap_oo (confo o7) (confo o25))
(@nonoverlap_oo (confo o7) (confo o26))
(@nonoverlap_oo (confo o7) (confo o27))
(@nonoverlap_oo (confo o7) (confo o28))
(@nonoverlap_oo (confo o7) (confo o29))
(@nonoverlap_oo (confo o7) (confo o30))
(@nonoverlap_oo (confo o7) (confo o31))
(@nonoverlap_oo (confo o7) (confo o32))
(@nonoverlap_oo (confo o7) (confo o33))
(@nonoverlap_oo (confo o7) (confo o34))
(@nonoverlap_oo (confo o7) (confo o35))
(@nonoverlap_oo (confo o8) (confo o9))
(@nonoverlap_oo (confo o8) (confo o10))
(@nonoverlap_oo (confo o8) (confo o11))
(@nonoverlap_oo (confo o8) (confo o12))
(@nonoverlap_oo (confo o8) (confo o13))
(@nonoverlap_oo (confo o8) (confo o14))
(@nonoverlap_oo (confo o8) (confo o15))
(@nonoverlap_oo (confo o8) (confo o16))
(@nonoverlap_oo (confo o8) (confo o17))
(@nonoverlap_oo (confo o8) (confo o18))
(@nonoverlap_oo (confo o8) (confo o19))
(@nonoverlap_oo (confo o8) (confo o20))
(@nonoverlap_oo (confo o8) (confo o21))
(@nonoverlap_oo (confo o8) (confo o22))
(@nonoverlap_oo (confo o8) (confo o23))
(@nonoverlap_oo (confo o8) (confo o24))
(@nonoverlap_oo (confo o8) (confo o25))
(@nonoverlap_oo (confo o8) (confo o26))
(@nonoverlap_oo (confo o8) (confo o27))
(@nonoverlap_oo (confo o8) (confo o28))
(@nonoverlap_oo (confo o8) (confo o29))
(@nonoverlap_oo (confo o8) (confo o30))
(@nonoverlap_oo (confo o8) (confo o31))
(@nonoverlap_oo (confo o8) (confo o32))
(@nonoverlap_oo (confo o8) (confo o33))
(@nonoverlap_oo (confo o8) (confo o34))
(@nonoverlap_oo (confo o8) (confo o35))
(@nonoverlap_oo (confo o9) (confo o10))
(@nonoverlap_oo (confo o9) (confo o11))
(@nonoverlap_oo (confo o9) (confo o12))
(@nonoverlap_oo (confo o9) (confo o13))
(@nonoverlap_oo (confo o9) (confo o14))
(@nonoverlap_oo (confo o9) (confo o15))
(@nonoverlap_oo (confo o9) (confo o16))
(@nonoverlap_oo (confo o9) (confo o17))
(@nonoverlap_oo (confo o9) (confo o18))
(@nonoverlap_oo (confo o9) (confo o19))
(@nonoverlap_oo (confo o9) (confo o20))
(@nonoverlap_oo (confo o9) (confo o21))
(@nonoverlap_oo (confo o9) (confo o22))
(@nonoverlap_oo (confo o9) (confo o23))
(@nonoverlap_oo (confo o9) (confo o24))
(@nonoverlap_oo (confo o9) (confo o25))
(@nonoverlap_oo (confo o9) (confo o26))
(@nonoverlap_oo (confo o9) (confo o27))
(@nonoverlap_oo (confo o9) (confo o28))
(@nonoverlap_oo (confo o9) (confo o29))
(@nonoverlap_oo (confo o9) (confo o30))
(@nonoverlap_oo (confo o9) (confo o31))
(@nonoverlap_oo (confo o9) (confo o32))
(@nonoverlap_oo (confo o9) (confo o33))
(@nonoverlap_oo (confo o9) (confo o34))
(@nonoverlap_oo (confo o9) (confo o35))
(@nonoverlap_oo (confo o10) (confo o11))
(@nonoverlap_oo (confo o10) (confo o12))
(@nonoverlap_oo (confo o10) (confo o13))
(@nonoverlap_oo (confo o10) (confo o14))
(@nonoverlap_oo (confo o10) (confo o15))
(@nonoverlap_oo (confo o10) (confo o16))
(@nonoverlap_oo (confo o10) (confo o17))
(@nonoverlap_oo (confo o10) (confo o18))
(@nonoverlap_oo (confo o10) (confo o19))
(@nonoverlap_oo (confo o10) (confo o20))
(@nonoverlap_oo (confo o10) (confo o21))
(@nonoverlap_oo (confo o10) (confo o22))
(@nonoverlap_oo (confo o10) (confo o23))
(@nonoverlap_oo (confo o10) (confo o24))
(@nonoverlap_oo (confo o10) (confo o25))
(@nonoverlap_oo (confo o10) (confo o26))
(@nonoverlap_oo (confo o10) (confo o27))
(@nonoverlap_oo (confo o10) (confo o28))
(@nonoverlap_oo (confo o10) (confo o29))
(@nonoverlap_oo (confo o10) (confo o30))
(@nonoverlap_oo (confo o10) (confo o31))
(@nonoverlap_oo (confo o10) (confo o32))
(@nonoverlap_oo (confo o10) (confo o33))
(@nonoverlap_oo (confo o10) (confo o34))
(@nonoverlap_oo (confo o10) (confo o35))
(@nonoverlap_oo (confo o11) (confo o12))
(@nonoverlap_oo (confo o11) (confo o13))
(@nonoverlap_oo (confo o11) (confo o14))
(@nonoverlap_oo (confo o11) (confo o15))
(@nonoverlap_oo (confo o11) (confo o16))
(@nonoverlap_oo (confo o11) (confo o17))
(@nonoverlap_oo (confo o11) (confo o18))
(@nonoverlap_oo (confo o11) (confo o19))
(@nonoverlap_oo (confo o11) (confo o20))
(@nonoverlap_oo (confo o11) (confo o21))
(@nonoverlap_oo (confo o11) (confo o22))
(@nonoverlap_oo (confo o11) (confo o23))
(@nonoverlap_oo (confo o11) (confo o24))
(@nonoverlap_oo (confo o11) (confo o25))
(@nonoverlap_oo (confo o11) (confo o26))
(@nonoverlap_oo (confo o11) (confo o27))
(@nonoverlap_oo (confo o11) (confo o28))
(@nonoverlap_oo (confo o11) (confo o29))
(@nonoverlap_oo (confo o11) (confo o30))
(@nonoverlap_oo (confo o11) (confo o31))
(@nonoverlap_oo (confo o11) (confo o32))
(@nonoverlap_oo (confo o11) (confo o33))
(@nonoverlap_oo (confo o11) (confo o34))
(@nonoverlap_oo (confo o11) (confo o35))
(@nonoverlap_oo (confo o12) (confo o13))
(@nonoverlap_oo (confo o12) (confo o14))
(@nonoverlap_oo (confo o12) (confo o15))
(@nonoverlap_oo (confo o12) (confo o16))
(@nonoverlap_oo (confo o12) (confo o17))
(@nonoverlap_oo (confo o12) (confo o18))
(@nonoverlap_oo (confo o12) (confo o19))
(@nonoverlap_oo (confo o12) (confo o20))
(@nonoverlap_oo (confo o12) (confo o21))
(@nonoverlap_oo (confo o12) (confo o22))
(@nonoverlap_oo (confo o12) (confo o23))
(@nonoverlap_oo (confo o12) (confo o24))
(@nonoverlap_oo (confo o12) (confo o25))
(@nonoverlap_oo (confo o12) (confo o26))
(@nonoverlap_oo (confo o12) (confo o27))
(@nonoverlap_oo (confo o12) (confo o28))
(@nonoverlap_oo (confo o12) (confo o29))
(@nonoverlap_oo (confo o12) (confo o30))
(@nonoverlap_oo (confo o12) (confo o31))
(@nonoverlap_oo (confo o12) (confo o32))
(@nonoverlap_oo (confo o12) (confo o33))
(@nonoverlap_oo (confo o12) (confo o34))
(@nonoverlap_oo (confo o12) (confo o35))
(@nonoverlap_oo (confo o13) (confo o14))
(@nonoverlap_oo (confo o13) (confo o15))
(@nonoverlap_oo (confo o13) (confo o16))
(@nonoverlap_oo (confo o13) (confo o17))
(@nonoverlap_oo (confo o13) (confo o18))
(@nonoverlap_oo (confo o13) (confo o19))
(@nonoverlap_oo (confo o13) (confo o20))
(@nonoverlap_oo (confo o13) (confo o21))
(@nonoverlap_oo (confo o13) (confo o22))
(@nonoverlap_oo (confo o13) (confo o23))
(@nonoverlap_oo (confo o13) (confo o24))
(@nonoverlap_oo (confo o13) (confo o25))
(@nonoverlap_oo (confo o13) (confo o26))
(@nonoverlap_oo (confo o13) (confo o27))
(@nonoverlap_oo (confo o13) (confo o28))
(@nonoverlap_oo (confo o13) (confo o29))
(@nonoverlap_oo (confo o13) (confo o30))
(@nonoverlap_oo (confo o13) (confo o31))
(@nonoverlap_oo (confo o13) (confo o32))
(@nonoverlap_oo (confo o13) (confo o33))
(@nonoverlap_oo (confo o13) (confo o34))
(@nonoverlap_oo (confo o13) (confo o35))
(@nonoverlap_oo (confo o14) (confo o15))
(@nonoverlap_oo (confo o14) (confo o16))
(@nonoverlap_oo (confo o14) (confo o17))
(@nonoverlap_oo (confo o14) (confo o18))
(@nonoverlap_oo (confo o14) (confo o19))
(@nonoverlap_oo (confo o14) (confo o20))
(@nonoverlap_oo (confo o14) (confo o21))
(@nonoverlap_oo (confo o14) (confo o22))
(@nonoverlap_oo (confo o14) (confo o23))
(@nonoverlap_oo (confo o14) (confo o24))
(@nonoverlap_oo (confo o14) (confo o25))
(@nonoverlap_oo (confo o14) (confo o26))
(@nonoverlap_oo (confo o14) (confo o27))
(@nonoverlap_oo (confo o14) (confo o28))
(@nonoverlap_oo (confo o14) (confo o29))
(@nonoverlap_oo (confo o14) (confo o30))
(@nonoverlap_oo (confo o14) (confo o31))
(@nonoverlap_oo (confo o14) (confo o32))
(@nonoverlap_oo (confo o14) (confo o33))
(@nonoverlap_oo (confo o14) (confo o34))
(@nonoverlap_oo (confo o14) (confo o35))
(@nonoverlap_oo (confo o15) (confo o16))
(@nonoverlap_oo (confo o15) (confo o17))
(@nonoverlap_oo (confo o15) (confo o18))
(@nonoverlap_oo (confo o15) (confo o19))
(@nonoverlap_oo (confo o15) (confo o20))
(@nonoverlap_oo (confo o15) (confo o21))
(@nonoverlap_oo (confo o15) (confo o22))
(@nonoverlap_oo (confo o15) (confo o23))
(@nonoverlap_oo (confo o15) (confo o24))
(@nonoverlap_oo (confo o15) (confo o25))
(@nonoverlap_oo (confo o15) (confo o26))
(@nonoverlap_oo (confo o15) (confo o27))
(@nonoverlap_oo (confo o15) (confo o28))
(@nonoverlap_oo (confo o15) (confo o29))
(@nonoverlap_oo (confo o15) (confo o30))
(@nonoverlap_oo (confo o15) (confo o31))
(@nonoverlap_oo (confo o15) (confo o32))
(@nonoverlap_oo (confo o15) (confo o33))
(@nonoverlap_oo (confo o15) (confo o34))
(@nonoverlap_oo (confo o15) (confo o35))
(@nonoverlap_oo (confo o16) (confo o17))
(@nonoverlap_oo (confo o16) (confo o18))
(@nonoverlap_oo (confo o16) (confo o19))
(@nonoverlap_oo (confo o16) (confo o20))
(@nonoverlap_oo (confo o16) (confo o21))
(@nonoverlap_oo (confo o16) (confo o22))
(@nonoverlap_oo (confo o16) (confo o23))
(@nonoverlap_oo (confo o16) (confo o24))
(@nonoverlap_oo (confo o16) (confo o25))
(@nonoverlap_oo (confo o16) (confo o26))
(@nonoverlap_oo (confo o16) (confo o27))
(@nonoverlap_oo (confo o16) (confo o28))
(@nonoverlap_oo (confo o16) (confo o29))
(@nonoverlap_oo (confo o16) (confo o30))
(@nonoverlap_oo (confo o16) (confo o31))
(@nonoverlap_oo (confo o16) (confo o32))
(@nonoverlap_oo (confo o16) (confo o33))
(@nonoverlap_oo (confo o16) (confo o34))
(@nonoverlap_oo (confo o16) (confo o35))
(@nonoverlap_oo (confo o17) (confo o18))
(@nonoverlap_oo (confo o17) (confo o19))
(@nonoverlap_oo (confo o17) (confo o20))
(@nonoverlap_oo (confo o17) (confo o21))
(@nonoverlap_oo (confo o17) (confo o22))
(@nonoverlap_oo (confo o17) (confo o23))
(@nonoverlap_oo (confo o17) (confo o24))
(@nonoverlap_oo (confo o17) (confo o25))
(@nonoverlap_oo (confo o17) (confo o26))
(@nonoverlap_oo (confo o17) (confo o27))
(@nonoverlap_oo (confo o17) (confo o28))
(@nonoverlap_oo (confo o17) (confo o29))
(@nonoverlap_oo (confo o17) (confo o30))
(@nonoverlap_oo (confo o17) (confo o31))
(@nonoverlap_oo (confo o17) (confo o32))
(@nonoverlap_oo (confo o17) (confo o33))
(@nonoverlap_oo (confo o17) (confo o34))
(@nonoverlap_oo (confo o17) (confo o35))
(@nonoverlap_oo (confo o18) (confo o19))
(@nonoverlap_oo (confo o18) (confo o20))
(@nonoverlap_oo (confo o18) (confo o21))
(@nonoverlap_oo (confo o18) (confo o22))
(@nonoverlap_oo (confo o18) (confo o23))
(@nonoverlap_oo (confo o18) (confo o24))
(@nonoverlap_oo (confo o18) (confo o25))
(@nonoverlap_oo (confo o18) (confo o26))
(@nonoverlap_oo (confo o18) (confo o27))
(@nonoverlap_oo (confo o18) (confo o28))
(@nonoverlap_oo (confo o18) (confo o29))
(@nonoverlap_oo (confo o18) (confo o30))
(@nonoverlap_oo (confo o18) (confo o31))
(@nonoverlap_oo (confo o18) (confo o32))
(@nonoverlap_oo (confo o18) (confo o33))
(@nonoverlap_oo (confo o18) (confo o34))
(@nonoverlap_oo (confo o18) (confo o35))
(@nonoverlap_oo (confo o19) (confo o20))
(@nonoverlap_oo (confo o19) (confo o21))
(@nonoverlap_oo (confo o19) (confo o22))
(@nonoverlap_oo (confo o19) (confo o23))
(@nonoverlap_oo (confo o19) (confo o24))
(@nonoverlap_oo (confo o19) (confo o25))
(@nonoverlap_oo (confo o19) (confo o26))
(@nonoverlap_oo (confo o19) (confo o27))
(@nonoverlap_oo (confo o19) (confo o28))
(@nonoverlap_oo (confo o19) (confo o29))
(@nonoverlap_oo (confo o19) (confo o30))
(@nonoverlap_oo (confo o19) (confo o31))
(@nonoverlap_oo (confo o19) (confo o32))
(@nonoverlap_oo (confo o19) (confo o33))
(@nonoverlap_oo (confo o19) (confo o34))
(@nonoverlap_oo (confo o19) (confo o35))
(@nonoverlap_oo (confo o20) (confo o21))
(@nonoverlap_oo (confo o20) (confo o22))
(@nonoverlap_oo (confo o20) (confo o23))
(@nonoverlap_oo (confo o20) (confo o24))
(@nonoverlap_oo (confo o20) (confo o25))
(@nonoverlap_oo (confo o20) (confo o26))
(@nonoverlap_oo (confo o20) (confo o27))
(@nonoverlap_oo (confo o20) (confo o28))
(@nonoverlap_oo (confo o20) (confo o29))
(@nonoverlap_oo (confo o20) (confo o30))
(@nonoverlap_oo (confo o20) (confo o31))
(@nonoverlap_oo (confo o20) (confo o32))
(@nonoverlap_oo (confo o20) (confo o33))
(@nonoverlap_oo (confo o20) (confo o34))
(@nonoverlap_oo (confo o20) (confo o35))
(@nonoverlap_oo (confo o21) (confo o22))
(@nonoverlap_oo (confo o21) (confo o23))
(@nonoverlap_oo (confo o21) (confo o24))
(@nonoverlap_oo (confo o21) (confo o25))
(@nonoverlap_oo (confo o21) (confo o26))
(@nonoverlap_oo (confo o21) (confo o27))
(@nonoverlap_oo (confo o21) (confo o28))
(@nonoverlap_oo (confo o21) (confo o29))
(@nonoverlap_oo (confo o21) (confo o30))
(@nonoverlap_oo (confo o21) (confo o31))
(@nonoverlap_oo (confo o21) (confo o32))
(@nonoverlap_oo (confo o21) (confo o33))
(@nonoverlap_oo (confo o21) (confo o34))
(@nonoverlap_oo (confo o21) (confo o35))
(@nonoverlap_oo (confo o22) (confo o23))
(@nonoverlap_oo (confo o22) (confo o24))
(@nonoverlap_oo (confo o22) (confo o25))
(@nonoverlap_oo (confo o22) (confo o26))
(@nonoverlap_oo (confo o22) (confo o27))
(@nonoverlap_oo (confo o22) (confo o28))
(@nonoverlap_oo (confo o22) (confo o29))
(@nonoverlap_oo (confo o22) (confo o30))
(@nonoverlap_oo (confo o22) (confo o31))
(@nonoverlap_oo (confo o22) (confo o32))
(@nonoverlap_oo (confo o22) (confo o33))
(@nonoverlap_oo (confo o22) (confo o34))
(@nonoverlap_oo (confo o22) (confo o35))
(@nonoverlap_oo (confo o23) (confo o24))
(@nonoverlap_oo (confo o23) (confo o25))
(@nonoverlap_oo (confo o23) (confo o26))
(@nonoverlap_oo (confo o23) (confo o27))
(@nonoverlap_oo (confo o23) (confo o28))
(@nonoverlap_oo (confo o23) (confo o29))
(@nonoverlap_oo (confo o23) (confo o30))
(@nonoverlap_oo (confo o23) (confo o31))
(@nonoverlap_oo (confo o23) (confo o32))
(@nonoverlap_oo (confo o23) (confo o33))
(@nonoverlap_oo (confo o23) (confo o34))
(@nonoverlap_oo (confo o23) (confo o35))
(@nonoverlap_oo (confo o24) (confo o25))
(@nonoverlap_oo (confo o24) (confo o26))
(@nonoverlap_oo (confo o24) (confo o27))
(@nonoverlap_oo (confo o24) (confo o28))
(@nonoverlap_oo (confo o24) (confo o29))
(@nonoverlap_oo (confo o24) (confo o30))
(@nonoverlap_oo (confo o24) (confo o31))
(@nonoverlap_oo (confo o24) (confo o32))
(@nonoverlap_oo (confo o24) (confo o33))
(@nonoverlap_oo (confo o24) (confo o34))
(@nonoverlap_oo (confo o24) (confo o35))
(@nonoverlap_oo (confo o25) (confo o26))
(@nonoverlap_oo (confo o25) (confo o27))
(@nonoverlap_oo (confo o25) (confo o28))
(@nonoverlap_oo (confo o25) (confo o29))
(@nonoverlap_oo (confo o25) (confo o30))
(@nonoverlap_oo (confo o25) (confo o31))
(@nonoverlap_oo (confo o25) (confo o32))
(@nonoverlap_oo (confo o25) (confo o33))
(@nonoverlap_oo (confo o25) (confo o34))
(@nonoverlap_oo (confo o25) (confo o35))
(@nonoverlap_oo (confo o26) (confo o27))
(@nonoverlap_oo (confo o26) (confo o28))
(@nonoverlap_oo (confo o26) (confo o29))
(@nonoverlap_oo (confo o26) (confo o30))
(@nonoverlap_oo (confo o26) (confo o31))
(@nonoverlap_oo (confo o26) (confo o32))
(@nonoverlap_oo (confo o26) (confo o33))
(@nonoverlap_oo (confo o26) (confo o34))
(@nonoverlap_oo (confo o26) (confo o35))
(@nonoverlap_oo (confo o27) (confo o28))
(@nonoverlap_oo (confo o27) (confo o29))
(@nonoverlap_oo (confo o27) (confo o30))
(@nonoverlap_oo (confo o27) (confo o31))
(@nonoverlap_oo (confo o27) (confo o32))
(@nonoverlap_oo (confo o27) (confo o33))
(@nonoverlap_oo (confo o27) (confo o34))
(@nonoverlap_oo (confo o27) (confo o35))
(@nonoverlap_oo (confo o28) (confo o29))
(@nonoverlap_oo (confo o28) (confo o30))
(@nonoverlap_oo (confo o28) (confo o31))
(@nonoverlap_oo (confo o28) (confo o32))
(@nonoverlap_oo (confo o28) (confo o33))
(@nonoverlap_oo (confo o28) (confo o34))
(@nonoverlap_oo (confo o28) (confo o35))
(@nonoverlap_oo (confo o29) (confo o30))
(@nonoverlap_oo (confo o29) (confo o31))
(@nonoverlap_oo (confo o29) (confo o32))
(@nonoverlap_oo (confo o29) (confo o33))
(@nonoverlap_oo (confo o29) (confo o34))
(@nonoverlap_oo (confo o29) (confo o35))
(@nonoverlap_oo (confo o30) (confo o31))
(@nonoverlap_oo (confo o30) (confo o32))
(@nonoverlap_oo (confo o30) (confo o33))
(@nonoverlap_oo (confo o30) (confo o34))
(@nonoverlap_oo (confo o30) (confo o35))
(@nonoverlap_oo (confo o31) (confo o32))
(@nonoverlap_oo (confo o31) (confo o33))
(@nonoverlap_oo (confo o31) (confo o34))
(@nonoverlap_oo (confo o31) (confo o35))
(@nonoverlap_oo (confo o32) (confo o33))
(@nonoverlap_oo (confo o32) (confo o34))
(@nonoverlap_oo (confo o32) (confo o35))
(@nonoverlap_oo (confo o33) (confo o34))
(@nonoverlap_oo (confo o33) (confo o35))
(@nonoverlap_oo (confo o34) (confo o35))



)
)
