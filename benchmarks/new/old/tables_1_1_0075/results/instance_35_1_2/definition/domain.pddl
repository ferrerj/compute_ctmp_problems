;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; Robot domain with mobile objects
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; A robot moves around a 2D room with possible obstacles.
;;; The encoding uses state constraints to handle the non-overlapping requirement
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(define (domain fn-robot-navigation)

    (:types

        ;; Any mobile body
        body_id - object

        ;; A mobile body with autonomous motion
        robot_id - body_id

        ;; A mobile body without autonomous motion - only these can be moved by the robots
        nullable_object_id - body_id
        
        object_id - nullable_object_id

        ;; an identifier of a robot base configuration
        conf_base - object

        ;; an identifier of a robot arm configuration
        conf_arm - object

        ;; an identifier of an object configuration
        conf_obj - object

        ;; a possible direction for a translation
        edge - object

        ;; a possible trajectory for the arm
        trajectory - object

    )

    (:constants
        rob - robot_id
        c_held - conf_obj
        ;; undefined confs
        undef_base - conf_base
        undef_obj - conf_obj
        undef_arm - conf_arm
        t_init - trajectory
        ca0 - conf_arm
        no_object - nullable_object_id
    )

    (:predicates
        
		(@graspable  ?b_conf - conf_base ?a_conf - conf_arm  ?o_conf - conf_obj)
        (@placeable  ?b_conf - conf_base ?a_conf - conf_arm)

        (@nonoverlap_ro ?cb - conf_base ?t - trajectory ?o_conf - conf_obj)
        (@nonoverlap_oo ?o1_conf - conf_obj ?o2_conf - conf_obj)

        (@source_base ?r_conf - conf_base ?e - edge)
        (@source_arm ?r_conf - conf_arm ?t - trajectory)
    )

    (:functions
        ;; Returns the current configuration of the given body
        (confb ?b - robot_id) - conf_base

        (confa ?b - robot_id) - conf_arm

        (confo ?b - object_id) - conf_obj

        (traj ?b - robot_id) - trajectory

        (holding) - nullable_object_id

        (@target_base ?c - conf_base ?e - edge) - conf_base

        (@target_arm ?c - conf_arm ?t - trajectory) - conf_arm

        (@placing_pose ?b_conf - conf_base ?a_conf - conf_arm) - conf_obj
    )

    ;; Transition actions for robot base
    (:action transition_base :parameters (?e - edge)
        :precondition (and
                          (= (confa rob) ca0)
                          (@source_base (confb rob) ?e))
        :effect (and (assign (confb rob) (@target_base (confb rob) ?e)) )

    )

    ;; Transition actions for robot arm
    (:action transition_arm :parameters(?t - trajectory)
        :precondition (and (@source_arm (confa rob) ?t))
        :effect (and (assign (confa rob) (@target_arm (confa rob) ?t))
                     (assign (traj rob) ?t)

            )
    )


    (:action grasp-object
      :parameters (?o - object_id)
      :precondition (and 
        (= (holding) no_object)   
        (@graspable (confb rob) (confa rob) (confo ?o))
        )
      :effect (and 
            (assign (holding) ?o)
            (assign (confo ?o) c_held)
        )
      )

    (:action place-object
        :parameters (?o - object_id)
        :precondition (and 
            (= (holding) ?o)
            (@placeable (confb rob) (confa rob) ))
        :effect (and
            (assign (holding) no_object)
            (assign (confo ?o) (@placing_pose (confb rob) (confa rob)))
            )
    )
)




