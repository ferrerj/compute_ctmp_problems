
#include "external.hxx"
#include <constraints/registry.hxx>
#include <constraints/gecode/translators/component_translator.hxx>


//! Template instantiations
// template void Serializer::BoostDeserialize<table>(const std::string& filename, table& data);


void External::registerComponents() const {
        LogicalComponentRegistry::instance().
			addFormulaCreator("@nonoverlap_ro", [](const std::vector<const fs::Term*>& subterms){ return new NonoverlapROFormula(subterms); });

        LogicalComponentRegistry::instance().
			addFormulaCreator("@nonoverlap_oo", [](const std::vector<const fs::Term*>& subterms){ return new NonoverlapOOFormula(subterms); });

        // Explicitly register an extensional translator that will be used with gecode
        LogicalComponentRegistry::instance().add(typeid(NonoverlapROFormula), new gecode::ExtensionalTranslator());
		LogicalComponentRegistry::instance().add(typeid(NonoverlapOOFormula), new gecode::ExtensionalTranslator());
}
