
#pragma once

#include <fs_types.hxx>
#include <lib/rapidjson/document.h>
#include "external.hxx"

#include <utils/component_factory.hxx>

namespace fs0 { class Problem; }

using namespace fs0;



class ComponentFactory : public BaseComponentFactory {
    std::map<std::string, Function> instantiateFunctions(const ProblemInfo& info) const override {
		return {
			{"@graspable", [](const ObjectIdxVector& parameters){ return static_cast<const External&>(ProblemInfo::getInstance().get_external()).graspable(parameters); }},
			{"@placeable", [](const ObjectIdxVector& parameters){ return static_cast<const External&>(ProblemInfo::getInstance().get_external()).placeable(parameters); }},
			{"@target_arm", [](const ObjectIdxVector& parameters){ return static_cast<const External&>(ProblemInfo::getInstance().get_external()).target_arm(parameters); }},
			{"@target_base", [](const ObjectIdxVector& parameters){ return static_cast<const External&>(ProblemInfo::getInstance().get_external()).target_base(parameters); }},
			{"@source_base", [](const ObjectIdxVector& parameters){ return static_cast<const External&>(ProblemInfo::getInstance().get_external()).source_base(parameters); }},
			{"@nonoverlap_ro", [](const ObjectIdxVector& parameters){ return static_cast<const External&>(ProblemInfo::getInstance().get_external()).nonoverlap_ro(parameters); }},
			{"@nonoverlap_oo", [](const ObjectIdxVector& parameters){ return static_cast<const External&>(ProblemInfo::getInstance().get_external()).nonoverlap_oo(parameters); }},
			{"@source_arm", [](const ObjectIdxVector& parameters){ return static_cast<const External&>(ProblemInfo::getInstance().get_external()).source_arm(parameters); }},
			{"@placing_pose", [](const ObjectIdxVector& parameters){ return static_cast<const External&>(ProblemInfo::getInstance().get_external()).placing_pose(parameters); }}
		};
	}
};

/* Generate the whole planning problem */
Problem* generate(const rapidjson::Document& data, const std::string& data_dir);
