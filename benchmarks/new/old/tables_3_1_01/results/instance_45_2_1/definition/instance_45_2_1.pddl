(define (problem sample)
	(:domain fn-robot-navigation)
	(:objects 

		o1 o2 o3 o4 o5 o6 o7 o8 o9 o10 o11 o12 o13 o14 o15 o16 o17 o18 o19 o20 o21 o22 o23 o24 o25 o26 o27 o28 o29 o30 o31 o32 o33 o34 o35 o36 o37 o38 o39 o40 o41 o42 o43 o44 o45 

 - object_id

	cb23
	cb22
	cb25
	cb26
	cb24
	cb30
	cb31
	cb27
	cb28
	cb29
	cb18
	cb19
	cb20
	cb21
	cb32
	cb33
	cb34
	cb35
	cb36
	cb37
	cb38
	cb39
	cb40
	cb66
	cb64
	cb65
	cb63
	cb62
	cb72
	cb71
	cb74
	cb73
	cb68
	cb67
	cb69
	cb70
	cb43
	cb42
	cb44
	cb41
	cb45
	cb46
	cb47
	cb48
	cb79
	cb82
	cb80
	cb81
	cb77
	cb76
	cb75
	cb78
	cb49
	cb51
	cb50
	cb52
	cb14
	cb15
	cb16
	cb17
	cb0
	cb83
	cb84
	cb85
	cb86
	cb1
	cb2
	cb3
	cb4
	cb5
	cb89
	cb87
	cb88
	cb91
	cb90
	cb13
	cb10
	cb12
	cb11
	cb7
	cb6
	cb9
	cb8
	cb53
	cb54
	cb55
	cb56
	cb60
	cb59
	cb61
	cb57
	cb58
	 - conf_base
	co97
	co119
	co28
	co70
	co188
	co79
	co1
	co89
	co153
	co12
	co171
	co39
	co112
	co175
	co73
	co106
	co161
	co21
	co8
	co13
	co162
	co47
	co108
	co36
	co49
	co69
	co30
	co14
	co149
	co163
	co141
	co191
	co88
	co139
	co172
	co178
	co32
	co137
	co152
	co91
	co65
	co78
	co140
	co81
	co125
	co43
	co18
	co37
	co57
	co38
	co29
	co11
	co23
	co99
	co177
	co98
	co173
	co121
	co42
	co20
	co5
	co184
	co75
	co146
	co186
	co48
	co109
	co63
	co86
	co160
	co124
	co101
	co58
	co165
	co176
	co111
	co155
	co55
	co27
	co190
	co50
	co80
	co154
	co2
	co87
	co19
	co164
	co90
	co22
	co7
	co123
	co147
	co100
	co31
	co110
	co74
	co148
	co6
	co150
	co144
	co180
	co136
	co3
	co134
	co103
	co116
	co0
	co183
	co93
	co166
	co156
	co179
	co157
	co192
	co102
	co82
	co92
	co114
	co127
	co129
	co133
	co143
	co135
	co169
	co76
	co83
	co170
	co115
	co68
	co84
	co77
	co130
	co94
	co104
	co9
	co72
	co159
	co151
	co158
	co181
	co182
	co167
	co145
	co71
	co168
	co35
	co26
	co17
	co118
	co10
	co4
	co85
	co41
	co45
	co52
	co54
	co46
	co34
	co60
	co61
	co62
	co117
	co105
	co16
	co128
	co95
	co25
	co53
	co185
	co66
	co142
	co113
	co174
	co107
	co189
	co131
	co120
	co64
	co132
	co126
	co187
	co122
	co96
	co56
	co138
	co15
	co24
	co33
	co40
	co44
	co51
	co59
	co67
 - conf_obj 
 
	ca13
	ca6
	ca20
	ca3
	ca2
	ca10
	ca8
	ca7
	ca12
	ca14
	ca11
	ca17
	ca18
	ca19
	ca16
	ca15
	ca25
	ca23
	ca24
	ca21
	ca22
	ca1
	ca9
	ca5
	ca4
	
 - conf_arm 
 
	e202
	e203
	e204
	e10064
	e10068
	e10072
	e10076
	e10112
	e10116
	e10120
	e10124
	e10202
	e10205
	e10208
	e10211
	e10214
	e10259
	e10262
	e10265
	e10268
	e217
	e218
	e219
	e220
	e221
	e222
	e223
	e224
	e225
	e226
	e227
	e228
	e229
	e230
	e231
	e262
	e263
	e264
	e259
	e260
	e261
	e268
	e269
	e270
	e265
	e266
	e267
	e250
	e251
	e252
	e256
	e257
	e258
	e253
	e254
	e255
	e247
	e248
	e249
	e190
	e191
	e192
	e10190
	e10193
	e10196
	e10199
	e10283
	e10286
	e10289
	e10292
	e10295
	e193
	e194
	e195
	e196
	e197
	e198
	e199
	e200
	e201
	e35
	e36
	e37
	e10022
	e10025
	e10028
	e10031
	e10034
	e10035
	e10038
	e10041
	e10044
	e10180
	e10183
	e10186
	e10189
	e10204
	e10273
	e10276
	e10279
	e10282
	e10284
	e10287
	e10290
	e10293
	e10296
	e38
	e39
	e40
	e41
	e42
	e43
	e44
	e45
	e46
	e4
	e5
	e6
	e7
	e0
	e1
	e2
	e3
	e10000
	e10004
	e10011
	e10015
	e10019
	e10021
	e10024
	e10027
	e10030
	e10033
	e10036
	e10039
	e10042
	e10045
	e10191
	e10194
	e10197
	e10200
	e10203
	e10207
	e10210
	e10213
	e10216
	e10219
	e10222
	e10225
	e10228
	e10231
	e10249
	e10252
	e10255
	e10258
	e10261
	e10264
	e10267
	e10270
	e10285
	e10288
	e10291
	e10294
	e10297
	e16
	e17
	e18
	e19
	e10007
	e8
	e9
	e10
	e11
	e10003
	e10008
	e10012
	e10016
	e12
	e13
	e14
	e15
	e26
	e27
	e28
	e29
	e30
	e31
	e32
	e33
	e34
	e20
	e21
	e22
	e10001
	e10005
	e10009
	e10013
	e10017
	e10020
	e10023
	e10026
	e10029
	e10032
	e10037
	e10040
	e10043
	e10046
	e10168
	e10171
	e10174
	e10177
	e10192
	e10195
	e10198
	e10201
	e10206
	e10209
	e10212
	e10215
	e10234
	e10237
	e10240
	e10243
	e10246
	e23
	e24
	e25
	e47
	e48
	e49
	e10047
	e10050
	e10053
	e10056
	e10060
	e10063
	e10067
	e10071
	e10075
	e10128
	e10132
	e10136
	e10140
	e10143
	e10146
	e10149
	e10152
	e50
	e51
	e52
	e10059
	e53
	e54
	e55
	e56
	e57
	e58
	e59
	e60
	e61
	e10048
	e62
	e63
	e64
	e65
	e10051
	e10054
	e10057
	e10062
	e10066
	e10070
	e10074
	e10081
	e10085
	e10089
	e10093
	e10097
	e10127
	e10131
	e10135
	e10139
	e10179
	e10182
	e10185
	e10188
	e66
	e67
	e68
	e69
	e70
	e71
	e72
	e73
	e74
	e75
	e76
	e77
	e138
	e139
	e140
	e141
	e130
	e131
	e132
	e133
	e126
	e127
	e128
	e129
	e10049
	e10052
	e10055
	e10058
	e10061
	e10113
	e10117
	e10121
	e10125
	e10126
	e10130
	e10134
	e10138
	e10144
	e10147
	e10150
	e10153
	e10218
	e10221
	e10224
	e10227
	e10230
	e134
	e135
	e136
	e137
	e142
	e143
	e144
	e10142
	e10145
	e10148
	e10151
	e10272
	e10275
	e10278
	e10281
	e145
	e146
	e147
	e148
	e149
	e150
	e151
	e152
	e153
	e178
	e179
	e180
	e10178
	e10181
	e10184
	e10187
	e10217
	e10220
	e10223
	e10226
	e10229
	e184
	e185
	e186
	e181
	e182
	e183
	e187
	e188
	e189
	e271
	e272
	e273
	e10271
	e10274
	e10277
	e10280
	e274
	e275
	e276
	e277
	e278
	e279
	e280
	e281
	e282
	e292
	e293
	e294
	e295
	e296
	e297
	e286
	e287
	e288
	e283
	e284
	e285
	e289
	e290
	e291
	e94
	e95
	e96
	e97
	e10079
	e90
	e91
	e92
	e93
	e82
	e83
	e84
	e85
	e10094
	e86
	e87
	e88
	e89
	e78
	e79
	e80
	e81
	e10065
	e10069
	e10073
	e10077
	e10078
	e10082
	e10086
	e10090
	e10095
	e10100
	e10103
	e10106
	e10109
	e10111
	e10115
	e10119
	e10123
	e10156
	e10159
	e10162
	e10165
	e10167
	e10170
	e10173
	e10176
	e10248
	e10251
	e10254
	e10257
	e114
	e115
	e116
	e117
	e110
	e111
	e112
	e113
	e10083
	e10087
	e10091
	e10099
	e10102
	e10105
	e10108
	e10110
	e10114
	e10118
	e10122
	e10129
	e10133
	e10137
	e10141
	e118
	e119
	e120
	e121
	e122
	e123
	e124
	e125
	e101
	e102
	e103
	e98
	e99
	e100
	e10080
	e10084
	e10088
	e10092
	e10096
	e10098
	e10101
	e10104
	e10107
	e10155
	e10158
	e10161
	e10164
	e107
	e108
	e109
	e104
	e105
	e106
	e172
	e173
	e174
	e169
	e170
	e171
	e166
	e167
	e168
	e10166
	e10169
	e10172
	e10175
	e10247
	e10250
	e10253
	e10256
	e10260
	e10263
	e10266
	e10269
	e175
	e176
	e177
	e154
	e155
	e156
	e10154
	e10157
	e10160
	e10163
	e10233
	e10236
	e10239
	e10242
	e10245
	e160
	e161
	e162
	e163
	e164
	e165
	e157
	e158
	e159
	e205
	e206
	e207
	e10002
	e10006
	e10010
	e10014
	e10018
	e208
	e209
	e210
	e211
	e212
	e213
	e214
	e215
	e216
	e235
	e236
	e237
	e238
	e239
	e240
	e232
	e233
	e234
	e10232
	e10235
	e10238
	e10241
	e10244
	e244
	e245
	e246
	e241
	e242
	e243
 - edge 
 
	t0
	t1
	t2
	t3
	t4
	t5
	t6
	t7
	t8
	t9
	t10
	t11
	t12
	t13
	t14
	t15
	t16
	t17
	t18
	t19
	t20
	t21
	t22
	t23
	t24
	t25
	t26
	t27
	t28
	t29
	t30
	t31
	t32
	t33
	t34
	t35
	t36
	t37
	t38
	t39
	t40
	t41
	t42
	t43
	t44
	t45
	t46
	t47
	t48
	t49
	t50
	t51
	t52
	t53
	t54
	t55
	t56
	t57
	t58
	t59
	t60
	t61
	t62
	t63
	t64
	t65
	t66
	t67
	t68
	t69
	t70
	t71
	t72
	t73
	t74
	t75
	t76
	t77
	t78
	t79
	t80
	t81
	t82
	t83
	t84
	t85
	t86
	t87
	t88
	t89
	t90
	t91
	t10000
	t10001
	t10002
	t10003
	t10004
	t10005
	t10006
	t10007
	t10008
	t10009
	t10010
	t10011
	t10012
	t10013
	t10014
	t10015
	t10016
	t10017
	t10018
	t10019
	t10020
	t10021
	t10022
	t10023
	t10024
	t10025
	t10026
	t10027
	t10028
	t10029
	t10030
	t10031
	t10032
	t10033
	t10034
	t10035
	t10036
	t10037
	t10038
	t10039
	t10040
	t10041
	t10042
	t10043
	t10044
	t10045
	t10046
	t10047
	t10048
	t10049
	t10050
	t10051
	t10052
	t10053
	t10054
	t10055
	t10056
	t10057
	t10058
	t10059
	t10060
	t10061
	t10062
	t10063
	t10064
	t10065
	t10066
	t10067
	t10068
	t10069
	t10070
	t10071
	t10072
	t10073
	t10074
	t10075
	t10076
	t10077
	t10078
	t10079
	t10080
	t10081
	t10082
	t10083
	t10084
	t10085
	t10086
	t10087
	t10088
	t10089
	t10090
	t10091
 - trajectory 
 )
(:init
(= (confb rob ) cb0 )
(= (confa rob ) ca0 )
(= (traj rob ) t_init )
(= (holding) no_object)
; initial object configurations 
 
(= (confo o1) co165); -x -0.932032 -y -1.98884 -z 0.680012
(= (confo o2) co100); -x -1.30009 -y -1.85451 -z 0.679896
(= (confo o3) co83); -x 0.943585 -y -0.00522411 -z 0.679884
(= (confo o4) co192); -x 0.854701 -y 0.0993361 -z 0.680006
(= (confo o5) co156); -x 0.853519 -y -0.401283 -z 0.679892
(= (confo o6) co61); -x 1.03984 -y -0.220016 -z 0.68002
(= (confo o7) co97); -x -1.30045 -y 2.14544 -z 0.679896
(= (confo o8) co67); -x 0.654877 -y 0.299741 -z 0.680015
(= (confo o9) co134); -x 0.995056 -y -0.399165 -z 0.679886
(= (confo o10) co155); -x -1.02054 -y -2.03969 -z 0.679901
(= (confo o11) co135); -x 0.943358 -y -0.19364 -z 0.680001
(= (confo o12) co44); -x 0.654607 -y 9.72179e-05 -z 0.67989
(= (confo o13) co162); -x -1.16817 -y 1.99784 -z 0.679896
(= (confo o14) co169); -x 0.924641 -y 0.122367 -z 0.680005
(= (confo o15) co15); -x 0.654197 -y -0.399941 -z 0.680007
(= (confo o16) co186); -x -1.00071 -y 2.04534 -z 0.679896
(= (confo o17) co59); -x 0.654786 -y 0.200009 -z 0.680007
(= (confo o18) co109); -x -1.00135 -y 2.14512 -z 0.680011
(= (confo o19) co127); -x 0.75468 -y 8.20082e-05 -z 0.680007
(= (confo o20) co82); -x 0.754255 -y -0.400028 -z 0.67989
(= (confo o21) co133); -x 0.754793 -y 0.199692 -z 0.680001
(= (confo o22) co129); -x 0.754805 -y 0.100017 -z 0.679888
(= (confo o23) co168); -x 0.895219 -y -0.0987018 -z 0.679899
(= (confo o24) co25); -x 1.06618 -y -0.0763397 -z 0.68002
(= (confo o25) co84); -x 0.995584 -y 0.200118 -z 0.680018
(= (confo o26) co74); -x -1.388 -y -1.95787 -z 0.680012
(= (confo o27) co92); -x 0.754421 -y -0.299992 -z 0.680007
(= (confo o28) co130); -x 0.995205 -y -0.299479 -z 0.680015
(= (confo o29) co166); -x 0.853775 -y -0.299365 -z 0.680007
(= (confo o30) co111); -x -1.00144 -y -1.85489 -z 0.680011
(= (confo o31) co70); -x -1.24404 -y -2.05323 -z 0.679884
(= (confo o32) co152); -x -1.02033 -y 1.96044 -z 0.679738
(= (confo o33) co117); -x 1.02101 -y 0.0952791 -z 0.680001
(= (confo o34) co24); -x 0.654298 -y -0.299892 -z 0.680007
(= (confo o35) co170); -x 1.01036 -y 0.328515 -z 0.680006
(= (confo o36) co69); -x -1.24406 -y 1.9467 -z 0.679899
(= (confo o37) co33); -x 0.6544 -y -0.199859 -z 0.67989
(= (confo o38) co102); -x 0.754503 -y -0.200014 -z 0.67989
(= (confo o39) co68); -x 0.866519 -y 0.285761 -z 0.679891
(= (confo o40) co143); -x 0.754896 -y 0.299238 -z 0.67989
(= (confo o41) co149); -x -1.10737 -y -2.08994 -z 0.679975
(= (confo o42) co88); -x -1.19457 -y 2.0936 -z 0.679896
(= (confo o43) co187); -x -0.782869 -y 2.1002 -z 0.679894
(= (confo o44) co51); -x 0.654665 -y 0.100083 -z 0.67989
(= (confo o45) co189); -x -0.782868 -y -1.89984 -z 0.679894


)
(:goal (and

;Red cylinders
(= (confo o16) co21)
(= (confo o4) co178)
;(= (confo o14) co75)

;Blue cylinders
(= (confo o41) co29)
(= (confo o27) co86)
;(= (confo o11) co48)

))
(:constraints

(@nonoverlap_ro (confb rob) (traj rob) (confo o1))
(@nonoverlap_ro (confb rob) (traj rob) (confo o2))
(@nonoverlap_ro (confb rob) (traj rob) (confo o3))
(@nonoverlap_ro (confb rob) (traj rob) (confo o4))
(@nonoverlap_ro (confb rob) (traj rob) (confo o5))
(@nonoverlap_ro (confb rob) (traj rob) (confo o6))
(@nonoverlap_ro (confb rob) (traj rob) (confo o7))
(@nonoverlap_ro (confb rob) (traj rob) (confo o8))
(@nonoverlap_ro (confb rob) (traj rob) (confo o9))
(@nonoverlap_ro (confb rob) (traj rob) (confo o10))
(@nonoverlap_ro (confb rob) (traj rob) (confo o11))
(@nonoverlap_ro (confb rob) (traj rob) (confo o12))
(@nonoverlap_ro (confb rob) (traj rob) (confo o13))
(@nonoverlap_ro (confb rob) (traj rob) (confo o14))
(@nonoverlap_ro (confb rob) (traj rob) (confo o15))
(@nonoverlap_ro (confb rob) (traj rob) (confo o16))
(@nonoverlap_ro (confb rob) (traj rob) (confo o17))
(@nonoverlap_ro (confb rob) (traj rob) (confo o18))
(@nonoverlap_ro (confb rob) (traj rob) (confo o19))
(@nonoverlap_ro (confb rob) (traj rob) (confo o20))
(@nonoverlap_ro (confb rob) (traj rob) (confo o21))
(@nonoverlap_ro (confb rob) (traj rob) (confo o22))
(@nonoverlap_ro (confb rob) (traj rob) (confo o23))
(@nonoverlap_ro (confb rob) (traj rob) (confo o24))
(@nonoverlap_ro (confb rob) (traj rob) (confo o25))
(@nonoverlap_ro (confb rob) (traj rob) (confo o26))
(@nonoverlap_ro (confb rob) (traj rob) (confo o27))
(@nonoverlap_ro (confb rob) (traj rob) (confo o28))
(@nonoverlap_ro (confb rob) (traj rob) (confo o29))
(@nonoverlap_ro (confb rob) (traj rob) (confo o30))
(@nonoverlap_ro (confb rob) (traj rob) (confo o31))
(@nonoverlap_ro (confb rob) (traj rob) (confo o32))
(@nonoverlap_ro (confb rob) (traj rob) (confo o33))
(@nonoverlap_ro (confb rob) (traj rob) (confo o34))
(@nonoverlap_ro (confb rob) (traj rob) (confo o35))
(@nonoverlap_ro (confb rob) (traj rob) (confo o36))
(@nonoverlap_ro (confb rob) (traj rob) (confo o37))
(@nonoverlap_ro (confb rob) (traj rob) (confo o38))
(@nonoverlap_ro (confb rob) (traj rob) (confo o39))
(@nonoverlap_ro (confb rob) (traj rob) (confo o40))
(@nonoverlap_ro (confb rob) (traj rob) (confo o41))
(@nonoverlap_ro (confb rob) (traj rob) (confo o42))
(@nonoverlap_ro (confb rob) (traj rob) (confo o43))
(@nonoverlap_ro (confb rob) (traj rob) (confo o44))
(@nonoverlap_ro (confb rob) (traj rob) (confo o45))
(@nonoverlap_oo (confo o1) (confo o2))
(@nonoverlap_oo (confo o1) (confo o3))
(@nonoverlap_oo (confo o1) (confo o4))
(@nonoverlap_oo (confo o1) (confo o5))
(@nonoverlap_oo (confo o1) (confo o6))
(@nonoverlap_oo (confo o1) (confo o7))
(@nonoverlap_oo (confo o1) (confo o8))
(@nonoverlap_oo (confo o1) (confo o9))
(@nonoverlap_oo (confo o1) (confo o10))
(@nonoverlap_oo (confo o1) (confo o11))
(@nonoverlap_oo (confo o1) (confo o12))
(@nonoverlap_oo (confo o1) (confo o13))
(@nonoverlap_oo (confo o1) (confo o14))
(@nonoverlap_oo (confo o1) (confo o15))
(@nonoverlap_oo (confo o1) (confo o16))
(@nonoverlap_oo (confo o1) (confo o17))
(@nonoverlap_oo (confo o1) (confo o18))
(@nonoverlap_oo (confo o1) (confo o19))
(@nonoverlap_oo (confo o1) (confo o20))
(@nonoverlap_oo (confo o1) (confo o21))
(@nonoverlap_oo (confo o1) (confo o22))
(@nonoverlap_oo (confo o1) (confo o23))
(@nonoverlap_oo (confo o1) (confo o24))
(@nonoverlap_oo (confo o1) (confo o25))
(@nonoverlap_oo (confo o1) (confo o26))
(@nonoverlap_oo (confo o1) (confo o27))
(@nonoverlap_oo (confo o1) (confo o28))
(@nonoverlap_oo (confo o1) (confo o29))
(@nonoverlap_oo (confo o1) (confo o30))
(@nonoverlap_oo (confo o1) (confo o31))
(@nonoverlap_oo (confo o1) (confo o32))
(@nonoverlap_oo (confo o1) (confo o33))
(@nonoverlap_oo (confo o1) (confo o34))
(@nonoverlap_oo (confo o1) (confo o35))
(@nonoverlap_oo (confo o1) (confo o36))
(@nonoverlap_oo (confo o1) (confo o37))
(@nonoverlap_oo (confo o1) (confo o38))
(@nonoverlap_oo (confo o1) (confo o39))
(@nonoverlap_oo (confo o1) (confo o40))
(@nonoverlap_oo (confo o1) (confo o41))
(@nonoverlap_oo (confo o1) (confo o42))
(@nonoverlap_oo (confo o1) (confo o43))
(@nonoverlap_oo (confo o1) (confo o44))
(@nonoverlap_oo (confo o1) (confo o45))
(@nonoverlap_oo (confo o2) (confo o3))
(@nonoverlap_oo (confo o2) (confo o4))
(@nonoverlap_oo (confo o2) (confo o5))
(@nonoverlap_oo (confo o2) (confo o6))
(@nonoverlap_oo (confo o2) (confo o7))
(@nonoverlap_oo (confo o2) (confo o8))
(@nonoverlap_oo (confo o2) (confo o9))
(@nonoverlap_oo (confo o2) (confo o10))
(@nonoverlap_oo (confo o2) (confo o11))
(@nonoverlap_oo (confo o2) (confo o12))
(@nonoverlap_oo (confo o2) (confo o13))
(@nonoverlap_oo (confo o2) (confo o14))
(@nonoverlap_oo (confo o2) (confo o15))
(@nonoverlap_oo (confo o2) (confo o16))
(@nonoverlap_oo (confo o2) (confo o17))
(@nonoverlap_oo (confo o2) (confo o18))
(@nonoverlap_oo (confo o2) (confo o19))
(@nonoverlap_oo (confo o2) (confo o20))
(@nonoverlap_oo (confo o2) (confo o21))
(@nonoverlap_oo (confo o2) (confo o22))
(@nonoverlap_oo (confo o2) (confo o23))
(@nonoverlap_oo (confo o2) (confo o24))
(@nonoverlap_oo (confo o2) (confo o25))
(@nonoverlap_oo (confo o2) (confo o26))
(@nonoverlap_oo (confo o2) (confo o27))
(@nonoverlap_oo (confo o2) (confo o28))
(@nonoverlap_oo (confo o2) (confo o29))
(@nonoverlap_oo (confo o2) (confo o30))
(@nonoverlap_oo (confo o2) (confo o31))
(@nonoverlap_oo (confo o2) (confo o32))
(@nonoverlap_oo (confo o2) (confo o33))
(@nonoverlap_oo (confo o2) (confo o34))
(@nonoverlap_oo (confo o2) (confo o35))
(@nonoverlap_oo (confo o2) (confo o36))
(@nonoverlap_oo (confo o2) (confo o37))
(@nonoverlap_oo (confo o2) (confo o38))
(@nonoverlap_oo (confo o2) (confo o39))
(@nonoverlap_oo (confo o2) (confo o40))
(@nonoverlap_oo (confo o2) (confo o41))
(@nonoverlap_oo (confo o2) (confo o42))
(@nonoverlap_oo (confo o2) (confo o43))
(@nonoverlap_oo (confo o2) (confo o44))
(@nonoverlap_oo (confo o2) (confo o45))
(@nonoverlap_oo (confo o3) (confo o4))
(@nonoverlap_oo (confo o3) (confo o5))
(@nonoverlap_oo (confo o3) (confo o6))
(@nonoverlap_oo (confo o3) (confo o7))
(@nonoverlap_oo (confo o3) (confo o8))
(@nonoverlap_oo (confo o3) (confo o9))
(@nonoverlap_oo (confo o3) (confo o10))
(@nonoverlap_oo (confo o3) (confo o11))
(@nonoverlap_oo (confo o3) (confo o12))
(@nonoverlap_oo (confo o3) (confo o13))
(@nonoverlap_oo (confo o3) (confo o14))
(@nonoverlap_oo (confo o3) (confo o15))
(@nonoverlap_oo (confo o3) (confo o16))
(@nonoverlap_oo (confo o3) (confo o17))
(@nonoverlap_oo (confo o3) (confo o18))
(@nonoverlap_oo (confo o3) (confo o19))
(@nonoverlap_oo (confo o3) (confo o20))
(@nonoverlap_oo (confo o3) (confo o21))
(@nonoverlap_oo (confo o3) (confo o22))
(@nonoverlap_oo (confo o3) (confo o23))
(@nonoverlap_oo (confo o3) (confo o24))
(@nonoverlap_oo (confo o3) (confo o25))
(@nonoverlap_oo (confo o3) (confo o26))
(@nonoverlap_oo (confo o3) (confo o27))
(@nonoverlap_oo (confo o3) (confo o28))
(@nonoverlap_oo (confo o3) (confo o29))
(@nonoverlap_oo (confo o3) (confo o30))
(@nonoverlap_oo (confo o3) (confo o31))
(@nonoverlap_oo (confo o3) (confo o32))
(@nonoverlap_oo (confo o3) (confo o33))
(@nonoverlap_oo (confo o3) (confo o34))
(@nonoverlap_oo (confo o3) (confo o35))
(@nonoverlap_oo (confo o3) (confo o36))
(@nonoverlap_oo (confo o3) (confo o37))
(@nonoverlap_oo (confo o3) (confo o38))
(@nonoverlap_oo (confo o3) (confo o39))
(@nonoverlap_oo (confo o3) (confo o40))
(@nonoverlap_oo (confo o3) (confo o41))
(@nonoverlap_oo (confo o3) (confo o42))
(@nonoverlap_oo (confo o3) (confo o43))
(@nonoverlap_oo (confo o3) (confo o44))
(@nonoverlap_oo (confo o3) (confo o45))
(@nonoverlap_oo (confo o4) (confo o5))
(@nonoverlap_oo (confo o4) (confo o6))
(@nonoverlap_oo (confo o4) (confo o7))
(@nonoverlap_oo (confo o4) (confo o8))
(@nonoverlap_oo (confo o4) (confo o9))
(@nonoverlap_oo (confo o4) (confo o10))
(@nonoverlap_oo (confo o4) (confo o11))
(@nonoverlap_oo (confo o4) (confo o12))
(@nonoverlap_oo (confo o4) (confo o13))
(@nonoverlap_oo (confo o4) (confo o14))
(@nonoverlap_oo (confo o4) (confo o15))
(@nonoverlap_oo (confo o4) (confo o16))
(@nonoverlap_oo (confo o4) (confo o17))
(@nonoverlap_oo (confo o4) (confo o18))
(@nonoverlap_oo (confo o4) (confo o19))
(@nonoverlap_oo (confo o4) (confo o20))
(@nonoverlap_oo (confo o4) (confo o21))
(@nonoverlap_oo (confo o4) (confo o22))
(@nonoverlap_oo (confo o4) (confo o23))
(@nonoverlap_oo (confo o4) (confo o24))
(@nonoverlap_oo (confo o4) (confo o25))
(@nonoverlap_oo (confo o4) (confo o26))
(@nonoverlap_oo (confo o4) (confo o27))
(@nonoverlap_oo (confo o4) (confo o28))
(@nonoverlap_oo (confo o4) (confo o29))
(@nonoverlap_oo (confo o4) (confo o30))
(@nonoverlap_oo (confo o4) (confo o31))
(@nonoverlap_oo (confo o4) (confo o32))
(@nonoverlap_oo (confo o4) (confo o33))
(@nonoverlap_oo (confo o4) (confo o34))
(@nonoverlap_oo (confo o4) (confo o35))
(@nonoverlap_oo (confo o4) (confo o36))
(@nonoverlap_oo (confo o4) (confo o37))
(@nonoverlap_oo (confo o4) (confo o38))
(@nonoverlap_oo (confo o4) (confo o39))
(@nonoverlap_oo (confo o4) (confo o40))
(@nonoverlap_oo (confo o4) (confo o41))
(@nonoverlap_oo (confo o4) (confo o42))
(@nonoverlap_oo (confo o4) (confo o43))
(@nonoverlap_oo (confo o4) (confo o44))
(@nonoverlap_oo (confo o4) (confo o45))
(@nonoverlap_oo (confo o5) (confo o6))
(@nonoverlap_oo (confo o5) (confo o7))
(@nonoverlap_oo (confo o5) (confo o8))
(@nonoverlap_oo (confo o5) (confo o9))
(@nonoverlap_oo (confo o5) (confo o10))
(@nonoverlap_oo (confo o5) (confo o11))
(@nonoverlap_oo (confo o5) (confo o12))
(@nonoverlap_oo (confo o5) (confo o13))
(@nonoverlap_oo (confo o5) (confo o14))
(@nonoverlap_oo (confo o5) (confo o15))
(@nonoverlap_oo (confo o5) (confo o16))
(@nonoverlap_oo (confo o5) (confo o17))
(@nonoverlap_oo (confo o5) (confo o18))
(@nonoverlap_oo (confo o5) (confo o19))
(@nonoverlap_oo (confo o5) (confo o20))
(@nonoverlap_oo (confo o5) (confo o21))
(@nonoverlap_oo (confo o5) (confo o22))
(@nonoverlap_oo (confo o5) (confo o23))
(@nonoverlap_oo (confo o5) (confo o24))
(@nonoverlap_oo (confo o5) (confo o25))
(@nonoverlap_oo (confo o5) (confo o26))
(@nonoverlap_oo (confo o5) (confo o27))
(@nonoverlap_oo (confo o5) (confo o28))
(@nonoverlap_oo (confo o5) (confo o29))
(@nonoverlap_oo (confo o5) (confo o30))
(@nonoverlap_oo (confo o5) (confo o31))
(@nonoverlap_oo (confo o5) (confo o32))
(@nonoverlap_oo (confo o5) (confo o33))
(@nonoverlap_oo (confo o5) (confo o34))
(@nonoverlap_oo (confo o5) (confo o35))
(@nonoverlap_oo (confo o5) (confo o36))
(@nonoverlap_oo (confo o5) (confo o37))
(@nonoverlap_oo (confo o5) (confo o38))
(@nonoverlap_oo (confo o5) (confo o39))
(@nonoverlap_oo (confo o5) (confo o40))
(@nonoverlap_oo (confo o5) (confo o41))
(@nonoverlap_oo (confo o5) (confo o42))
(@nonoverlap_oo (confo o5) (confo o43))
(@nonoverlap_oo (confo o5) (confo o44))
(@nonoverlap_oo (confo o5) (confo o45))
(@nonoverlap_oo (confo o6) (confo o7))
(@nonoverlap_oo (confo o6) (confo o8))
(@nonoverlap_oo (confo o6) (confo o9))
(@nonoverlap_oo (confo o6) (confo o10))
(@nonoverlap_oo (confo o6) (confo o11))
(@nonoverlap_oo (confo o6) (confo o12))
(@nonoverlap_oo (confo o6) (confo o13))
(@nonoverlap_oo (confo o6) (confo o14))
(@nonoverlap_oo (confo o6) (confo o15))
(@nonoverlap_oo (confo o6) (confo o16))
(@nonoverlap_oo (confo o6) (confo o17))
(@nonoverlap_oo (confo o6) (confo o18))
(@nonoverlap_oo (confo o6) (confo o19))
(@nonoverlap_oo (confo o6) (confo o20))
(@nonoverlap_oo (confo o6) (confo o21))
(@nonoverlap_oo (confo o6) (confo o22))
(@nonoverlap_oo (confo o6) (confo o23))
(@nonoverlap_oo (confo o6) (confo o24))
(@nonoverlap_oo (confo o6) (confo o25))
(@nonoverlap_oo (confo o6) (confo o26))
(@nonoverlap_oo (confo o6) (confo o27))
(@nonoverlap_oo (confo o6) (confo o28))
(@nonoverlap_oo (confo o6) (confo o29))
(@nonoverlap_oo (confo o6) (confo o30))
(@nonoverlap_oo (confo o6) (confo o31))
(@nonoverlap_oo (confo o6) (confo o32))
(@nonoverlap_oo (confo o6) (confo o33))
(@nonoverlap_oo (confo o6) (confo o34))
(@nonoverlap_oo (confo o6) (confo o35))
(@nonoverlap_oo (confo o6) (confo o36))
(@nonoverlap_oo (confo o6) (confo o37))
(@nonoverlap_oo (confo o6) (confo o38))
(@nonoverlap_oo (confo o6) (confo o39))
(@nonoverlap_oo (confo o6) (confo o40))
(@nonoverlap_oo (confo o6) (confo o41))
(@nonoverlap_oo (confo o6) (confo o42))
(@nonoverlap_oo (confo o6) (confo o43))
(@nonoverlap_oo (confo o6) (confo o44))
(@nonoverlap_oo (confo o6) (confo o45))
(@nonoverlap_oo (confo o7) (confo o8))
(@nonoverlap_oo (confo o7) (confo o9))
(@nonoverlap_oo (confo o7) (confo o10))
(@nonoverlap_oo (confo o7) (confo o11))
(@nonoverlap_oo (confo o7) (confo o12))
(@nonoverlap_oo (confo o7) (confo o13))
(@nonoverlap_oo (confo o7) (confo o14))
(@nonoverlap_oo (confo o7) (confo o15))
(@nonoverlap_oo (confo o7) (confo o16))
(@nonoverlap_oo (confo o7) (confo o17))
(@nonoverlap_oo (confo o7) (confo o18))
(@nonoverlap_oo (confo o7) (confo o19))
(@nonoverlap_oo (confo o7) (confo o20))
(@nonoverlap_oo (confo o7) (confo o21))
(@nonoverlap_oo (confo o7) (confo o22))
(@nonoverlap_oo (confo o7) (confo o23))
(@nonoverlap_oo (confo o7) (confo o24))
(@nonoverlap_oo (confo o7) (confo o25))
(@nonoverlap_oo (confo o7) (confo o26))
(@nonoverlap_oo (confo o7) (confo o27))
(@nonoverlap_oo (confo o7) (confo o28))
(@nonoverlap_oo (confo o7) (confo o29))
(@nonoverlap_oo (confo o7) (confo o30))
(@nonoverlap_oo (confo o7) (confo o31))
(@nonoverlap_oo (confo o7) (confo o32))
(@nonoverlap_oo (confo o7) (confo o33))
(@nonoverlap_oo (confo o7) (confo o34))
(@nonoverlap_oo (confo o7) (confo o35))
(@nonoverlap_oo (confo o7) (confo o36))
(@nonoverlap_oo (confo o7) (confo o37))
(@nonoverlap_oo (confo o7) (confo o38))
(@nonoverlap_oo (confo o7) (confo o39))
(@nonoverlap_oo (confo o7) (confo o40))
(@nonoverlap_oo (confo o7) (confo o41))
(@nonoverlap_oo (confo o7) (confo o42))
(@nonoverlap_oo (confo o7) (confo o43))
(@nonoverlap_oo (confo o7) (confo o44))
(@nonoverlap_oo (confo o7) (confo o45))
(@nonoverlap_oo (confo o8) (confo o9))
(@nonoverlap_oo (confo o8) (confo o10))
(@nonoverlap_oo (confo o8) (confo o11))
(@nonoverlap_oo (confo o8) (confo o12))
(@nonoverlap_oo (confo o8) (confo o13))
(@nonoverlap_oo (confo o8) (confo o14))
(@nonoverlap_oo (confo o8) (confo o15))
(@nonoverlap_oo (confo o8) (confo o16))
(@nonoverlap_oo (confo o8) (confo o17))
(@nonoverlap_oo (confo o8) (confo o18))
(@nonoverlap_oo (confo o8) (confo o19))
(@nonoverlap_oo (confo o8) (confo o20))
(@nonoverlap_oo (confo o8) (confo o21))
(@nonoverlap_oo (confo o8) (confo o22))
(@nonoverlap_oo (confo o8) (confo o23))
(@nonoverlap_oo (confo o8) (confo o24))
(@nonoverlap_oo (confo o8) (confo o25))
(@nonoverlap_oo (confo o8) (confo o26))
(@nonoverlap_oo (confo o8) (confo o27))
(@nonoverlap_oo (confo o8) (confo o28))
(@nonoverlap_oo (confo o8) (confo o29))
(@nonoverlap_oo (confo o8) (confo o30))
(@nonoverlap_oo (confo o8) (confo o31))
(@nonoverlap_oo (confo o8) (confo o32))
(@nonoverlap_oo (confo o8) (confo o33))
(@nonoverlap_oo (confo o8) (confo o34))
(@nonoverlap_oo (confo o8) (confo o35))
(@nonoverlap_oo (confo o8) (confo o36))
(@nonoverlap_oo (confo o8) (confo o37))
(@nonoverlap_oo (confo o8) (confo o38))
(@nonoverlap_oo (confo o8) (confo o39))
(@nonoverlap_oo (confo o8) (confo o40))
(@nonoverlap_oo (confo o8) (confo o41))
(@nonoverlap_oo (confo o8) (confo o42))
(@nonoverlap_oo (confo o8) (confo o43))
(@nonoverlap_oo (confo o8) (confo o44))
(@nonoverlap_oo (confo o8) (confo o45))
(@nonoverlap_oo (confo o9) (confo o10))
(@nonoverlap_oo (confo o9) (confo o11))
(@nonoverlap_oo (confo o9) (confo o12))
(@nonoverlap_oo (confo o9) (confo o13))
(@nonoverlap_oo (confo o9) (confo o14))
(@nonoverlap_oo (confo o9) (confo o15))
(@nonoverlap_oo (confo o9) (confo o16))
(@nonoverlap_oo (confo o9) (confo o17))
(@nonoverlap_oo (confo o9) (confo o18))
(@nonoverlap_oo (confo o9) (confo o19))
(@nonoverlap_oo (confo o9) (confo o20))
(@nonoverlap_oo (confo o9) (confo o21))
(@nonoverlap_oo (confo o9) (confo o22))
(@nonoverlap_oo (confo o9) (confo o23))
(@nonoverlap_oo (confo o9) (confo o24))
(@nonoverlap_oo (confo o9) (confo o25))
(@nonoverlap_oo (confo o9) (confo o26))
(@nonoverlap_oo (confo o9) (confo o27))
(@nonoverlap_oo (confo o9) (confo o28))
(@nonoverlap_oo (confo o9) (confo o29))
(@nonoverlap_oo (confo o9) (confo o30))
(@nonoverlap_oo (confo o9) (confo o31))
(@nonoverlap_oo (confo o9) (confo o32))
(@nonoverlap_oo (confo o9) (confo o33))
(@nonoverlap_oo (confo o9) (confo o34))
(@nonoverlap_oo (confo o9) (confo o35))
(@nonoverlap_oo (confo o9) (confo o36))
(@nonoverlap_oo (confo o9) (confo o37))
(@nonoverlap_oo (confo o9) (confo o38))
(@nonoverlap_oo (confo o9) (confo o39))
(@nonoverlap_oo (confo o9) (confo o40))
(@nonoverlap_oo (confo o9) (confo o41))
(@nonoverlap_oo (confo o9) (confo o42))
(@nonoverlap_oo (confo o9) (confo o43))
(@nonoverlap_oo (confo o9) (confo o44))
(@nonoverlap_oo (confo o9) (confo o45))
(@nonoverlap_oo (confo o10) (confo o11))
(@nonoverlap_oo (confo o10) (confo o12))
(@nonoverlap_oo (confo o10) (confo o13))
(@nonoverlap_oo (confo o10) (confo o14))
(@nonoverlap_oo (confo o10) (confo o15))
(@nonoverlap_oo (confo o10) (confo o16))
(@nonoverlap_oo (confo o10) (confo o17))
(@nonoverlap_oo (confo o10) (confo o18))
(@nonoverlap_oo (confo o10) (confo o19))
(@nonoverlap_oo (confo o10) (confo o20))
(@nonoverlap_oo (confo o10) (confo o21))
(@nonoverlap_oo (confo o10) (confo o22))
(@nonoverlap_oo (confo o10) (confo o23))
(@nonoverlap_oo (confo o10) (confo o24))
(@nonoverlap_oo (confo o10) (confo o25))
(@nonoverlap_oo (confo o10) (confo o26))
(@nonoverlap_oo (confo o10) (confo o27))
(@nonoverlap_oo (confo o10) (confo o28))
(@nonoverlap_oo (confo o10) (confo o29))
(@nonoverlap_oo (confo o10) (confo o30))
(@nonoverlap_oo (confo o10) (confo o31))
(@nonoverlap_oo (confo o10) (confo o32))
(@nonoverlap_oo (confo o10) (confo o33))
(@nonoverlap_oo (confo o10) (confo o34))
(@nonoverlap_oo (confo o10) (confo o35))
(@nonoverlap_oo (confo o10) (confo o36))
(@nonoverlap_oo (confo o10) (confo o37))
(@nonoverlap_oo (confo o10) (confo o38))
(@nonoverlap_oo (confo o10) (confo o39))
(@nonoverlap_oo (confo o10) (confo o40))
(@nonoverlap_oo (confo o10) (confo o41))
(@nonoverlap_oo (confo o10) (confo o42))
(@nonoverlap_oo (confo o10) (confo o43))
(@nonoverlap_oo (confo o10) (confo o44))
(@nonoverlap_oo (confo o10) (confo o45))
(@nonoverlap_oo (confo o11) (confo o12))
(@nonoverlap_oo (confo o11) (confo o13))
(@nonoverlap_oo (confo o11) (confo o14))
(@nonoverlap_oo (confo o11) (confo o15))
(@nonoverlap_oo (confo o11) (confo o16))
(@nonoverlap_oo (confo o11) (confo o17))
(@nonoverlap_oo (confo o11) (confo o18))
(@nonoverlap_oo (confo o11) (confo o19))
(@nonoverlap_oo (confo o11) (confo o20))
(@nonoverlap_oo (confo o11) (confo o21))
(@nonoverlap_oo (confo o11) (confo o22))
(@nonoverlap_oo (confo o11) (confo o23))
(@nonoverlap_oo (confo o11) (confo o24))
(@nonoverlap_oo (confo o11) (confo o25))
(@nonoverlap_oo (confo o11) (confo o26))
(@nonoverlap_oo (confo o11) (confo o27))
(@nonoverlap_oo (confo o11) (confo o28))
(@nonoverlap_oo (confo o11) (confo o29))
(@nonoverlap_oo (confo o11) (confo o30))
(@nonoverlap_oo (confo o11) (confo o31))
(@nonoverlap_oo (confo o11) (confo o32))
(@nonoverlap_oo (confo o11) (confo o33))
(@nonoverlap_oo (confo o11) (confo o34))
(@nonoverlap_oo (confo o11) (confo o35))
(@nonoverlap_oo (confo o11) (confo o36))
(@nonoverlap_oo (confo o11) (confo o37))
(@nonoverlap_oo (confo o11) (confo o38))
(@nonoverlap_oo (confo o11) (confo o39))
(@nonoverlap_oo (confo o11) (confo o40))
(@nonoverlap_oo (confo o11) (confo o41))
(@nonoverlap_oo (confo o11) (confo o42))
(@nonoverlap_oo (confo o11) (confo o43))
(@nonoverlap_oo (confo o11) (confo o44))
(@nonoverlap_oo (confo o11) (confo o45))
(@nonoverlap_oo (confo o12) (confo o13))
(@nonoverlap_oo (confo o12) (confo o14))
(@nonoverlap_oo (confo o12) (confo o15))
(@nonoverlap_oo (confo o12) (confo o16))
(@nonoverlap_oo (confo o12) (confo o17))
(@nonoverlap_oo (confo o12) (confo o18))
(@nonoverlap_oo (confo o12) (confo o19))
(@nonoverlap_oo (confo o12) (confo o20))
(@nonoverlap_oo (confo o12) (confo o21))
(@nonoverlap_oo (confo o12) (confo o22))
(@nonoverlap_oo (confo o12) (confo o23))
(@nonoverlap_oo (confo o12) (confo o24))
(@nonoverlap_oo (confo o12) (confo o25))
(@nonoverlap_oo (confo o12) (confo o26))
(@nonoverlap_oo (confo o12) (confo o27))
(@nonoverlap_oo (confo o12) (confo o28))
(@nonoverlap_oo (confo o12) (confo o29))
(@nonoverlap_oo (confo o12) (confo o30))
(@nonoverlap_oo (confo o12) (confo o31))
(@nonoverlap_oo (confo o12) (confo o32))
(@nonoverlap_oo (confo o12) (confo o33))
(@nonoverlap_oo (confo o12) (confo o34))
(@nonoverlap_oo (confo o12) (confo o35))
(@nonoverlap_oo (confo o12) (confo o36))
(@nonoverlap_oo (confo o12) (confo o37))
(@nonoverlap_oo (confo o12) (confo o38))
(@nonoverlap_oo (confo o12) (confo o39))
(@nonoverlap_oo (confo o12) (confo o40))
(@nonoverlap_oo (confo o12) (confo o41))
(@nonoverlap_oo (confo o12) (confo o42))
(@nonoverlap_oo (confo o12) (confo o43))
(@nonoverlap_oo (confo o12) (confo o44))
(@nonoverlap_oo (confo o12) (confo o45))
(@nonoverlap_oo (confo o13) (confo o14))
(@nonoverlap_oo (confo o13) (confo o15))
(@nonoverlap_oo (confo o13) (confo o16))
(@nonoverlap_oo (confo o13) (confo o17))
(@nonoverlap_oo (confo o13) (confo o18))
(@nonoverlap_oo (confo o13) (confo o19))
(@nonoverlap_oo (confo o13) (confo o20))
(@nonoverlap_oo (confo o13) (confo o21))
(@nonoverlap_oo (confo o13) (confo o22))
(@nonoverlap_oo (confo o13) (confo o23))
(@nonoverlap_oo (confo o13) (confo o24))
(@nonoverlap_oo (confo o13) (confo o25))
(@nonoverlap_oo (confo o13) (confo o26))
(@nonoverlap_oo (confo o13) (confo o27))
(@nonoverlap_oo (confo o13) (confo o28))
(@nonoverlap_oo (confo o13) (confo o29))
(@nonoverlap_oo (confo o13) (confo o30))
(@nonoverlap_oo (confo o13) (confo o31))
(@nonoverlap_oo (confo o13) (confo o32))
(@nonoverlap_oo (confo o13) (confo o33))
(@nonoverlap_oo (confo o13) (confo o34))
(@nonoverlap_oo (confo o13) (confo o35))
(@nonoverlap_oo (confo o13) (confo o36))
(@nonoverlap_oo (confo o13) (confo o37))
(@nonoverlap_oo (confo o13) (confo o38))
(@nonoverlap_oo (confo o13) (confo o39))
(@nonoverlap_oo (confo o13) (confo o40))
(@nonoverlap_oo (confo o13) (confo o41))
(@nonoverlap_oo (confo o13) (confo o42))
(@nonoverlap_oo (confo o13) (confo o43))
(@nonoverlap_oo (confo o13) (confo o44))
(@nonoverlap_oo (confo o13) (confo o45))
(@nonoverlap_oo (confo o14) (confo o15))
(@nonoverlap_oo (confo o14) (confo o16))
(@nonoverlap_oo (confo o14) (confo o17))
(@nonoverlap_oo (confo o14) (confo o18))
(@nonoverlap_oo (confo o14) (confo o19))
(@nonoverlap_oo (confo o14) (confo o20))
(@nonoverlap_oo (confo o14) (confo o21))
(@nonoverlap_oo (confo o14) (confo o22))
(@nonoverlap_oo (confo o14) (confo o23))
(@nonoverlap_oo (confo o14) (confo o24))
(@nonoverlap_oo (confo o14) (confo o25))
(@nonoverlap_oo (confo o14) (confo o26))
(@nonoverlap_oo (confo o14) (confo o27))
(@nonoverlap_oo (confo o14) (confo o28))
(@nonoverlap_oo (confo o14) (confo o29))
(@nonoverlap_oo (confo o14) (confo o30))
(@nonoverlap_oo (confo o14) (confo o31))
(@nonoverlap_oo (confo o14) (confo o32))
(@nonoverlap_oo (confo o14) (confo o33))
(@nonoverlap_oo (confo o14) (confo o34))
(@nonoverlap_oo (confo o14) (confo o35))
(@nonoverlap_oo (confo o14) (confo o36))
(@nonoverlap_oo (confo o14) (confo o37))
(@nonoverlap_oo (confo o14) (confo o38))
(@nonoverlap_oo (confo o14) (confo o39))
(@nonoverlap_oo (confo o14) (confo o40))
(@nonoverlap_oo (confo o14) (confo o41))
(@nonoverlap_oo (confo o14) (confo o42))
(@nonoverlap_oo (confo o14) (confo o43))
(@nonoverlap_oo (confo o14) (confo o44))
(@nonoverlap_oo (confo o14) (confo o45))
(@nonoverlap_oo (confo o15) (confo o16))
(@nonoverlap_oo (confo o15) (confo o17))
(@nonoverlap_oo (confo o15) (confo o18))
(@nonoverlap_oo (confo o15) (confo o19))
(@nonoverlap_oo (confo o15) (confo o20))
(@nonoverlap_oo (confo o15) (confo o21))
(@nonoverlap_oo (confo o15) (confo o22))
(@nonoverlap_oo (confo o15) (confo o23))
(@nonoverlap_oo (confo o15) (confo o24))
(@nonoverlap_oo (confo o15) (confo o25))
(@nonoverlap_oo (confo o15) (confo o26))
(@nonoverlap_oo (confo o15) (confo o27))
(@nonoverlap_oo (confo o15) (confo o28))
(@nonoverlap_oo (confo o15) (confo o29))
(@nonoverlap_oo (confo o15) (confo o30))
(@nonoverlap_oo (confo o15) (confo o31))
(@nonoverlap_oo (confo o15) (confo o32))
(@nonoverlap_oo (confo o15) (confo o33))
(@nonoverlap_oo (confo o15) (confo o34))
(@nonoverlap_oo (confo o15) (confo o35))
(@nonoverlap_oo (confo o15) (confo o36))
(@nonoverlap_oo (confo o15) (confo o37))
(@nonoverlap_oo (confo o15) (confo o38))
(@nonoverlap_oo (confo o15) (confo o39))
(@nonoverlap_oo (confo o15) (confo o40))
(@nonoverlap_oo (confo o15) (confo o41))
(@nonoverlap_oo (confo o15) (confo o42))
(@nonoverlap_oo (confo o15) (confo o43))
(@nonoverlap_oo (confo o15) (confo o44))
(@nonoverlap_oo (confo o15) (confo o45))
(@nonoverlap_oo (confo o16) (confo o17))
(@nonoverlap_oo (confo o16) (confo o18))
(@nonoverlap_oo (confo o16) (confo o19))
(@nonoverlap_oo (confo o16) (confo o20))
(@nonoverlap_oo (confo o16) (confo o21))
(@nonoverlap_oo (confo o16) (confo o22))
(@nonoverlap_oo (confo o16) (confo o23))
(@nonoverlap_oo (confo o16) (confo o24))
(@nonoverlap_oo (confo o16) (confo o25))
(@nonoverlap_oo (confo o16) (confo o26))
(@nonoverlap_oo (confo o16) (confo o27))
(@nonoverlap_oo (confo o16) (confo o28))
(@nonoverlap_oo (confo o16) (confo o29))
(@nonoverlap_oo (confo o16) (confo o30))
(@nonoverlap_oo (confo o16) (confo o31))
(@nonoverlap_oo (confo o16) (confo o32))
(@nonoverlap_oo (confo o16) (confo o33))
(@nonoverlap_oo (confo o16) (confo o34))
(@nonoverlap_oo (confo o16) (confo o35))
(@nonoverlap_oo (confo o16) (confo o36))
(@nonoverlap_oo (confo o16) (confo o37))
(@nonoverlap_oo (confo o16) (confo o38))
(@nonoverlap_oo (confo o16) (confo o39))
(@nonoverlap_oo (confo o16) (confo o40))
(@nonoverlap_oo (confo o16) (confo o41))
(@nonoverlap_oo (confo o16) (confo o42))
(@nonoverlap_oo (confo o16) (confo o43))
(@nonoverlap_oo (confo o16) (confo o44))
(@nonoverlap_oo (confo o16) (confo o45))
(@nonoverlap_oo (confo o17) (confo o18))
(@nonoverlap_oo (confo o17) (confo o19))
(@nonoverlap_oo (confo o17) (confo o20))
(@nonoverlap_oo (confo o17) (confo o21))
(@nonoverlap_oo (confo o17) (confo o22))
(@nonoverlap_oo (confo o17) (confo o23))
(@nonoverlap_oo (confo o17) (confo o24))
(@nonoverlap_oo (confo o17) (confo o25))
(@nonoverlap_oo (confo o17) (confo o26))
(@nonoverlap_oo (confo o17) (confo o27))
(@nonoverlap_oo (confo o17) (confo o28))
(@nonoverlap_oo (confo o17) (confo o29))
(@nonoverlap_oo (confo o17) (confo o30))
(@nonoverlap_oo (confo o17) (confo o31))
(@nonoverlap_oo (confo o17) (confo o32))
(@nonoverlap_oo (confo o17) (confo o33))
(@nonoverlap_oo (confo o17) (confo o34))
(@nonoverlap_oo (confo o17) (confo o35))
(@nonoverlap_oo (confo o17) (confo o36))
(@nonoverlap_oo (confo o17) (confo o37))
(@nonoverlap_oo (confo o17) (confo o38))
(@nonoverlap_oo (confo o17) (confo o39))
(@nonoverlap_oo (confo o17) (confo o40))
(@nonoverlap_oo (confo o17) (confo o41))
(@nonoverlap_oo (confo o17) (confo o42))
(@nonoverlap_oo (confo o17) (confo o43))
(@nonoverlap_oo (confo o17) (confo o44))
(@nonoverlap_oo (confo o17) (confo o45))
(@nonoverlap_oo (confo o18) (confo o19))
(@nonoverlap_oo (confo o18) (confo o20))
(@nonoverlap_oo (confo o18) (confo o21))
(@nonoverlap_oo (confo o18) (confo o22))
(@nonoverlap_oo (confo o18) (confo o23))
(@nonoverlap_oo (confo o18) (confo o24))
(@nonoverlap_oo (confo o18) (confo o25))
(@nonoverlap_oo (confo o18) (confo o26))
(@nonoverlap_oo (confo o18) (confo o27))
(@nonoverlap_oo (confo o18) (confo o28))
(@nonoverlap_oo (confo o18) (confo o29))
(@nonoverlap_oo (confo o18) (confo o30))
(@nonoverlap_oo (confo o18) (confo o31))
(@nonoverlap_oo (confo o18) (confo o32))
(@nonoverlap_oo (confo o18) (confo o33))
(@nonoverlap_oo (confo o18) (confo o34))
(@nonoverlap_oo (confo o18) (confo o35))
(@nonoverlap_oo (confo o18) (confo o36))
(@nonoverlap_oo (confo o18) (confo o37))
(@nonoverlap_oo (confo o18) (confo o38))
(@nonoverlap_oo (confo o18) (confo o39))
(@nonoverlap_oo (confo o18) (confo o40))
(@nonoverlap_oo (confo o18) (confo o41))
(@nonoverlap_oo (confo o18) (confo o42))
(@nonoverlap_oo (confo o18) (confo o43))
(@nonoverlap_oo (confo o18) (confo o44))
(@nonoverlap_oo (confo o18) (confo o45))
(@nonoverlap_oo (confo o19) (confo o20))
(@nonoverlap_oo (confo o19) (confo o21))
(@nonoverlap_oo (confo o19) (confo o22))
(@nonoverlap_oo (confo o19) (confo o23))
(@nonoverlap_oo (confo o19) (confo o24))
(@nonoverlap_oo (confo o19) (confo o25))
(@nonoverlap_oo (confo o19) (confo o26))
(@nonoverlap_oo (confo o19) (confo o27))
(@nonoverlap_oo (confo o19) (confo o28))
(@nonoverlap_oo (confo o19) (confo o29))
(@nonoverlap_oo (confo o19) (confo o30))
(@nonoverlap_oo (confo o19) (confo o31))
(@nonoverlap_oo (confo o19) (confo o32))
(@nonoverlap_oo (confo o19) (confo o33))
(@nonoverlap_oo (confo o19) (confo o34))
(@nonoverlap_oo (confo o19) (confo o35))
(@nonoverlap_oo (confo o19) (confo o36))
(@nonoverlap_oo (confo o19) (confo o37))
(@nonoverlap_oo (confo o19) (confo o38))
(@nonoverlap_oo (confo o19) (confo o39))
(@nonoverlap_oo (confo o19) (confo o40))
(@nonoverlap_oo (confo o19) (confo o41))
(@nonoverlap_oo (confo o19) (confo o42))
(@nonoverlap_oo (confo o19) (confo o43))
(@nonoverlap_oo (confo o19) (confo o44))
(@nonoverlap_oo (confo o19) (confo o45))
(@nonoverlap_oo (confo o20) (confo o21))
(@nonoverlap_oo (confo o20) (confo o22))
(@nonoverlap_oo (confo o20) (confo o23))
(@nonoverlap_oo (confo o20) (confo o24))
(@nonoverlap_oo (confo o20) (confo o25))
(@nonoverlap_oo (confo o20) (confo o26))
(@nonoverlap_oo (confo o20) (confo o27))
(@nonoverlap_oo (confo o20) (confo o28))
(@nonoverlap_oo (confo o20) (confo o29))
(@nonoverlap_oo (confo o20) (confo o30))
(@nonoverlap_oo (confo o20) (confo o31))
(@nonoverlap_oo (confo o20) (confo o32))
(@nonoverlap_oo (confo o20) (confo o33))
(@nonoverlap_oo (confo o20) (confo o34))
(@nonoverlap_oo (confo o20) (confo o35))
(@nonoverlap_oo (confo o20) (confo o36))
(@nonoverlap_oo (confo o20) (confo o37))
(@nonoverlap_oo (confo o20) (confo o38))
(@nonoverlap_oo (confo o20) (confo o39))
(@nonoverlap_oo (confo o20) (confo o40))
(@nonoverlap_oo (confo o20) (confo o41))
(@nonoverlap_oo (confo o20) (confo o42))
(@nonoverlap_oo (confo o20) (confo o43))
(@nonoverlap_oo (confo o20) (confo o44))
(@nonoverlap_oo (confo o20) (confo o45))
(@nonoverlap_oo (confo o21) (confo o22))
(@nonoverlap_oo (confo o21) (confo o23))
(@nonoverlap_oo (confo o21) (confo o24))
(@nonoverlap_oo (confo o21) (confo o25))
(@nonoverlap_oo (confo o21) (confo o26))
(@nonoverlap_oo (confo o21) (confo o27))
(@nonoverlap_oo (confo o21) (confo o28))
(@nonoverlap_oo (confo o21) (confo o29))
(@nonoverlap_oo (confo o21) (confo o30))
(@nonoverlap_oo (confo o21) (confo o31))
(@nonoverlap_oo (confo o21) (confo o32))
(@nonoverlap_oo (confo o21) (confo o33))
(@nonoverlap_oo (confo o21) (confo o34))
(@nonoverlap_oo (confo o21) (confo o35))
(@nonoverlap_oo (confo o21) (confo o36))
(@nonoverlap_oo (confo o21) (confo o37))
(@nonoverlap_oo (confo o21) (confo o38))
(@nonoverlap_oo (confo o21) (confo o39))
(@nonoverlap_oo (confo o21) (confo o40))
(@nonoverlap_oo (confo o21) (confo o41))
(@nonoverlap_oo (confo o21) (confo o42))
(@nonoverlap_oo (confo o21) (confo o43))
(@nonoverlap_oo (confo o21) (confo o44))
(@nonoverlap_oo (confo o21) (confo o45))
(@nonoverlap_oo (confo o22) (confo o23))
(@nonoverlap_oo (confo o22) (confo o24))
(@nonoverlap_oo (confo o22) (confo o25))
(@nonoverlap_oo (confo o22) (confo o26))
(@nonoverlap_oo (confo o22) (confo o27))
(@nonoverlap_oo (confo o22) (confo o28))
(@nonoverlap_oo (confo o22) (confo o29))
(@nonoverlap_oo (confo o22) (confo o30))
(@nonoverlap_oo (confo o22) (confo o31))
(@nonoverlap_oo (confo o22) (confo o32))
(@nonoverlap_oo (confo o22) (confo o33))
(@nonoverlap_oo (confo o22) (confo o34))
(@nonoverlap_oo (confo o22) (confo o35))
(@nonoverlap_oo (confo o22) (confo o36))
(@nonoverlap_oo (confo o22) (confo o37))
(@nonoverlap_oo (confo o22) (confo o38))
(@nonoverlap_oo (confo o22) (confo o39))
(@nonoverlap_oo (confo o22) (confo o40))
(@nonoverlap_oo (confo o22) (confo o41))
(@nonoverlap_oo (confo o22) (confo o42))
(@nonoverlap_oo (confo o22) (confo o43))
(@nonoverlap_oo (confo o22) (confo o44))
(@nonoverlap_oo (confo o22) (confo o45))
(@nonoverlap_oo (confo o23) (confo o24))
(@nonoverlap_oo (confo o23) (confo o25))
(@nonoverlap_oo (confo o23) (confo o26))
(@nonoverlap_oo (confo o23) (confo o27))
(@nonoverlap_oo (confo o23) (confo o28))
(@nonoverlap_oo (confo o23) (confo o29))
(@nonoverlap_oo (confo o23) (confo o30))
(@nonoverlap_oo (confo o23) (confo o31))
(@nonoverlap_oo (confo o23) (confo o32))
(@nonoverlap_oo (confo o23) (confo o33))
(@nonoverlap_oo (confo o23) (confo o34))
(@nonoverlap_oo (confo o23) (confo o35))
(@nonoverlap_oo (confo o23) (confo o36))
(@nonoverlap_oo (confo o23) (confo o37))
(@nonoverlap_oo (confo o23) (confo o38))
(@nonoverlap_oo (confo o23) (confo o39))
(@nonoverlap_oo (confo o23) (confo o40))
(@nonoverlap_oo (confo o23) (confo o41))
(@nonoverlap_oo (confo o23) (confo o42))
(@nonoverlap_oo (confo o23) (confo o43))
(@nonoverlap_oo (confo o23) (confo o44))
(@nonoverlap_oo (confo o23) (confo o45))
(@nonoverlap_oo (confo o24) (confo o25))
(@nonoverlap_oo (confo o24) (confo o26))
(@nonoverlap_oo (confo o24) (confo o27))
(@nonoverlap_oo (confo o24) (confo o28))
(@nonoverlap_oo (confo o24) (confo o29))
(@nonoverlap_oo (confo o24) (confo o30))
(@nonoverlap_oo (confo o24) (confo o31))
(@nonoverlap_oo (confo o24) (confo o32))
(@nonoverlap_oo (confo o24) (confo o33))
(@nonoverlap_oo (confo o24) (confo o34))
(@nonoverlap_oo (confo o24) (confo o35))
(@nonoverlap_oo (confo o24) (confo o36))
(@nonoverlap_oo (confo o24) (confo o37))
(@nonoverlap_oo (confo o24) (confo o38))
(@nonoverlap_oo (confo o24) (confo o39))
(@nonoverlap_oo (confo o24) (confo o40))
(@nonoverlap_oo (confo o24) (confo o41))
(@nonoverlap_oo (confo o24) (confo o42))
(@nonoverlap_oo (confo o24) (confo o43))
(@nonoverlap_oo (confo o24) (confo o44))
(@nonoverlap_oo (confo o24) (confo o45))
(@nonoverlap_oo (confo o25) (confo o26))
(@nonoverlap_oo (confo o25) (confo o27))
(@nonoverlap_oo (confo o25) (confo o28))
(@nonoverlap_oo (confo o25) (confo o29))
(@nonoverlap_oo (confo o25) (confo o30))
(@nonoverlap_oo (confo o25) (confo o31))
(@nonoverlap_oo (confo o25) (confo o32))
(@nonoverlap_oo (confo o25) (confo o33))
(@nonoverlap_oo (confo o25) (confo o34))
(@nonoverlap_oo (confo o25) (confo o35))
(@nonoverlap_oo (confo o25) (confo o36))
(@nonoverlap_oo (confo o25) (confo o37))
(@nonoverlap_oo (confo o25) (confo o38))
(@nonoverlap_oo (confo o25) (confo o39))
(@nonoverlap_oo (confo o25) (confo o40))
(@nonoverlap_oo (confo o25) (confo o41))
(@nonoverlap_oo (confo o25) (confo o42))
(@nonoverlap_oo (confo o25) (confo o43))
(@nonoverlap_oo (confo o25) (confo o44))
(@nonoverlap_oo (confo o25) (confo o45))
(@nonoverlap_oo (confo o26) (confo o27))
(@nonoverlap_oo (confo o26) (confo o28))
(@nonoverlap_oo (confo o26) (confo o29))
(@nonoverlap_oo (confo o26) (confo o30))
(@nonoverlap_oo (confo o26) (confo o31))
(@nonoverlap_oo (confo o26) (confo o32))
(@nonoverlap_oo (confo o26) (confo o33))
(@nonoverlap_oo (confo o26) (confo o34))
(@nonoverlap_oo (confo o26) (confo o35))
(@nonoverlap_oo (confo o26) (confo o36))
(@nonoverlap_oo (confo o26) (confo o37))
(@nonoverlap_oo (confo o26) (confo o38))
(@nonoverlap_oo (confo o26) (confo o39))
(@nonoverlap_oo (confo o26) (confo o40))
(@nonoverlap_oo (confo o26) (confo o41))
(@nonoverlap_oo (confo o26) (confo o42))
(@nonoverlap_oo (confo o26) (confo o43))
(@nonoverlap_oo (confo o26) (confo o44))
(@nonoverlap_oo (confo o26) (confo o45))
(@nonoverlap_oo (confo o27) (confo o28))
(@nonoverlap_oo (confo o27) (confo o29))
(@nonoverlap_oo (confo o27) (confo o30))
(@nonoverlap_oo (confo o27) (confo o31))
(@nonoverlap_oo (confo o27) (confo o32))
(@nonoverlap_oo (confo o27) (confo o33))
(@nonoverlap_oo (confo o27) (confo o34))
(@nonoverlap_oo (confo o27) (confo o35))
(@nonoverlap_oo (confo o27) (confo o36))
(@nonoverlap_oo (confo o27) (confo o37))
(@nonoverlap_oo (confo o27) (confo o38))
(@nonoverlap_oo (confo o27) (confo o39))
(@nonoverlap_oo (confo o27) (confo o40))
(@nonoverlap_oo (confo o27) (confo o41))
(@nonoverlap_oo (confo o27) (confo o42))
(@nonoverlap_oo (confo o27) (confo o43))
(@nonoverlap_oo (confo o27) (confo o44))
(@nonoverlap_oo (confo o27) (confo o45))
(@nonoverlap_oo (confo o28) (confo o29))
(@nonoverlap_oo (confo o28) (confo o30))
(@nonoverlap_oo (confo o28) (confo o31))
(@nonoverlap_oo (confo o28) (confo o32))
(@nonoverlap_oo (confo o28) (confo o33))
(@nonoverlap_oo (confo o28) (confo o34))
(@nonoverlap_oo (confo o28) (confo o35))
(@nonoverlap_oo (confo o28) (confo o36))
(@nonoverlap_oo (confo o28) (confo o37))
(@nonoverlap_oo (confo o28) (confo o38))
(@nonoverlap_oo (confo o28) (confo o39))
(@nonoverlap_oo (confo o28) (confo o40))
(@nonoverlap_oo (confo o28) (confo o41))
(@nonoverlap_oo (confo o28) (confo o42))
(@nonoverlap_oo (confo o28) (confo o43))
(@nonoverlap_oo (confo o28) (confo o44))
(@nonoverlap_oo (confo o28) (confo o45))
(@nonoverlap_oo (confo o29) (confo o30))
(@nonoverlap_oo (confo o29) (confo o31))
(@nonoverlap_oo (confo o29) (confo o32))
(@nonoverlap_oo (confo o29) (confo o33))
(@nonoverlap_oo (confo o29) (confo o34))
(@nonoverlap_oo (confo o29) (confo o35))
(@nonoverlap_oo (confo o29) (confo o36))
(@nonoverlap_oo (confo o29) (confo o37))
(@nonoverlap_oo (confo o29) (confo o38))
(@nonoverlap_oo (confo o29) (confo o39))
(@nonoverlap_oo (confo o29) (confo o40))
(@nonoverlap_oo (confo o29) (confo o41))
(@nonoverlap_oo (confo o29) (confo o42))
(@nonoverlap_oo (confo o29) (confo o43))
(@nonoverlap_oo (confo o29) (confo o44))
(@nonoverlap_oo (confo o29) (confo o45))
(@nonoverlap_oo (confo o30) (confo o31))
(@nonoverlap_oo (confo o30) (confo o32))
(@nonoverlap_oo (confo o30) (confo o33))
(@nonoverlap_oo (confo o30) (confo o34))
(@nonoverlap_oo (confo o30) (confo o35))
(@nonoverlap_oo (confo o30) (confo o36))
(@nonoverlap_oo (confo o30) (confo o37))
(@nonoverlap_oo (confo o30) (confo o38))
(@nonoverlap_oo (confo o30) (confo o39))
(@nonoverlap_oo (confo o30) (confo o40))
(@nonoverlap_oo (confo o30) (confo o41))
(@nonoverlap_oo (confo o30) (confo o42))
(@nonoverlap_oo (confo o30) (confo o43))
(@nonoverlap_oo (confo o30) (confo o44))
(@nonoverlap_oo (confo o30) (confo o45))
(@nonoverlap_oo (confo o31) (confo o32))
(@nonoverlap_oo (confo o31) (confo o33))
(@nonoverlap_oo (confo o31) (confo o34))
(@nonoverlap_oo (confo o31) (confo o35))
(@nonoverlap_oo (confo o31) (confo o36))
(@nonoverlap_oo (confo o31) (confo o37))
(@nonoverlap_oo (confo o31) (confo o38))
(@nonoverlap_oo (confo o31) (confo o39))
(@nonoverlap_oo (confo o31) (confo o40))
(@nonoverlap_oo (confo o31) (confo o41))
(@nonoverlap_oo (confo o31) (confo o42))
(@nonoverlap_oo (confo o31) (confo o43))
(@nonoverlap_oo (confo o31) (confo o44))
(@nonoverlap_oo (confo o31) (confo o45))
(@nonoverlap_oo (confo o32) (confo o33))
(@nonoverlap_oo (confo o32) (confo o34))
(@nonoverlap_oo (confo o32) (confo o35))
(@nonoverlap_oo (confo o32) (confo o36))
(@nonoverlap_oo (confo o32) (confo o37))
(@nonoverlap_oo (confo o32) (confo o38))
(@nonoverlap_oo (confo o32) (confo o39))
(@nonoverlap_oo (confo o32) (confo o40))
(@nonoverlap_oo (confo o32) (confo o41))
(@nonoverlap_oo (confo o32) (confo o42))
(@nonoverlap_oo (confo o32) (confo o43))
(@nonoverlap_oo (confo o32) (confo o44))
(@nonoverlap_oo (confo o32) (confo o45))
(@nonoverlap_oo (confo o33) (confo o34))
(@nonoverlap_oo (confo o33) (confo o35))
(@nonoverlap_oo (confo o33) (confo o36))
(@nonoverlap_oo (confo o33) (confo o37))
(@nonoverlap_oo (confo o33) (confo o38))
(@nonoverlap_oo (confo o33) (confo o39))
(@nonoverlap_oo (confo o33) (confo o40))
(@nonoverlap_oo (confo o33) (confo o41))
(@nonoverlap_oo (confo o33) (confo o42))
(@nonoverlap_oo (confo o33) (confo o43))
(@nonoverlap_oo (confo o33) (confo o44))
(@nonoverlap_oo (confo o33) (confo o45))
(@nonoverlap_oo (confo o34) (confo o35))
(@nonoverlap_oo (confo o34) (confo o36))
(@nonoverlap_oo (confo o34) (confo o37))
(@nonoverlap_oo (confo o34) (confo o38))
(@nonoverlap_oo (confo o34) (confo o39))
(@nonoverlap_oo (confo o34) (confo o40))
(@nonoverlap_oo (confo o34) (confo o41))
(@nonoverlap_oo (confo o34) (confo o42))
(@nonoverlap_oo (confo o34) (confo o43))
(@nonoverlap_oo (confo o34) (confo o44))
(@nonoverlap_oo (confo o34) (confo o45))
(@nonoverlap_oo (confo o35) (confo o36))
(@nonoverlap_oo (confo o35) (confo o37))
(@nonoverlap_oo (confo o35) (confo o38))
(@nonoverlap_oo (confo o35) (confo o39))
(@nonoverlap_oo (confo o35) (confo o40))
(@nonoverlap_oo (confo o35) (confo o41))
(@nonoverlap_oo (confo o35) (confo o42))
(@nonoverlap_oo (confo o35) (confo o43))
(@nonoverlap_oo (confo o35) (confo o44))
(@nonoverlap_oo (confo o35) (confo o45))
(@nonoverlap_oo (confo o36) (confo o37))
(@nonoverlap_oo (confo o36) (confo o38))
(@nonoverlap_oo (confo o36) (confo o39))
(@nonoverlap_oo (confo o36) (confo o40))
(@nonoverlap_oo (confo o36) (confo o41))
(@nonoverlap_oo (confo o36) (confo o42))
(@nonoverlap_oo (confo o36) (confo o43))
(@nonoverlap_oo (confo o36) (confo o44))
(@nonoverlap_oo (confo o36) (confo o45))
(@nonoverlap_oo (confo o37) (confo o38))
(@nonoverlap_oo (confo o37) (confo o39))
(@nonoverlap_oo (confo o37) (confo o40))
(@nonoverlap_oo (confo o37) (confo o41))
(@nonoverlap_oo (confo o37) (confo o42))
(@nonoverlap_oo (confo o37) (confo o43))
(@nonoverlap_oo (confo o37) (confo o44))
(@nonoverlap_oo (confo o37) (confo o45))
(@nonoverlap_oo (confo o38) (confo o39))
(@nonoverlap_oo (confo o38) (confo o40))
(@nonoverlap_oo (confo o38) (confo o41))
(@nonoverlap_oo (confo o38) (confo o42))
(@nonoverlap_oo (confo o38) (confo o43))
(@nonoverlap_oo (confo o38) (confo o44))
(@nonoverlap_oo (confo o38) (confo o45))
(@nonoverlap_oo (confo o39) (confo o40))
(@nonoverlap_oo (confo o39) (confo o41))
(@nonoverlap_oo (confo o39) (confo o42))
(@nonoverlap_oo (confo o39) (confo o43))
(@nonoverlap_oo (confo o39) (confo o44))
(@nonoverlap_oo (confo o39) (confo o45))
(@nonoverlap_oo (confo o40) (confo o41))
(@nonoverlap_oo (confo o40) (confo o42))
(@nonoverlap_oo (confo o40) (confo o43))
(@nonoverlap_oo (confo o40) (confo o44))
(@nonoverlap_oo (confo o40) (confo o45))
(@nonoverlap_oo (confo o41) (confo o42))
(@nonoverlap_oo (confo o41) (confo o43))
(@nonoverlap_oo (confo o41) (confo o44))
(@nonoverlap_oo (confo o41) (confo o45))
(@nonoverlap_oo (confo o42) (confo o43))
(@nonoverlap_oo (confo o42) (confo o44))
(@nonoverlap_oo (confo o42) (confo o45))
(@nonoverlap_oo (confo o43) (confo o44))
(@nonoverlap_oo (confo o43) (confo o45))
(@nonoverlap_oo (confo o44) (confo o45))


)
)
