#pragma once
#include <iostream>
#include <fstream>
#include <problem.hxx>
#include <utils/utils.hxx>


using namespace std;

class FSTRIPS3D {
  
  
private:
  
  ofstream _pddl;
  Problem _problem;
 

public:
  
  
  FSTRIPS3D(Problem& problem, string& filename): 
	  _problem(problem),
	  _pddl(filename)
	    {}
  ~FSTRIPS3D() {}
  
  
  void generate_instance() {
    
    
    head();
    objects();
    init();
    goal();
    constraints();
    close();
    
  }
  
  void head() {
    if (_pddl.is_open()) {
      _pddl << "(define (problem sample)\n";
      _pddl << "\t(:domain fn-robot-navigation)\n";
    }
        
    
  }
  
  void objects() {
    
    if (_pddl.is_open()) {
      _pddl << "\t(:objects \n" << endl;
      _pddl << " - object_id\n\n";
      for(auto& it : _problem.get_bconfs())
	_pddl << "\tcb" << it.id() << endl;
      _pddl << "\t - conf_base" << endl;
      for(auto& it: _problem.get_oconfs())
	_pddl << "\tco" << it.id() << endl;
      _pddl << " - conf_obj \n " << endl; 
      for(auto& it: _problem.get_aconfs())
	if(it.id() != 0)
	  _pddl << "\tca" << it.id() << endl;
      _pddl << " - conf_arm \n " << endl;
      for(auto& it: _problem.get_base_graph())
	_pddl << "\te" << it.first.second << endl;
      _pddl << " - edge \n " << endl;
      for(auto& it: _problem.get_arm_graph())
	if(it.first.second != 0)
	_pddl << "\tt" << it.first.second << endl;
      _pddl << " - trajectory \n )" << endl;

    }
  }
  
  
  
  void init() {
    
    if (_pddl.is_open()) {
	_pddl << "(:init" << endl;
	_pddl << "(= (confb rob ) cb0 )" << endl;
	_pddl << "(= (confa rob ) ca0 )" << endl;
	_pddl << "(= (traj rob ) t0 )" << endl;
	_pddl << "(= (holding) no_object)" << endl;
	_pddl << "; initial object configurations \n" << endl;
	_pddl << ")" << endl;

    } 
  }
  
  
  /*TODO: For 2D
  void init() {
    
    if (_pddl.is_open()) {
      _pddl << "(:init" << endl;
     for(unsigned i = 0; i < _problem.get_init().size(); ++i){
       if(i == 0){
	 _pddl << "(= (confb " << _problem.get_init().at(i).first << ") cb" << _problem.get_init().at(i).second << ")" << endl;
	  _pddl << "(= (confa " << _problem.get_init().at(i).first << ") ca" << _problem.get_init().at(i).second << ")" << endl;
       }
	  else
	  _pddl << "(= (confo " << _problem.get_init().at(i).first << ") co" << _problem.get_init().at(i).second << ")" << endl;

     }
    } 
  }
  */

  

  
  //TODO: For 2D
  void goal() {
    
    if (_pddl.is_open()) {
      _pddl << "(:goal (and" << endl;
       //for(unsigned i = 0; i < _problem.get_goal().size(); ++i)
	 // _pddl << "(= (confa " << _problem.get_goal().at(i).first << ") ca" << _problem.get_goal().at(i).second << ")" << endl;
       _pddl << "))" <<  endl;
      }
    
  }
  
    void constraints() {
     if (_pddl.is_open()) {
      _pddl << "(:constraints" << endl;
/*      
      for(unsigned i = 1; i < _problem.get_bodies().size(); ++i)
	  _pddl << "\t(@nonoverlap_ro (confb rob) (confa rob) (confo o"<< i <<"))" << endl;
      
      for(unsigned i = 1; i < _problem.get_bodies().size(); ++i) 
	for(unsigned j = i+1; j <  _problem.get_bodies().size(); ++j) 
	  _pddl << "(@nonoverlap_oo" << " (confo o"<< i<< ") (confo o" << j << "))" << std::endl;
	*/
      _pddl << ")" << endl; 
     }
  }
  
  
  void close() {
     if (_pddl.is_open())
       _pddl << ")" << endl;
     _pddl.close();

    
  }
  
  
  
  
  
  
  
  
  
};