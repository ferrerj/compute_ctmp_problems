#pragma once
#include <iostream>
#include <vector>
#include <set>
#include <map>
#include <boost/serialization/serialization.hpp>
#include <boost/serialization/map.hpp>
#include <boost/serialization/set.hpp>
#include <boost/serialization/vector.hpp>
#include <boost/archive/text_iarchive.hpp>
#include <boost/archive/text_oarchive.hpp>
#include <types.hxx>
#include <utils/serialize_tuple.hxx>
#include <configuration.hxx>
#include <fstream>


class Serializer {
  
  
public:
  

  Serializer() {}
  virtual ~Serializer(){}

	//Serialize a vector of tuples 
	static void save(const std::string filename, const Overlaps& ov) {
	  std::ofstream ofs(filename);
	  boost::archive::text_oarchive oarch(ofs);
	  oarch << ov;
	  ofs.close();
	}

	//Deserialize a vector of tuples
	static void load(const std::string filename, Overlaps& ov) {
	  std::ifstream ifs(filename);
	  if(ifs.is_open()) {
	    boost::archive::text_iarchive iarch(ifs);
	    iarch >> ov;
	  }
	  ifs.close();
	}
	
		//Serialize a vector of tuples 
	static void save(const std::string filename, const VOverlaps& ov) {
	  std::ofstream ofs(filename);
	  boost::archive::text_oarchive oarch(ofs);
	  oarch << ov;
	  ofs.close();
	}

	//Deserialize a vector of tuples
	static void load(const std::string filename, VOverlaps& ov) {
	  std::ifstream ifs(filename);
	  if(ifs.is_open()) {
	    boost::archive::text_iarchive iarch(ifs);
	    iarch >> ov;
	  }
	  ifs.close();
	}
	
	  //Serialize a vector of tuples 
	static void save(const std::string filename, const VGraspable& ov) {
	  std::ofstream ofs(filename);
	  boost::archive::text_oarchive oarch(ofs);
	  oarch << ov;
	  ofs.close();
	}

	//Deserialize a vector of tuples
	static void load(const std::string filename, VGraspable& ov) {
	  std::ifstream ifs(filename);
	  if(ifs.is_open()) {
	    boost::archive::text_iarchive iarch(ifs);
	    iarch >> ov;
	  }
	  ifs.close();
	}

	//Serialize a graph
	static void save(const std::string filename, const Graph& g) {
	  std::ofstream ofs(filename);
	  boost::archive::text_oarchive oarch(ofs);
	  oarch << g;
	  ofs.close();
	}

	//Deserialize a graph
	static void load(const std::string filename, Graph& g) {
	  std::ifstream ifs(filename);
	  if(ifs.is_open()) {
	    boost::archive::text_iarchive iarch(ifs);
	    iarch >> g;
	  }
	  ifs.close();
	}
	
	//Serialize a graph
	static void save(const std::string filename, const std::vector<unsigned> v) {
	  std::ofstream ofs(filename);
	  boost::archive::text_oarchive oarch(ofs);
	  oarch << v;
	  ofs.close();
	}

	//Deserialize a graph
	static void load(const std::string filename, std::vector<unsigned>& v) {
	  std::ifstream ifs(filename);
	  if(ifs.is_open()) {
	    boost::archive::text_iarchive iarch(ifs);
	    iarch >> v;
	  }
	  ifs.close();
	}
	
	//Serialize a map of trajectories (Motion plans)
	static void save(const std::string filename, const std::map<int, Trajectory>& t) {
	  std::ofstream ofs(filename);
	  boost::archive::text_oarchive oarch(ofs);
	  oarch << t;
	  ofs.close();
	}

	//Deserialize a map of trajectories (Motion plans)
	static void load(const std::string filename, std::map<int, Trajectory>& t) {
	  std::ifstream ifs(filename);
	  if(ifs.is_open()) {
	    boost::archive::text_iarchive iarch(ifs);
	    iarch >> t;
	  }
	  ifs.close();
	}

	//Serialize a map of configurations
	static void save(const std::string filename, const std::map<unsigned, std::vector<double>>& c) {
	  std::ofstream ofs(filename);
	  boost::archive::text_oarchive oarch(ofs);
	  oarch << c;
	  ofs.close();
	}

	//Deserialize a map of grasping actions
	static void load(const std::string filename, std::map<unsigned, std::vector<double>>& c) {
	  std::ifstream ifs(filename);
	  if(ifs.is_open()) {
	    boost::archive::text_iarchive iarch(ifs);
	    iarch >> c;
	  }
	  ifs.close();
	}
	


	
	
	
	
	
	
	

  
protected:

  
};