#pragma once
#include <stdexcept>
#include <memory>
#include <boost/property_tree/ptree.hpp>
#include <boost/property_tree/json_parser.hpp>

namespace pt = boost::property_tree;



class Config {
	
	
	
protected:
	static std::unique_ptr<Config> _instance;
	
	boost::property_tree::ptree _root;
	
	std::string _filename;
		
	
public:

	Config(const std::string& filename): _filename(filename) {
	  pt::json_parser::read_json(filename, _root);	
	}
  
	Config() {}
	~Config() {}
	


	//! A generic getter
	template <typename T>
	T getOption(const std::string& key) const {
		return _root.get<T>(key);
	}
	
	template <typename T>
	std::vector<T> as_vector(boost::property_tree::ptree::key_type const& key) const
	{
	  std::vector<T> r;
	  for (auto& item : _root.get_child(key))
	    r.push_back(item.second.get_value<T>());
	  return r;
	}
	
	template <typename T>
	std::pair<T,T> as_pair(boost::property_tree::ptree::key_type const& key) const
	{
	  std::vector<T> r;
	  std::pair<T,T> p;
	  for (auto& item : _root.get_child(key))
	    r.push_back(item.second.get_value<T>());
	  p = std::make_pair(r.at(0),r.at(1));
	  return p;
	}

};



