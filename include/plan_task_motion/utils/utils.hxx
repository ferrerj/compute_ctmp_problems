#pragma once

#include <cmath>
#include <fstream>
#include <configuration.hxx>
#include <unistd.h>
#include <sys/types.h>
#include <pwd.h>
#include <types.hxx>
#include <boost/numeric/conversion/cast.hpp>
#include <boost/filesystem.hpp>
#include <sys/stat.h>

#include <csignal>
#include <cstdio>
#include <cstring>
#include <ctype.h>
#include <fcntl.h>
#include <fstream>
#include <iostream>
#include <limits>
#include <new>
#include <stdlib.h>
#include <unistd.h>





static const double EPSILON = 0.00001;
static const double PI = 3.14159265358979323846;       

using namespace std;


class System {
  
  
public:
  
  static std::string get_home_folder() {
        struct passwd *pw = getpwuid(getuid());
    const char *homedir = pw->pw_dir;
  return std::string(homedir);
  }
  
static void create_folder(string& filename) {
	boost::filesystem::path dir(filename);
	if(boost::filesystem::create_directories(dir)) {
		std::cout << "Success" << "\n";
	}
}

static bool exists_file(const std::string& name) {
  struct stat buffer;   
  return (stat (name.c_str(), &buffer) == 0); 
}

static void soft_links(string& problem_folder) {
  
/*  
string command1 = "ln -s "+ problem_folder + "/../common/domain.pddl " + problem_folder; 
string command2 = "ln -s "+ problem_folder + "/../common/external.hxx " + problem_folder; 
string command3 = "ln -s "+ problem_folder + "/../common/external.cxx " + problem_folder; */
  
string command1 = "ln -s ../common/domain.pddl " + problem_folder; 
string command2 = "ln -s ../common/external.hxx " + problem_folder; 
string command3 = "ln -s ../common/external.cxx " + problem_folder; 


system(command1.c_str());
system(command2.c_str());
system(command3.c_str());
  
}

static int get_peak_memory_in_kb() {
    // On error, produces a warning on std::cerr and returns -1.
    int memory_in_kb = -1;

    std::ifstream procfile;
    procfile.open("/proc/self/status");
    std::string word;
    while (procfile.good()) {
        procfile >> word;
        if (word == "VmPeak:") {
            procfile >> memory_in_kb;
            break;
        }
        // Skip to end of line.
        procfile.ignore(std::numeric_limits<std::streamsize>::max(), '\n');
    }
    
    if (procfile.fail()) {
        memory_in_kb = -1;
	}

    if (memory_in_kb == -1) {
        std::cerr << "warning: could not determine peak memory" << std::endl;
	}
    return memory_in_kb;
}
    
  
  
};



class Geometry {
  
  
public:
  
  //Translate a point (obj configuration), which is the end point of a vector, to a new base.
  static Point translation(Point P, Point Q) {
	double nx = P.first + Q.first;
	double ny = P.second + Q.second;
	Point Pn = make_pair(nx, ny);
	return Pn;
  }
  
  //Rotate a point(obj configuration), which is the end of a point vector, depending on robot orientation
  static Point rotation(Point P, Point base, double angle) {
    
	double nx = base.first + (P.first - base.first)*cos(angle) - (P.second - base.second)*sin(angle);
	double ny = base.second + (P.first - base.first)*sin(angle) + (P.second - base.second)*cos(angle);
	Point Pn = make_pair(nx, ny);
	return Pn; 
    
  }
  
    //Translate a point (obj configuration), which is the end point of a vector, to a new base.
  static Configuration translation(Configuration P, Configuration Q) {
	double nx = P[0] + Q[0];
	double ny = P[1] + Q[1];
	vector<double> v = {nx, ny};
	Configuration c(v);
	return c;
  }
  
  //Rotate a point(obj configuration), which is the end of a point vector, depending on robot orientation
  static Configuration rotation(Configuration P, Configuration base, double angle) {
    
	double nx = base[0] + (P[0]- base[0])*cos(angle) - (P[1] - base[1])*sin(angle);
	double ny = base[1] + (P[0]- base[0])*sin(angle) + (P[1] - base[1])*cos(angle);
	vector<double> v = {nx, ny};
	Configuration c(v);
	return c; 
    
  }
  
  static vector<double> conf_conversion(Configuration P, Configuration base) {
    Configuration Pt = translation(P, base);
    Configuration Pr = rotation(Pt, base, base[2]);
    return Pr.get_values();
  }
  
  
  static vector<double> inv_conf_conversion_rot_trans(Configuration objr, Configuration base) {
    
 
    Configuration Pr = rotation(objr, base, base[2]*(-1));
     vector<double> tmp = {base[0]*(-1), base[1]*(-1)};
     Configuration base_neg(tmp); 
    Configuration Pt = translation(Pr, base_neg);
    return Pt.get_values();
  }
  
  
  static vector<double> inv_conf_conversion_trans_rot(Configuration objr, Configuration base) {
    vector<double> tmp = {base[0]*(-1), base[1]*(-1)};
    Configuration base_neg(tmp); 
    Configuration Pt = translation(objr, base_neg);
    Configuration Pr = rotation(Pt, base_neg, base[2]*(-1));
    return Pr.get_values();
    
    
  }
  
  
  
  static Point conf_conversion(Point P, Point base, double angle) {
    Point Pt = translation(P, base);
    Point Pr = rotation(Pt, base, angle);
    return Pr;
  }
  
  
  
  
  
  
  
  static double radians_to_degrees(double radians) {
	double PI = 3.14159265359;
	return radians * (180.0 / PI);
    
  }
  
  static double degrees_to_radians(double degrees) {
	double PI = 3.14159265359;
	return degrees * (PI / 180.0);
    
  }
  
  static double angle_between(const vector<double>& A, const vector<double>& B, const vector<double>& C) {
    
   double dot = ( (B[0] - A[0]) * (C[0] - A[0]) + (B[1] - A[1]) * (C[1] - A[1])); // dot product
   double cross = ((B[0] - A[0]) * (C[1] - A[1]) - ( B[1] - A[1]) * (C[0] - A[0])); // cross product
   double angle_r = atan2(cross, dot);
   return  (angle_r * 180.0 / PI);
    
}
  
  
  static vector<double> circumference_point(double x, double y, double r, double angle) {
   
    double xn = cos(angle)*r + x;
    double yn = sin(angle)*r + y;
    vector<double> p = {xn, yn};
    return p;
    
  }
  
    static vector<double> circumference_point(vector<double> c, double r, double angle) {
   
    double xn = cos(angle)*r + c[0];
    double yn = sin(angle)*r + c[1];
    vector<double> p = {xn, yn};
    return p;
    
  }

    
    static vector<double> eef_yaw(double x, double y, double r, double angle) {
    
      vector<double> c = {x,y};
      vector<double> eef = circumference_point(c, r, angle);
      
      double xn = cos(0)*1 + eef[0];
      double yn = sin(0)*1 + eef[1];
      vector<double> p = {xn, yn};
      double yaw = angle_between(eef, p, c);
      eef.push_back(degrees_to_radians(yaw));
      
      return eef;
    
    
  }
 
 
  
  
  
};

class Utils {

  
  Utils(){}
  ~Utils(){}
  
public:
  
  
 static bool are_same(double a, double b) {
   return fabs(a - b) < EPSILON;
 }
 
 
 static double	distance_between(double x_a, double y_a, double x_b, double y_b) {
    return sqrt( ((x_b-x_a)*(x_b-x_a))+((y_b-y_a)*(y_b-y_a)));
}


//For base distances only
static double distance_between(const Configuration& c1, const Configuration& c2) {
  
      return sqrt( ((c2[0]-c1[0])*(c2[0]-c1[0]))+((c2[1]-c1[1])*(c2[1]-c1[1])));
  
  
}

static unsigned int convert( size_t what ) {
    return boost::numeric_cast<unsigned int>( what );
}


 static bool inside_area(double x, double y, string table) {
   
   if( 	(x > D3::tables[table].at(0)) && (x < D3::tables[table].at(1)) && 
	(y > D3::tables[table].at(2)) && (y < D3::tables[table].at(3)) )
	  return true;
   else return false;
 }
 
 
 static void create_problem_folders(string& filename) {
  
   string data = filename+"/data";
   string info = filename+"/info";
   string results = filename+"/results";
   System::create_folder(data);
   System::create_folder(info);
   System::create_folder(results);
   
 }
 
 
static void store_configurations(set<Configuration>& confs, string filename) {
  
 ofstream myfile(filename);
 for(auto & it : confs) 
  if(myfile.is_open()) {
    myfile << it.id() << " "; 
    for(unsigned i = 0; i < it.size(); ++i)
      myfile << it[i]  << " ";
    myfile << endl;
  }
 myfile.close(); 
  
}


static void store_overlaps(Overlaps& ov, string filename) {
 
  ofstream myfile(filename);
  for(auto & it : ov) 
    if(myfile.is_open()) 
      myfile << std::get<0>(it) << ", " << std::get<1>(it) << ", " << std::get<2>(it) << endl;
    myfile.close();
}

static void store_overlaps(VOverlaps& ov, string filename) {
 
  ofstream myfile(filename);
  for(auto & it : ov) 
    if(myfile.is_open()) 
      myfile << it.first << " " << it.second << endl;
    myfile.close();
}


static void store_real_virtual_mapping(ConfConversion& cc, string filename) {
  
  ofstream myfile(filename);
  for(auto & it : cc) 
    if(myfile.is_open()) 
      myfile << it.first.first << " " << it.first.second << " " << it.second << endl;
    myfile.close();
  
  

}


static void store_rel_virtual(map<unsigned, unsigned>& m, string filename) {
    ofstream myfile(filename);
  for(auto & it : m) 
    if(myfile.is_open()) 
      myfile << it.first << " " << it.second << endl;
    myfile.close();
  
  
}


static void store_graph(Graph g, string filename) {
   ofstream myfile(filename);
 for(auto & it : g) 
  if(myfile.is_open()) 
    myfile << it.first.first << " " << it.first.second << " " << it.second << endl; 
 myfile.close(); 
  
}

static void store_graspable(Graspable& graspable, string filename) {
 ofstream myfile(filename);
 for(auto & it : graspable) 
  if(myfile.is_open()) 
    myfile << it.first.first << " " << it.first.second << " " << it.second << endl; 
 myfile.close(); 
  
}


static void store_Vgraspable(VGraspable& graspable, string filename) {
   ofstream myfile(filename);
 for(auto & it : graspable) 
  if(myfile.is_open()) 
    myfile << it.first << " " << it.second << endl; 
 myfile.close(); 
  
  
}


static void store_trajectories(map<int, Trajectory>& traj) {
  
  ofstream myfile("/home/jonathanaig/catkin_ws/src/plan_task_motion/benchmarks/trajectories.data");
  if(myfile.is_open()) {
    for(auto & it : traj) {   
      myfile << "#" << it.first << endl;
      for(unsigned i = 0; i < it.second.size(); ++i) {
	for(unsigned j = 0; j < it.second.at(i).size(); ++j) 
	 myfile << it.second.at(i).at(j) << " ";
	myfile << "\n";
      }
      myfile << "\n";
    }
  }
    myfile.close();
}



static string edge(int k) {
 if(k == 1)
   return "cw";
 else if(k == 2)
   return "ccw";
 else if(k == 3)
   return "f";
  
}


static double angle_between(const Configuration& A, const Configuration& B, const Configuration& C) {
    
   double dot = ( (B[0] - A[0]) * (C[0] - A[0]) + (B[1] - A[1]) * (C[1] - A[1])); // dot product
   double cross = ((B[0] - A[0]) * (C[1] - A[1]) - ( B[1] - A[1]) * (C[0] - A[0])); // cross product
   double angle_r = atan2(cross, dot);
   return  (angle_r * 180.0 / PI);
    
}

static double angle_between(const vector<double>& A, const vector<double>& B, const vector<double>& C) {
    
   double dot = ( (B[0] - A[0]) * (C[0] - A[0]) + (B[1] - A[1]) * (C[1] - A[1])); // dot product
   double cross = ((B[0] - A[0]) * (C[1] - A[1]) - ( B[1] - A[1]) * (C[0] - A[0])); // cross product
   double angle_r = atan2(cross, dot);
   return  (angle_r * 180.0 / PI);
    
}


static vector<Configuration> k_nearest(const Configuration& c, set<Configuration>& confs) {
  double dist = 0.0;
  double max_dist = mc;
  map<double, Configuration> distances;
  vector<Configuration> k_nearest_confs;
  for(auto& it: confs) {
    dist = Utils::distance_between(c, it);
    if(dist <= 0.002) {
      double rad_dist = std::fabs(c[2] - it[2]);
      if(rad_dist > 0.02)
	distances.insert(make_pair(rad_dist, it));
      }
    else if((dist > 0.002) && (dist <= max_dist))
      distances.insert(make_pair(dist, it));
  }
  for(unsigned i = 0; i < kn; i++ ) {
    if( (distances.size() > 0) && (i < distances.size())) {
      auto it = distances.begin();
      std::advance(it, i);
      k_nearest_confs.push_back(it->second);
    }
  }
    
  return k_nearest_confs;
  
}


void connect(const Configuration& c, set<Configuration>& confs, 
				     map<pair<Configuration, Configuration>, unsigned>& connections, int& id) {
  
  double dist = 0.0;
  const double max_dist = (mc*2)-0.2;
  //vector<Configuration> adjacent;
  
  
  for(auto& it: confs) {//1
	    if(c.id() == it.id())
	      continue;
	    
	    auto it2 = connections.find(make_pair(it, c));
	    if(it2 != connections.end())
	      continue;
	      
	    
	    
	    dist = Utils::distance_between(c, it);
	    if(dist <= max_dist) {//2
	      if(dist <= 0.02) {//3
		  double rad_dist = std::fabs(c[2] - it[2]);
		  if(rad_dist > 0.01) {
		   
		    connections.insert(make_pair(make_pair(c,it),id));
		    id++;
		    //adjacent.push_back(it);
		    
		  }
	      }//3
	      
	      else if(dist > 0.02) {//4
		  double r_d = std::fabs(c[2] - it[2]);
		  if(r_d <= 0.3) {
		   connections.insert(make_pair(make_pair(c,it),id));
		   id++;
		  }
		  // adjacent.push_back(it);


		    //distances.insert(make_pair(dist, it));
	      }
	      
	      
	      
	    }//2 
  }//1

  //return adjacent;
  
}



static vector<double> mid_point(vector<double>& a, vector<double>& b) {
  vector<double> mid;
  mid.push_back((a[0]+b[0])/2);
  mid.push_back((a[1]+b[1])/2);
  return mid;
  
}





 
 
 
 
};