#pragma once
#include <iostream>
#include <configuration.hxx>
#include <action.hxx>
#include <types.hxx>
#include <utils/serializer.hxx>

using namespace std;



class Problem {
  
  
public: 
    
    
    Problem(set<Configuration>& bconfs, set<Configuration>& aconfs, set<Configuration>& oconfs, Graph& bg, Graph& ag, map<int, MotionPlan>& base_motion_plan, map<int, MotionPlan>& arm_motion_plan, Graspable& graspable, Overlaps& rob_obj, Overlaps& obj_obj):
	_bconfs(bconfs), _aconfs(aconfs), _oconfs(oconfs), _g(bg), _ag(ag), _base_motion_plans(base_motion_plan),
	_arm_motion_plans(arm_motion_plan), _graspable(graspable), _rob_obj(rob_obj), _obj_obj(obj_obj) {}  
	
    Problem(ConfigurationSet& bconfs, ConfigurationSet& aconfs, ConfigurationSet& r_oconfs, ConfigurationSet& v_oconfs, ConfConversion& real_virtual_obj_confs, ConfConversion& real_relative_obj_confs, Graph& bg, Graph& ag,
	    map<int, MotionPlan>& base_motion_plan, map<int, MotionPlan>& arm_motion_plan, VGraspable& v_graspable, RelOverlaps& rel_overlaps_empty, RelOverlaps& rel_overlaps_holding):
	_bconfs(bconfs), _aconfs(aconfs), _oconfs(r_oconfs), _v_oconfs(v_oconfs), _real_virtual_obj_confs(real_virtual_obj_confs), _real_relative_obj_confs(real_relative_obj_confs), _g(bg), _ag(ag), _base_motion_plans(base_motion_plan),
	_arm_motion_plans(arm_motion_plan), _v_graspable(v_graspable), _rel_overlaps_empty(rel_overlaps_empty), _rel_overlaps_holding(rel_overlaps_holding){}  
  
  ~Problem() {}
  
  
  ConfigurationMap& get_map_bconfs() {return _map_bconfs;} 
  ConfigurationMap& get_map_aconfs() {return _map_aconfs;} 
  ConfigurationMap& get_map_oconfs() {return _map_oconfs;} 

  
  ConfigurationSet& get_bconfs() {return _bconfs;}
  ConfigurationSet& get_oconfs() {return _oconfs;}
  ConfigurationSet& get_aconfs() {return _aconfs;}
  ConfigurationSet& get_v_oconfs() {return _v_oconfs;}
  
  ConfConversion& get_real_virtual_conversion() {return _real_virtual_obj_confs;}
  ConfConversion& get_real_relative_conversion() {return _real_relative_obj_confs;}

  
  map<int, MotionPlan>& get_arm_motion_plans() {return _arm_motion_plans;}
  map<int, MotionPlan>& get_base_motion_plans() {return _base_motion_plans;}

  Graph& get_base_graph() {return _g;} 
  Graph& get_arm_graph() {return _ag;}

  Graspable& get_graspable() {return _graspable;}
  VGraspable get_v_graspable() {return _v_graspable;}
  
  Overlaps& get_rob_obj() {return _rob_obj;}
  Overlaps& get_obj_obj() {return _obj_obj;}
  RelOverlaps& get_relative_overlaps_empty() {return _rel_overlaps_empty;}
  RelOverlaps& get_relative_overlaps_holding() {return _rel_overlaps_holding;}

  
  
  void serialize_data(string& path) {
    
    serialize_virtual_data(path);
    Serializer::save(path+"/data/base_graph.boost", _g);
    Serializer::save(path+"/data/arm_graph.boost", _ag);
    //Serializer::save(path+"/data/overlaps_R_O.boost", _rob_obj);
    //Serializer::save(path+"/data/overlaps_O_O.boost", _obj_obj);
    //Serializer::save(path+"/data/placing_poses.boost", _graspable);
    
    //Map for base, arm and objects configurations
    map<unsigned, vector<double>> mb;
    map<unsigned, vector<double>> ma;
    map<unsigned, vector<double>> mo;

    for(auto& it: _bconfs)
      mb.insert(make_pair(it.id(), it.get_values()));
    for(auto& it: _aconfs)
      ma.insert(make_pair(it.id(), it.get_values()));
    for(auto& it: _oconfs)
      mo.insert(make_pair(it.id(), it.get_values()));

     Serializer::save(path+"/info/base_configurations.boost", mb);
     Serializer::save(path+"/info/arm_configurations.boost", ma);
     Serializer::save(path+"/info/object_configurations.boost", mo);
    
    
    map<int, Trajectory> base_traj;
    map<int, Trajectory> arm_traj;
    Trajectory t1;
    Trajectory t2;

    for(auto& it: _base_motion_plans) {
      int size = it.second.trajectory_.multi_dof_joint_trajectory.points.size();
      for(unsigned i = 0; i < size; ++i) {
	
	vector<double> joint_values;
	double x = it.second.trajectory_.multi_dof_joint_trajectory.points[i].transforms[0].translation.x;
	double y = it.second.trajectory_.multi_dof_joint_trajectory.points[i].transforms[0].translation.y;
	double xr = it.second.trajectory_.multi_dof_joint_trajectory.points[i].transforms[0].rotation.x;
	double yr = it.second.trajectory_.multi_dof_joint_trajectory.points[i].transforms[0].rotation.y;
	double zr = it.second.trajectory_.multi_dof_joint_trajectory.points[i].transforms[0].rotation.z;
	double w =  it.second.trajectory_.multi_dof_joint_trajectory.points[i].transforms[0].rotation.w;
	
	tf::Quaternion q(xr, yr, zr, w);
	tf::Matrix3x3 m(q);
	double roll, pitch, yaw;
	m.getRPY(roll, pitch, yaw);
	joint_values.push_back(x);
	joint_values.push_back(y);
	joint_values.push_back(yaw);
	t1.push_back(joint_values);
      }
      base_traj.insert(make_pair(it.first, t1));
      t1.clear();
    }
        
    for(auto& it: _arm_motion_plans) {
     int size = it.second.trajectory_.joint_trajectory.points.size();
     for(unsigned i = 0; i < size; ++i) {
	vector<double> joint_values = it.second.trajectory_.joint_trajectory.points[i].positions;
	t2.push_back(joint_values);
      }
      arm_traj.insert(make_pair(it.first, t2));
      t2.clear();
    }
    
    
    Serializer::save(path+"/info/base_trajectories.boost", base_traj);
    Serializer::save(path+"/info/arm_trajectories.boost", arm_traj);

  }
  
  
  void serialize_virtual_data(string& path) {
        Serializer::save(path+"/data/overlaps_empty.boost", _rel_overlaps_empty);
        Serializer::save(path+"/data/overlaps_holding.boost", _rel_overlaps_holding);
	Serializer::save(path+"/data/placing_poses.boost", _v_graspable);
	Serializer::save(path+"/data/real_virtual.boost", _real_virtual_obj_confs );
	Serializer::save(path+"/data/real_relative.boost", _real_relative_obj_confs );




    
    
  }
  
 
  
protected:
  
  //Robot configurations
  ConfigurationSet _bconfs;
  ConfigurationSet _oconfs;
  ConfigurationSet _aconfs;
  ConfigurationSet _v_oconfs;
  
  ConfConversion _real_virtual_obj_confs;
  ConfConversion _real_relative_obj_confs;//<cb, co_real> = co_relative
  
  //All configurations
  ConfigurationMap _map_bconfs;
  ConfigurationMap _map_aconfs;
  ConfigurationMap _map_oconfs;

  Graph _g;
  Graph _ag;
  
  map<int, MotionPlan> _arm_motion_plans;
  map<int, MotionPlan> _base_motion_plans;
  //Graspable actions
  Graspable _graspable;
  VGraspable _v_graspable;
  //R-O overlaps
  Overlaps _rob_obj;
  //O-O overlaps
  Overlaps _obj_obj;
  //Virtual R-O overlaps (hripper empty)
  RelOverlaps _rel_overlaps_empty;
  RelOverlaps _rel_overlaps_holding;

  
  
};