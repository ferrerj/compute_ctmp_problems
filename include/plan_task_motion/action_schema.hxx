#pragma once



class ActionSchema {
protected:
	//! The name of the action
	const std::string _name;
		
	//! The name of the parameters
	const std::vector<std::string> _parameters;

public:
	typedef const ActionSchema* cptr;
	

	ActionSchema(const std::string& name, const std::vector<std::string>& parameters)
	: _name(name), _parameters(parameters){}

	~ActionSchema() {}
	
	inline const std::string& getName() const { return _name; }
		
	inline const std::vector<std::string>& getParameters() const { return _parameters; }

	//! Prints a representation of the object to the given stream.
	friend std::ostream& operator<<(std::ostream &os, const ActionSchema& o) { return o.print(os); }
	
	std::ostream& print(std::ostream& os) const {
	  os << _name << ": ";
	  for(unsigned i = 0; i < _parameters.size(); ++i)
	    os << _parameters.at(i) << " ";
	  os << std::endl;
	  return os;
}};


