#pragma once

#include "action_schema.hxx"



class Action {
  
protected:
	unsigned _id;
	//! The values of the action parameters, if any.
	const std::vector<float> _binding;
	const std::vector<unsigned int> _params;
	
	//ActionSchema::cptr _schema;
	ActionSchema _schema;


public:
	typedef const Action* cptr;
	/*
	Action(unsigned id, const std::vector<float>& binding, ActionSchema::cptr schema)
		: _id(id), _binding(binding), _schema(schema) {}
		
	Action(unsigned id, const std::vector<float>& binding)
		: _id(id), _binding(binding) {}
// 		
	Action(unsigned id, const std::vector<int>& params, ActionSchema::cptr schema)
		: _id(id), _params(params), _schema(schema) {}
		
	Action(unsigned id, const std::vector<int>& params)
		: _id(id), _params(params) {}
		*/
	Action(const std::vector<unsigned int>& params, ActionSchema& schema)
	: _params(params), _schema(schema) {}
	
	
	~Action(){}
	
	bool operator<(const Action& a) const {
	  return _id < a._id;  
	}
	
	
	//! Returns the name of the action, e.g. 'move'
	const std::string& getName() const { return _schema.getName(); }
			
	//! Returns the concrete binding that created this action from its action schema
	const std::vector<float>& getBinding() const { return _binding; }
	const std::vector<unsigned int>& getParams() const { return _params; }
	
	//! Prints a representation of the object to the given stream.
	friend std::ostream& operator<<(std::ostream &os, const Action&  entity) { return entity.print(os); }
	
	std::ostream& print(std::ostream& os) const {
        os << "Action #" << _id;
	//os << print::action_name(*this) << std::endl;	
	return os;
	
	}
};


