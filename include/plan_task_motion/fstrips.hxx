#pragma once
#include <iostream>
#include <fstream>
#include <problem.hxx>
#include <utils/utils.hxx>


using namespace std;

class FSTRIPS {
  
  
private:
  
  ofstream _pddl;
  Problem _problem;
 

public:
  
  
  FSTRIPS(Problem& problem, string& filename): 
	  _problem(problem),
	  _pddl(filename)
	    {}
  ~FSTRIPS() {}
  
  
  void generate_instance() {
    
    
    head();
    objects();
    init();
    graph();
    actions();
    //overlaps();
    other_init();
    goal();
    constraints();
    close();
    
  }
  
  void head() {
    if (_pddl.is_open()) {
      _pddl << "(define (problem sample)\n";
      _pddl << "\t(:domain fn-robot-navigation)\n";
    }
        
    
  }
  
  void objects() {
    
    if (_pddl.is_open()) {
      _pddl << "\t(:objects \n" << endl;
      _pddl << " - object_id\n\n";
      for(auto& it : _problem.get_bconfs())
	_pddl << "\tcr" << it.hash() << endl;
      _pddl << "\t - conf_rob" << endl;
      for(auto& it: _problem.get_oconfs())
	_pddl << "\tco" << it.hash() << endl;
      _pddl << " - conf_obj \n )" << endl; 
    }
  }
  /*
  void init() {
    
    if (_pddl.is_open()) {
      _pddl << "(:init" << endl;
     for(unsigned i = 0; i < _problem.get_init().size(); ++i)
       if(i == 0)
	  _pddl << "(= (confr " << _problem.get_init().at(i).first << ") cr" << _problem.get_init().at(i).second << ")" << endl;
       else
	  _pddl << "(= (confo " << _problem.get_init().at(i).first << ") co" << _problem.get_init().at(i).second << ")" << endl;

    } 
  }
  */
  void actions() {
    if (_pddl.is_open()) {
     for(unsigned i = 0; i < _problem.get_actions().size(); ++i) {
	vector<unsigned> params = _problem.get_actions().at(i).getParams();

	if( _problem.get_actions().at(i).getName() == "graspable") {
	    _pddl << "\t(= (placing_pose cr" << params.at(0) << ") co" << params.at(1) << ")" << endl;
	    _pddl << "\t(graspable cr" << params.at(0) << " co" << params.at(1) << ")" << endl;
	    _pddl << "\t(placeable cr" << params.at(0) << ")" << endl;


	}
     }
      
    }
  }
  
  void graph() {
     if (_pddl.is_open()) {
	for(auto& it: _problem.get_graph()) {
	 _pddl << "\t(= (transition cr" << it.first.first << " e" << Utils::edge(it.first.second) << ") cr" << it.second << ")" << endl; 
	 _pddl << "\t(exist_transition cr" << it.first.first << " e" << Utils::edge(it.first.second) << ")" << endl; 
	}
    }
  }
  
  /*
  void overlaps() {
    if (_pddl.is_open()) {
      for(unsigned i = 0; i < _problem.get_rob_obj().size(); ++i)
	_pddl << "(ov_r_o cr" << _problem.get_rob_obj().at(i).first << " co" << _problem.get_rob_obj().at(i).second << ")" << endl;
      for(unsigned i = 0; i < _problem.get_obj_obj().size(); ++i)
	_pddl << "(ov_o_o co" << _problem.get_obj_obj().at(i).first << " co" << _problem.get_obj_obj().at(i).second << ")" << endl;
      for(auto& it: _problem.get_bconfs()) 
	_pddl << "(ov_r_o cr" << it.hash() << " c_held" << ")" << endl;
      for(auto& it2: _problem.get_oconfs()) {
      	_pddl << "(ov_o_o co" << it2.hash() << " c_held" << ")" << endl;
	_pddl << "(ov_o_o c_held"  << " co" << it2.hash() << ")" << endl;

	
      }

    }
  }
  */
  

  void other_init() {
     if (_pddl.is_open()) {
      _pddl << "(handempty rob)" << endl;
      _pddl << ")" << endl; // close init
     }

  }
  
  void goal() {
    
    if (_pddl.is_open()) {
      _pddl << "(:goal (and" << endl;
       for(unsigned i = 0; i < _problem.get_goal().size(); ++i)
	  _pddl << "(= (confr " << _problem.get_goal().at(i).first << ") cr" << _problem.get_goal().at(i).second << ")" << endl;
       _pddl << "))" <<  endl;
      }
    
  }
  
    void constraints() {
     if (_pddl.is_open()) {
      _pddl << "(:constraints" << endl;
	  _pddl << "\t(ov_r_o (confr rob) (confo o1))" << endl;
	  _pddl << "\t(ov_r_o (confr rob) (confo o2))" << endl;
	  _pddl << "\t(ov_o_o (confo o1) (confo o2))" << endl;

      _pddl << ")" << endl; 
     }
  }
  
  void close() {
     if (_pddl.is_open())
       _pddl << ")" << endl;
     _pddl.close();

    
  }
  
  
  
  
  
  
  
  
  
};