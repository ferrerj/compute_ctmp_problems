#pragma once
#include <simulator/3D/virtual_robot.hxx>
#include <boost/graph/graph_concepts.hpp>
#include <environment.hxx>


class ProblemInfo  {
  
  
public:
  
  ProblemInfo(VirtualRobot& problem):
    _problem(problem) {}
    
  ~ProblemInfo() {}
  
  
  void info() {
    
      cout << "Base configurations :" << _problem.get_bconfs().size() << endl;
      cout << "Arm configurations: " << _problem.get_aconfs().size() << endl;
      cout << "Real Object configurations: " << _problem.get_oconfs().size() << endl;
      cout << "Virtual Object configurations: " << _problem.get_v_oconfs().size() << endl;
      cout << "Virtual graspable: " << _problem.get_v_graspable().size() << endl;
      cout << "Base graph size: " << _problem.get_graph_base().size() << endl;
      cout << "Arm graph size: " << _problem.get_graph_arm().size() << endl;
      cout << "# Relative overlaps (R-O) empty: " << _problem.get_relative_overlaps_empty().size() << endl;
      cout << "# Relative overlaps (R-O) holding: " << _problem.get_relative_overlaps_holding().size() << endl;
      cout << "Base discretization around tables: " << mc << endl;
      cout << "Workspace: " << common::x_from << ", " << common::x_to << ", " << common::y_from << ", " << common::y_to << endl;
      cout << "End effector discretization: " << D3::m_object << endl;
      cout << "K-nearest: " << kn << endl;
      cout << "Robot: "  << D3::robot << endl;
      
  }
  
  void store_problem_info(string filename, float time, int memmory) {
   
  ofstream myfile(filename);
    if(myfile.is_open()) {
      
      
      myfile << "Base configurations :" << _problem.get_bconfs().size() << endl;
      myfile << "Arm configurations: " << _problem.get_aconfs().size() << endl;
      myfile << "Real Object configurations: " << _problem.get_oconfs().size() << endl;
      myfile << "Virtual Object configurations: " << _problem.get_v_oconfs().size() << endl;
      myfile << "Relative Object configurations: " << _problem.get_relative_oconfs().size() << endl;
      myfile << "Virtual graspable: " << _problem.get_v_graspable().size() << endl;
      myfile << "Arm trajectories: " << _problem.get_arm_motion_plan().size() << endl;
      myfile << "Base trajectories: " << _problem.get_base_motion_plan().size() << endl;
      myfile << "Base graph size: " << _problem.get_graph_base().size() << endl;
      myfile << "Arm graph size: " << _problem.get_graph_arm().size() << endl;
      myfile << "# Relative overlaps (R-O) empty: " << _problem.get_relative_overlaps_empty().size() << endl;
      myfile << "# Relative overlaps (R-O) holding: " << _problem.get_relative_overlaps_holding().size() << endl;
      myfile << "Base discretization around tables: " << mc << endl;
      myfile << "End effector discretization: " << D3::m_object << endl;
      myfile << "K-nearest: " << kn << endl;
     // myfile << "End effector poses per virtual object configuration: " << D3::grasping_poses_per_virtual_conf << endl;
      //myfile << "Max number of trajectories per grasping poses: " << D3::traj_per_grasping_pose << endl;
     // myfile << "Trajectories per virtual object configuration: " << D3::grasping_poses_per_virtual_conf*D3::traj_per_grasping_pose << endl;
  
      myfile << "Robot: "  << D3::robot << endl;
      myfile << "Time: " << time << endl;
      myfile << "Memmory: " << memmory << endl;

    }
    myfile.close();

    
  }
  
  

    

protected:
  
  VirtualRobot _problem;
 
  
};