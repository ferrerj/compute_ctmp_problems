#pragma once

#include <iostream>
#include <cstdlib>
#include <memory>
#include <set>
#include <vector>
#include <boost/functional/hash.hpp>
#include <environment.hxx>
#include <cmath>


class Configuration {
protected:
  
	// Configuration values correspond to the features taken as configuration parameters:
	// x,y, theta for 2D; j1,j2,...jn for 3D and so on..
	unsigned _id;
	std::vector<double> _values;//real values from the simulator
	std::vector<int> _discr_values;	//discretized values
	std::size_t _hash;

public:
	typedef std::shared_ptr<const Configuration> cptr;

	// The only allowed constructor receives all values making up the state
	Configuration(unsigned id, std::vector<double> values) : _id(id), _values(values) {
	  
	  for(unsigned i = 0; i < values.size(); ++i) { 
	    int discr_val = round(values.at(i)*discr);
	    _discr_values.push_back(discr_val);
	   
	  }
	    _hash = computeHash();

	  
	}
	Configuration(std::vector<double> values) : _values(values) {
	    for(unsigned i = 0; i < values.size(); ++i) {
	      int discr_val = round(values.at(i)*discr);
	      _discr_values.push_back(discr_val);
	  }
	  	   _hash = computeHash();

	}
	
	Configuration() {}
	~Configuration() {}
	
	
	bool operator<(const Configuration& c) const {
	  return _hash < c._hash;  
	}
	
	/*
	bool operator<(const Configuration& c) const {
	  return _id < c._id;  
	}
	*/


	//! Default copy constructors and assignment operators
	Configuration(const Configuration& conf) = default;
	Configuration( Configuration&& conf ) = default;
	Configuration& operator=(const Configuration &conf) = default;
	Configuration& operator=(Configuration&& conf) = default;

	// Check the hash first for performance.
	bool operator==(const Configuration &rhs) const { return _discr_values == rhs._discr_values;}
	bool operator!=(const Configuration &rhs) const { return !(this->operator==(rhs));}
	
	double& operator[](unsigned idx) { return _values[idx]; }
	const double operator[](unsigned idx) const { return _values[idx]; }

	inline std::size_t size() const { return _values.size(); }

	//! Prints a representation of the state to the given stream.
	friend std::ostream& operator<<(std::ostream &os, const Configuration&  conf) { return conf.print(os); }
	
	
	std::ostream& print(std::ostream& os) const {
	os << "Configuration";
	//os << "(" << _hash << ")={";
	os << "(" << _id << ")={";

	for (unsigned i = 0; i < _values.size(); ++i) {
		os << _values.at(i);
		if (i < _values.size() - 1) os << ", ";
	}
	os << "}\n{";
	for (unsigned i = 0; i < _discr_values.size(); ++i) {
		os << _discr_values.at(i);
		if (i < _discr_values.size() - 1) os << ", ";
	}
	os << "}";
	return os;
}
	
	std::size_t hash() const { return _hash; }
	const unsigned id() const  {return _id;}
	void set_id(unsigned id) {_id = id;}
	
	void set_value(unsigned idx, double value) {_values[idx] = value; _discr_values[idx] = (int)(value*discr); }
	
	const int get_discr_val(unsigned idx) const {return _discr_values.at(idx);}
	
	const std::vector<double>& get_values() const {return _values;}
	
	
protected:
	void updateHash() { _hash = computeHash(); }
	
	std::size_t computeHash() const { return boost::hash_range(_discr_values.begin(), _discr_values.end()); };

};

