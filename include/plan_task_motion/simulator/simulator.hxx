#pragma once

#include <ros/ros.h>
#include <gazebo_msgs/GetModelState.h>
#include <gazebo_msgs/SetModelState.h>
#include <gazebo_msgs/GetWorldProperties.h>
#include <gazebo_msgs/GetModelProperties.h>
#include <gazebo_msgs/GetJointProperties.h>
#include <gazebo_msgs/SetModelConfiguration.h>
#include <configuration.hxx>
#include <action.hxx>
#include <gazebo_msgs/GetLinkState.h>
#include <tf/transform_datatypes.h>
#include <boost/property_map/property_map.hpp>
#include <boost/graph/graph_concepts.hpp>
#include <geometry_msgs/Pose.h>
#include <moveit/move_group_interface/move_group.h>
#include <trajectory_msgs/JointTrajectory.h>
#include<moveit_msgs/ApplyPlanningScene.h>
#include <moveit/planning_scene_interface/planning_scene_interface.h>
#include<moveit/planning_scene/planning_scene.h>
#include <moveit_msgs/ExecuteKnownTrajectory.h>
#include <shape_tools/solid_primitive_dims.h>
#include "gazebo_ros_link_attacher/Attach.h"
#include <pr2_controllers_msgs/Pr2GripperCommand.h>
#include <std_srvs/Empty.h>
#include <gazebo_msgs/DeleteModel.h>




using namespace std;


class Simulator {
  
  
protected:
  
  ros::NodeHandle _n;
  
public:
  
  Simulator(ros::NodeHandle n): _n(n) {}
  Simulator() {}
  ~Simulator() {}
  
  bool isStatic(std::string model) {
	 bool isstatic = false;
	 ros::ServiceClient client = _n.serviceClient<gazebo_msgs::GetModelProperties>("/gazebo/get_model_properties");
	 gazebo_msgs::GetModelProperties getmodelproperties; 
	 getmodelproperties.request.model_name= model;
	 if(client.call(getmodelproperties)) 
	    isstatic = getmodelproperties.response.is_static;
	return isstatic;
}

bool delete_model(string& model) {
  
  ros::ServiceClient client = _n.serviceClient<gazebo_msgs::DeleteModel>("/gazebo/delete_model");
  gazebo_msgs::DeleteModel dm;
  dm.request.model_name = model;
  if(client.call(dm))
    return dm.response.success;
  else return false;
}

 std::vector<double> get_model_state(std::string model, std::string ref="world", bool d2=true) {
  
   std::vector<double> pos;
   ros::ServiceClient client = _n.serviceClient<gazebo_msgs::GetModelState>("/gazebo/get_model_state"); 
   gazebo_msgs::GetModelState getmodelstate;
    getmodelstate.request.model_name= model;
    getmodelstate.request.relative_entity_name = ref;
      if(client.call(getmodelstate)) {
	tf::Quaternion q(getmodelstate.response.pose.orientation.x, getmodelstate.response.pose.orientation.y, 
			 getmodelstate.response.pose.orientation.z, getmodelstate.response.pose.orientation.w);
	tf::Matrix3x3 m(q);
	double roll, pitch, yaw;
	m.getRPY(roll, pitch, yaw); 
	pos.push_back(getmodelstate.response.pose.position.x);
	pos.push_back(getmodelstate.response.pose.position.y);
	if(d2)
	  pos.push_back(yaw);
	else { 
	  pos.push_back(getmodelstate.response.pose.position.z);
	  pos.push_back(roll);
	  pos.push_back(pitch);
	  pos.push_back(yaw);
	}
      }
      return pos;
}



 bool set_model_state(double x, double y, double theta, std::string model, double z = 0.0, bool d2=true)  {
		geometry_msgs::Pose to_pose;
		to_pose.position.x = x;
		to_pose.position.y = y;
		if(d2)
		  to_pose.position.z = 0.0;//TODO: By default, or maybe include it in the state?
		else 
		  to_pose.position.z = z;
		tf::Quaternion q = tf::createQuaternionFromRPY(0.0,0.0,theta);
		set_quaternion(to_pose, q);
								
		gazebo_msgs::ModelState modelstate;
		modelstate.model_name = model;
		modelstate.reference_frame = (std::string) "world";
		modelstate.pose = to_pose;
		
		ros::ServiceClient client = _n.serviceClient<gazebo_msgs::SetModelState>("/gazebo/set_model_state");
		gazebo_msgs::SetModelState setmodelstate;
		setmodelstate.request.model_state = modelstate;
		if(client.call(setmodelstate))
		  return setmodelstate.response.success;
		else return false;
}


 bool set_model_state(const Configuration& c, std::string model, bool d2=true)  {
		geometry_msgs::Pose to_pose;
		double theta = 0.0;
		to_pose.position.x = c[0];
		to_pose.position.y = c[1];
		if(d2) {
		  to_pose.position.z = 0.0;
		  theta = c[2];
		}
		else 
		  to_pose.position.z = c[2];
		
		tf::Quaternion q = tf::createQuaternionFromRPY(0.0,0.0,theta);
		set_quaternion(to_pose, q);
								
		gazebo_msgs::ModelState modelstate;
		modelstate.model_name = model;
		modelstate.reference_frame = (std::string) "world";
		modelstate.pose = to_pose;
		
		ros::ServiceClient client = _n.serviceClient<gazebo_msgs::SetModelState>("/gazebo/set_model_state");
		gazebo_msgs::SetModelState setmodelstate;
		setmodelstate.request.model_state = modelstate;
		if(client.call(setmodelstate))
		  return setmodelstate.response.success;
		else return false;
}

bool set_model_state(double x, double y, double z, double roll, double pitch, double yaw, string ref, string model) {
	      	      cout << x << " " << y << " " <<  z << " " <<  roll << " " <<  pitch << " " <<  yaw << endl;

  		geometry_msgs::Pose to_pose;
		to_pose.position.x = x;
		to_pose.position.y = y;
		to_pose.position.z = z;
		tf::Quaternion q = tf::createQuaternionFromRPY(roll,pitch,yaw);
		set_quaternion(to_pose, q);
		gazebo_msgs::ModelState modelstate;
		modelstate.model_name = model;
		modelstate.reference_frame = ref;
		modelstate.pose = to_pose;
		ros::ServiceClient client = _n.serviceClient<gazebo_msgs::SetModelState>("/gazebo/set_model_state");
		gazebo_msgs::SetModelState setmodelstate;
		setmodelstate.request.model_state = modelstate;
		if(client.call(setmodelstate))
		  return setmodelstate.response.success;
		else return false;

}


 bool set_model_state(double x, double y, double z,  double theta, std::string model)  {
		geometry_msgs::Pose to_pose;
		to_pose.position.x = x;
		to_pose.position.y = y;
		to_pose.position.z = z;
		tf::Quaternion q = tf::createQuaternionFromRPY(0.0,0.0,theta);
		set_quaternion(to_pose, q);
								
		gazebo_msgs::ModelState modelstate;
		modelstate.model_name = model;
		modelstate.reference_frame = (std::string) "world";
		modelstate.pose = to_pose;
		
		ros::ServiceClient client = _n.serviceClient<gazebo_msgs::SetModelState>("/gazebo/set_model_state");
		gazebo_msgs::SetModelState setmodelstate;
		setmodelstate.request.model_state = modelstate;
		if(client.call(setmodelstate))
		  return setmodelstate.response.success;
		else return false;
}


void set_quaternion(geometry_msgs::Pose& pose, tf::Quaternion& q)  {
	pose.orientation.x = q[0];
	pose.orientation.y = q[1];
	pose.orientation.z = q[2];
	pose.orientation.w = q[3];
}

 std::vector<double> get_link_state(std::string link, std::string reference) {   
    std::vector<double> pos;
     ros::ServiceClient client = _n.serviceClient<gazebo_msgs::GetLinkState>("/gazebo/get_link_state");
     gazebo_msgs::GetLinkState getlinkstate;
     getlinkstate.request.link_name = link;
     getlinkstate.request.reference_frame = reference;
     if(client.call(getlinkstate)) {
       tf::Quaternion q(getlinkstate.response.link_state.pose.orientation.x,getlinkstate.response.link_state.pose.orientation.y, 
			getlinkstate.response.link_state.pose.orientation.z, getlinkstate.response.link_state.pose.orientation.w);
	tf::Matrix3x3 m(q);
	double roll, pitch, yaw;
	m.getRPY(roll, pitch, yaw); 
	pos.push_back(getlinkstate.response.link_state.pose.position.x);
	pos.push_back(getlinkstate.response.link_state.pose.position.y);
	pos.push_back(yaw);
	       
     }
          
     return pos;  
     
}

 std::vector<double> get_link_state3(std::string link, std::string reference) {   
    std::vector<double> pos;
     ros::ServiceClient client = _n.serviceClient<gazebo_msgs::GetLinkState>("/gazebo/get_link_state");
     gazebo_msgs::GetLinkState getlinkstate;
     getlinkstate.request.link_name = link;
     getlinkstate.request.reference_frame = reference;
     if(client.call(getlinkstate)) {
       tf::Quaternion q(getlinkstate.response.link_state.pose.orientation.x,getlinkstate.response.link_state.pose.orientation.y, 
			getlinkstate.response.link_state.pose.orientation.z, getlinkstate.response.link_state.pose.orientation.w);
	tf::Matrix3x3 m(q);
	double roll, pitch, yaw;
	m.getRPY(roll, pitch, yaw); 
	pos.push_back(getlinkstate.response.link_state.pose.position.x);
	pos.push_back(getlinkstate.response.link_state.pose.position.y);
	pos.push_back(getlinkstate.response.link_state.pose.position.z);
	pos.push_back(yaw);
	       
     }
          
     return pos;  
     
}


//Set the end-effector to a pose relative to the robot position
 void set_end_effector_pose(  geometry_msgs::Pose pose , moveit::planning_interface::MoveGroup& group) {
 
  group.setPoseTarget(pose);
  group.move();
 
}



void set_arm_joints(const Configuration& c, unsigned arm) {
  
  string j0 = boost::lexical_cast<std::string>(c[0]);
  string j1 = boost::lexical_cast<std::string>(c[1]);
  string j2 = boost::lexical_cast<std::string>(c[2]);
  string j3 = boost::lexical_cast<std::string>(c[3]);
  string j4 = boost::lexical_cast<std::string>(c[4]);
  string j5 = boost::lexical_cast<std::string>(c[5]);
  string j6 = boost::lexical_cast<std::string>(c[6]);

  string command = "";
  if(arm == 0)
     command = "rostopic pub -1 /r_arm_controller/command trajectory_msgs/JointTrajectory '{header: {stamp: {secs: 0,nsecs: 0}} ,joint_names: ['r_shoulder_pan_joint', 'r_shoulder_lift_joint', 'r_upper_arm_roll_joint', 'r_elbow_flex_joint', 'r_forearm_roll_joint', 'r_wrist_flex_joint', 'r_wrist_roll_joint'], points: [{positions:  ["+j0+","+ j1+","+ j2 +","+ j3 +","+ j4 +","+j5+","+ j6+"], velocities: [], accelerations: [], time_from_start: {secs: 1, nsecs: 0}}]}'";
   else 
     command = "rostopic pub -1 /l_arm_controller/command trajectory_msgs/JointTrajectory '{header: {stamp: {secs: 0,nsecs: 0}} ,joint_names: ['l_shoulder_pan_joint', 'l_shoulder_lift_joint', 'l_upper_arm_roll_joint', 'l_elbow_flex_joint', 'l_forearm_roll_joint', 'l_wrist_flex_joint', 'l_wrist_roll_joint'], points: [{positions:  ["+j0+","+ j1+","+ j2 +","+ j3 +","+ j4 +","+j5+","+ j6+"], velocities: [], accelerations: [], time_from_start: {secs: 1, nsecs: 0}}]}'";

  system(command.c_str());
  
}


bool pause_physics() {
 
   ros::ServiceClient client = _n.serviceClient<std_srvs::Empty>("/gazebo/pause_physics");
   std_srvs::Empty e;
   client.call(e);
}


bool unpause_physics() {
     ros::ServiceClient client = _n.serviceClient<std_srvs::Empty>("/gazebo/unpause_physics");
     std_srvs::Empty e;
      client.call(e);
}


void set_arm_joints(const vector<double>& values, unsigned arm) {
  
  string j0 = boost::lexical_cast<std::string>(values[0]);
  string j1 = boost::lexical_cast<std::string>(values[1]);
  string j2 = boost::lexical_cast<std::string>(values[2]);
  string j3 = boost::lexical_cast<std::string>(values[3]);
  string j4 = boost::lexical_cast<std::string>(values[4]);
  string j5 = boost::lexical_cast<std::string>(values[5]);
  string j6 = boost::lexical_cast<std::string>(values[6]);

  string command = "";
  if(arm == 0)
     command = "rostopic pub -1 /r_arm_controller/command trajectory_msgs/JointTrajectory '{header: {stamp: {secs: 0,nsecs: 0}} ,joint_names: ['r_shoulder_pan_joint', 'r_shoulder_lift_joint', 'r_upper_arm_roll_joint', 'r_elbow_flex_joint', 'r_forearm_roll_joint', 'r_wrist_flex_joint', 'r_wrist_roll_joint'], points: [{positions:  ["+j0+","+ j1+","+ j2 +","+ j3 +","+ j4 +","+j5+","+ j6+"], velocities: [], accelerations: [], time_from_start: {secs: 1, nsecs: 0}}]}'";
   else 
     command = "rostopic pub -1 /l_arm_controller/command trajectory_msgs/JointTrajectory '{header: {stamp: {secs: 0,nsecs: 0}} ,joint_names: ['l_shoulder_pan_joint', 'l_shoulder_lift_joint', 'l_upper_arm_roll_joint', 'l_elbow_flex_joint', 'l_forearm_roll_joint', 'l_wrist_flex_joint', 'l_wrist_roll_joint'], points: [{positions:  ["+j0+","+ j1+","+ j2 +","+ j3 +","+ j4 +","+j5+","+ j6+"], velocities: [], accelerations: [], time_from_start: {secs: 1, nsecs: 0}}]}'";

  system(command.c_str());
  
}



vector<double> get_arm_joints(const vector<string>& arm_joints_names) {
  
 vector<double> arm_joints_values;
 ros::ServiceClient client = _n.serviceClient<gazebo_msgs::GetJointProperties>("/gazebo/get_joint_properties");
 gazebo_msgs::GetJointProperties getjointproperties;
 
 
 for(unsigned i = 0; i < arm_joints_names.size(); ++i) {
    getjointproperties.request.joint_name = arm_joints_names.at(i);
    if(client.call(getjointproperties)) {
	double joint = getjointproperties.response.position[0];
	arm_joints_values.push_back(joint);
    }
 }
 return arm_joints_values;

}


double get_arm_joint(const string& joint_name) {
  
  double joint_value = -1; 
  ros::ServiceClient client = _n.serviceClient<gazebo_msgs::GetJointProperties>("/gazebo/get_joint_properties");
  gazebo_msgs::GetJointProperties getjointproperties;
  getjointproperties.request.joint_name = joint_name;
  if(client.call(getjointproperties))
    joint_value = getjointproperties.response.position[0];
  
  return joint_value;

}


  void control_gripper(double position) {
    string command = "rostopic pub -1 r_gripper_controller/command pr2_controllers_msgs/Pr2GripperCommand \"{position: " + std::to_string(position) + ", max_effort: 100.0}\"";
    system(command.c_str());
  }


void control_gripper(double position, ros::Publisher& gripper_controller) {
 
  pr2_controllers_msgs::Pr2GripperCommand gc;
  gc.max_effort = 100;
  gc.position = position;
  gripper_controller.publish(gc);
  
}


int execute_trajectory(moveit_msgs::RobotTrajectory rt) {
  
 ros::ServiceClient client = _n.serviceClient<moveit_msgs::ExecuteKnownTrajectory>("/execute_kinematic_path"); 
 moveit_msgs::ExecuteKnownTrajectory et;
 et.request.trajectory = rt;
 et.request.wait_for_execution = true;
 if(client.call(et)) 
     return et.response.error_code.val;
 
}



//Add an object at configuration c on the planning scene
void dummy_table(double x, double y, string& name, planning_scene::PlanningScene& ps) {
  
 ros::Publisher planning_scene_diff_publisher = _n.advertise<moveit_msgs::PlanningScene>("planning_scene", 1);
 
 moveit_msgs::CollisionObject collision_object;
 collision_object.id = name;
 collision_object.header.frame_id = "base_footprint";
 
 vector<double> table = get_model_state(name, "pr2::base_footprint");

geometry_msgs::Pose pose;
pose.orientation.w = 1.0;
pose.position.x = table[0];
pose.position.y = table[1];
pose.position.z = 0.315;

tf::Quaternion q = tf::createQuaternionFromRPY(0.0, 0.0, table[2]);
pose.orientation.x = q[0];
pose.orientation.y = q[1];
pose.orientation.z = q[2];
pose.orientation.w = q[3];

/* Define a box to be attached */
shape_msgs::SolidPrimitive primitive;
primitive.type = primitive.BOX;
primitive.dimensions.resize(3);
primitive.dimensions[shape_msgs::SolidPrimitive::BOX_X] = 0.615;
primitive.dimensions[shape_msgs::SolidPrimitive::BOX_Y] = 1.15;
primitive.dimensions[shape_msgs::SolidPrimitive::BOX_Z] = 0.632;

collision_object.primitives.push_back(primitive);
collision_object.primitive_poses.push_back(pose);
collision_object.operation = moveit_msgs::CollisionObject::ADD;


moveit_msgs::PlanningScene planning_scene;
planning_scene.world.collision_objects.push_back(collision_object);
planning_scene.is_diff = true;
planning_scene_diff_publisher.publish(planning_scene);
ros::WallDuration(0.1).sleep();
ps.setPlanningSceneDiffMsg(planning_scene);
ros::WallDuration(0.1).sleep();

 
}


//Add an object at configuration c on the planning scene
void dummy_object(const Configuration& r, const Configuration& o, planning_scene::PlanningScene& ps) {
  
 ros::Publisher planning_scene_diff_publisher = _n.advertise<moveit_msgs::PlanningScene>("planning_scene", 1);
 
 moveit_msgs::CollisionObject collision_object;
 collision_object.id = "cylinder";
 collision_object.header.frame_id = "base_footprint";
 
  vector<double> obj = get_model_state(D3::obj, "pr2::base_footprint", false);

geometry_msgs::Pose pose;
pose.orientation.w = 1.0;
pose.position.x = obj[0];
pose.position.y = obj[1];
pose.position.z = obj[2]-0.03;

/* Define a box to be attached */
shape_msgs::SolidPrimitive primitive;
primitive.type = primitive.CYLINDER;
primitive.dimensions.resize(3);
primitive.dimensions[0] = 0.12;
primitive.dimensions[1] = 0.03;

collision_object.primitives.push_back(primitive);
collision_object.primitive_poses.push_back(pose);
collision_object.operation = moveit_msgs::CollisionObject::ADD;;

moveit_msgs::PlanningScene planning_scene;
planning_scene.world.collision_objects.push_back(collision_object);
planning_scene.is_diff = true;
planning_scene_diff_publisher.publish(planning_scene);
ros::WallDuration(0.4).sleep();
ps.setPlanningSceneDiffMsg(planning_scene);

  
}


//Add an object at configuration c on the planning scene
void dummy_object(string id, const Configuration& o, planning_scene::PlanningScene& ps) {
  
 ros::Publisher planning_scene_diff_publisher = _n.advertise<moveit_msgs::PlanningScene>("planning_scene", 1);
 
 moveit_msgs::CollisionObject collision_object;
 collision_object.id = id;
 collision_object.header.frame_id = "base_footprint";
 
 
geometry_msgs::Pose pose;
pose.orientation.w = 1.0;
pose.position.x = o[0];
pose.position.y = o[1];
pose.position.z = o[2];

/* Define a box to be attached */
shape_msgs::SolidPrimitive primitive;
primitive.type = primitive.CYLINDER;
primitive.dimensions.resize(3);
primitive.dimensions[0] = 0.15;//16//20
primitive.dimensions[1] = 0.032;

collision_object.primitives.push_back(primitive);
collision_object.primitive_poses.push_back(pose);
collision_object.operation = moveit_msgs::CollisionObject::ADD;;

moveit_msgs::PlanningScene planning_scene;
planning_scene.world.collision_objects.push_back(collision_object);
planning_scene.is_diff = true;
planning_scene_diff_publisher.publish(planning_scene);
//ros::WallDuration(0.1).sleep();
ps.setPlanningSceneDiffMsg(planning_scene);
ros::WallDuration(0.1).sleep();

  
}


void delete_objects(string id, planning_scene::PlanningScene& ps) {
   ros::Publisher planning_scene_diff_publisher = _n.advertise<moveit_msgs::PlanningScene>("planning_scene", 1);
 moveit_msgs::CollisionObject collision_object;
 collision_object.id = id;
 collision_object.header.frame_id = "base_footprint";
 moveit_msgs::CollisionObject remove_object;
remove_object.id = id;
remove_object.header.frame_id = "odom_combined";
remove_object.operation = remove_object.REMOVE;
moveit_msgs::PlanningScene planning_scene;
planning_scene.world.collision_objects.clear();
planning_scene.world.collision_objects.push_back(remove_object);
planning_scene.is_diff = true;
planning_scene_diff_publisher.publish(planning_scene);
//ros::WallDuration(0.1).sleep();
ps.setPlanningSceneDiffMsg(planning_scene);
ros::WallDuration(0.1).sleep();

}

//Add an object at configuration c on the planning scene
void dummy_object(string id, planning_scene::PlanningScene& ps) {
  
 ros::Publisher planning_scene_diff_publisher = _n.advertise<moveit_msgs::PlanningScene>("planning_scene", 1);
 
 moveit_msgs::CollisionObject collision_object;
 collision_object.id = id;
 collision_object.header.frame_id = "base_footprint";
 
 string ref = "pr2::base_footprint";
 vector<double> o = get_model_state(D3::obj, ref, false );
 cout << "Setting object at: " << o[0] << " " << o[1] << " " << o[2] << endl;
 
geometry_msgs::Pose pose;
pose.orientation.w = 1.0;
pose.position.x = o[0];
pose.position.y = o[1];
pose.position.z = o[2];

/* Define a box to be attached */
shape_msgs::SolidPrimitive primitive;
primitive.type = primitive.CYLINDER;
primitive.dimensions.resize(3);
primitive.dimensions[0] = 0.13;//15
primitive.dimensions[1] = 0.03;

collision_object.primitives.push_back(primitive);
collision_object.primitive_poses.push_back(pose);
collision_object.operation = moveit_msgs::CollisionObject::ADD;;

moveit_msgs::PlanningScene planning_scene;
planning_scene.world.collision_objects.push_back(collision_object);
planning_scene.is_diff = true;
planning_scene_diff_publisher.publish(planning_scene);
ros::WallDuration(1.5).sleep();
ps.setPlanningSceneDiffMsg(planning_scene);
ros::WallDuration(1.5).sleep();

  
}


void add_attached_object(planning_scene::PlanningScene& ps) {
  
ros::Publisher planning_scene_diff_publisher = _n.advertise<moveit_msgs::PlanningScene>("planning_scene", 1);

  
moveit_msgs::AttachedCollisionObject attached_object;
attached_object.link_name = "r_wrist_roll_link";
/* The header must contain a valid TF frame*/
attached_object.object.header.frame_id = "r_wrist_roll_link";
/* The id of the object */
attached_object.object.id = "5000";

/* A default pose */
geometry_msgs::Pose pose;
pose.orientation.w = 1.0;
pose.position.x = 0.18;
pose.position.y = 0.0;
pose.position.z = -0.1;

/* Define a box to be attached */
shape_msgs::SolidPrimitive primitive;
primitive.type = primitive.CYLINDER;
primitive.dimensions.resize(3);
primitive.dimensions[0] = 0.13;
primitive.dimensions[1] = 0.03;

attached_object.object.primitives.push_back(primitive);
attached_object.object.primitive_poses.push_back(pose);

ROS_INFO("Adding the object into the world at the location of the right wrist.");
moveit_msgs::PlanningScene planning_scene;
planning_scene.world.collision_objects.push_back(attached_object.object);
planning_scene.is_diff = true;
planning_scene_diff_publisher.publish(planning_scene);
ros::WallDuration(4.5).sleep();
ps.setPlanningSceneDiffMsg(planning_scene);
ros::WallDuration(4.5).sleep();

moveit_msgs::CollisionObject remove_object;
remove_object.id = "5000";
remove_object.header.frame_id = "odom_combined";
remove_object.operation = remove_object.REMOVE;

planning_scene.world.collision_objects.clear();
planning_scene.world.collision_objects.push_back(remove_object);
planning_scene.robot_state.attached_collision_objects.push_back(attached_object);
planning_scene.robot_state.is_diff = true;
planning_scene_diff_publisher.publish(planning_scene);
ros::WallDuration(4.5).sleep();
ps.setPlanningSceneDiffMsg(planning_scene);
ros::WallDuration(4.5).sleep();

  
  
  
}





//Add an object at configuration c on the planning scene
void add_object(string& o, planning_scene::PlanningScene& ps) {
  
  cout << "Adding " << o << endl;
 ros::Publisher planning_scene_diff_publisher = _n.advertise<moveit_msgs::PlanningScene>("planning_scene", 1);
 
 moveit_msgs::CollisionObject collision_object;
 collision_object.id = o;
 collision_object.header.frame_id = "base_footprint";
 
  vector<double> obj = get_model_state(o, "pr2::base_footprint", false);

geometry_msgs::Pose pose;
pose.orientation.w = 1.0;
pose.position.x = obj[0];
pose.position.y = obj[1];
pose.position.z = obj[2];

/* Define a box to be attached */
shape_msgs::SolidPrimitive primitive;
primitive.type = primitive.CYLINDER;
primitive.dimensions.resize(3);
primitive.dimensions[0] = 0.15;
primitive.dimensions[1] = 0.03;

collision_object.primitives.push_back(primitive);
collision_object.primitive_poses.push_back(pose);
collision_object.operation = moveit_msgs::CollisionObject::ADD;;

moveit_msgs::PlanningScene planning_scene;
planning_scene.world.collision_objects.push_back(collision_object);
planning_scene.is_diff = true;
planning_scene_diff_publisher.publish(planning_scene);
ros::WallDuration(0.5).sleep();
ps.setPlanningSceneDiffMsg(planning_scene);

  
}


void add_table(moveit::planning_interface::MoveGroup& group, string& obj) {
 
  add_table(group);
  
}

void add_table(moveit::planning_interface::MoveGroup& group) {
  
  ros::Publisher pub_co = _n.advertise<moveit_msgs::CollisionObject>("collision_object", 10);
  ros::WallDuration(0.5).sleep();
  
  moveit_msgs::CollisionObject co;
  co.header.stamp = ros::Time::now();
  co.header.frame_id = "base_footprint";
  
 // remove table
  co.id = "vtable";
  co.operation = moveit_msgs::CollisionObject::REMOVE;
  pub_co.publish(co);

  vector<double> table = get_model_state("table0", "pr2::base_footprint");
  
  // add table
  co.operation = moveit_msgs::CollisionObject::ADD;
  co.primitives.resize(1);
  co.primitives[0].type = shape_msgs::SolidPrimitive::BOX;
  co.primitives[0].dimensions.resize(shape_tools::SolidPrimitiveDimCount<shape_msgs::SolidPrimitive::BOX>::value);
  co.primitives[0].dimensions[shape_msgs::SolidPrimitive::BOX_X] = 0.62;
  co.primitives[0].dimensions[shape_msgs::SolidPrimitive::BOX_Y] = 1.4;
  co.primitives[0].dimensions[shape_msgs::SolidPrimitive::BOX_Z] = 0.64;
  co.primitive_poses.resize(1);
  co.primitive_poses[0].position.x = table[0];
  co.primitive_poses[0].position.y = table[1];
  co.primitive_poses[0].position.z = 0.3175;


  pub_co.publish(co);

  // wait a bit for ros things to initialize
  ros::WallDuration(0.4).sleep();
  
  group.setSupportSurfaceName("vtable");
 
    
}


void remove_collision_object(string object) {
  
  ros::Publisher pub_co = _n.advertise<moveit_msgs::CollisionObject>("collision_object", 10);
  ros::WallDuration(0.5).sleep();
  
  moveit_msgs::CollisionObject co;
  co.header.stamp = ros::Time::now();
  co.header.frame_id = "base_footprint";
  
 // remove table
  co.id = object;
  co.operation = moveit_msgs::CollisionObject::REMOVE;
  pub_co.publish(co);
  ros::WallDuration(0.1).sleep();

  
  
}

vector<double> mid_between_gripper() {
	string ref = "world";
	vector<double> rfinger = get_link_state(D3::rgripper, ref);
	usleep(10000);
	vector<double> lfinger = get_link_state(D3::lgripper, ref);
	usleep(10000);
	double x = (lfinger[0] + rfinger[0])/2;
	double y = (lfinger[1] + rfinger[1])/2;
	vector<double> mid = {x, y};
	return mid;

}

vector<double> mid_between_gripper_3() {
	string ref = "world";
	vector<double> rfinger = get_link_state3(D3::rgripper, ref);
	vector<double> lfinger = get_link_state3(D3::lgripper, ref);
	double x = (lfinger[0] + rfinger[0])/2;
	double y = (lfinger[1] + rfinger[1])/2;
	double z = (lfinger[2] + rfinger[2])/2;
	vector<double> mid = {x, y, z};
	return mid;

}


void attach(string& model1, string& link1, string& model2, string& link2) {
 
  cout << "Attaching links " << model1  << " " << link1 << " " <<  model2 << " " << link2 << endl;
   ros::ServiceClient attach_pub = _n.serviceClient<gazebo_ros_link_attacher::Attach>("/link_attacher_node/attach");
   gazebo_ros_link_attacher::Attach attach;
   attach.request.model_name_1 = model1;
   attach.request.link_name_1 = link1;
   attach.request.model_name_2 = model2;
   attach.request.link_name_2 = link2;
   attach_pub.call(attach);
  
}



void dettach(string& model1, string& link1, string& model2, string& link2) {
  
      cout << "Dettaching links " << model1  << " " << link1 << " " <<  model2 << " " << link2 << endl;
      ros::ServiceClient dettach_pub = _n.serviceClient<gazebo_ros_link_attacher::Attach>("/link_attacher_node/detach");
   
      gazebo_ros_link_attacher::Attach dettach;
      dettach.request.model_name_1 = model1;
      dettach.request.link_name_1 = link1;
      dettach.request.model_name_2 = model2;
      dettach.request.link_name_2 = link2;
      dettach_pub.call(dettach);


}


void dettach2(string& model1, string& link1, string& model2, string& link2) {


string command = "rosservice call /link_attacher_node/detach \"model_name_1: '"+ model1 +"\n' link_name_1: '"+link1+"\n' model_name_2: '"+model2+"\n' link_name_2: '"+link2+"'\"";
system(command.c_str());


}








  
  
  
  
  
  
  
  
  
};
