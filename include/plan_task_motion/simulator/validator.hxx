#pragma once
#include<types.hxx>
#include<fstream>
#include <boost/algorithm/string.hpp>
#include<types.hxx>
#include<configuration.hxx>
#include <simulator/simulator.hxx>
#include <utils/serializer.hxx>
#include<moveit/robot_model_loader/robot_model_loader.h>
#include <moveit/move_group_interface/move_group.h>
#include <tf/transform_listener.h>




using namespace std;


class Validator {
  
protected:
  
 Graph _base_graph;
 Graph _arm_graph;
 map<int, Trajectory> _arm_motion_plans;
 map<int, Trajectory> _base_motion_plans;
 map<unsigned, vector<double>> _bconfs;
 map<unsigned, vector<double>> _aconfs;
 map<unsigned, vector<double>> _oconfs;
 Plan _plan;
 ros::NodeHandle _n;
 Simulator* _sim;
 
 moveit::planning_interface::PlanningSceneInterface planning_scene_interface; 
 planning_scene::PlanningScene* _planning_scene;
 
  ros::Publisher _arm_controller;
  ros::Publisher _gripper_controller;
  string _holding;
  
  double _ja = 0.11;

 

  
public:
  
  
  Validator(ros::NodeHandle n): _n(n) {
    _sim = new Simulator(_n);
    usleep(500000);
    _sim->set_model_state(0.0, 0.0, 0.0, D3::robot);
    _holding = "";
    robot_model_loader::RobotModelLoader robot_model_loader("robot_description");
    robot_model::RobotModelPtr kinematic_model = robot_model_loader.getModel();
    _planning_scene = new planning_scene::PlanningScene(kinematic_model);
    _arm_controller = _n.advertise<trajectory_msgs::JointTrajectory>("/r_arm_controller/command", 100);
    _gripper_controller = _n.advertise<pr2_controllers_msgs::Pr2GripperCommand>("/r_gripper_controller/command", 100);
    arms_to_resting();

  }
  
 ~Validator() {}
 
   void arms_to_resting() {
   
    //string r_init_command = "rostopic pub -1 /r_arm_controller/command trajectory_msgs/JointTrajectory '{header: {stamp: {secs: 0,nsecs: 0}} ,joint_names: ['r_shoulder_pan_joint', 'r_shoulder_lift_joint', 'r_upper_arm_roll_joint', 'r_elbow_flex_joint', 'r_forearm_roll_joint', 'r_wrist_flex_joint', 'r_wrist_roll_joint'], points: [{positions:  [-0.34924421572730996, -0.3590364814831058, 0.08996533893117822, -1.215242640873074, 9.989199714806636, -0.7977029089307964, -3.445567822950754], velocities: [], accelerations: [], time_from_start: {secs: 3, nsecs: 0}}]}'";
    string r_init_command = "rostopic pub -1 /r_arm_controller/command trajectory_msgs/JointTrajectory '{header: {stamp: {secs: 0,nsecs: 0}} ,joint_names: ['r_shoulder_pan_joint', 'r_shoulder_lift_joint', 'r_upper_arm_roll_joint', 'r_elbow_flex_joint', 'r_forearm_roll_joint', 'r_wrist_flex_joint', 'r_wrist_roll_joint'], points: [{positions:  [-0.34924421572730996, -0.09090364814831058, 0.08996533893117822, -1.215242640873074, 9.989199714806636, -1.4, -3.4], velocities: [], accelerations: [], time_from_start: {secs: 3, nsecs: 0}}]}'";
    system(r_init_command.c_str());
    string l_init_command = "rostopic pub -1 /l_arm_controller/command trajectory_msgs/JointTrajectory '{header: {stamp: {secs: 0,nsecs: 0}} ,joint_names: ['l_shoulder_pan_joint', 'l_shoulder_lift_joint', 'l_upper_arm_roll_joint', 'l_elbow_flex_joint', 'l_forearm_roll_joint', 'l_wrist_flex_joint', 'l_wrist_roll_joint'], points: [{positions: [1.6431974315459932, -0.09090364814831058, -0.0018735992040751774, -2.057017707657844, -4.8422367378059965, -0.8063503723497168, -2.8872863373370956], velocities: [], accelerations: [], time_from_start: {secs: 3, nsecs: 0}}]}'";
    system(l_init_command.c_str()); 
    //_sim->control_gripper(0.04, _gripper_controller);
    usleep(2000000);
  }
  
  
  
 
//Parse a .plan file
 void parse_plan(string& filename) {
   string line;
   ifstream ifs(filename.c_str());
   if(ifs.is_open()) {
    while ( getline (ifs,line) ) {
      std::string delimiters("()");
      std::vector<std::string> parts;
      boost::split(parts, line, boost::is_any_of(delimiters));
      _plan.push_back(make_pair(parts.at(0), parts.at(1)));
      parts.clear();
    }
   }
   ifs.close();
 }
 
 
//Perform the parsed plan on the simulator
void perform_plan() {
	string obj = "";
	for(auto& it: _plan) {
	  cout << it.first << endl;
	  if(it.first == "transition_base") {
		//Parse e, find it in the map trajectories and perform the base trajectory
		int traj = get_trajectory(std::string("e"), it.second);
		if(traj >= 10000) 
 		 move_base(traj-10000, true);
		
		else 
		  move_base(traj, false);
	  }
	  else if(it.first == "transition_arm") {
		//Parse t, find it in the map trajectories and perform the trajectory.
		int traj = get_trajectory(std::string("t"), it.second);
		if(traj >= 10000) {
		  //move_arm(traj-10000, true);
		  arms_to_resting();
		}
		else 
		  move_arm(traj, false);
		usleep(1000000);

	  }
 	  else if(it.first == "grasp-object") {
 		//Pick-up using moveit or attaching link
 		//obj = it.second;
 		grasp(it.second);
 	  }
 	  
 	  else if(it.first == "place-object") {
 		//Place using moveit or dettaching link
 		place(it.second);
 	  }
	}
 }
 
 
 unsigned get_trajectory(string delimiters, string line) {
	std::vector<std::string> parts;
	boost::split(parts, line, boost::is_any_of(delimiters));	
	return std::stoi(parts.at(1));
 }
 
 
 bool move_base(unsigned id, bool rev) {
	ROS_INFO("Execute base trajectory");
	
	if(_holding != "") {
	  cout << "Holding: " << _holding << endl; 
	  _sim->pause_physics();
	  usleep(10000);
	  _sim->dettach(D3::robot, D3::rob_attaching_link1, _holding, D3::obj_attaching_link);
	  usleep(50000);
	  _sim->set_model_state(100, 100, 0, 0, _holding);
	  usleep(50000);
	  _sim->unpause_physics();

	}
	
	Trajectory traj = _base_motion_plans[id];
	if(rev==true)
	  std::reverse(traj.begin(), traj.end());

	    for(int i = 0; i < traj.size(); ++i) {
		  vector<double> conf = traj.at(i);
		  _sim->set_model_state(conf[0], conf[1], conf[2], D3::robot);
		  usleep(12000);
	    }
	    
	    if(_holding != "") {
		//_sim->control_gripper(0.02, _gripper_controller);
		usleep(10000);
		vector<double> mid = _sim->mid_between_gripper_3();
		_sim->pause_physics();
		usleep(500000);
// 		_sim->set_model_state(mid[0], mid[1], 0, _holding, mid[2]-0.01, false);
		_sim->set_model_state(mid[0], mid[1], 0, _holding, mid[2]-0.05, false);

		usleep(50000);
		_sim->attach(D3::robot, D3::rob_attaching_link1, _holding, D3::obj_attaching_link);
		usleep(500000);
		_sim->unpause_physics();
		usleep(1500000);
	      
	    }

	return true;
 }
 


 void move_arm(unsigned id, bool rev) {
	  ROS_INFO("Execute arm trajectory");
	  Trajectory traj = _arm_motion_plans[id];
	  if(rev==true)
	    std::reverse(traj.begin(), traj.end());
	  vector<string> joint_names = {"r_shoulder_pan_joint", "r_shoulder_lift_joint", "r_upper_arm_roll_joint", "r_elbow_flex_joint", "r_forearm_roll_joint", "r_wrist_flex_joint", "r_wrist_roll_joint"};
	  trajectory_msgs::JointTrajectory jt;
	  jt.header.stamp = ros::Time::now() + ros::Duration(0.01);
	  jt.joint_names = joint_names;
	  for(int i = 0; i < traj.size(); ++i) {
	    vector<double> conf = traj.at(i);
	    trajectory_msgs::JointTrajectoryPoint jtp;
	    jtp.positions = conf;
	    jt.points.push_back(jtp);
	    jtp.time_from_start = ros::Duration(0.5);
	  }
	  _arm_controller.publish(jt);
	  usleep(3500000);
 }
 

 /*Pre grasping control
  * 
  * 1- Approach the gripper in z axis
  * 2- Approach the gripper in pitch axis
  * 
  */
 void pre_grasp(double val) {
   
   ROS_INFO("Pre-grasp");
   vector<string> joint_names = {"r_shoulder_pan_joint", "r_shoulder_lift_joint", "r_upper_arm_roll_joint", "r_elbow_flex_joint", "r_forearm_roll_joint", "r_wrist_flex_joint", "r_wrist_roll_joint"};
   vector<double> joint_values = _sim->get_arm_joints(joint_names);
   double joint1_nvalue = joint_values.at(1) + val;//Approach the gripper in z axis
   joint_values[1] = joint1_nvalue;   
   double joint2_nvalue = joint_values.at(5) - 0.1;//Appoach the gripper in pitch axis
   joint_values[5] = joint2_nvalue;
   _sim->set_arm_joints(joint_values, 0);

 }
 
 
 void post_grasp(double val) {
   ROS_INFO("Post-grasp");
   vector<string> joint_names = {"r_shoulder_pan_joint", "r_shoulder_lift_joint", "r_upper_arm_roll_joint", "r_elbow_flex_joint", "r_forearm_roll_joint", "r_wrist_flex_joint", "r_wrist_roll_joint"};
   vector<double> joint_values = _sim->get_arm_joints(joint_names);
   double joint1_nvalue = joint_values.at(1) - val;
   joint_values[1] = joint1_nvalue;
   double joint2_nvalue = joint_values.at(5) + 0.1;//Appoach the gripper in pitch axis
   joint_values[5] = joint2_nvalue;
   _sim->set_arm_joints(joint_values, 0);

 }


 void grasp(string& obj) {
	ROS_INFO("Grasping");
	pre_grasp(_ja);
	//_sim->control_gripper(0.01, _gripper_controller);
	usleep(1500000);
	_holding = obj;
	_sim->attach(D3::robot, D3::rob_attaching_link1, obj, D3::obj_attaching_link);
	usleep(1000000);

	post_grasp(_ja);
	usleep(1500000);

	
 }
 
void pre_place(string obj) {
   ROS_INFO("Pre-place");
      vector<string> joint_names = {"r_shoulder_pan_joint", "r_shoulder_lift_joint", "r_upper_arm_roll_joint", "r_elbow_flex_joint", "r_forearm_roll_joint", "r_wrist_flex_joint", "r_wrist_roll_joint"};
   vector<double> joint_values = _sim->get_arm_joints(joint_names);
   double joint1_nvalue = joint_values.at(1) + _ja;//Approach the gripper in z axis
   joint_values[1] = joint1_nvalue; 
   //double joint2_nvalue = joint_values.at(5) - 0.1;//Appoach the gripper in pitch axis
   //joint_values[5] = joint2_nvalue;
   _sim->set_arm_joints(joint_values, 0);
   _sim->dettach(D3::robot, D3::rob_attaching_link1, obj, D3::obj_attaching_link);

}

void post_place() {
  ROS_INFO("Post-place");
   vector<string> joint_names = {"r_shoulder_pan_joint", "r_shoulder_lift_joint", "r_upper_arm_roll_joint", "r_elbow_flex_joint", "r_forearm_roll_joint", "r_wrist_flex_joint", "r_wrist_roll_joint"};
   vector<double> joint_values = _sim->get_arm_joints(joint_names);
   double joint1_nvalue = joint_values.at(1) - _ja;
   joint_values[1] = joint1_nvalue;
   _sim->set_arm_joints(joint_values, 0);
  
}

 
 void place(string& obj) {
	ROS_INFO("Placing");
	pre_place(obj);
	//_sim->control_gripper(0.07, _gripper_controller);
	usleep(1500000);
	_holding = "";
	  _sim->dettach(D3::robot, D3::rob_attaching_link1, obj, D3::obj_attaching_link);
	  usleep(1000);	
	post_place();
	usleep(1500000);

 }
 
 

 void load_data(string base_graph, string arm_graph, string c_base, string c_arm, 
		   string c_obj, string base_motion, string arm_motion) {

	ROS_INFO("Loading serialized data");
	Serializer::load(base_graph, _base_graph);
	Serializer::load(arm_graph, _arm_graph);
	Serializer::load(c_base, _bconfs);
	Serializer::load(c_arm, _aconfs);
	Serializer::load(c_obj, _oconfs);
	Serializer::load(base_motion, _base_motion_plans);
	Serializer::load(arm_motion, _arm_motion_plans);

 }

 
  
 
};