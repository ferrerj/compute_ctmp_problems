#pragma once
#include <simulator/simulator.hxx>
#include <simulator/configuration_generator.hxx>
#include <sensor.hxx>
#include <environment.hxx>
#include <queue>
#include <utils/utils.hxx>
#include <fstream>
#include <types.hxx>
#include <moveit/move_group_interface/move_group.h>
#include <moveit/planning_scene_interface/planning_scene_interface.h>
#include <moveit_msgs/DisplayRobotState.h>
#include <moveit_msgs/DisplayTrajectory.h>
#include <moveit_msgs/AttachedCollisionObject.h>
#include <moveit_msgs/CollisionObject.h>
#include <shape_tools/solid_primitive_dims.h>
#include<moveit/robot_model_loader/robot_model_loader.h>
#include<moveit/planning_scene/planning_scene.h>
#include<moveit/collision_detection/collision_common.h>
#include<moveit_msgs/ApplyPlanningScene.h>

#include<ompl/geometric/planners/prm/PRM.h>





using namespace std;

class Tiago {
  
/*
 * This class generate configurations and actions between configurations due to a resolution parameter 
 * for a robot with n-DOF.
 * Since a physic simulator is used, there is no need to know about geometric specifications of the robot.
 */

protected:
  
  //Base robot configurations
  ConfigurationSet _base_confs; // (x, y, theta)
  //Arm configurations
  ConfigurationSet _arm_confs;//[j0, j1, ..., jn]
  //Object configurations
  ConfigurationSet _obj_confs; // (x, y, theta)
  //Map of base configurations
  ConfigurationMap _map_base_confs; // <id, (x, y, theta)>
  //Map of arm configurations
  ConfigurationMap _map_arm_confs; //<id, [j0, j1, ..., jn]>
  //Map of object configurations
  ConfigurationMap _map_obj_confs; //<id, (x, y, theta)>
  //Configuration base graph: 
  Graph _graph_base; //std::map<std::pair<unsigned, unsigned>, unsigned> Graph;
  //Configuration arm graph
  Graph _graph_arm; //std::map<std::pair<unsigned, unsigned>, unsigned> Graph;
  //VECTOR actions
  //All actions
  vector<Action> _all_actions;
  map<pair<unsigned, unsigned>, unsigned> _grasping_actions;
  //Init
  Init _init;
  //Goal
  Goal _goal;
  
  //OVERLAPS for state constraints
  //Robot-object overlaps
  Overlaps _rob_obj_overlaps;//<base, arm, obj>
  //Object-object overlaps
  Overlaps _obj_obj_overlaps;//<obj1, 0, obj2>
    
  map<int, MotionPlan> _arm_motion_plan;
  map<int, MotionPlan> _base_motion_plans;
    
  ros::NodeHandle _n;
  Simulator* _sim;
  
  moveit::planning_interface::PlanningSceneInterface planning_scene_interface; 
  
robot_model_loader::RobotModelLoader* _robot_model_loader2;
robot_model::RobotModelPtr _kinematic_model2;
planning_scene::PlanningScene* _planning_scene2;


  

public:
  
  Tiago() {
    _sim = new Simulator();
    set_init_goal();
    _robot_model_loader2 = new robot_model_loader::RobotModelLoader("robot_description");
    _kinematic_model2 = _robot_model_loader2->getModel();
    _planning_scene2 = new planning_scene::PlanningScene(_kinematic_model2);

  }
  
  ~Tiago() {}
  
  ConfigurationSet& get_bconfs() {return _base_confs;}
  ConfigurationSet& get_oconfs() {return _obj_confs;}
  ConfigurationSet& get_aconfs() {return _arm_confs;}

  ConfigurationMap& get_map_bconfs() {return _map_base_confs;}
  ConfigurationMap& get_map_aconfs() {return _map_arm_confs;}
  ConfigurationMap& get_map_oconfs() {return _map_obj_confs;}
  
  Graph& get_graph_base() {return _graph_base;}
  Graph& get_graph_arm() {return _graph_arm;}
  
  vector<Action>&   get_actions() {return _all_actions;}
  
  map<int, MotionPlan>& get_arm_motion_plan() {return _arm_motion_plan;}
  map<int, MotionPlan>& get_base_motion_plan() {return _base_motion_plans;}

  
  Overlaps& get_rob_obj_overlaps() {return _rob_obj_overlaps;}
  Overlaps& get_obj_obj_overlaps() {return _obj_obj_overlaps;}

  Init& init() {return _init;}
  Goal& goal(){return _goal;}

  
  void arms_to_resting() {
     //Move right arm to init posiiton
    string r_init_command = "rostopic pub -1 /r_arm_controller/command trajectory_msgs/JointTrajectory '{header: {stamp: {secs: 0,nsecs: 0}} ,joint_names: ['r_shoulder_pan_joint', 'r_shoulder_lift_joint', 'r_upper_arm_roll_joint', 'r_elbow_flex_joint', 'r_forearm_roll_joint', 'r_wrist_flex_joint', 'r_wrist_roll_joint'], points: [{positions:  [-0.34924421572730996, -0.09090364814831058, 0.08996533893117822, -1.215242640873074, 9.989199714806636, -0.5977029089307964, -3.445567822950754], velocities: [], accelerations: [], time_from_start: {secs: 3, nsecs: 0}}]}'";
    system(r_init_command.c_str());
    //Move left arm to init posiiton
    string l_init_command = "rostopic pub -1 /l_arm_controller/command trajectory_msgs/JointTrajectory '{header: {stamp: {secs: 0,nsecs: 0}} ,joint_names: ['l_shoulder_pan_joint', 'l_shoulder_lift_joint', 'l_upper_arm_roll_joint', 'l_elbow_flex_joint', 'l_forearm_roll_joint', 'l_wrist_flex_joint', 'l_wrist_roll_joint'], points: [{positions: [1.6431974315459932, -0.09090364814831058, -0.0018735992040751774, -2.057017707657844, -4.8422367378059965, -0.8063503723497168, -2.8872863373370956], velocities: [], accelerations: [], time_from_start: {secs: 3, nsecs: 0}}]}'";
    system(l_init_command.c_str());
    usleep(50000);
    
    
  }
  

  void open_gripper() {
    string command = "rostopic pub -1 r_gripper_controller/command pr2_controllers_msgs/Pr2GripperCommand \"{position: 1.0, max_effort: 100.0}\"";
    system(command.c_str());
  }


  void set_init_goal() {
    
    _sim->set_model_state(0.0, 0.0, 0.0, D3::robot);
    arms_to_resting();
    open_gripper();

    string ref = "world";

    //Initial state for robot involves base and arm 
    vector<double> rob = _sim->get_model_state(D3::robot);
    Configuration c_rob(10000, rob);
    _init.push_back(make_pair("rob", c_rob.hash()));
    //_base_confs.insert(c_rob);
    
    vector<double> o1 = _sim->get_model_state("biscuits_pack", ref, false);
    Configuration c_o1(o1);
    _init.push_back(make_pair("o1", c_o1.hash()));
    _goal.push_back(make_pair("rob", 10));
    
  }
 
 

  
 void generate_samplings() {
   
    unsigned id = 0;
    
    for(unsigned i = 0; i < D3::surfaces.size(); ++i) {
      cout << D3::surfaces.at(i) << endl;
      dummy_table(0.0, 0.0, D3::surfaces.at(i), *_planning_scene2);
     usleep(10000); 
    }
    double p = 0.9;
   
   ROS_INFO("Computing samplings");

   for(auto& it: D3::tables) {
     	 vector<double> pos = _sim->get_model_state(it.first);
	 for(double x = pos[0]-1.0; x < pos[0]+1.5; x+=mc) {
	   for(double y = pos[1]-1.0; y < pos[1]+1.5; y+=mc) {
	      for(double a = -3.1416; a < 3.1416; a+=0.261799) { 
		    
	robot_state::RobotState r_state = _planning_scene2->getCurrentState();	
		       
       	  vector<double> joints;
	  joints.push_back(x);
	  joints.push_back(y);
	  joints.push_back(a);
	  r_state.setJointGroupPositions("base", joints);
	  _planning_scene2->setCurrentState(r_state);
	  bool isColliding = _planning_scene2->isStateColliding("base");
      
      if(isColliding) 
	  cout << "Is colliding in: " << x << ", " << y  << endl;
      else {

	  //Robot base pos
	  vector<double> pos1;
	  pos1.push_back(x);
	  pos1.push_back(y);
	  pos1.push_back(a);
	  Configuration c1(id, pos1); 
    
	  double nx = cos(c1[2])*mc + c1[0];
	  double ny = sin(c1[2])*mc + c1[1];
	  //Point at mc distance of robot base
	  vector<double> pos2;
	  pos2.push_back(nx);
	  pos2.push_back(ny);
	  pos2.push_back(c1[2]);
	  Configuration c2(pos2);
	  
	  //Table pos
	 vector<double> pos3 = _sim->get_model_state(it.first);
	  Configuration c3(pos3);
	  
	  double angle = std::fabs(Utils::angle_between(c1, c2, c3));
	  if(angle <= 20.0) {
	   // if(a < 3.1416) {
	     _base_confs.insert(c1);
	     _map_base_confs.insert(make_pair(c1.id(), c1));
	     id++;
	   // }
	  }
	}
		   
		 }
	 }

   }
 

 
}
 
 _planning_scene2->removeAllCollisionObjects();

  generate_base_trajectories();
 
}


void generate_base_trajectories() {
  
   _planning_scene2->removeAllCollisionObjects();
   
    for(unsigned i = 0; i < D3::surfaces.size(); ++i) {
      dummy_table(0.0, 0.0, D3::surfaces.at(i), *_planning_scene2);
     usleep(10000); 
    }
  
    ROS_INFO("Generating base trajectories");
    moveit::planning_interface::MoveGroup group("base");

    group.setPoseReferenceFrame("odom_combined");
    group.setPlanningTime(2.0);
    robot_state::RobotStatePtr r_state = group.getCurrentState();
    group.setWorkspace(-6.0, -6.0, -6.0, 6.0, 6.0, 6.0);
    int traj_id = 0;
  
  for(auto& it_from: _base_confs) {
    for(unsigned i = 0; i < it_from.get_values().size(); ++i)
    r_state->setJointGroupPositions("base", it_from.get_values());
    group.setStartState(*r_state);
    vector<Configuration> nearest_confs = k_nearest(it_from);
    for(auto& it_to: nearest_confs) {
      group.setJointValueTarget(it_to.get_values());
      MotionPlan plan;
      bool success = group.plan(plan);
      if(success) {
	  _base_motion_plans.insert(make_pair(traj_id, plan));
	  //Graph
	  _graph_base.insert(make_pair(make_pair(it_from.id(), traj_id), it_to.id() )); 
	  _graph_base.insert(make_pair(make_pair(it_to.id(), traj_id+1000), it_from.id() )); 
	  traj_id++;      
	
      }

    }
  }
     _planning_scene2->removeAllCollisionObjects();
}


vector<Configuration> k_nearest(const Configuration& c) {
  double dist = 0.0;
  double max_dist = 3.0;
  map<int, Configuration> distances;
  vector<Configuration> k_nearest_confs;
  
  for(auto& it: _base_confs) {
    dist = Utils::distance_between(c, it);
    if((dist > 0.001) && (dist <= max_dist))
      distances.insert(make_pair(dist, it));
  }
  for(unsigned i = 0; i < kn; i++ ) {
    if( (distances.size() > 0) && (i < distances.size())) {
      auto it = distances.begin();
      std::advance(it, i);
      k_nearest_confs.push_back(it->second);
    }
  }
    
  return k_nearest_confs;
  
}


 void generate_overlaps() {
   cout << "Checking overlaps....." << endl;

   arms_to_resting();
   generate_rob_obj_overlaps();
   generate_obj_obj_overlap();
}


//Overlaps will be generated during arm transitions
void generate_rob_obj_overlaps() {

  int idx = 1;
  for(auto& b_it : _base_confs) {
    _sim->set_model_state(b_it[0], b_it[1], b_it[2], D3::robot);
    cout << idx << "/" << _base_confs.size() << endl;
    idx++;

    for(auto& g_it: _graph_arm) {//For all transitions in the graph
      if(g_it.first.second < 1000) {//Check only transitions from resting pose to other nodes
	auto it = _grasping_actions.find(make_pair(b_it.id(), g_it.second));
	if(it != _grasping_actions.end()) {
		for(auto& o_it: _obj_confs) {//For all object configurations. TODO: keep in mind different surfaces
  
	  if(Utils::distance_between(b_it[0], b_it[1], o_it[0], o_it[1]) <= 1.0) {
	    _sim->set_model_state(o_it[0], o_it[1], 0.0, D3::obj, o_it[2], false );
	    _planning_scene2->setCurrentState(_arm_motion_plan[g_it.first.second].start_state_);
	    dummy_object(b_it, o_it, *_planning_scene2);	    	    
	      bool isvalid = _planning_scene2->isPathValid(_arm_motion_plan[g_it.first.second].start_state_, 
							_arm_motion_plan[g_it.first.second].trajectory_, "right_arm");
	      
	      //If there is a collision but is a grasping pose, we count it as non-overlap
	      if(isvalid == false && isGraspable(b_it.id(), g_it.second, o_it.id())) 
		isvalid = true;
	      if(isvalid == false)
		  _rob_obj_overlaps.push_back(make_tuple(b_it.id(), g_it.second, o_it.id()));
	  }
	  _planning_scene2->removeAllCollisionObjects();
	} 
	}
      }
    }
  }

}



bool isGraspable(unsigned b_id, unsigned a_id, unsigned o_id) {
  
  auto it = _grasping_actions.find(make_pair(b_id, a_id));
  if(it != _grasping_actions.end() ) {
   if((*it).second == o_id)
     return true;
  }
  else return false;

}



//Add an object at configuration c on the planning scene
void dummy_table(double x, double y, string& name, planning_scene::PlanningScene& ps) {
  
 ros::Publisher planning_scene_diff_publisher = _n.advertise<moveit_msgs::PlanningScene>("planning_scene", 1);
 
 moveit_msgs::CollisionObject collision_object;
 collision_object.id = name;
 collision_object.header.frame_id = "base_footprint";
 
 vector<double> table = _sim->get_model_state(name, "pr2::base_footprint");

geometry_msgs::Pose pose;
pose.orientation.w = 1.0;
pose.position.x = table[0];
pose.position.y = table[1];
pose.position.z = 0.315;
tf::Quaternion q = tf::createQuaternionFromRPY(0.0, 0.0, table[2]);
pose.orientation.x = q[0];
pose.orientation.y = q[1];
pose.orientation.z = q[2];
pose.orientation.w = q[3];

/* Define a box to be attached */
shape_msgs::SolidPrimitive primitive;
primitive.type = primitive.BOX;
primitive.dimensions.resize(3);
primitive.dimensions[shape_msgs::SolidPrimitive::BOX_X] = 0.61;
primitive.dimensions[shape_msgs::SolidPrimitive::BOX_Y] = 1.1;
primitive.dimensions[shape_msgs::SolidPrimitive::BOX_Z] = 0.63;

collision_object.primitives.push_back(primitive);
collision_object.primitive_poses.push_back(pose);
collision_object.operation = moveit_msgs::CollisionObject::ADD;


moveit_msgs::PlanningScene planning_scene;
planning_scene.world.collision_objects.push_back(collision_object);
planning_scene.is_diff = true;
planning_scene_diff_publisher.publish(planning_scene);
ros::WallDuration(0.1).sleep();
ps.setPlanningSceneDiffMsg(planning_scene);
ros::WallDuration(0.1).sleep();

 
}


//Add an object at configuration c on the planning scene
void dummy_object(const Configuration& r, const Configuration& o, planning_scene::PlanningScene& ps) {
  
 ros::Publisher planning_scene_diff_publisher = _n.advertise<moveit_msgs::PlanningScene>("planning_scene", 1);
 
 moveit_msgs::CollisionObject collision_object;
 collision_object.id = "cylinder";
 collision_object.header.frame_id = "base_footprint";
 
  vector<double> obj = _sim->get_model_state(D3::obj, "pr2::base_footprint", false);

geometry_msgs::Pose pose;
pose.orientation.w = 1.0;
pose.position.x = obj[0];
pose.position.y = obj[1];
pose.position.z = obj[2]-0.03;

/* Define a box to be attached */
shape_msgs::SolidPrimitive primitive;
primitive.type = primitive.CYLINDER;
primitive.dimensions.resize(3);
primitive.dimensions[0] = 0.15;
primitive.dimensions[1] = 0.03;

collision_object.primitives.push_back(primitive);
collision_object.primitive_poses.push_back(pose);
collision_object.operation = moveit_msgs::CollisionObject::ADD;;

moveit_msgs::PlanningScene planning_scene;
planning_scene.world.collision_objects.push_back(collision_object);
planning_scene.is_diff = true;
planning_scene_diff_publisher.publish(planning_scene);
ros::WallDuration(0.2).sleep();
ps.setPlanningSceneDiffMsg(planning_scene);

  
}


 
//Generate overlaps O-O by distance.
//It is an overlap if the distance between o1 and o2 is less than the sum of both radius.
  void generate_obj_obj_overlap() {
    ROS_INFO("Computing object-object overlaps");
   string ref = "world";
   for(auto& o_it: _obj_confs) {
       _sim->set_model_state(o_it[0], o_it[1], 0.0, D3::obj, o_it[2], false);
     usleep(dT);
     for(auto& o_it2 : _obj_confs) {
      if(Utils::distance_between(o_it[0], o_it[1], o_it2[0], o_it2[1]) < 0.15) { 	 //break;
       _sim->set_model_state(o_it2[0], o_it2[1], 0.0, D3::obj2, o_it2[2], false);
       usleep(dT);
       vector<double> pos = _sim->get_model_state(D3::obj2, ref, false);
       usleep(dT);
       Configuration c_got(pos);
       if(Utils::distance_between(o_it[0], o_it[1], pos.at(0), pos.at(1)) < 0.06)
	  _obj_obj_overlaps.push_back(make_tuple(o_it.id(), 0,o_it2.id())); 
              
    }
   }
 }
}


void add_table(moveit::planning_interface::MoveGroup& group, string& obj) {
  
  ros::Publisher pub_co = _n.advertise<moveit_msgs::CollisionObject>("collision_object", 10);
  ros::WallDuration(0.5).sleep();
  
  group.setPlanningTime(2.5);

  moveit_msgs::CollisionObject co;
  co.header.stamp = ros::Time::now();
  co.header.frame_id = "base_footprint";
  
 // remove table
  co.id = "table";
  co.operation = moveit_msgs::CollisionObject::REMOVE;
  pub_co.publish(co);

  vector<double> table = _sim->get_model_state("table1", "pr2::base_footprint");
  
  // add table
  co.operation = moveit_msgs::CollisionObject::ADD;
  co.primitives.resize(1);
  co.primitives[0].type = shape_msgs::SolidPrimitive::BOX;
  co.primitives[0].dimensions.resize(shape_tools::SolidPrimitiveDimCount<shape_msgs::SolidPrimitive::BOX>::value);
  co.primitives[0].dimensions[shape_msgs::SolidPrimitive::BOX_X] = 0.6;
  co.primitives[0].dimensions[shape_msgs::SolidPrimitive::BOX_Y] = 1.0;
  co.primitives[0].dimensions[shape_msgs::SolidPrimitive::BOX_Z] = 0.63;
  co.primitive_poses.resize(1);
  co.primitive_poses[0].position.x = table[0];
  co.primitive_poses[0].position.y = table[1];
  co.primitive_poses[0].position.z = 0.315;


  pub_co.publish(co);

  // wait a bit for ros things to initialize
  ros::WallDuration(0.4).sleep();
  
  group.setSupportSurfaceName("table");
 
    
}



void generate_arm_configurations() {
  
  unsigned traj_id = 0;
  unsigned c_arm_id = 0;
  moveit::planning_interface::MoveGroup group("right_arm");
  //Add the support surface to the planning scene
  group.setEndEffectorLink("r_wrist_roll_link");
  add_table(group, D3::obj);
  //Set motion planner      
  group.setPlannerId("RRTConnectkConfigDefault");
  //group.setEndEffectorLink("r_gripper_tool_frame");
  //Init arm conf
  robot_state::RobotStatePtr start_state = group.getCurrentState();
  Configuration c_init(c_arm_id, group.getCurrentJointValues());
  _arm_confs.insert(c_init);
  _map_arm_confs.insert(make_pair(c_init.id(), c_init));
  c_arm_id++;
  
  MotionPlan aplan;
  group.plan(aplan);
  	  
  double x = 0.0;
  double y = 0.0;
  double z = D3::z_table + 0.15;//0.30 for 1.5 in pitch and roll
  double pitch = 0.0;//1.5
  double yaw = 0.0;
  double roll = 0.0;//1.5
  geometry_msgs::Pose p;
  
   //With this joint constraint we ensure the robot is not going to collide with the static elements (table)
   moveit_msgs::JointConstraint jc;
   jc.joint_name = "r_shoulder_lift_joint";
   jc.position = 0.1;
   jc.tolerance_above = 0.15;//0.2
   jc.tolerance_below = 0.15;//0.2
   moveit_msgs::Constraints test_constraints;
   test_constraints.joint_constraints.push_back(jc);
   group.setPathConstraints(test_constraints);
   
   
   group.setGoalPositionTolerance(0.01);
   group.setGoalOrientationTolerance(0.01);

  
   
   for(x = D3::minx; x <= D3::maxx; x+=D3::m_object) {
    for(y = D3::miny; y <= D3::maxy; y+=D3::m_object) {
      //for(yaw = -1.0; yaw <= 1.0; yaw+=1.0 ) {
       
     //TODO: gripper horizontal and vertical 
   /*  for(double a = 0; a < 2; a+=1.5) {
 
 	//It works only if z is more elevated (0.3)
 	pitch = a;
 	roll = a;
 	
	if(a == 1.5)
 	  z = D3::z_table + 0.30;
 	else z = D3::z_table + 0.15;*/
 
      
tf::Quaternion q = tf::createQuaternionFromRPY(roll, pitch, yaw);
      //for(unsigned i = 0; i < 8; ++i) {

	
	//Set the desired pose
	p.position.x = x;
	p.position.y = y;
	p.position.z = z;
	p.orientation.x = q[0];
	p.orientation.y = q[1];
	p.orientation.z = q[2];
	p.orientation.w = q[3];
	
	
	std::vector<geometry_msgs::Pose> waypoints;
	geometry_msgs::Pose p2 = p;
	geometry_msgs::Pose p3 = p;
	geometry_msgs::Pose p4 = p;

	
	/*
	if(i == 0) {
	  p2.position.x -= 0.15;
	  waypoints.push_back(p2);
	  waypoints.push_back(p);
	  
	}
	else if(i == 1) {
	  p2.position.z += 0.2;
	  waypoints.push_back(p2);
	  waypoints.push_back(p);
	}
	
	else if(i == 2) {
	  p2.position.x -= 0.15;
	  p2.position.y -= 0.15;
	  waypoints.push_back(p2);
	  waypoints.push_back(p);
	  
	}
	
	else if(i == 3) {
	  p2.position.x -= 0.15;
	  p2.position.y += 0.15;
	  waypoints.push_back(p2);
	  waypoints.push_back(p);
	  
	}
	
	else if(i == 4) {
	  p2.position.x -= 0.15;
	  p2.position.z += 0.15;
	  p3.position.x -= 0.1;
	  p3.position.z += 0.1;
	  waypoints.push_back(p2);
	  waypoints.push_back(p3);
	  waypoints.push_back(p);
	  
	}
	
	else if(i == 5) {
	  p2.position.z += 0.2;
	  p2.position.y -= 0.15;
	  p2.position.x -= 0.15;
	  p3.position.z += 0.1;
	  p3.position.y -= 0.1;
	  p3.position.x -= 0.1;
	  waypoints.push_back(p2);
	  waypoints.push_back(p3);
	  waypoints.push_back(p);
	  
	}
	else if(i == 6) {
	  p2.position.z += 0.2;
	  p2.position.y += 0.15;
	  p2.position.x -= 0.15;
	  p3.position.z += 0.1;
	  p3.position.y += 0.1;
	  p3.position.x -= 0.11;
	  waypoints.push_back(p2);
	  waypoints.push_back(p3);
	  waypoints.push_back(p);

	}
	
	
	else*/
	  waypoints.push_back(p);
	
	moveit_msgs::RobotTrajectory trajectory;
	double fraction = group.computeCartesianPath(waypoints, 0.01, 0.0, trajectory);
	waypoints.clear();
	
	
	if(fraction >= 0.99) {
	   // cout << "[INFO]: Motion plan to pose [" << x << " " << y << " " << z << " " << roll << " " << pitch <<  " " << yaw <<
		//				 "] is feasible with " << i << " and motion: " << traj_id << endl; 
	  cout << "[INFO]: Motion plan to pose [" << x << " " << y << " " << z << " " << roll << " " << pitch <<  " " << yaw <<
						 "] is feasible with " << endl;

						 
	  string link = "r_wrist_roll_link";
	  string ref = "pr2::base_footprint";
	vector<double> tmp = _sim->get_link_state(link, ref);
	
						 
						 
	  MotionPlan plan;
	  plan.start_state_ = aplan.start_state_;
	  plan.trajectory_ = trajectory;
	  plan.planning_time_ = 0.0;
	  
	  group.execute(plan);
	  

	  
	  int size = plan.trajectory_.joint_trajectory.points.size()-1;
	  vector<double> joint_values = plan.trajectory_.joint_trajectory.points[size].positions;

	  //New arm configuration
	  Configuration c_arm(c_arm_id, joint_values);
	  auto res = _arm_confs.insert(c_arm);
	  _map_arm_confs.insert(make_pair(c_arm.id(), c_arm));
	  
	  //A map of motion plans
	  _arm_motion_plan.insert(make_pair(traj_id, plan));
	  
	  if(res.second) {
	    _graph_arm.insert(make_pair(make_pair(c_init.id(), traj_id), c_arm.id() )); //From init/resting to new IK
	    _graph_arm.insert(make_pair(make_pair(c_arm.id(), traj_id+1000), c_init.id() )); //From new IK to init/resting
	    c_arm_id++;

	  }
	  
	  else{//If the arm configuration exists, create a new graph transition with the exiting arm configuration id
	    auto it = res.first;
	    _graph_arm.insert(make_pair(make_pair(c_init.id(), traj_id), (*it).id() )); //From init/resting to new IK
	    _graph_arm.insert(make_pair(make_pair((*it).id(), traj_id+1000), c_init.id() )); //From new IK to init/resting
	  }
	  
	  traj_id++;
	  group.setStartState(*start_state);
	}
     // }
	//}  
      //}
     }
   }
   
   cout << "Arm configurations: " << _arm_confs.size() << endl;
}





void generate_objects_configurations() {
 
  string ref = "world";
  unsigned c_obj_id = 0;

  
    for(auto& t_it: _arm_motion_plan) {      
      
     _sim->execute_trajectory(t_it.second.trajectory_);
     usleep(1000);

    for(auto& b_it : _base_confs) {

    vector<double> tmp;
    double yaw = b_it[2];
    double nx = round(cos(yaw))*m + b_it[0];
    double ny = round(sin(yaw))*m + b_it[1];
    tmp.push_back(nx);    
    tmp.push_back(ny);
    tmp.push_back(yaw);
    Configuration c2(tmp);


    for(unsigned i = 0; i < D3::surfaces.size(); ++i) {
      
      vector<double> table = _sim->get_model_state(D3::surfaces.at(i));
      Configuration t(table);
      double angle = std::fabs(Utils::angle_between(b_it, c2, t));

      if( (Utils::distance_between(b_it[0], b_it[1], t[0], t[1]) <= 1.0)  && (angle <= 30.0) ) {
    
    
	  _sim->set_model_state(b_it[0], b_it[1], b_it[2], D3::robot);
	  usleep(10000);
	  
	  //Calculate the point between the finger tips
	  vector<double> rfinger = _sim->get_link_state(D3::rgripper, ref);
	  vector<double> lfinger = _sim->get_link_state(D3::lgripper, ref);
	  vector<double> mid;
	  mid.push_back((lfinger[0] + rfinger[0])/2);
	  mid.push_back((lfinger[1] + rfinger[1])/2);
	  usleep(5000);
	  
	  //If the end-effector yields in top of the table
	  if(Utils::inside_area(mid[0], mid[1], D3::surfaces.at(i)) == true ) {
	  _sim->set_model_state(mid[0]+0.01, mid[1], (D3::obj_z), 0.0, D3::obj );
	  usleep(10000);
	  
	  //Object configuration
	  vector<double> obj_values = _sim->get_model_state(D3::obj, ref, false);//To get z instead of yaw
	  if(Utils::inside_area(obj_values[0], obj_values[1], D3::surfaces.at(i)) == true) {//if object inside table

	  
	  //We must check if the object configuration exists. If it doesn't, create new one and increment c_obj_id
	  Configuration c_obj(c_obj_id, obj_values);
	  cout << "Configuration object " << c_obj.id() << "( " << c_obj[0] << ", " << c_obj[1] << ") is inside area" << endl;

	  auto res = _obj_confs.insert(c_obj);
	  
	  if (res.second) {
	    //Generate a grasping action graspable(base, arm, obj)
	     atomic_grasping(b_it.id(), _graph_arm[make_pair(0, t_it.first)], c_obj.id());
	    _grasping_actions.insert(make_pair(make_pair(b_it.id(),  _graph_arm[make_pair(0, t_it.first)]),c_obj.id()));

	    c_obj_id++;
	  }
	  
	  else {//If the object configuration exists, create a new grasping action with the exiting object configuration id
	   auto it = res.first;
	     atomic_grasping(b_it.id(),_graph_arm[make_pair(0, t_it.first)], (*it).id());
	    _grasping_actions.insert(make_pair(make_pair(b_it.id(), _graph_arm[make_pair(0, t_it.first)]), (*it).id()));
	  }
	  }
	  _sim->set_model_state(10, 10, 0.0, D3::obj );

	  }
      }
      
      
    }


  }  
  
  }
   cout << "All actions: " << _all_actions.size() << endl;
  cout << "Grasping actions: " << _grasping_actions.size() << endl;
  
    for(unsigned i = 0; i < _all_actions.size(); ++i) {
	vector<unsigned> params = _all_actions.at(i).getParams();
	cout << "cb" << params[0] << " ,ca" << params[1] << " ,co" << params[2] << endl;
	
    }
  
  
}

 //Insert an atomic grasping (base, arm, object)
  void atomic_grasping(unsigned confb, unsigned confa, unsigned confo) {
    vector<string> parameters;
    parameters.push_back("confb");
    parameters.push_back("confa");
    parameters.push_back("confo");
    ActionSchema as("graspable", parameters);
    vector<unsigned int> params;
    params.push_back(confb);
    params.push_back(confa);
    params.push_back(confo);
    Action a(params, as);
    _all_actions.push_back(a);
 }

  
};
