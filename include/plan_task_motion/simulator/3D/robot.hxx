#pragma once
#include <simulator/simulator.hxx>
#include <environment.hxx>
#include <utils/utils.hxx>
#include <fstream>
#include <types.hxx>
#include <moveit/move_group_interface/move_group.h>
#include <moveit_msgs/DisplayRobotState.h>
#include <moveit_msgs/DisplayTrajectory.h>
#include <moveit_msgs/AttachedCollisionObject.h>
#include <moveit_msgs/CollisionObject.h>
#include<moveit/robot_model_loader/robot_model_loader.h>
#include<moveit/collision_detection/collision_common.h>

using namespace std;


class Robot {
  
  
  protected:
  
  //Base robot configurations
  ConfigurationSet _base_confs; // (x, y, theta)
  //Arm configurations
  ConfigurationSet _arm_confs;//[j0, j1, ..., jn]
  //Object configurations
  ConfigurationSet _obj_confs; // (x, y, theta)
  //Map of base configurations
  ConfigurationMap _map_base_confs; // <id, (x, y, theta)>
  //Map of arm configurations
  ConfigurationMap _map_arm_confs; //<id, [j0, j1, ..., jn]>
  //Map of object configurations
  ConfigurationMap _map_obj_confs; //<id, (x, y, theta)>
  //Configuration base graph: 
  Graph _graph_base; //std::map<std::pair<unsigned, unsigned>, unsigned> Graph;
  //Configuration arm graph
  Graph _graph_arm; //std::map<std::pair<unsigned, unsigned>, unsigned> Graph;
  //Actions
  map<pair<unsigned, unsigned>, unsigned> _graspable;
  
  //Robot-object overlaps
  Overlaps _rob_obj_overlaps;//<base, arm, obj>
  //Object-object overlaps
  Overlaps _obj_obj_overlaps;//<obj1, 0, obj2>
    
  map<int, MotionPlan> _arm_motion_plan;
  map<int, MotionPlan> _base_motion_plans;
    
  ros::NodeHandle _n;
  Simulator* _sim;
  
  moveit::planning_interface::PlanningSceneInterface planning_scene_interface; 
  planning_scene::PlanningScene* _planning_scene;
  ros::Publisher _planning_scene_diff_publisher;

  
  
  public:
  
  Robot() {
    _sim = new Simulator();
    initialize();
    robot_model_loader::RobotModelLoader robot_model_loader("robot_description");
    robot_model::RobotModelPtr kinematic_model = robot_model_loader.getModel();
    _planning_scene = new planning_scene::PlanningScene(kinematic_model);
    _planning_scene_diff_publisher = _n.advertise<moveit_msgs::PlanningScene>("planning_scene", 1);

  }
  
  ~Robot() {}
  
  ConfigurationSet& get_bconfs() {return _base_confs;}
  ConfigurationSet& get_oconfs() {return _obj_confs;}
  ConfigurationSet& get_aconfs() {return _arm_confs;}

  ConfigurationMap& get_map_bconfs() {return _map_base_confs;}
  ConfigurationMap& get_map_aconfs() {return _map_arm_confs;}
  ConfigurationMap& get_map_oconfs() {return _map_obj_confs;}
  
  Graph& get_graph_base() {return _graph_base;}
  Graph& get_graph_arm()  {return _graph_arm;}
  
  map<pair<unsigned, unsigned>, unsigned>& get_graspable() {return _graspable;}
    
  map<int, MotionPlan>& get_arm_motion_plan() 	{return _arm_motion_plan;}
  map<int, MotionPlan>& get_base_motion_plan() 	{return _base_motion_plans;}

  Overlaps& get_rob_obj_overlaps() {return _rob_obj_overlaps;}
  Overlaps& get_obj_obj_overlaps() {return _obj_obj_overlaps;}

  
  void arms_to_resting() {
    string r_init_command = "rostopic pub -1 /r_arm_controller/command trajectory_msgs/JointTrajectory '{header: {stamp: {secs: 0,nsecs: 0}} ,joint_names: ['r_shoulder_pan_joint', 'r_shoulder_lift_joint', 'r_upper_arm_roll_joint', 'r_elbow_flex_joint', 'r_forearm_roll_joint', 'r_wrist_flex_joint', 'r_wrist_roll_joint'], points: [{positions:  [-0.34924421572730996, -0.09090364814831058, 0.08996533893117822, -1.215242640873074, 9.989199714806636, -0.5977029089307964, -3.445567822950754], velocities: [], accelerations: [], time_from_start: {secs: 3, nsecs: 0}}]}'";
    system(r_init_command.c_str());
    string l_init_command = "rostopic pub -1 /l_arm_controller/command trajectory_msgs/JointTrajectory '{header: {stamp: {secs: 0,nsecs: 0}} ,joint_names: ['l_shoulder_pan_joint', 'l_shoulder_lift_joint', 'l_upper_arm_roll_joint', 'l_elbow_flex_joint', 'l_forearm_roll_joint', 'l_wrist_flex_joint', 'l_wrist_roll_joint'], points: [{positions: [1.6431974315459932, -0.09090364814831058, -0.0018735992040751774, -2.057017707657844, -4.8422367378059965, -0.8063503723497168, -2.8872863373370956], velocities: [], accelerations: [], time_from_start: {secs: 3, nsecs: 0}}]}'";
    system(l_init_command.c_str());    
  }
  
  void initialize() {
    arms_to_resting();
    //_sim->control_gripper(1.0);
    _sim->set_model_state(0.0, 0.0, 0.0, D3::robot);
    usleep(10000);


  }
  
  
  void generate_samplings() {
      
    ROS_INFO("Computing samplings");
    unsigned id = 1;
    for(unsigned i = 0; i < D3::surfaces.size(); ++i) {
      _sim->dummy_table(0.0, 0.0, D3::surfaces.at(i), *_planning_scene);
     usleep(10000); 
    }
    
    vector<double> tmp = {0,0,0};
    Configuration init(0, tmp);
    _base_confs.insert(init);
    
   for(auto& it: D3::tables) {
     	 vector<double> pos = _sim->get_model_state(it.first);
	 for(double x = pos[0]-1.1; x < pos[0]+1.6; x+=mc) {
	   for(double y = pos[1]-1.1; y < pos[1]+1.6; y+=mc) {
	      for(double a = -3.1416; a < 3.1416; a+=0.261799) { 
		    
		robot_state::RobotState r_state = _planning_scene->getCurrentState();	 		
		vector<double> joints = {x, y, a};
		r_state.setJointGroupPositions("base", joints);
		_planning_scene->setCurrentState(r_state);
		bool isColliding = _planning_scene->isStateColliding("base");
      
		if(isColliding==false) {
		  vector<double> pos1 = {x, y, a};
		  Configuration c1(id, pos1); 
	    
		  double nx = cos(c1[2])*mc + c1[0];
		  double ny = sin(c1[2])*mc + c1[1];
		  //Point at mc distance of robot base
		  vector<double> pos2 = {nx, ny, c1[2]};
		  Configuration c2(pos2);
		  
		  //Table pos
		  vector<double> pos3 = _sim->get_model_state(it.first);
		  Configuration c3(pos3);
		  if(std::fabs(Utils::angle_between(c1, c2, c3)) <= 35.0) {
		    _base_confs.insert(c1);
		    _map_base_confs.insert(make_pair(c1.id(), c1));
		    id++;
		  }
		}	   
	  }
      }
   }
}
 _planning_scene->removeAllCollisionObjects();

  generate_base_trajectories();
 
}

void generate_base_trajectories() {
  
   _planning_scene->removeAllCollisionObjects();

  
    ROS_INFO("Generating base trajectories");
    for(unsigned i = 0; i < D3::surfaces.size(); ++i) {
      _sim->dummy_table(0.0, 0.0, D3::surfaces.at(i), *_planning_scene);
     usleep(10000);
    }
    
    moveit::planning_interface::MoveGroup group("base");
    group.setPlannerId("RRTConnectkConfigDefault");
    group.setPoseReferenceFrame("odom_combined");
    group.setPlanningTime(2.0);
    group.setWorkspace(-6.0, -6.0, -6.0, 6.0, 6.0, 6.0);

    robot_state::RobotStatePtr r_state = group.getCurrentState();
    int traj_id = 0;
  
  for(auto& it_from: _base_confs) {
    r_state->setJointGroupPositions("base", it_from.get_values());
    group.setStartState(*r_state);
    vector<Configuration> nearest_confs = Utils::k_nearest(it_from, _base_confs);
    for(auto& it_to: nearest_confs) {
      group.setJointValueTarget(it_to.get_values());
      MotionPlan plan;
      bool success = group.plan(plan);
      if(success) {
	  _base_motion_plans.insert(make_pair(traj_id, plan));
	  _graph_base.insert(make_pair(make_pair(it_from.id(), traj_id), it_to.id() )); 
	  _graph_base.insert(make_pair(make_pair(it_to.id(), traj_id+10000), it_from.id() )); 
	  traj_id++;      
      }
    }
  }
     _planning_scene->removeAllCollisionObjects();
}



 void generate_overlaps() {
   cout << "Checking overlaps....." << endl;
   arms_to_resting();
   generate_rob_obj_overlaps();
   generate_obj_obj_overlap();
}

void generate_rob_obj_overlaps() {
  
  ROS_INFO("Computing robot-object overlaps");
  int idx = 1;
  for(auto& b_it : _base_confs) {
      _sim->set_model_state(b_it, D3::robot);
      cout << idx << "/" << _base_confs.size() << endl;
      idx++;
      for(auto& m_it: _arm_motion_plan) {
	unsigned c = _graph_arm[make_pair(0, m_it.first)];
	auto it = _graspable.find(make_pair(b_it.id(), c));
	if(it != _graspable.end()) {
	   for(auto& o_it: _obj_confs) {
	      if(Utils::distance_between(b_it, o_it) <= 1.0) {
		  _sim->set_model_state(o_it, D3::obj, false);
		  usleep(1000);
		  _planning_scene->setCurrentState(m_it.second.start_state_);
		  _sim->dummy_object(b_it, o_it, *_planning_scene);
		  usleep(1000);

		  
		  bool isvalid = _planning_scene->isPathValid(m_it.second.start_state_, m_it.second.trajectory_, "right_arm");
		  usleep(10000);
		  if(isvalid == false && isGraspable(b_it.id(), c, o_it.id()) == false) {
		   cout << "Overlap!" << endl;
		    _rob_obj_overlaps.push_back(make_tuple(b_it.id(), m_it.first, o_it.id()));
		      
		  }
	      }
	   }
	}
    }
  }
}




collision_detection::CollisionResult::ContactMap isPathValid( robot_trajectory::RobotTrajectory& trajectory)  {
   collision_detection::CollisionResult::ContactMap contacts;
   std::size_t n_wp = trajectory.getWayPointCount();
   for (std::size_t i = 0 ; i < n_wp ; ++i) {
     const robot_state::RobotState &st = trajectory.getWayPoint(i);
     _planning_scene->getCollidingPairs(contacts, st);
   }
   return contacts;
 }
 

bool isGraspable(unsigned b_id, unsigned a_id, unsigned o_id) {
  
  auto it = _graspable.find(make_pair(b_id, a_id));
  if(it != _graspable.end() ) {
   if((*it).second == o_id)
     return true;
  }
  else return false;
}


//Generate overlaps O-O by distance.
//It is an overlap if the distance between o1 and o2 is less than the sum of both radius.
  void generate_obj_obj_overlap() {
   ROS_INFO("Computing object-object overlaps");
   string ref = "world";
   for(auto& o_it: _obj_confs) {
      for(auto& o_it2 : _obj_confs) {
	if(Utils::distance_between(o_it, o_it2) < 0.07)
	  _obj_obj_overlaps.push_back(make_tuple(o_it.id(),0,o_it2.id())); 
   }
 }
}

void generate_arm_configurations() {
  
    moveit::planning_interface::MoveGroup group("right_arm");
    group.setPlannerId("RRTConnectkConfigDefault");
    group.setPlanningTime(10.0);
    robot_state::RobotStatePtr start_state = group.getCurrentState();    
    _sim->add_table(group, D3::obj);
    //With this joint constraint we ensure the robot is not going to collide with the static elements (table)
    moveit_msgs::JointConstraint jc;
    jc.joint_name = "r_shoulder_lift_joint";
    jc.position = 0.15;
    jc.tolerance_above = 0.1;//0.2
    jc.tolerance_below = 0.1;//0.2
    moveit_msgs::Constraints test_constraints;
    test_constraints.joint_constraints.push_back(jc);
    group.setPathConstraints(test_constraints);
    group.setGoalPositionTolerance(0.005);
    group.setGoalOrientationTolerance(0.005);
    
    MotionPlan plan;
    group.plan(plan);
    
    double roll = 0.0, pitch = 0.0, yaw = 0.0;
    geometry_msgs::Pose p1, p2, p3, p4;
    double z = D3::z_table + 0.13;
    
    int c_arm_id = 1, traj_id = 0;
    
    Configuration c_init(0, group.getCurrentJointValues());
    _arm_confs.insert(c_init);
    
    
    for(double x = D3::minx; x <= D3::maxx; x+=D3::m_object) {
      for(double y = D3::miny; y <= D3::maxy; y+=D3::m_object) {
	
	tf::Quaternion q = tf::createQuaternionFromRPY(roll, pitch, yaw);
	
	p1.position.x = x;
	p1.position.y = y;
	p1.position.z = z;
	p1.orientation.x = q[0];
	p1.orientation.y = q[1];
	p1.orientation.z = q[2];
	p1.orientation.w = q[3];
	p2 = p1;
	p3 = p1;
	p4 = p1;
	
	for(unsigned dir = 0; dir < 8; ++dir) {
	    vector<geometry_msgs::Pose> waypoints;
	   
	    //Directly
	    if(dir == 0) 
		waypoints.push_back(p1);
	      
	    //From behind
	    else if(dir == 1) {
		p2.position.x -= 0.15; 
		waypoints.push_back(p2);
		waypoints.push_back(p1);
	    }
	    //From above
	    else if(dir == 2) {
		p2.position.z +=0.15;
		waypoints.push_back(p2);
		waypoints.push_back(p1);
	    }
	    //From above and behind
	    else if(dir == 3) {
		p2.position.z += 0.2;
		p2.position.x -= 0.15;
		p3.position.z += 0.1;
		p3.position.x -= 0.05;
		waypoints.push_back(p2);
		waypoints.push_back(p3);
		waypoints.push_back(p1);  
	    }
	    
	    //From above and left sideways
	    else if(dir == 4) {
		p2.position.z += 0.15;
		p2.position.y -= 0.1;
		p3.position.z += 0.1;
		p3.position.y -= 0.05;
		waypoints.push_back(p2);
		waypoints.push_back(p3);
		waypoints.push_back(p1); 
	    }
	    //From above and right sideways
	    else if(dir == 5) {
	      	p2.position.z += 0.15;
		p2.position.y += 0.1;
		p3.position.z += 0.1;
		p3.position.y += 0.05;
		waypoints.push_back(p2);
		waypoints.push_back(p3);
		waypoints.push_back(p1);
	    }
	    //From above, behind and left sideways
	    else if(dir == 6) {
		p2.position.x -= 0.15;
		p2.position.z += 0.15;
		p2.position.y -= 0.1;
		p3.position.x -= 0.1;
		p3.position.z += 0.1;
		p3.position.y -= 0.05;
		waypoints.push_back(p2);
		waypoints.push_back(p3);
		waypoints.push_back(p1);
	    }
	    //From above, behind and right sideways
	    else {
	      
		p2.position.x -= 0.15;
		p2.position.z += 0.15;
		p2.position.y += 0.11;
		p3.position.x -= 0.1;
		p3.position.z += 0.1;
		p3.position.y += 0.05;
		waypoints.push_back(p2);
		waypoints.push_back(p3);
		waypoints.push_back(p1);
	    }
	    
	    moveit_msgs::RobotTrajectory trajectory;
	    double fraction = group.computeCartesianPath(waypoints, 0.01, 0.0, trajectory);
	    waypoints.clear();
	    
	    if(fraction >= 0.99) {
	      
		cout << "[INFO]: Motion plan to pose [" << x << " " << y << " " << z << " " << roll << " " << pitch <<  
			" " << yaw << "] is feasible with " << dir << " and motion: " << traj_id << endl; 
			
		MotionPlan mplan;
		mplan.start_state_ = plan.start_state_;
		mplan.trajectory_ = trajectory;
		mplan.planning_time_ = 0.0;
		
				group.execute(mplan);
		
		cout << group.getCurrentPose() << endl;
		
		
		geometry_msgs::PoseStamped ps = group.getCurrentPose();
				
		vector<double> tmp = {ps.pose.position.x, ps.pose.position.y, ps.pose.position.z, pitch, roll, yaw};
		Configuration c(c_arm_id, tmp);
		auto res = _arm_confs.insert(c);
		_arm_motion_plan.insert(make_pair(traj_id, mplan));
		
		if(res.second) {
			_graph_arm.insert(make_pair(make_pair(0,traj_id),c.id()));
			_graph_arm.insert(make_pair(make_pair(c.id(),traj_id+10000),0));
			c_arm_id++;
		}
		else {
			auto it = res.first;
			_graph_arm.insert(make_pair(make_pair(0,traj_id),(*it).id()));
			_graph_arm.insert(make_pair(make_pair((*it).id(),traj_id+10000),0));
		}
		traj_id++;
	    }
	    group.setStartState(*start_state);
	}
      }
    }
    arms_to_resting();
     cout << "Arm configurations: " << _arm_confs.size() << endl;
}


  void generate_object_configurations() {
    
   int c_obj_id = 0;
   
   for(auto& t_it: _arm_motion_plan) { //1
	_sim->execute_trajectory(t_it.second.trajectory_);
	usleep(100000);
	for(auto& b_it : _base_confs) {//2
		int check = check_if_graspable(b_it.id(), _graph_arm[make_pair(0,t_it.first)]);
		if(check != -1)
		  continue;
		  //break;
		for(unsigned i = 0; i < D3::surfaces.size(); ++i) {//3
			vector<double> table = _sim->get_model_state(D3::surfaces.at(i));
			Configuration t(table);
			if( Utils::distance_between(b_it, t) <= 1.1) {//4
				_sim->set_model_state(b_it[0], b_it[1], b_it[2], D3::robot);
				usleep(300000);
				//Calculate the point between the finger tips
				vector<double> mid = _sim->mid_between_gripper();
				usleep(10000);
				 if(Utils::inside_area(mid[0], mid[1], D3::surfaces.at(i)) == true ) {//5
					_sim->set_model_state(mid[0], mid[1], (D3::obj_z), 0.0, D3::obj );
					usleep(300000);
					vector<double> obj_values = _sim->get_model_state(D3::obj, string("world"), false);
					usleep(100000);
					if(Utils::inside_area(obj_values[0], obj_values[1], D3::surfaces.at(i)) == true) {//6 

						Configuration c_obj(c_obj_id, obj_values);
						cout << "Configuration object " << c_obj.id() << "( " << c_obj[0] << ", " << 
						c_obj[1] << ") is inside a table area" << endl;
						 auto res = _obj_confs.insert(c_obj);
						 
						 if(res.second) {//7
						   atomic_grasping(b_it.id(),_graph_arm[make_pair(0,t_it.first)], c_obj.id());
						   c_obj_id++; 
						 }//7
						 else {//8
						   auto it = res.first;
						    atomic_grasping(b_it.id(), _graph_arm[make_pair(0, t_it.first)], (*it).id());

						 }//8
					}//6
					_sim->set_model_state(10, 10, 0.0, D3::obj);
					usleep(10000);
				 }//5
			}//4
		}//3

	}//2
	_sim->set_model_state(-5, -5, 0.0, D3::robot);
	usleep(10000);
   }//1
}
  
  int check_if_graspable(int confb, int confa) {
   
    auto it = _graspable.find(make_pair(confb, confa));
    if(it != _graspable.end() )
      return _graspable[make_pair(confb, confa)];
    else return -1;
        
  }

 //Insert an atomic grasping (base, arm, object)
  void atomic_grasping(unsigned confb, unsigned confa, unsigned confo) {
    _graspable.insert(make_pair(make_pair(confb, confa), confo));
 }
  
  
};