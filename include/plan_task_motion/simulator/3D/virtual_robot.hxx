#pragma once
#include <simulator/simulator.hxx>
#include <environment.hxx>
#include <utils/utils.hxx>
#include <fstream>
#include <types.hxx>
#include <moveit/move_group_interface/move_group.h>
#include <moveit_msgs/DisplayRobotState.h>
#include <moveit_msgs/DisplayTrajectory.h>
#include <moveit_msgs/AttachedCollisionObject.h>
#include <moveit_msgs/CollisionObject.h>
#include<moveit/robot_model_loader/robot_model_loader.h>
#include<moveit/collision_detection/collision_common.h>
#include<moveit/robot_state/conversions.h>

using namespace std;


class VirtualRobot {
  
  
  protected:
  
  //Base robot configurations
  ConfigurationSet _base_confs; // (x, y, theta)
  //Arm configurations
  ConfigurationSet _arm_confs;//[j0, j1, ..., jn]
  //Object configurations
  ConfigurationSet _obj_confs; // (x, y, theta)
  ConfigurationSet _v_obj_confs; // (x, y, theta)
  ConfigurationSet _rel_obj_confs;

  //Map of base configurations
  ConfigurationMap _map_base_confs; // <id, (x, y, theta)>
  //Map of arm configurations
  ConfigurationMap _map_arm_confs; //<id, [j0, j1, ..., jn]>
  //Map of object configurations
  ConfigurationMap _map_obj_confs; //<id, (x, y, theta)>
  //Configuration base graph: 
  Graph _graph_base; //std::map<std::pair<unsigned, unsigned>, unsigned> Graph;
  //Configuration arm graph
  Graph _graph_arm; //std::map<std::pair<unsigned, unsigned>, unsigned> Graph;
  //Actions
  Graspable _graspable;
  VGraspable _v_graspable;
  ConfConversion _real_virtual_obj_confs;//<cb, co_real> = co_virtual
  ConfConversion _real_relative_obj_confs;//<cb, co_real> = co_relative
  

  
  //Robot-object overlaps
  Overlaps _rob_obj_overlaps;//<base, arm, obj>
  //Object-object overlaps
  Overlaps _obj_obj_overlaps;//<obj1, 0, obj2>
  VOverlaps _v_overlaps_empty;
  VOverlaps _v_overlaps_holding;
  RelOverlaps _rel_overlaps_empty;
  RelOverlaps _rel_overlaps_holding;

    
  map<int, MotionPlan> _arm_motion_plan;
  map<int, MotionPlan> _base_motion_plans;
  
    map<pair<Configuration, Configuration>, unsigned> _connections;

  
  
  map<unsigned, unsigned> _rel_virtual;
    
  ros::NodeHandle _n;
  Simulator* _sim;
  
  moveit::planning_interface::PlanningSceneInterface planning_scene_interface; 
  planning_scene::PlanningScene* _planning_scene;
  ros::Publisher _planning_scene_diff_publisher;
  robot_state::RobotStatePtr _start_state;

  
  
  public:
  
  VirtualRobot() {
    _sim = new Simulator();
    initialize();
    robot_model_loader::RobotModelLoader robot_model_loader("robot_description");
    robot_model::RobotModelPtr kinematic_model = robot_model_loader.getModel();
    _planning_scene = new planning_scene::PlanningScene(kinematic_model);
    _planning_scene_diff_publisher = _n.advertise<moveit_msgs::PlanningScene>("planning_scene", 1);
    moveit::planning_interface::MoveGroup group("right_arm");
    _start_state = group.getCurrentState();
    

  }
  
  ~VirtualRobot() {}
  
  ConfigurationSet& get_bconfs() {return _base_confs;}
  ConfigurationSet& get_oconfs() {return _obj_confs;}
  ConfigurationSet& get_aconfs() {return _arm_confs;}

  ConfigurationMap& get_map_bconfs() {return _map_base_confs;}
  ConfigurationMap& get_map_aconfs() {return _map_arm_confs;}
  ConfigurationMap& get_map_oconfs() {return _map_obj_confs;}
  
  Graph& get_graph_base() {return _graph_base;}
  Graph& get_graph_arm()  {return _graph_arm;}
  
  Graspable& get_graspable() {return _graspable;}
    
  map<int, MotionPlan>& get_arm_motion_plan() 	{return _arm_motion_plan;}
  map<int, MotionPlan>& get_base_motion_plan() 	{return _base_motion_plans;}

  Overlaps& get_rob_obj_overlaps() {return _rob_obj_overlaps;}
  Overlaps& get_obj_obj_overlaps() {return _obj_obj_overlaps;}
  
  //Virtual getters
  ConfigurationSet& get_v_oconfs() {return _v_obj_confs;}
  ConfigurationSet& get_relative_oconfs() {return _rel_obj_confs;}

  
  VGraspable& get_v_graspable() {return _v_graspable;}
  ConfConversion& get_real_virtual_conversion() {return _real_virtual_obj_confs;}//b, real = virtual
  ConfConversion& get_real_relative_conversion() {return _real_relative_obj_confs;}//b, real = relative
/*
  VOverlaps& get_virtual_overlaps_empty() {return _v_overlaps_empty;}
  VOverlaps& get_virtual_overlaps_holding() {return _v_overlaps_holding;}
  */
  RelOverlaps& get_relative_overlaps_empty() {return _rel_overlaps_empty;}
  RelOverlaps& get_relative_overlaps_holding() {return _rel_overlaps_holding;}
  
  map<unsigned, unsigned>& get_rel_virtual() {return _rel_virtual;}

  

  
  void arms_to_resting() {
    string r_init_command = "rostopic pub -1 /r_arm_controller/command trajectory_msgs/JointTrajectory '{header: {stamp: {secs: 0,nsecs: 0}} ,joint_names: ['r_shoulder_pan_joint', 'r_shoulder_lift_joint', 'r_upper_arm_roll_joint', 'r_elbow_flex_joint', 'r_forearm_roll_joint', 'r_wrist_flex_joint', 'r_wrist_roll_joint'], points: [{positions:  [-0.34924421572730996, -0.09090364814831058, 0.08996533893117822, -1.215242640873074, 9.989199714806636, -0.5977029089307964, -3.445567822950754], velocities: [], accelerations: [], time_from_start: {secs: 3, nsecs: 0}}]}'";
    system(r_init_command.c_str());
    string l_init_command = "rostopic pub -1 /l_arm_controller/command trajectory_msgs/JointTrajectory '{header: {stamp: {secs: 0,nsecs: 0}} ,joint_names: ['l_shoulder_pan_joint', 'l_shoulder_lift_joint', 'l_upper_arm_roll_joint', 'l_elbow_flex_joint', 'l_forearm_roll_joint', 'l_wrist_flex_joint', 'l_wrist_roll_joint'], points: [{positions: [1.6431974315459932, -0.09090364814831058, -0.0018735992040751774, -2.057017707657844, -4.8422367378059965, -0.8063503723497168, -2.8872863373370956], velocities: [], accelerations: [], time_from_start: {secs: 3, nsecs: 0}}]}'";
    system(l_init_command.c_str());    
  }
  
  void initialize() {
    arms_to_resting();
    _sim->control_gripper(0.5);
    _sim->set_model_state(0.00000, 0.000000, 0.000000, D3::robot);
    usleep(10000);

    
  }
  
  
  void generate_samplings() {
      
    ROS_INFO("Computing samplings");
    unsigned id = 1;
    for(unsigned i = 1; i < D3::surfaces.size(); ++i) {
      _sim->dummy_table(0.0, 0.0, D3::surfaces.at(i), *_planning_scene);
     usleep(10000); 
    }
    
    vector<double> tmp = {0,0,0};
    Configuration init(0, tmp);
    _base_confs.insert(init);
    
   for(auto& it: D3::tables) {
     	 vector<double> pos = _sim->get_model_state(it.first);
 	 for(double x = pos[0]-1.1; x < pos[0]+1.6; x+=mc) {
 	   for(double y = pos[1]-1.1; y < pos[1]+1.6; y+=mc) {
	  //for(double x = pos[0]-2.1; x < pos[0]+2.1; x+=mc) {
	  // for(double y = pos[1]-2.1; y < pos[1]+2.1; y+=mc) {
	      for(double a = -3.1416; a < 3.1416; a+=0.261799) { 
		    
		robot_state::RobotState r_state = _planning_scene->getCurrentState();	 		
		vector<double> joints = {x, y, a};
		r_state.setJointGroupPositions("base", joints);
		_planning_scene->setCurrentState(r_state);
		bool isColliding = _planning_scene->isStateColliding("base");
      
		if(isColliding==false) {
		  vector<double> pos1 = {x, y, a};
		  Configuration c1(id, pos1); 
	    
		  double nx = cos(c1[2])*mc + c1[0];
		  double ny = sin(c1[2])*mc + c1[1];
		  //Point at mc distance of robot base
		  vector<double> pos2 = {nx, ny, c1[2]};
		  Configuration c2(pos2);
		  
		  //Table pos
		  vector<double> pos3 = _sim->get_model_state(it.first);
		  Configuration c3(pos3);
		  if(std::fabs(Utils::angle_between(c1, c2, c3)) <= 35.0) {
		    
		    
		    
		    _base_confs.insert(c1);
		    _map_base_confs.insert(make_pair(c1.id(), c1));
		    id++;
		  }
		}	   
	  }
      }
   }
}
 _planning_scene->removeAllCollisionObjects();
 cout << "Bases: " << _base_confs.size() << endl;

  generate_base_trajectories();
 
}
/*
void generate_base_trajectories() {
  
   _planning_scene->removeAllCollisionObjects();

  
    ROS_INFO("Generating base trajectories");
    for(unsigned i = 1; i < D3::surfaces.size(); ++i) {
      _sim->dummy_table(0.0, 0.0, D3::surfaces.at(i), *_planning_scene);
     usleep(10000);
    }
    
    moveit::planning_interface::MoveGroup group("base");
    group.setPlannerId("RRTConnectkConfigDefault");
    group.setPoseReferenceFrame("odom_combined");
    group.setPlanningTime(2.0);
    group.setWorkspace(-6.0, -6.0, -6.0, 6.0, 6.0, 6.0);

    robot_state::RobotStatePtr r_state = group.getCurrentState();
    int traj_id = 0;
  
  for(auto& it_from: _base_confs) {
    r_state->setJointGroupPositions("base", it_from.get_values());
    group.setStartState(*r_state);
    vector<Configuration> nearest_confs = Utils::connect(it_from, _base_confs, _connections, traj_id);
   // Utils::connect(it_from, _base_confs, _connections, traj_id);
    for(auto& it_to: nearest_confs) {
      group.setJointValueTarget(it_to.get_values());
      MotionPlan plan;
      bool success = group.plan(plan);
      if(success) {
	  _base_motion_plans.insert(make_pair(traj_id, plan));
	  _graph_base.insert(make_pair(make_pair(it_from.id(), traj_id), it_to.id() )); 
	  _graph_base.insert(make_pair(make_pair(it_to.id(), traj_id+10000), it_from.id() )); 
	  traj_id++;      
      }
    }
  }
     _planning_scene->removeAllCollisionObjects();
}
*/


void generate_base_trajectories() {
  
   _planning_scene->removeAllCollisionObjects();

  
    ROS_INFO("Generating base trajectories");
    for(unsigned i = 1; i < D3::surfaces.size(); ++i) {
      _sim->dummy_table(0.0, 0.0, D3::surfaces.at(i), *_planning_scene);
     usleep(10000);
    }
    
    moveit::planning_interface::MoveGroup group("base");
    group.setPlannerId("RRTConnectkConfigDefault");
    group.setPoseReferenceFrame("odom_combined");
    group.setPlanningTime(2.0);
    group.setWorkspace(-6.0, -6.0, -6.0, 6.0, 6.0, 6.0);

    robot_state::RobotStatePtr r_state = group.getCurrentState();
    int traj_id = 0;
  
  for(auto& it: _base_confs)
        connect(it, _base_confs, _connections);
  
  for(auto& it_c: _connections) {
      r_state->setJointGroupPositions("base", it_c.first.first.get_values());
      group.setStartState(*r_state);
      group.setJointValueTarget(it_c.first.second.get_values());
      MotionPlan plan;
      bool success = group.plan(plan);
      if(success) {
	  _base_motion_plans.insert(make_pair(traj_id, plan));
	  _graph_base.insert(make_pair(make_pair(it_c.first.first.id(), traj_id), it_c.first.second.id() )); 
	  _graph_base.insert(make_pair(make_pair(it_c.first.second.id(), traj_id+10000), it_c.first.first.id() )); 
	  traj_id++;      
      }
  }
     _planning_scene->removeAllCollisionObjects();
     cout << "Base trajectories: " << _base_motion_plans.size() << endl;
     cout << "Base graph: " << _graph_base.size() << endl;
}





void connect(const Configuration& c, set<Configuration>& confs, 
	     map<pair<Configuration, Configuration>, unsigned>& connections) {
  
  double dist = 0.0;
  const double max_dist = (mc*2)-0.2;  
  
  for(auto& it: confs) {//1
	    if(c.id() == it.id())
	      continue;
	    
	    auto it2 = connections.find(make_pair(it, c));
	    if(it2 != connections.end())
	      continue;

	    dist = Utils::distance_between(c, it);
	    if(dist <= max_dist) {//2
	      if(dist <= 0.02) {//3
		  double rad_dist = std::fabs(c[2] - it[2]);
		  if(rad_dist > 0.01) {
		    connections.insert(make_pair(make_pair(c,it),0));
		  }
	      }//3
	      
	      else if(dist > 0.02) {//4
		  double r_d = std::fabs(c[2] - it[2]);
		  if(r_d <= 0.3) {
		   connections.insert(make_pair(make_pair(c,it),0));
		  }
	      }  
	    }//2 
  }//1
  
}




typedef std::set<pair<string, string>> CollisionSet;
CollisionSet isPathValid( robot_trajectory::RobotTrajectory& trajectory, unsigned traj)  {
   std::size_t n_wp = trajectory.getWayPointCount();
   std::set<pair<string, string>> result;
   for (std::size_t i = 0 ; i < n_wp ; ++i) {
     const robot_state::RobotState &st = trajectory.getWayPoint(i);  
     collision_detection::CollisionResult::ContactMap contacts;
     _planning_scene->getCollidingPairs(contacts, st);
   
     for(auto& it: contacts) 
       result.insert(make_pair(it.first.first, it.first.second));

   }
   return result;
 }


void generate_relative_rob_obj_overlaps() {
  
     _planning_scene->setCurrentState(*_start_state);
    _sim->add_attached_object(*_planning_scene);
    collision_detection::CollisionRequest req;
    
    const unsigned BATCH_SIZE = 100;
    std::vector<const Configuration*> untested;
    untested.reserve(_rel_obj_confs.size());
    for(auto& elem: _rel_obj_confs) 
      untested.push_back(&elem);
    
  for (unsigned i = 0; i < untested.size();) {
    vector<int> ids;
    unsigned j = 0;
    for (; j < BATCH_SIZE && i+j < untested.size(); ++j) {
      const Configuration& config = *(untested[i+j]);
       _sim->dummy_object(to_string(config.id()), config, *_planning_scene);
      ids.push_back(config.id());
    }
    
    for(auto& m_it: _arm_motion_plan) {

	      robot_trajectory::RobotTrajectory t(_planning_scene->getRobotModel(), "right_arm");
	      robot_state::RobotState start(_planning_scene->getCurrentState());
	      t.setRobotTrajectoryMsg(start, m_it.second.trajectory_);
	      CollisionSet contacts = isPathValid(t, m_it.first);
	      //usleep(15000);
	      for(auto& it: contacts) {
		  //cout << "*** Traj id, obj, link: " << m_it.first << " " << it.first << " " << it.second << endl;
		if( it.first == "5000")
		    _rel_overlaps_holding.insert(make_pair(m_it.first, stoi(it.second)));
		  else if(it.second == "5000")
		    _rel_overlaps_holding.insert(make_pair(m_it.first, stoi(it.first)));
		  else {
		      _rel_overlaps_empty.insert(make_pair(m_it.first, stoi(it.first)));	
		      _rel_overlaps_holding.insert(make_pair(m_it.first, stoi(it.first)));
		  }

	      }
    }

    i += j;
    cout << "Group: " << i << endl;
    for (int id:ids) 
      _sim->delete_objects(to_string(id), *_planning_scene);
    

  }
}


void generate_arm_configurations() {
  
    moveit::planning_interface::MoveGroup group("right_arm");
    group.setPlannerId("RRTConnectkConfigDefault");
    group.setPlanningTime(10.0);
    robot_state::RobotStatePtr start_state = group.getCurrentState();  
    
    _sim->add_table(group);
    //With this joint constraint we ensure the robot is not going to collide with the static elements (table)
    moveit_msgs::JointConstraint jc;
    jc.joint_name = "r_shoulder_lift_joint";
    jc.position = 0.15;
    jc.tolerance_above = 0.1;//0.2
    jc.tolerance_below = 0.1;//0.2
    moveit_msgs::Constraints test_constraints;
    test_constraints.joint_constraints.push_back(jc);
    group.setPathConstraints(test_constraints);
    group.setGoalPositionTolerance(0.005);
    group.setGoalOrientationTolerance(0.005);
    
    MotionPlan plan;
    group.plan(plan);
    
    double roll = 0.0, pitch = 0.0, yaw = 0.0;
    geometry_msgs::Pose p1, p2, p3, p4;
    double z = D3::z_table + 0.13;
    
    int c_arm_id = 1, traj_id = 1, c_obj_id = 0;
    
    Configuration c_init(0, group.getCurrentJointValues());
    _arm_confs.insert(c_init);
    
    _graph_arm.insert(make_pair(make_pair(0,0),0));
    
    vector<double> angles;
   /*
    for(double a = 1.5708; a < 3.1416; a+=D3::pose_discr) 
      angles.push_back(a);
    for(double a = -3.14168; a <= -1.5708; a+=D3::pose_discr)
      angles.push_back(a);
    */
   
    angles.push_back(2.35619);
    angles.push_back(-1.5708);
    angles.push_back(-2.35619);
    angles.push_back(3.1416);

 

    
    
    for(double x = D3::minx; x <= D3::maxx; x+=D3::m_object) {
      for(double y = D3::miny; y <= D3::maxy; y+=D3::m_object) {
		
	for(double i = 0; i < angles.size(); ++i) {
	  
	 
	    //Get the eef (x,y,yaw) which is located at some radius r from the object and oriented in yaw axis
	    vector<double> eef = Geometry::eef_yaw(x,y,D3::r, angles.at(i));

	    tf::Quaternion q = tf::createQuaternionFromRPY(roll, pitch, eef[2]);
	    p1.position.x = eef[0];
	    p1.position.y = eef[1];
	    p1.position.z = z;
	    p1.orientation.x = q[0];
	    p1.orientation.y = q[1];
	    p1.orientation.z = q[2];
	    p1.orientation.w = q[3];
	    p2 = p1;
	    p3 = p1;
	    p4 = p1;
	    
	    for(unsigned dir = 0; dir < 4; ++dir) {
		vector<geometry_msgs::Pose> waypoints;
	      
		//Directly
		if(dir == 0) 
		    waypoints.push_back(p1);
		  
		//From behind
		else if(dir == 1) {
		    p2.position.x -= 0.15; 
		    waypoints.push_back(p2);
		    waypoints.push_back(p1);
		}
		//From above
		else if(dir == 2) {
		    p2.position.z +=0.15;
		    waypoints.push_back(p2);
		    waypoints.push_back(p1);
		}
		//From above and behind
		else if(dir == 3) {
		    p2.position.z += 0.2;
		    p2.position.x -= 0.15;
		    p3.position.z += 0.1;
		    p3.position.x -= 0.05;
		    waypoints.push_back(p2);
		    waypoints.push_back(p3);
		    waypoints.push_back(p1);  
		}

		
		moveit_msgs::RobotTrajectory trajectory;
		double fraction = group.computeCartesianPath(waypoints, 0.01, 0.0, trajectory);
		waypoints.clear();
		
		if(fraction >= 0.99) {//Execute

			    
		  	vector<double> obj = {x,y, D3::obj_z};
			Configuration o(c_obj_id, obj);
			_v_obj_confs.insert(o);
		  
		    MotionPlan mplan;
		    mplan.start_state_ = plan.start_state_;
		    mplan.trajectory_ = trajectory;
		    mplan.planning_time_ = 0.0;
		    //group.execute(mplan);
	    
		    vector<double> tmp = {eef[0], eef[1], z, roll, pitch, eef[2]};
		    Configuration c(c_arm_id, tmp);
		    
		   // cout << "arm id: " << c_arm_id << ": " << eef[0] << " " << eef[1] << " " << " " << eef[2] << endl;
		    auto res = _arm_confs.insert(c);
		    _arm_motion_plan.insert(make_pair(traj_id, mplan));
// 		    _v_graspable.insert(make_pair(traj_id, c_obj_id));
		    
		    if(res.second) {
			    _graph_arm.insert(make_pair(make_pair(0,traj_id),c.id()));
			    _graph_arm.insert(make_pair(make_pair(c.id(),traj_id+10000),0));
			    _v_graspable.insert(make_pair(c_arm_id, c_obj_id));
			    c_arm_id++;
		    }
		    else {
			    auto it = res.first;
			    _graph_arm.insert(make_pair(make_pair(0,traj_id),(*it).id()));
			    _graph_arm.insert(make_pair(make_pair((*it).id(),traj_id+10000),0));
			    _v_graspable.insert(make_pair((*it).id(), c_obj_id));
		    }
		    traj_id++;
		    

		}//Execute
	    group.setStartState(*start_state);
	
	    }  
	    
	    }
	    c_obj_id++;
      }
    }
             _sim->remove_collision_object("vtable");

    //arms_to_resting();
     cout << "Arm configurations: " << _arm_confs.size() << endl;
}


  void generate_real_object_configurations() {

      int id = 1000;
      for(auto& b_it: _base_confs) {//1
	for(auto& vo_it: _v_obj_confs) {//2
	    vector<double> values = Geometry::conf_conversion(vo_it, b_it);
	    for(unsigned i = 1; i < D3::surfaces.size(); ++i) {//3
	      if(Utils::inside_area(values[0], values[1], D3::surfaces.at(i)) == true) {//4
		Configuration c(id, values);
		auto res =_obj_confs.insert(c);
		if(res.second) {
		  _real_virtual_obj_confs.insert(make_pair(make_pair(b_it.id(),id),vo_it.id()));
		  id++;
		}
		else {
		  auto it = res.first;
		  _real_virtual_obj_confs.insert(make_pair(make_pair(b_it.id(),(*it).id()),vo_it.id()));
		} 
	      }//4
	    }//3
	}//2
      }//1
   
}


void generate_relative_configurations() {
  
 
  float max_dist = 1.0;
   
  int id = 10000;
   for(auto& b_it: _base_confs) {//1
     for(auto& o_it: _obj_confs) {
      vector<double> values =  Geometry::inv_conf_conversion_rot_trans(o_it, b_it);
      values.push_back(D3::obj_z);
      Configuration rel(id, values);
      
      double dist = Utils::distance_between(b_it, o_it);
      
      if(dist <= max_dist) {
	  auto res = _rel_obj_confs.insert(rel);
	  if(res.second) {
	      _real_relative_obj_confs.insert(make_pair(make_pair(b_it.id(),o_it.id()),id));
	      id++;
	  }
	  else {
	      auto it = res.first;
	      _real_relative_obj_confs.insert(make_pair(make_pair(b_it.id(),o_it.id()),(*it).id()));
	  }
      }
     }
   }//1

   cout << "#Relative confs: " << _rel_obj_confs.size() << endl;
}
 




  
};