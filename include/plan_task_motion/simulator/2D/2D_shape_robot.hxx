#pragma once

#include <simulator/simulator.hxx>
#include <sensor.hxx>
#include <environment.hxx>
#include <queue>
#include <utils/utils.hxx>
#include <fstream>
#include <types.hxx>

using namespace std;

class ShapeRobot {
  
/*
 * This class generate configurations and actions between configurations due to a resolution parameter 
 * for a 2D-shaped ShapeRobot and a 2D shape object.
 * Since a physic simulator is used, there is no need to know about geometric specifications of the robot.
 */

protected:
  
  //Robot only configurations
  set<Configuration> _robot_confs;
  //Object only configurations
  set<Configuration> _obj_confs;
  //All unique configurations(Robot and object)
  map<unsigned, Configuration> _all_confs;
  //Configuration base graph: 
  Graph _graph; //std::map<std::pair<unsigned, int>, unsigned> Graph;
  //VECTOR actions
  //Robot rotation actions
  vector<Action> _rot_actions;
  //Robot move actions
  vector<Action> _move_actions;
  //Robot grasping actions
  vector<Action> _grasping_actions;
  //All actions
  vector<Action> _all_actions;
  //Init
  Init _init;
  //Goal
  Goal _goal;
  //OVERLAPS
  //Robot-object overlaps
  vector<pair<unsigned, unsigned>> _rob_obj_overlaps;
  //Object-object overlaps
  vector<pair<unsigned, unsigned>> _obj_obj_overlaps;
   //Robot-object non-overlaps
  vector<pair<unsigned, unsigned>> _non_rob_obj_overlaps;
  //Object-object non-overlaps
  vector<pair<unsigned, unsigned>> _non_obj_obj_overlaps;
  //Robot model
  string _model;
  
  ros::NodeHandle _n;
  Simulator* _sim;
  

public:
  
  ShapeRobot() {
    _sim = new Simulator();
    set_init_goal();
  }
  ~ShapeRobot() {}
  
  
  set<Configuration>& get_rconfs() {return _robot_confs;}
  set<Configuration>& get_oconfs() {return _obj_confs;}
  map<unsigned, Configuration>& get_confs() {return _all_confs;}
  vector<Action>&  get_actions() {return _all_actions;}
  Graph& get_graph() {return _graph;}
  Init& init() {return _init;}
  Goal& goal(){return _goal;}
  
  vector<pair<unsigned, unsigned>>& get_rob_obj_overlaps() {return _rob_obj_overlaps;}
  vector<pair<unsigned, unsigned>>& get_obj_obj_overlaps() {return _obj_obj_overlaps;}
  vector<pair<unsigned, unsigned>>& get_non_rob_obj_overlaps() {return _non_rob_obj_overlaps;}
  vector<pair<unsigned, unsigned>>& get_non_obj_obj_overlaps() {return _non_obj_obj_overlaps;}

  
  void set_init_goal() {
    
   vector<double> rob = _sim->get_model_state(_model);
   Configuration c_rob(rob);
   _init.push_back(make_pair(_model, c_rob.hash()));
   
   vector<double> o1 = _sim->get_model_state("beer_1");
   Configuration c_o1(o1);
  _init.push_back(make_pair("o1", c_o1.hash()));
  
   vector<double> o2 = _sim->get_model_state("beer_2");
   Configuration c_o2(o2);
  _init.push_back(make_pair("o2", c_o2.hash()));
  
  _goal.push_back(make_pair("rob", 10));
    
  }
  
  //By exploration
  void generate_configurations() {
    
    int id = 0;
    Configuration *c_tmp;
        
    for(double x = x_from; x < x_to; x+=m) {
     for(double y = y_from; y < y_to; y+=m) {
       
       //Generate all rotation angles per each position
      for(unsigned i = 0; i < 10; ++i) {//angles
	_sim->set_model_state(x, y, angles[i], _model);
	usleep(dT);
	vector<double> tmp1, tmp2;
	tmp1.push_back(x);
	tmp1.push_back(y);
	tmp1.push_back(angles[i]);
	Configuration c_expected(tmp1);//expected conf

	tmp2 = _sim->get_model_state(_model);
	usleep(dT);
	Configuration c_got(tmp2);//Resulting conf

	//Check if the resulting conf is the same as the expected
	if(Sensor::check_odom(c_expected, c_got)) {
	  if(i != 8) {
	    _robot_confs.insert(c_got);
	    _all_confs.insert(make_pair(c_got.hash(), c_got));
	    if(id != 0) {//generate rotation actions
	      
	      atomic_rotation(c_tmp->hash(), c_got.hash(), 1);
	      atomic_rotation(c_got.hash(), c_tmp->hash(), 2);
	      _graph.insert(make_pair(make_pair(c_tmp->hash(),1),c_got.hash() ));
	      _graph.insert(make_pair(make_pair(c_got.hash(),2),c_tmp->hash() ));
 
	    }
	    id++;
	    c_tmp = new Configuration(c_got);
	  }
	}
	else {
	  id = 0;
	  break;
	}
     }//fi angles
     id = 0;
    }
   }
   
   generate_move_actions();
   generate_obj_confs();
   
 }

 //Insert an atomic rotation
 void atomic_rotation(size_t from, size_t to, int dir) {
    vector<string> parameters;
    parameters.push_back("from");
    parameters.push_back("to");
    parameters.push_back("dir");//1- CW; 2- CCW
    ActionSchema as("rotation", parameters);
    vector<unsigned> params;
    params.push_back(Utils::convert(from));
    params.push_back(Utils::convert(to));
    params.push_back(dir);
    Action a(params, as);
    _rot_actions.push_back(a);
    _all_actions.push_back(a);
 }
 
 //Insert an atomic move
 void atomic_move(size_t from, size_t to, int dir) {
    vector<string> parameters;
    parameters.push_back("from");
    parameters.push_back("to");
    parameters.push_back("dir");//1- Forward, 2- Backward
    ActionSchema as("translation", parameters);
    vector<unsigned> params;
    params.push_back(Utils::convert(from));
    params.push_back(Utils::convert(to));
    params.push_back(dir);
    Action a(params, as);
    _move_actions.push_back(a);
    _all_actions.push_back(a);

 }
 
 //Insert an atomic grasping
  void atomic_grasping(size_t confr, size_t confo) {
    vector<string> parameters;
    parameters.push_back("confr");
    parameters.push_back("confo");
    ActionSchema as("graspable", parameters);
    vector<unsigned> params;
    params.push_back(Utils::convert(confr));
    params.push_back(Utils::convert(confo));
    Action a(params, as);
    _grasping_actions.push_back(a);
    _all_actions.push_back(a);
 }
 

 
 //Generate moving actions (F,B)
 void generate_move_actions() {
   bool is_in = false;
   for(auto& it : _robot_confs) {
    vector<double> tmp;
    double yaw = it[2];
    double nx = round(cos(yaw))*m + it[0];
    double ny = round(sin(yaw))*m + it[1];
    tmp.push_back(nx);
    tmp.push_back(ny);
    tmp.push_back(yaw);
    Configuration c(0, tmp);
    is_in = _robot_confs.find(c) != _robot_confs.end();
    auto conf_it = _robot_confs.find(c);
    if(conf_it != _robot_confs.end()) {
      atomic_move(it.hash(), (*conf_it).hash(), 1);
     _graph.insert(make_pair(make_pair(it.hash(), 3),(*conf_it).hash() ));
    }
   }
 }

 
 
 //Generate obj configurations based on graspable configurations
 int generate_obj_confs() {
   
   for(auto& c: _robot_confs) {
    vector<double> tmp, tmp2;
    double yaw = c[2];
    double nx = round(cos(yaw))*grasp_dist + c[0];
    double ny = round(sin(yaw))*grasp_dist + c[1];
    tmp.push_back(nx);
    tmp.push_back(ny);
    tmp.push_back(0.0);
    Configuration obj_expected(tmp);
    _sim->set_model_state(nx, ny, 0.0, obj_model);
    usleep(dT);
    tmp2 = _sim->get_model_state(obj_model);
    usleep(dT);
    Configuration obj_got(tmp2);//Resulting conf
    //Check if the resulting conf is the same as the expected
    if(Sensor::check_odom(obj_expected, obj_got)) {
       _obj_confs.insert(obj_got);
        _all_confs.insert(make_pair(obj_got.hash(), obj_got));
	atomic_grasping(c.hash(), obj_got.hash());
    }
   }  
 }
 
 void generate_overlaps() {
   generate_rob_obj_overlaps();
   generate_obj_obj_overlap();
}

 //Generate pairs of overlapping configurations (R-O)
 void generate_rob_obj_overlaps() {

    for(auto& r_it: _robot_confs) {
      _sim->set_model_state(r_it[0], r_it[1], r_it[2], _model);
      usleep(dT);
      for(auto& o_it: _obj_confs) {
	 if(Utils::distance_between(r_it[0], r_it[1], o_it[0], o_it[1]) < 1.5) {
	       
       Configuration c_expected(o_it);
       vector<double> pos;
       _sim->set_model_state(o_it[0], o_it[1], o_it[2], obj_model );
       usleep(dT);
       pos = _sim->get_model_state(obj_model);
       usleep(dT);
       Configuration c_got(pos);
       if(Sensor::check_odom(c_expected, c_got)==false) 
	_rob_obj_overlaps.push_back(make_pair(Utils::convert(r_it.hash()), Utils::convert(o_it.hash())));
       else 
	 _non_rob_obj_overlaps.push_back(make_pair(Utils::convert(r_it.hash()), Utils::convert(o_it.hash())));
      }
      else
	_non_rob_obj_overlaps.push_back(make_pair(Utils::convert(r_it.hash()), Utils::convert(o_it.hash())));
     }
   }

 }


//Generate overlaps O-O by distance.
//It is an overlap if the distance between o1 and o2 is less than the sum of both radius.
  void generate_obj_obj_overlap() {

   _sim->set_model_state(15, 15, 0.0, _model);//Move the robot out of the map
   for(auto& o_it: _obj_confs) {
     _sim->set_model_state(o_it[0], o_it[1], o_it[2], obj_model);
     usleep(dT);
     for(auto& o_it2 : _obj_confs) {
      if(Utils::distance_between(o_it[0], o_it[1], o_it2[0], o_it2[1]) < 1.0) { 	 //break;
       _sim->set_model_state(o_it2[0], o_it2[1], o_it2[2], obj_model2 );
       usleep(dT);
       vector<double> pos = _sim->get_model_state(obj_model2);
       usleep(dT);
       Configuration c_got(pos);
       if(Utils::distance_between(o_it[0], o_it[1], pos.at(0), pos.at(1)) < 0.1) 
	  _obj_obj_overlaps.push_back(make_pair(o_it.hash(), o_it2.hash()));
       else 	
	  _non_obj_obj_overlaps.push_back(make_pair(o_it.hash(), o_it2.hash()));  
    }
    else 
      	 _non_obj_obj_overlaps.push_back(make_pair(o_it.hash(), o_it2.hash()));  
    
   }
 }
}

void undef_entries() {
  //90 -> undef_confr
  //91 -> undef_confo
  bool found = false;
  for(auto& it: _robot_confs) {
    for(unsigned i = 1; i < 4; ++i)
       _graph.insert(make_pair(make_pair(it.hash(), i),90));//Is a map which doesn't allow repeated keys, so entries which are not definied previously have as a value the undefined conf
    
    _non_rob_obj_overlaps.push_back(make_pair(Utils::convert(it.hash()), 91)); //ov_r_o(Cr, 91)
 
       
       
    found=false;
    for(auto& it2: _all_actions){
      if( it2.getName() == "graspable") {
	if(it.hash() == it2.getParams().at(0))
	  found = true;
      }
    }
    if(found==false)
      atomic_grasping(it.hash(), 91); 
    
    
    
  }
  
  //From an undefined conf I have a transition to the undifined conf
  for(unsigned i = 1; i < 4; ++i)
   _graph.insert(make_pair(make_pair(90, i),90));
  
  atomic_grasping(90, 91);
  
  //TODO: ov_r_o(90, Cobj); ov_r_o(Cr, 91); ov_o_o(91, Cobj); ov_o_o(Cobj, 91)
  
  for(auto& it: _obj_confs) {
   
        _non_rob_obj_overlaps.push_back(make_pair(90, Utils::convert(it.hash()))); // ov_r_o(90, Cobj)
	_non_obj_obj_overlaps.push_back(make_pair(91, Utils::convert(it.hash()))); // ov_o_o(91, Cobj)
	_non_obj_obj_overlaps.push_back(make_pair(Utils::convert(it.hash()), 91)); // ov_o_o(Cobj, 91)

  }
  
  

}
 


  
};