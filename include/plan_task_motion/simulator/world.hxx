#pragma once
#include<simulator/simulator.hxx>
#include <environment.hxx>
#include<algorithm>
#include<utils/serializer.hxx>
#include <utils/utils.hxx>
#include <ctime>
#include <stdio.h>     
#include <stdlib.h>     
#include <time.h>     

#include <iostream>
#include <iomanip>
#include <string>
#include <map>
#include <random>
#include <boost/random.hpp>
#include <boost/random/normal_distribution.hpp>

class World {
  
protected:
  
  ros::NodeHandle _n;
    Simulator* _sim;

  
  
public:
  
  World(ros::NodeHandle n): _n(n){
        _sim = new Simulator();
    
  }
  
  
  
  void spawn_object(string& model, string& name, double x, double y, double z) {
   
    string spawn = "rosrun gazebo_ros spawn_model -file ~/.gazebo/models/"+ model + " -sdf -model " + name + " -x " + std::to_string(x) + " -y " + std::to_string(y) + " -z " + std::to_string(z);
    system(spawn.c_str());
    
  }
  
  
  void setup_environment() {
    
    string ref = "world";
    
    
    for(unsigned i = 0; i <= D3::num_surfaces; ++i) {
	  string surface = "table"+to_string(i);
          D3::surfaces.push_back(surface);

    }
    
    for(unsigned i = 0; i < D3::surfaces.size(); ++i) {
     
      string front_left_leg = D3::surfaces.at(i) + "::front_left_leg";
      string front_right_leg = D3::surfaces.at(i) + "::front_right_leg";
      string back_left_leg = D3::surfaces.at(i) + "::back_left_leg";
      string back_right_leg = D3::surfaces.at(i) + "::back_right_leg";
      
      vector<double> front_left_leg_link = _sim->get_link_state(front_left_leg, ref);
      vector<double> front_right_leg_link = _sim->get_link_state(front_right_leg, ref);
      vector<double> back_left_leg_link = _sim->get_link_state(back_left_leg, ref);
      vector<double> back_right_leg_link = _sim->get_link_state(back_right_leg, ref);
      
      
      vector<double> xs;
      xs.push_back(front_left_leg_link[0]);
      xs.push_back(front_right_leg_link[0]);
      xs.push_back(back_left_leg_link[0]);
      xs.push_back(back_right_leg_link[0]);

      vector<double> ys;
      ys.push_back(front_left_leg_link[1]);
      ys.push_back(front_right_leg_link[1]);
      ys.push_back(back_left_leg_link[1]);
      ys.push_back(back_right_leg_link[1]);
      
      std::sort(xs.begin(), xs.end());
      std::sort(ys.begin(), ys.end());
      vector<double> coordinates;
      coordinates.push_back(xs[0]);
      coordinates.push_back(xs[3]);
      coordinates.push_back(ys[0]);
      coordinates.push_back(ys[3]);

      D3::tables.insert(make_pair(D3::surfaces.at(i), coordinates));
      
    }

  }
  
  void check_object_configurations(string filename) {
    
   string ref = "world";
    vector<Configuration> obj_sim_confs;
    map<unsigned, vector<double>> obj_confs;
    vector<string> objects;
    vector<int> ids;
    for(unsigned i = 1; i <= num_objects; ++i) {
      string obj = "o"+std::to_string(i);
     objects.push_back(obj);
    }
    
    
    for(unsigned i = 0; i < objects.size(); ++i) {
      vector<double> pos = _sim->get_model_state(objects.at(i),ref, false);
      Configuration tmp(pos);
      obj_sim_confs.push_back(pos);
    }
    
    
    Serializer::load(filename, obj_confs);
    

    for(auto& it: obj_sim_confs) {
       double dist = 1000;
      int obj_conf_id = 1000;
     for(auto& it2: obj_confs) {
	Configuration c(it2.second);
       if(Utils::distance_between(it, c) < dist) {
	dist = Utils::distance_between(it, c);
	obj_conf_id = it2.first;
       }
     }
     ids.push_back(obj_conf_id);

    }
    
    
    
    for(unsigned i = 0; i < ids.size(); ++i) {
      cout << "(= (confo o" << i+1 << ") co" << ids.at(i) << "); -x " << obj_confs[ids.at(i)][0] << " -y " << obj_confs[ids.at(i)][1] << " -z " << 0.68 << endl;
      string o = "o"+std::to_string(i+1);
//      _sim->set_model_state(obj_confs[ids.at(i)][0], obj_confs[ids.at(i)][1], 0.0, o, obj_confs[ids.at(i)][2], false); 
           _sim->set_model_state(obj_confs[ids.at(i)][0], obj_confs[ids.at(i)][1], 0.0, o, 0.68, false); 


    }
 /*   
    string folder = "";
    cout << "Type folder to place spawner for this instance: " << endl;
    cin >> folder;
    if(folder != "")
      
      */
 string folder = "spawner";
      
      spawners(obj_confs, ids, folder);

  }
  
  
  
  
  
  /*Generate a random init taking as input:
  *
  * 1- Number of objects
  * 2- Number of tables
  * 
  */
  void generate_instance(int tables, int objects, string& obj_confs_filename, string& o_o_overlaps_filename) {
	map<unsigned, vector<double>> obj_confs;
	Overlaps o_o_overlaps;
      srand (time(NULL));
	
	//Serializer::load(obj_confs_filename, obj_confs);
	//Serializer::load(o_o_overlaps_filename, o_o_overlaps);
	
	map<unsigned, unsigned> init;





  boost::mt19937 *rng = new boost::mt19937();
  rng->seed(time(NULL));

  boost::normal_distribution<> distribution(70, 10);
  boost::variate_generator< boost::mt19937, boost::normal_distribution<> > dist(*rng, distribution);

  std::map<int, int> hist;
  for (int n = 0; n < 100000; ++n) {
    ++hist[std::round(dist())];
  }

  for (auto p : hist) {
    std::cout << std::fixed << std::setprecision(1) << std::setw(2)
              << p.first << ' ' << std::string(p.second/200, '*') << '\n';
  }

    
    
    
  }
  
  
  
  bool check_overlap_consistency() {
    
    
    
    
  }
  
  
  
  
  
  
  
  
  
  
  
void spawners(map<unsigned, vector<double>>& obj_confs, vector<int> ids, string filename) {
  
 ofstream myfile(filename);
 
if(myfile.is_open()) 
 for(unsigned i = 0; i < ids.size(); ++i)
    myfile << "rosrun gazebo_ros spawn_model -file ~/.gazebo/models/cylinder_green/cylinder_green.sdf -sdf -model o" << i+1 << " -x "  << obj_confs[ids.at(i)][0] << " -y " << obj_confs[ids.at(i)][1] << " -z " << 0.68 << endl; 
  
 myfile.close(); 
  
}
  
  
  
  
  
  
  
  
  
  
  
};