#pragma once



static  int discr = 100;
static  double m = 1.0;
static  double mc = 0.5;
static  double kn = 8;


static  std::string robot = "a_robot_big";
static  std::string obj_model = "beer_1";
static  std::string obj_model2 = "beer_2";
static  unsigned num_objects;
static  std::string reference_pos = "pr2::base_footprint";

 


namespace common {
  
static  int S = 1;
static  unsigned int dT = 15000;

//Base Workspace
static double x_from = -2.5;
static double x_to = 2.5;
static double y_from = -3.0;
static double y_to = 3.0; 

static  double min_grasp_dist = 0.3;
static  double max_grasp_dist = 0.5;
static  double grasp_dist = 0.5;
static  double angle_dist = 0.15;
  
}

namespace D2 {
  

  
}



namespace D3 {
 

static  std::vector<std::string> arm_joints;
static std::string obj = "o1";
static std::string obj2 = "o2";
static std::string table = "table";
static std::string robot = "pr2";
static std::string rgripper = "pr2::r_gripper_r_finger_tip_link";
static std::string lgripper = "pr2::r_gripper_l_finger_tip_link";
static std::string end_effector;
static std::string rob_attaching_link1 = rgripper;
static std::string rob_attaching_link2 = lgripper;
static std::string rob_attaching_link3 = "pr2::r_gripper_motor_slider_link";
static std::string rob_attaching_link4 = "pr2::r_wrist_roll_link";
static std::string obj_attaching_link = "link";

//It doesn't depends on the table. But to generate the arm configurations we use the table in front of the robot in order to avoid collisions
static double minx = 0.62;
static double miny = -0.6;
static double maxx = 0.9;
static double maxy = 0.6;

static std::map<std::string, std::vector<double>> tables;//<table, (minx, maxx, miny, maxy)>
static int num_surfaces = 3;
static std::vector<std::string> surfaces;//table1, table2....
// static double obj_z = 0.67;
static double obj_z = 0.69;//0.68;
static double obj_size = 0.15;
static double z_table = 0.63;
static double eef_z = 0.78;
static double r = 0.155;
static double pose_discr = 0.785398;
static double PI = 3.14159265359;
static  double m_object = 0.15;
  
}



