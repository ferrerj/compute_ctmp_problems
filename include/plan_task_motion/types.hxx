#pragma once
#include <vector>
#include<action.hxx>
#include<configuration.hxx>
#include <moveit/move_group_interface/move_group.h>



typedef std::map<unsigned, Configuration> ConfigurationMap;
typedef std::set<Configuration> ConfigurationSet;
typedef std::vector<std::tuple<unsigned , unsigned , unsigned >> Overlaps;
typedef std::set<std::pair<unsigned, unsigned>> VOverlaps;
typedef std::set<std::pair<unsigned, unsigned>> RelOverlaps;
typedef std::vector<std::pair<std::string, int>> Init;
typedef std::vector<std::pair<std::string, int>> Goal;
typedef std::map<std::pair<unsigned , unsigned >, unsigned > Graph; // <Conf, traj> ---> conf
typedef std::vector<std::pair<std::string,std::string>> Plan; //<action_name, parameter>
typedef std::vector<std::vector<double>> Trajectory;
typedef moveit::planning_interface::MoveGroup::Plan MotionPlan;
typedef std::map<std::pair<unsigned, unsigned>, unsigned> Graspable;
typedef std::map<unsigned, unsigned> VGraspable;
typedef std::map<std::pair<unsigned, unsigned>, unsigned> ConfConversion;
typedef std::pair<double, double> Point;

