#pragma once
#include<environment.hxx>
#include <utils/config.hxx>
#include <lib/rapidjson/document.h>



using namespace std;

class Loader {
  
protected:
  
  Config *_config;
  
  
public:
  
  Loader(string config){ 
    _config = new Config(config);
  }
  
  
  ~Loader(){}
  
  
  void load() {
    
    int type = load_problem();
    if(type == 2)
      load_robot_2D();
    else if(type == 3) 
      load_robot_3D();
    else throw std::runtime_error("S must be a 2D or 3D space");
    load_environment();
    
  }
  
  //2D or 3D
  int load_problem() {
   common::S = _config->getOption<int>("problem.S");
   common::dT = _config->getOption<int>("problem.dT");
   return common::S;
  }
  
  //Common data for 2D and 3D
  void load_robot(string field) {
    D3::robot = _config->getOption<string>(field+".robot");
    reference_pos = robot+"::"+_config->getOption<string>(field+".reference_pos");
    D3::end_effector = _config->getOption<string>(field+".end_effector");
    common::grasp_dist = _config->getOption<float>(field+".grasping_dist");
    common::angle_dist = _config->getOption<float>(field+".grasping_angle");
    pair<float, float> p = _config->as_pair<float>(field+".grasping_eval_dist");
    common::min_grasp_dist = p.first;
    common::max_grasp_dist = p.second;
   // grasping_links = _config->as_vector<string>(field+".grasping_links");
  }
  
  //load 2D robot specifications
  void load_robot_2D() {
    load_robot("2D"); 
  }
  
    
  //load 3D robot specifications
  void load_robot_3D() {
      load_robot("3D");
      D3::arm_joints = _config->as_vector<string>("robot3D.arm_joints");
      D3::z_table = _config->getOption<float>("environment.z_table");
      D3::m_object = _config->getOption<float>("environment.m_object");
  }
  
  //load environment specifications
  void load_environment() {
    common::x_from = _config->getOption<float>("environment.x_from");
    common::x_to = _config->getOption<float>("environment.x_to");
    common::y_from = _config->getOption<float>("environment.y_from");
    common::y_to = _config->getOption<float>("environment.y_to");
    discr = _config->getOption<int>("environment.discr");
    m = _config->getOption<float>("environment.m");
    
  }
  
  
  
  
  
};