#pragma once
#include <simulator/simulator.hxx>
#include <utils/utils.hxx>
#include <simulator/simulator.hxx>

class Sensor {
  
  Sensor(){}
  ~Sensor(){}
  
  
public:
  
  //Check if a robot is in the desired configuration
  static bool check_odom(Configuration& c1, Configuration& c2) {
   return c1==c2; 
  }
  
  
  static bool equal_confs(Configuration& c1, Configuration& c2) {
   double t = 0.015;
    if( fabs(c1[0]-c2[0]) < t && fabs(c1[1]-c2[1]) < t && fabs(c1[2]-c2[2]) < t  )
      return true;
    else return false;
    
  }
  
  
static int orientation(double yaw) {

   double angles[8] = {d_0, d_45, d_90, d_135, d_180, d_225, d_270, d_315  };

   double dist_mini = 10;
   int best_angle = -1;
   for( int i = 0; i < 8; i++ ){
     double dist_ac = abs( angles[ i ] - yaw );
     if( dist_ac < dist_mini ){
       dist_mini = dist_ac;
       best_angle = i;
    }
   }
   return best_angle;
 }
  
  
  
  
};