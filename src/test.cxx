#include <ros/ros.h>
#include <geometry_msgs/Twist.h>
#include <tf/transform_listener.h>
#include <utils/utils.hxx>
/*
class RobotDriver
{
private:
  //! The node handle we'll be using
  ros::NodeHandle nh_;
  //! We will be publishing to the "cmd_vel" topic to issue commands
  ros::Publisher cmd_vel_pub_;
  //! We will be listening to TF transforms as well
  tf::TransformListener listener_;

public:
  //! ROS node initialization
  RobotDriver(ros::NodeHandle &nh)
  {
    nh_ = nh;
    //set up the publisher for the cmd_vel topic
    cmd_vel_pub_ = nh_.advertise<geometry_msgs::Twist>("/base_controller/command", 1);
  }

  bool turnOdom(bool clockwise, double radians)
  {
    while(radians < 0) radians += 2*M_PI;
    while(radians > 2*M_PI) radians -= 2*M_PI;

    //wait for the listener to get the first message
    listener_.waitForTransform("odom_combined", "base_footprint", 
                               ros::Time(0), ros::Duration(5.0));
    
    //we will record transforms here
    tf::StampedTransform start_transform;
    tf::StampedTransform current_transform;

    //record the starting transform from the odometry to the base frame
    listener_.lookupTransform("odom_combined", "base_footprint", 
                              ros::Time(0), start_transform);
    
    //we will be sending commands of type "twist"
    geometry_msgs::Twist base_cmd;
    //the command will be to turn at 0.75 rad/s
    base_cmd.linear.x = base_cmd.linear.y = 0.0;
    base_cmd.angular.z = 0.75;
    if (clockwise) base_cmd.angular.z = -base_cmd.angular.z;
    
    //the axis we want to be rotating by
    tf::Vector3 desired_turn_axis(0,0,1);
    if (!clockwise) desired_turn_axis = -desired_turn_axis;
    
    ros::Rate rate(10.0);
    bool done = false;
    while (!done && nh_.ok())
    {
      //send the drive command
      cmd_vel_pub_.publish(base_cmd);
      rate.sleep();
      //get the current transform
      try
      {
        listener_.lookupTransform("odom_combined", "base_footprint", 
                                  ros::Time(0), current_transform);
      }
      catch (tf::TransformException ex)
      {
        ROS_ERROR("%s",ex.what());
        break;
      }
      tf::Transform relative_transform = 
        start_transform.inverse() * current_transform;
      tf::Vector3 actual_turn_axis = 
        relative_transform.getRotation().getAxis();
      double angle_turned = relative_transform.getRotation().getAngle();
      if ( fabs(angle_turned) < 1.0e-2) continue;

      if ( actual_turn_axis.dot( desired_turn_axis ) < 0 ) 
        angle_turned = 2 * M_PI - angle_turned;

      if (angle_turned > radians) done = true;
    }
    if (done) return true;
    return false;
  }
  
  
  //! Drive forward a specified distance based on odometry information
  bool driveForwardOdom(double distance)
  {
    //wait for the listener to get the first message
    listener_.waitForTransform("odom_combined", "base_footprint", 
                               ros::Time(0.0), ros::Duration(5.5));
    
    //we will record transforms here
    tf::StampedTransform start_transform;
    tf::StampedTransform current_transform;

    //record the starting transform from the odometry to the base frame
    listener_.lookupTransform("odom_combined", "base_footprint", 
                              ros::Time(0.0), start_transform);
    
    //we will be sending commands of type "twist"
    geometry_msgs::Twist base_cmd;
    //the command will be to go forward at 0.25 m/s
    base_cmd.linear.y = base_cmd.angular.z = 0;
    base_cmd.linear.x = 0.25;
    
    ros::Rate rate(10.0);
    bool done = false;
    while (!done && nh_.ok())
    {
      //send the drive command
      cmd_vel_pub_.publish(base_cmd);
      rate.sleep();
      //get the current transform
      try
      {
        listener_.lookupTransform("odom_combined", "odom_combined", 
                                  ros::Time(0.0), current_transform);
      }
      catch (tf::TransformException ex)
      {
        ROS_ERROR("%s",ex.what());
        break;
      }
      //see how far we've traveled
      tf::Transform relative_transform = 
        start_transform.inverse() * current_transform;
      double dist_moved = relative_transform.getOrigin().length();

      if(dist_moved > distance) done = true;
    }
    if (done) return true;
    return false;
  }
};*/

int main(int argc, char** argv)
{
  //init the ROS node
  ros::init(argc, argv, "robot_driver");
  ros::NodeHandle nh;

//   RobotDriver driver(nh);
//   driver.driveForwardOdom(0.5);
//   driver.turnOdom(true, 0.2);
  
  
 vector<double> tmp_p = {2,3};
 Configuration P(tmp_p);
 vector<double> tmp_b = {-3, -3, 1.5708};
 Configuration B(tmp_b);   
 vector<double> conv = Geometry::conf_conversion(P, B);
 Configuration objr(conv);
 cout << "Conv: " << conv[0] << " " << conv[1] << endl;
 
 vector<double> inv_conv_rot_trans = Geometry::inv_conf_conversion_rot_trans(objr, B);
 vector<double> inv_conv_trans_rot = Geometry::inv_conf_conversion_trans_rot(objr, B);
 
  cout << "Inv rot trans: " << inv_conv_rot_trans[0] << " " << inv_conv_rot_trans[1] << endl;
    cout << "Inv trans rot: " << inv_conv_trans_rot[0] << " " << inv_conv_trans_rot[1] << endl;


    cout << "Choose parameter discretization: " << endl;
    string p = "";
    cin >> p;
    
    double k = (3.1416/stoi(p));
    cout << "K poses per virtual object configuration: " <<  (stoi(p)+2) << endl;   
    cout << "Discr: " << k << endl;

    
     for(double a = 1.5708; a <= 3.1416; a+=k) 
      cout << a << endl;
    for(double a = -3.14168; a <= -1.5708; a+=k)
      cout << a << endl;

 
 
 
  
}
