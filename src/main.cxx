#include <ros/ros.h>
#include <plan_task_motion/configuration.hxx>
#include <plan_task_motion/simulator/3D/robot.hxx>
#include <plan_task_motion/simulator/3D/virtual_robot.hxx>
#include <plan_task_motion/problem_info.hxx>
#include <plan_task_motion/fstrips3d.hxx>
#include <plan_task_motion/utils/utils.hxx>
#include <time.h>
#include<plan_task_motion/utils/config.hxx>
#include<plan_task_motion/loader.hxx>
#include<simulator/world.hxx>


int main(int argc, char** argv) {
  
string pkg = "plan_task_motion";    
ros::init(argc,argv, pkg); 
  
// clock_t tStart = clock();  

ros::NodeHandle n;
ros::AsyncSpinner spinner(1);
spinner.start();

string home = System::get_home_folder();
string catkin_pkg = home+"/catkin_ws/src/"+pkg;
string benchmarks = catkin_pkg+"/benchmarks/new/";
string folder = "";
cout << "Type problem folder" << endl;
cin >> folder;
string problem_folder = benchmarks+folder;
Utils::create_problem_folders(problem_folder);

World w(n);
w.setup_environment();


clock_t tStart = clock();  


VirtualRobot robot;
robot.generate_arm_configurations();
robot.generate_samplings();
robot.generate_real_object_configurations();
robot.generate_relative_configurations();
robot.generate_relative_rob_obj_overlaps();



float time = (double)(clock() - tStart)/CLOCKS_PER_SEC;
int memmory = System::get_peak_memory_in_kb();
cout << "Time: " << time << endl;
cout << "Memmory: " << memmory << endl;

Utils::store_configurations(robot.get_bconfs(), string(problem_folder+"/info/base_confs.data"));
Utils::store_configurations(robot.get_aconfs(), string(problem_folder+"/info/arm_confs.data"));
Utils::store_configurations(robot.get_oconfs(), string(problem_folder+"/info/objects_confs.data"));
Utils::store_graph(robot.get_graph_base(), string(problem_folder+"/info/graph_base.data"));
Utils::store_graph(robot.get_graph_arm(), string(problem_folder+"/info/graph_arm.data"));
Utils::store_Vgraspable(robot.get_v_graspable(), string(problem_folder+"/info/graspable.data"));
Utils::store_overlaps(robot.get_relative_overlaps_empty(), string(problem_folder+"/info/overlaps_empty.data"));
Utils::store_overlaps(robot.get_relative_overlaps_holding(), string(problem_folder+"/info/overlaps_holding.data"));
Utils::store_real_virtual_mapping(robot.get_real_virtual_conversion(), string(problem_folder+"/info/real_virtual_conv.data"));
Utils::store_real_virtual_mapping(robot.get_real_relative_conversion(), string(problem_folder+"/info/real_relative_conv.data"));
Utils::store_rel_virtual(robot.get_rel_virtual(), string(problem_folder+"/info/relative_virtual.data"));



Problem problem(robot.get_bconfs(), robot.get_aconfs(), robot.get_oconfs(), robot.get_v_oconfs(), robot.get_real_virtual_conversion(), robot.get_real_relative_conversion(), robot.get_graph_base(), robot.get_graph_arm(), robot.get_base_motion_plan(),
	robot.get_arm_motion_plan(), robot.get_v_graspable(), robot.get_relative_overlaps_empty(), robot.get_relative_overlaps_holding());

//Print info
ProblemInfo pi(robot);
pi.info();


pi.store_problem_info(problem_folder+"/info/problem.info", time, memmory);
problem.serialize_data(problem_folder);

std::string filename = problem_folder+"/instance.pddl";
FSTRIPS3D fstrips(problem, filename);
fstrips.generate_instance();

System::soft_links(problem_folder);



ros::waitForShutdown();
 
return 0;
  
}
