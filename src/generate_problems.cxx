#include <ros/ros.h>
#include <environment.hxx>
#include <simulator/world.hxx>


using namespace std;

int main(int argc, char** argv) {
  
string pkg = "generate_problem_node";    
ros::init(argc,argv, pkg); 
  
clock_t tStart = clock();  

ros::NodeHandle n;
ros::AsyncSpinner spinner(1);
spinner.start();

World w(n);

string folder = "";
cin >> folder;

string file = folder+"/object_configurations.boost";

unsigned num = 0;
cout << "Num of objects: " << endl;
cin >> num;
num_objects = num;


w.check_object_configurations(file);




}