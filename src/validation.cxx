#include <ros/ros.h>
#include<plan_task_motion/simulator/validator.hxx>
#include<configuration.hxx>
#include <simulator/simulator.hxx>
#include <utils/utils.hxx>

using namespace std;

int main(int argc, char** argv) {
  
  string pkg = "plan_validation_node";    
  ros::init(argc,argv, pkg); 
    
  clock_t tStart = clock();  

  ros::NodeHandle n;
  ros::AsyncSpinner spinner(1);
  spinner.start();
  
  string home = System::get_home_folder();
  string catkin_pkg = home+"/catkin_ws/src/plan_task_motion/";
  string benchmarks = catkin_pkg+"/benchmarks/new/definitius/";
  
//   string instance = "";
    string instance = "tables_3_05_015_4grasping";

 // cout << "Type instance name: " << endl;
  //cin >> instance;
  string instance_folder = benchmarks+instance;
 
  string filename = "/home/jonathanaig/catkin_ws/src/plan_task_motion/benchmarks/new/definitius/"+instance+"/results/first.plan";

  string base_graph = instance_folder + "/data/base_graph.boost";
  string arm_graph = instance_folder+ "/data/arm_graph.boost";
  string c_base = instance_folder+ "/info/base_configurations.boost";
  string c_arm = instance_folder+ "/info/arm_configurations.boost";
  string c_obj = instance_folder+ "/info/object_configurations.boost";
  string base_motion = instance_folder + "/info/base_trajectories.boost";
  string arm_motion = instance_folder + "/info/arm_trajectories.boost";
  

  
  Validator val(n);
   val.load_data(base_graph, arm_graph, c_base, c_arm, c_obj, base_motion, arm_motion);
   val.parse_plan(filename);
   val.perform_plan();

  ros::waitForShutdown();

 
  return 0;



}